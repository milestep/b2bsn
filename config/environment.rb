# Load the rails application
require File.expand_path('../application', __FILE__)

# Initialize the rails application
Beanstock::Application.initialize!

Dir[Rails.root + "lib/ruby_extensions/*.rb"].each {|file| require file }
