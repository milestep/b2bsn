require 'resque/server'

Beanstock::Application.routes.draw do

  ActiveAdmin.routes(self)

  devise_for :admin_users, ActiveAdmin::Devise.config
  mount Resque::Server.new, :at => "/resque"

  # auth
  match "/login" => "sessions#new"
  match "/logout" => "sessions#destroy", :as => :logout
  match "/auth/linkedin/callback" => "sessions#create", provider: 'linkedin'
  match "/auth/:provider/callback" => "authorizations#create"

  namespace :signup do
    resources :experiences, :except => [:new, :create, :show]
    resources :users, :only => [:edit, :update]
    resources :skills, :except => [:new, :show]
    resources :beanstock_connections, :only => [:index, :create]
    resources :linkedin_connections, :only => [:index, :create]
  end

  match '/invitations' => 'invitations#update', :as => :invitations
  match 'audiences/autocomplete' => 'audiences#autocomplete', :as => :autocomplete_audiences
  match 'categories/autocomplete' => 'categories#autocomplete', :as => :autocomplete_categories
  match 'sites/autocomplete' => 'sites#autocomplete', :as => :autocomplete_sites
  match 'networks/autocomplete' => 'networks#autocomplete', :as => :autocomplete_networks

  resources :searches, :only => [:index] do
    collection do
      get :autocomplete
    end
  end

  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Demo routes
  match 'opportunity/1' => 'opportunities#show', :as => :demo_view_opportunity
  match 'opportunities' => 'opportunities#index'
  match 'opportunities/new' => 'opportunities#new', :as => :demo_new_opportunity

  resources :tools, :only => [:index, :show], :path => :connections do
    collection do
      get :new_entry
      get :nice_filter
      post :fine_fetch
    end
  end

  resources :activity_feeds, :only => :index

  resources :conversations, :only => [:index, :show, :new, :create], :path => :messages do
    collection do
      get :add_chat_message
      get :chat_history
      get :autocomplete
      get :archived
      get :window
      get :friend_preview_popup
    end
    member do
      post :quick_reply
      get :forward
      get :new_reply
      post :create_reply
      get :trash
      get :untrash
      get :archive
      get :unarchive
    end

  end

  resources :addresses, :except => [:index]
  resources :agencies do
    collection do
      get 'autocomplete'
    end
  end
  resources :brands do
    collection do
      get 'autocomplete'
    end
  end
  resources :advertisers do
    collection do
      get 'autocomplete'
    end
  end
  resources :buyers
  resources :publishers do
    resources :packages
  end
  resources :circles, :except => [:index, :show] do
    member do
      get 'add_user'
      get 'delete_user'
      get 'confirm_delete'
      get 'show_expanded'
    end
    collection do
      get 'collapse_expanded'
    end
  end
  resource :clicks
  resources :comments do
    resources :likes, :only => [:index, :create]
  end
  resources :answers do
    resources :likes, :only => [:index, :create]
  end
  resources :contacts, :except => [:index]
  resources :connections, :except => [:destroy], :path => :collaborate do
    collection do
      get 'add_contacts'
      get 'search'
    end
  end

  scope :module => "connections" do
    resources :manage_connections do
      collection do
        get "invite_users"
        get "co_workers"
        get "beanstock_connections"
        get "linkedin_connections"
      end
    end
    resources :my_connections do
      collection do
        get "all_connections"
        get "full_user_profile"
      end
    end
    resources :blocked_connections do
      collection do
        get "block_user"
        get "block_autocomplete_user"
        get "unblock_user"
        get "blocked_users"
      end
    end
    resources :ignored_connections
  end

  resources :experiences
  resources :user_profiles, :only => [:show] do
    member do
      get :edit_base_info
      put :update_base_info
      get :popup_profile
      post :update_photo
    end
    resources :viewings, :only => [:index]
  end
  resources :users, :only => [:show, :update] do
    collection do
      get 'show_users'
      get 'new_users_modal'
      get 'update_activity_feed'
      get 'search_friends'
      get 'update_status'
    end
    resources :authorizations
    resources :packages
  end
  resources :notices, :only => [:index, :destroy], :path => :notifications do
    collection do
      get :destroy_all
    end
  end
  resources :requests, :only => [:index]
  resources :suggestions, :only => :index
  resources :news, :as => "news_items", :only => [:index, :show] do
    resources :comments, :only => [:create, :destroy, :update]
    resources :likes, :only => [:index, :create]
    member do
      get :show_news, :as => :show
    end
  end
  match 'slide_elements' => 'news#slide_elements'
  match 'feeds' => 'news#feeds'
  match 'slide_news' => 'news#slide_news'
  resources :posts do
    resources :answers, :only => [:create, :destroy]
    resources :comments, :only => [:create, :destroy, :update]
    resources :likes, :only => [:index, :create]
  end
  resources :settings, except: [:new, :create, :destroy]
  resources :shares, only: [:create]
  resources :timeline_events do
    member do
      get :hide
    end
    collection do
      get :bulk_hide
      get :undo_bulk_hide
    end
  end
  resource :feature_requests, :controller => 'jira_issues', :only => [:new, :create], :type => 'FeatureRequest'
  resource :support_requests, :controller => 'jira_issues', :only => [:new, :create], :type => 'SupportRequest'

  match 'home' => 'users#show'

  root :to => "tools#index"

  unless Rails.env.production?
    match "/all_logins" => "logins#index"
    match "/simulate_login/:id" => "logins#simulate_login", :as => :simulate_login
  end
end
