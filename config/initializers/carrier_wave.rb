require 'fog'

Fog.credentials_path = Rails.root.join('config/fog_credentials.yml')
credentials_config = HashWithIndifferentAccess.new(YAML.load(File.read(Rails.root.join('config/fog_credentials.yml'))))
if Rails.env.test? or Rails.env.jenkins?
  storage = :file
  enable_processing = false
else
  storage = :fog
  enable_processing = true
end

CarrierWave.configure do |config|
  config.fog_credentials = {:provider => 'AWS'}
  config.fog_directory  = credentials_config[Rails.env]['bucket']
  config.storage = storage
  config.enable_processing = enable_processing
end
