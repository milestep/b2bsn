Rails.application.config.middleware.use OmniAuth::Builder do
  provider :twitter, TWITTER_CONSUMER_KEY, TWITTER_CONSUMER_SECRET
  provider :facebook, FACEBOOK_APP_ID, FACEBOOK_APP_SECRET,  {scope: "publish_stream"} #, {:client_options => {:ssl => {:ca_path => "vendor/ssl/server.crt"}}}
  provider :linked_in, LINKEDIN_CONSUMER_KEY, LINKEDIN_CONSUMER_SECRET
end

Twitter.configure do |config|
  config.consumer_key = TWITTER_CONSUMER_KEY
  config.consumer_secret = TWITTER_CONSUMER_SECRET
  #config.oauth_token = YOUR_OAUTH_TOKEN
  #config.oauth_token_secret = YOUR_OAUTH_TOKEN_SECRET
end

LinkedIn.configure do |config|
  config.token = LINKEDIN_CONSUMER_KEY
  config.secret = LINKEDIN_CONSUMER_SECRET
end
