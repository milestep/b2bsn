set :output, "#{path}/log/cron.log"

every 30.minutes do
  runner "NewsFeed.process_all_feeds"
end

every 5.minutes do
  runner "NewsFeed.fetch_feeds(force = true)"
end

every 1.day, :at => '8:00 am' do
  rake "ignore_list:remove_expired_users"
end