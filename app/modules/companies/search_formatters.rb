module Companies::SearchFormatters

  FINE_FILTER_TRUNCATED_NAME_LENGTH = 17
  NAVIGATION_AUTOCOMPLETE_TRUNCATED_NAME_LENGTH = 50

  def search_title
    name.truncate(NAVIGATION_AUTOCOMPLETE_TRUNCATED_NAME_LENGTH)
  end

  def full_search_title
    name
  end

  def search_location
    first_location_name
  end

  def search_site_name
    name
  end

  def short_search_site_name
    search_site_name.truncate(FINE_FILTER_TRUNCATED_NAME_LENGTH)
  end

  def search_category
    if company_categories.present?
      company_categories.first.category_name
    else
      I18n.t('.no_data')
    end
  end

  def search_like
    "#{search_site_name} #{search_location} #{search_category}"
  end

end
