module Companies
  module Publishers
    module Associations
      TYPES = [:audience, :category, :site, :network]

      def self.included(base)
        TYPES.each do |type|
          base.class_eval do
            before_save "check_#{type}".to_sym

            define_method("check_#{type}") do 
              self.send("company_#{type.to_s.pluralize}").each do |association|
                if association && association.send("#{type}_id").nil?
                  association.send("build_#{type}", :name => association.send("#{type}_name"))
                end
              end
            end
          end
        end
      end

      def initate_associations
        TYPES.each do |type|
          self.send("company_#{type.to_s.pluralize}").build if self.send("company_#{type.to_s.pluralize}").empty?
        end
      end
    end
  end
end
