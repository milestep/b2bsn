module TimelineEvents
  class QuestionAsked < TimelineEvent

    def source_url
      post_path(self.subject_id)
    end

    def message
      I18n.t('.timeline_events.question_asked_message')
    end

    def collect_noticed_users
      self.subject.mentioned_users
    end

    def question_asked?
      true
    end

  end
end
