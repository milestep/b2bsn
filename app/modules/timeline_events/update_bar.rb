module TimelineEvents::UpdateBar

  def visibility
    subject.try(:visibility) || 'public'
  end

  def create_public_events
    self.users << User.scoped.reject_with_id(actor_id)
  end

  def create_custom_events
    #Temporary. Will be implemented as soon as requirements are.
    self.users << User.scoped.reject_with_id(actor_id)
  end

  def create_private_events
    #It needs to avoid an error
  end

  def create_connections_events
    self.users << User.friends_of(actor) << actor.incoming_requests.map(&:user)
  end

end
