module TimelineEvents
  class ClickCreated < TimelineEvent

    def description
    end

    def message
      I18n.t('.timeline_events.click_created_message')
    end

  end
end