module TimelineEvents
  class MidbCompanyDeleted < TimelineEvent

    def description
    end

    def title
      self.subject.name
    end

    def status
      I18n.t('.timeline_events.deleted_status')
    end

    def message
      I18n.t('.timeline_events.removed_entry', :user_name => self.user_name, :company_name => self.company_name, :company_type => self.subject.type.capitalize)
    end

  end
end