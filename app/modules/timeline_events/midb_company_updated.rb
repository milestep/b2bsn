module TimelineEvents
  class MidbCompanyUpdated < TimelineEvent

    def description
    end

    def title
      self.subject.name
    end

    def status
      I18n.t('.timeline_events.updated_status')
    end

    def message
      if self.fields_changes.keys.count == 1
        I18n.t('.timeline_events.single_update', :user_name => self.user_name, :company_name => self.company_name, :field_name => self.fields_changes.keys.first, :field_value => self.fields_changes.values.first.last)
      else
        I18n.t('.timeline_events.multi_update', :user_name => self.user_name, :company_type => self.subject.type.capitalize)
      end
    end
  end
end