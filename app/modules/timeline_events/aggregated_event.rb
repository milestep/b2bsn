module TimelineEvents
  class AggregatedEvent < TimelineEvent
    
    def message
      list = self.aggregated_events
      I18n.t('.timeline_events.multiple_profile_updated_message', 
        :first_event => list.shift,
        :count => list.size,
        :rest_events => list.join(', ')
      ).html_safe
    end

  end
end