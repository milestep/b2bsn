module TimelineEvents
  class PackageUpdated < TimelineEvent

    def description
    end

    def message
      I18n.t('.timeline_events.package_updated_message')
    end

    def package_name
      subject.name
    end

    def package_path
      Rails.application.routes.url_helpers.user_package_path(subject.packagable_id, subject.id)
    end

    def package_updated?
      true
    end

  end
end
