module TimelineEvents
  class QuestionAnswered < TimelineEvent

    def source_url
      post_path(self.subject_id)
    end

    def message
      I18n.t('.timeline_events.question_answered_message')
    end

    def collect_noticed_users
      answer = self.subject
      answers = answer.post.answers

      answers.map(&:user) + [answer.post.user] - [answer.user]
    end

    def question_answered?
      true
    end

    def posting_path
      Rails.application.routes.url_helpers.post_path(subject.post_id)
    end

    def author_name
      subject.respond_to?(:fullname) ? subject.fullname : subject.user_fullname
    end
  end
end
