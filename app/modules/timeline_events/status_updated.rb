module TimelineEvents
  class StatusUpdated < TimelineEvent

    def source_url
      post_path(self.subject_id)
    end

    def message
      I18n.t('.timeline_events.status_updated_message', :to => self.to)
    end

    def collect_noticed_users
      self.subject.mentioned_users
    end

    def status_update?
      true
    end

    def updated_status
      self.to
    end
  end
end
