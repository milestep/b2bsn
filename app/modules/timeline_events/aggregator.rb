# Query that we build using Aggregator class
# 
# SELECT
#   `wrapper`.*,
#   GROUP_CONCAT(`wrapper`.`to` SEPARATOR '~') as 'updated_list',
#   IF(COUNT(`wrapper`.`id`) > 1, 'TimelineEvents::AggregatedEvent', `wrapper`.`type`) as 'type'
# FROM (
#   SELECT  
#     IF(
#       prev_te.`actor_id` IS NULL OR (prev_te.`actor_id` = te.`actor_id` AND te.`event_type` = prev_te.`event_type` AND te.`event_type` = 'profile_update'),
#       IF(@i IS NULL, @i := 0, @i), 
#       IF(@i IS NULL, @i := 0, @i := @i + 1)
#     ) AS 'actor_id_group',
#     `te`.*
#   FROM `timeline_events` AS te
#   LEFT JOIN `timeline_events` AS `prev_te` ON prev_te.id = (
#     SELECT temp_te.id
#     FROM `timeline_events` AS temp_te
#     WHERE temp_te.created_at >= te.created_at AND temp_te.id <> te.id
#     ORDER BY temp_te.created_at ASC
#     LIMIT 1
#   )
#   ORDER BY 
#     te.created_at DESC, 
#     te.id DESC
# ) AS `wrapper`
# GROUP BY `wrapper`.`actor_id_group`
# 
# * te      - timeline_event table alias
# * prev_te - previous timeline_event table alias, 
#             needed for finding previous id
# * temp_te - temp timeline_event table alias, needed for subquery
class TimelineEvents::Aggregator
  SEPARATOR = '~'
  
  def initialize(scope)
    @scope = scope
    @table_name = @scope.arel_table.name
    @current = nil
  end
  
  [:te, :prev_te, :temp_te].each do |name|
    class_eval <<-CODE, __FILE__, __LINE__ + 1
      def #{name}
        @#{name}_at ||= Arel::Table.new(@table_name, :as => '#{name}')
      end
    CODE
  end
  
  def aggregate
    build_projections
    join_previous_id_query
    add_order
    copy_constraints
    # we do not need to copy limitations for inner query
    # copy_limitations
    wrap_current_query
  end
  
  def build_projections
    @current = te.
      project(te[Arel.star]).
      project(actor_id_group)
  end
  
  def actor_id_group
    # check for NULL needed to prevent run extra query that init @i variable
    condition = <<SQL
IF(
prev_te.`actor_id` IS NULL OR (prev_te.`actor_id` = te.`actor_id` AND te.`event_type` = '#{TimelineEvent::PROFILE_UPDATE}' AND te.`event_type` = prev_te.`event_type`),
IF(@i IS NULL, @i := 0, @i), 
IF(@i IS NULL, @i := 0, @i := @i + 1)
)
SQL
    Arel.sql(condition).as('actor_id_group')
  end  
  
  def join_previous_id_query
    @current = @current.
      join(prev_te, Arel::Nodes::OuterJoin).
        on(prev_te[:id].eq(previous_id_query_sql))
  end
    
  
  def previous_id_query_sql
    query = temp_te.
      project(temp_te[:id]).
      where(temp_te[:created_at].gteq(te[:created_at])).
      where(temp_te[:id].not_eq(te[:id])).
      order('temp_te.created_at ASC').
      take(1)

    # copy where conditions
    copy_constraints(query)

    Arel.sql("(#{query.to_sql})")
  end
  
  def copy_constraints(copy_to = @current)
    return if @scope.arel.constraints.empty?
    
    @scope.arel.constraints.each do |where|
      sql = where.to_sql
      aliaz = copy_to.froms.first.table_alias
      # replace timeline_events.user_id = 10 to te.user_id = 10
      sql.gsub!(@table_name, aliaz) if aliaz
      sql = Arel.sql(sql)
      
      copy_to.where(sql)
    end
  end
  
  def copy_limitations(copy_to = @current)
    copy_to.take(@scope.arel.limit) if @scope.arel.limit
    copy_to.skip(@scope.arel.offset) if @scope.arel.offset
  end
  
  def add_order
    # it will work only with this order
    @current = @current.order('te.updated_at DESC, te.created_at DESC, te.id DESC')
  end

  def wrap_current_query
    wrapper = Arel::Table.new('wrapper')
    query = @scope.unscoped. # we need ActiveReccord::Relation object in the end
      from(Arel.sql("(#{@current.to_sql})").as('wrapper')).
      select(wrapper[Arel.star]).
      select(Arel.sql("GROUP_CONCAT(`wrapper`.`to` SEPARATOR '#{SEPARATOR}')").as('updated_list')).
      select(Arel.sql("IF(COUNT(`wrapper`.`id`) > 1, 'TimelineEvents::AggregatedEvent', `wrapper`.`type`)").as('type')).
      group(wrapper['actor_id_group'])

    copy_limitations(query.arel)

    query
  end
end