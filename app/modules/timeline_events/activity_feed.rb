module TimelineEvents::ActivityFeed

  def profile_path
    Rails.application.routes.url_helpers.user_profile_path(actor_id)
  end

  def event_path
    Rails.application.routes.url_helpers.timeline_event_path(id)
  end

  def posting_path
    Rails.application.routes.url_helpers.post_path(subject.id)
  end

  def event_title
    subject.title
  end

  def updated_status
    ''
  end

  def package_path
    ''
  end

  def package_name
    ''
  end

  [:status_update?, :question_asked?, :question_answered?, :profile_update?, :package_updated?].each do |name|
    define_method(name) { false }
  end

end
