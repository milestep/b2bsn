module TimelineEvents
  class ProfileUpdated < TimelineEvent

    def source_url
      user_profile_path(self.subject_id)
    end

    def message
      I18n.t('.timeline_events.profile_updated_message', :to => self.to)
    end

    def profile_update?
      true
    end

  end
end
