module TimelineEvents
  class CommentCreated < TimelineEvent

    def source_url
      comment_path(self.subject_id)
    end

    def message
      I18n.t('.timeline_events.comment_created_message')
    end

    def collect_noticed_users
      comment = self.subject
      commentable = comment.commentable
      comments = commentable.comments
      viewers = commentable.notifiers

      comments.map(&:user) + viewers - [comment.user]
    end

  end
end
