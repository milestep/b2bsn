module Users
  module ContactRequests
    include Rails.application.routes.url_helpers
    def default_url_options
      {:host => HOST}
    end

    def send_contact_requests(contact_ids)
      return if contact_ids.blank?
      contacts = User.find_all_by_id(contact_ids)
      contacts.each do |contact|
        self.connections.create(:status => 'pending', :user_id => self.id, :friend => contact)
      end
    end

    def send_invites_to_linkedin_accounts(linkedin_ids, linkedin_client)
      return if linkedin_ids.blank?
      linkedin_ids.each do |lid|
        invitation = Invitation.find_by_sender_id_and_linkedin_id(self.id, lid)
        unless invitation
          invitation = Invitation.create!(
            :sender_id => self.id,
            :role_id => Role.find_by_name(Role::SELLER),
            :token => Digest::SHA1.hexdigest([Time.now, rand].join)[0..9],
            :linkedin_id => lid
          )
        end
        profile = linkedin_client.profile(:id => lid)
        title = I18n.t('.contact_requests.invitation_message_title')
        body = I18n.t('.contact_requests.invitation_message_body',
                      :first_name => self.first_name,
                      :full_name => self.fullname,
                      :client_name => profile['first_name'],
                      :invitations_url => invitations_url(:token => invitation.token))

        # NOTICE: temporary disabled to prevent inappropriate users to access the system
        # TODO: uncomment this out on deploying real production
        # linkedin_client.send_message(title, body, [lid])
      end
    end
  end
end
