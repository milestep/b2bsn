module Users::Chats

  def send_or_reply_to_conversation(conversation, message, friend)
    if conversation
      self.reply_to_conversation(conversation,
                                 message,
                                 I18n.t("conversation.no_subject"),
                                ).message
      conversation
    else
      conversation = self.send_message([friend],
                                       message,
                                       I18n.t("conversation.no_subject"),
                                      ).conversation
      conversation.update_attribute(:chat, true)
      conversation
    end
  end
end
