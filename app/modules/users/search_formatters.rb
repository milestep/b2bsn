module Users::SearchFormatters

  FINE_FILTER_TRUNCATED_FULLNAME_LENGTH = 17
  NAVIGATION_AUTOCOMPLETE_TRUNCATED_FULLNAME_LENGTH = 50

  def search_title
    fullname.truncate(NAVIGATION_AUTOCOMPLETE_TRUNCATED_FULLNAME_LENGTH)
  end

  def full_search_title
    fullname
  end

  def search_location
    user_profile.location
  end

  def search_site_name
    fullname
  end

  def short_search_site_name
    search_site_name.truncate(FINE_FILTER_TRUNCATED_FULLNAME_LENGTH)
  end

  def search_category
    #TODO: need to be implemented
    I18n.t('.no_data')
  end

  def search_like
    "#{search_site_name} #{search_location} #{search_category}"
  end

end
