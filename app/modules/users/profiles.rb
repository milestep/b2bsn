module Users::Profiles

  DEFAULT_TITLE_LENGTH = 25

  def user_profile_title(length = DEFAULT_TITLE_LENGTH)
    user_profile.title.try(:truncate, length) if user_profile.present?
  end

  def company_name(length = DEFAULT_TITLE_LENGTH)
    user_profile.company_name.try(:truncate, length) if user_profile.present?
  end

  def user_profile_avatar
    user_profile.avatar.thumb.url rescue ::UserUploader.new.default_url
  end

end
