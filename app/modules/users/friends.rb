module Users
  module Friends
    def self.included(base)
      base.has_many :friends,
        :through => :connections,
        :conditions => { 'connections.status' => 'accepted' }
      base.has_many :inverse_friends,
        :through => :inverse_connections,
        :source => :user,
        :conditions => { 'connections.status' => 'accepted' }
      base.has_many :requested_friends,
        :through => :connections,
        :source => :friend,
        :conditions => { 'connections.status' => 'pending' }
      base.has_many :inverse_requested_friends,
        :through => :inverse_connections,
        :source => :user,
        :conditions => { 'connections.status' => 'pending' }

      base.scope :my_fr, base.joins(:friends)

      def my_friends(type = nil)
        if type.present?
          User.friends_of(self).by_type(type).order_by_name("ASC",nil)
        else
          User.friends_of(self).order_by_name("ASC",nil)
        end
      end

      def suggested_friends(num = 4)
        new_friends = my_friends.collect{|u| u.friends}.flatten.uniq - my_friends - [self]
        new_friends = new_users.limit(num) if new_friends.empty?
        new_friends
      end

      def my_requested_friends
        requested_friends + inverse_requested_friends
      end

      def find_friends(s = nil)
        User.friends_of(self).by_name(s)
      end

      def friends_for_seven_days
        connections.within_seven_days
      end

      def friends_for_seven_days_without_last
        friends_for_seven_days[0..-2]
      end

      def last_added_friend_name_for_seven_days
        friends_for_seven_days.last.friend.try(:fullname) rescue ''
      end

      def has_one_new_friend_for_seven_days?
        friends_for_seven_days.length == 1
      end

      def has_new_friends?
        friends_for_seven_days.any?
      end
    end
  end
end
