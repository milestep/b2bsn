module News
  module FormattedNews

    def formatted_date
      date_published.to_formatted_s(:day_week)
    end

    def formatted_time
      date_published.to_formatted_s(:short_time)
    end

    def formatted_description
      TextTruncater.new(description).sanitized_first_paragraph
    end

  end
end
