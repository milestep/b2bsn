module Posts::SearchFormatters

  def search_title
    title.truncate(50)
  end

  def full_search_title
    title
  end

end
