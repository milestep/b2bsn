module Posts::UserProfiles

  def user_profile_avatar
    user.avatar.thumb.url rescue ::UserUploader.new.default_url
  end

end
