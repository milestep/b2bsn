module Posts::UserProperties

  delegate :fullname, :to => :user, :prefix => true, :allow_nil => false
  delegate :user_profile, :to => :user, :allow_nil => false
  delegate :online_status, :to => :user, :allow_nil => true

end
