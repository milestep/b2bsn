module Notificators::MainmenuCounterns

  def self.included(controller)
    controller.class_eval do 
      before_filter :fetch_messages_and_notifications

      # STUBBED. Mockups required.
      # TODO: unstub this!
      def fetch_messages_and_notifications
        @mainmenu_header_alerts_count   = 7
      end

    end
  end
end
