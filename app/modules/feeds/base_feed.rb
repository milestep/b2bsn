class Feeds::BaseFeed
  attr_accessor :obj
  include ActiveModel::Serializers::JSON

  FEED_NEWS_PER_PAGE = 50

  def initialize(obj)
    @obj = obj
  end

  def method_missing(*args, &block)
    @obj.send(*args, &block)
  end

  def image_url
  end

  def feed_status
  end

  def url
  end

  private

  def get_instance_attributes
    a = {}
    self.class.instance_methods(false).each {|s| a.merge!({s.to_s => s.to_sym })}
    a
  end
end
