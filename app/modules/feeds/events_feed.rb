class Feeds::EventsFeed < Feeds::BaseFeed
  def feed_title
    obj.title
  end

  def feed_description
    obj.description
  end

  def feed_status
    obj.message
  end

  def feed_date
    obj.created_at.to_formatted_s(:day_week)
  end

  def feed_time
    obj.created_at.to_formatted_s(:short_time)
  end

  def feed_type
    I18n.t('.models.events_feed.activity')
  end

  def feed_class
    "activities"
  end

  def attributes
    get_instance_attributes
  end

  def real_time
    obj.created_at
  end

  def event_url
    obj.source_url
  end

end