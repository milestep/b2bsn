class Feeds::NewsReaderFeed < Feeds::BaseFeed
  def feed_title
    obj.title
  end

  def feed_description
    obj.formatted_description
  end

  def feed_date
    obj.formatted_date
  end

  def image_url
    obj.default_image? ? obj.image.default_url : obj.image_url
  end

  def url
    obj.url
  end

  def feed_time
    obj.formatted_time
  end

  def feed_type
    I18n.t('.models.events_feed.news')
  end

  def feed_class
    "news"
  end

  def attributes
    get_instance_attributes
  end

  def real_time
    obj.date_published
  end

  def event_url
    obj.url
  end

end