class ConnectNotificationMailer < ActionMailer::Base
  include Resque::Mailer

  def connect_request(connection)
    @message = connection["message"]
    id = connection["friend_id"]
    type = I18n.t("connect_notification_mailer.connect_request.user")
    user = User.find(id)
    mail(
      :from => "beanstock.marketplace@gmail.com",
      :to => "beanstock.marketplace@gmail.com",
      :subject => "#{type} #{I18n.t("connect_notification_mailer.connect_request.connection")} #{user.fullname}"
    )
  end
end
