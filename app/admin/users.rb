ActiveAdmin.register User do

  index do
    column :full_name do |u|
      link_to u.fullname, [:admin, u]
    end
    column :role
    column :email
    column :uid
    column :created_at
  end

end
