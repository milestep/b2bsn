class Network < ActiveRecord::Base
  
  has_many :company_networks
  has_many :companies, :through => :company_networks

  scope :search, lambda{|s| where("name like ?", "%#{s}%")}

end
