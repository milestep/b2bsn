class CompanyAudience < ActiveRecord::Base
  
  belongs_to :audience
  belongs_to :company

  attr_accessor :audience_name

  alias :virtual_audience_name :audience_name
  def audience_name
    virtual_audience_name || audience.try(:name)
  end

end
