class Agency < Company

  has_many :agency_brands
  has_many :brands, :through => :agency_brands
  has_many :agency_advertisers
  has_many :advertisers, :through => :agency_advertisers

  validate :brand_presence
  validate :brand_unique
  validate :advertiser_presence
  validate :advertiser_unique

  accepts_nested_attributes_for :agency_brands, :reject_if => proc { |attributes| attributes['brand_name'].blank? }
  accepts_nested_attributes_for :agency_advertisers, :reject_if => proc { |attributes| attributes['advertiser_name'].blank? }

  fires :midb_agency_created, :on    => :create,
                              :actor => :owner,
                              :type  => proc { 'TimelineEvents::MidbCompanyCreated' }

  fires :midb_agency_updated, :on    => :update,
                              :actor => :owner,
                              :fields_changes => :fields_changed,
                              :type  => proc { 'TimelineEvents::MidbCompanyUpdated' }

  fires :midb_agency_deleted, :on    => :destroy,
                              :actor => :owner,
                              :type  => proc { 'TimelineEvents::MidbCompanyDeleted' }

  scope :search, lambda { |search|
    where("LOWER(name) LIKE '%#{search.downcase}%'") if search.present?
  }

  scope :order_by_name, lambda { |order|
    order("name #{order || 'ASC'}")
  }

  #temporary. will be implemented as soon as requirements be
  scope :fine_search, scoped.limit(50)

  def self.expanded_company
    instance = super
    instance.agency_brands.build
    instance.agency_advertisers.build
    instance
  end

  def company_items_for_profile
    [brands.limit(Company::TILES_NUMBER_FOR_PROFILE), advertisers.limit(Company::TILES_NUMBER_FOR_PROFILE)]
  end

  def agency?
    true
  end

  private

  def brand_unique
    errors.add(:brands, :should_be_unique) if agency_brands.uniq.length != agency_brands.length
  end

  def advertiser_unique
    errors.add(:advertisers, :should_be_unique) if agency_advertisers.uniq.length != agency_advertisers.length
  end

  def brand_presence
    agency_brands.each do |agency_brand|
      if agency_brand.brand_id.nil?
        errors.add(:brands, :brand_do_not_exist)
      end
    end
  end

  def advertiser_presence
    agency_advertisers.each do |agency_advertiser|
      if agency_advertiser.advertiser_id.nil?
        errors.add(:advertisers, :advertiser_do_not_exist)
      end
    end
  end
end
