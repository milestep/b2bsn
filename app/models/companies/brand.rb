class Brand < Company

  has_many :agency_brands
  has_many :agencies, :through => :agency_brands
  has_many :brand_advertisers
  has_many :advertisers, :through => :brand_advertisers

  validate :agency_presence
  validate :advertiser_presence

  accepts_nested_attributes_for :agency_brands, :reject_if => proc { |attributes| attributes['agency_name'].blank? }
  accepts_nested_attributes_for :brand_advertisers, :reject_if => proc { |attributes| attributes['advertiser_name'].blank? }

  fires :midb_brand_created, :on    => :create,
                             :actor => :owner,
                             :type  => proc { 'TimelineEvents::MidbCompanyCreated' }

  fires :midb_brand_updated, :on    => :update,
                             :actor => :owner,
                             :fields_changes => :fields_changed,
                             :type  => proc { 'TimelineEvents::MidbCompanyUpdated' }

  fires :midb_brand_deleted, :on    => :destroy,
                             :actor => :owner,
                             :type  => proc { 'TimelineEvents::MidbCompanyDeleted' }

  scope :search, lambda { |search|
    where("LOWER(name) LIKE '%#{search.downcase}%'") if search.present?
  }

  scope :order_by_name, lambda { |order|
    order("name #{order || 'ASC'}")
  }

  scope :fine_search, lambda { |params| by_categories(params[:category_ids]).by_cities(params[:cities]) }

  searchable do
    text :name
  end

  def self.expanded_company
    instance = super
    instance.agency_brands.build
    instance.brand_advertisers.build
    instance
  end

  def company_items_for_profile
    [agencies.limit(Company::TILES_NUMBER_FOR_PROFILE), advertisers.limit(Company::TILES_NUMBER_FOR_PROFILE)]
  end

  def brand?
    true
  end

  private

  def agency_presence
    agency_brands.each do |agency_brand|
      if agency_brand.agency_id.nil?
        errors.add(:agencies, :agency_do_not_exist)
      end
    end
  end

  def advertiser_presence
    brand_advertisers.each do |brand_advertiser|
      if brand_advertiser.advertiser_id.nil?
        errors.add(:advertisers, :advertiser_do_not_exist)
      end
    end
  end
end
