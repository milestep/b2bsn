class Advertiser < Company

  has_many :agency_advertisers
  has_many :agencies, :through => :agency_advertisers
  has_many :brand_advertisers
  has_many :brands, :through => :brand_advertisers

  validate :agency_presence
  validate :brand_presence

  accepts_nested_attributes_for :agency_advertisers, :reject_if => proc { |attributes| attributes['agency_name'].blank? }
  accepts_nested_attributes_for :brand_advertisers, :reject_if => proc { |attributes| attributes['brand_name'].blank? }

  fires :midb_advertiser_created, :on    => :create,
                                  :actor => :owner,
                                  :type  => proc { 'TimelineEvents::MidbCompanyCreated' }

  fires :midb_advertiser_updated, :on    => :update,
                                  :actor => :owner,
                                  :fields_changes => :fields_changed,
                                  :type  => proc { 'TimelineEvents::MidbCompanyUpdated' }

  fires :midb_advertiser_deleted, :on    => :destroy,
                                  :actor => :owner,
                                  :type  => proc { 'TimelineEvents::MidbCompanyDeleted' }

  def self.expanded_company
    instance = super
    instance.agency_advertisers.build
    instance.brand_advertisers.build
    instance
  end

  def build_associations
    agency_advertisers.new if agency_advertisers.empty?
    brand_advertisers.new if brand_advertisers.empty?
  end

  def company_items_for_profile
    [agencies.limit(Company::TILES_NUMBER_FOR_PROFILE), brands.limit(Company::TILES_NUMBER_FOR_PROFILE)]
  end

  def advertiser?
    true
  end

  private

  def agency_presence
    agency_advertisers.each do |agency_advertiser|
      if agency_advertiser.agency_id.nil?
        errors.add(:agencies, :agency_do_not_exist)
      end
    end
  end

  def brand_presence
    brand_advertisers.each do |brand_advertiser|
      if brand_advertiser.brand_id.nil?
        errors.add(:brands, :brand_do_not_exist)
      end
    end
  end
end
