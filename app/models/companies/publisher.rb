class Publisher < Company
  include Companies::Publishers::Associations

  has_many :packages, :as => :packagable, :dependent => :destroy
  has_many :package_publishers
  has_many :linked_packages, :through => :package_publishers, :source => :package

  fires :midb_publisher_created, :on    => :create,
                                 :actor => :owner,
                                 :type  => proc { 'TimelineEvents::MidbCompanyCreated' }

  fires :midb_publisher_updated, :on    => :update,
                                 :actor => :owner,
                                 :fields_changes => :fields_changed,
                                 :type  => proc { 'TimelineEvents::MidbCompanyUpdated' }

  fires :midb_publisher_deleted, :on    => :destroy,
                                 :actor => :owner,
                                 :type  => proc { 'TimelineEvents::MidbCompanyDeleted' }

  scope :fine_search, lambda { |params| by_categories(params[:category_ids]).by_ad_types(params[:ad_type_ids]).by_cities(params[:cities]) }

  def self.expanded_company
    instance = super
    instance.initate_associations
    instance
  end

  def company_items_for_profile
    [linked_packages.limit(Company::TILES_NUMBER_FOR_PROFILE), []] #ad_types.limit(Company::TILES_NUMBER_FOR_PROFILE)
  end

  def publisher?
    true
  end
end
