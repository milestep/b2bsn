class Connection < ActiveRecord::Base
  CONNECTIONS_PER_PAGE = 6

  belongs_to :user
  belongs_to :friend, :class_name => "User", :foreign_key => "friend_id"

  scope :by_friend_id_or_owner_id, lambda { |statuses_questions_owners_ids| where("(user_id IN (?) OR friend_id IN (?))", statuses_questions_owners_ids, statuses_questions_owners_ids).includes(:user, :friend) }

  scope :within_seven_days, where("created_at > ? AND status = 'accepted'", 7.days.ago).order(:created_at, :id)
  scope :accepted_or_pending, where("status = 'accepted' OR status = 'pending'")

  validates_length_of :message, :maximum => 250
  validate :user_and_friend_difference

  state_machine :status, :initial => 'pending' do
    event 'Accept' do
      transition all => 'accepted'
    end

    event 'Reject' do
      transition all => 'declined'
    end

    event 'Delete' do
      transition all => 'deleted'
    end

    event 'Undo' do
      transition all => 'pending'
    end

    event 'Block' do
      transition all => 'blocked'
    end

    event 'Ignore' do
      transition all => 'ignored'
    end
  end

  alias_method :scoped_friend, :friend
  def friend
    scoped_friend || User.unscoped.find(self.friend_id)
  end

  def incoming_for?(user)
    self.friend == user
  end

  def actor(user)
    incoming_for?(user) ? self.user : self.friend
  end

  def request_type(user)
    incoming_for?(user) ? 'incoming' : 'outgoing'
  end

  private

  def user_and_friend_difference
    errors.add(:friend, :should_be_different_from_user) if user && scoped_friend && user == scoped_friend
  end
end
