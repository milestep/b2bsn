class Invitation < ActiveRecord::Base
  belongs_to :sender, :class_name => 'User'
  belongs_to :role
  scope :available, where(:used_at => nil)
  
  def self.validate_token(token)
    return Invitation.available.find_by_token(token)
  end
  
  def self.generate_tokens(role, num = 1)
    role ||= Role.seller
    (1..num).each do |i|
      Invitation.create!(:role => role, :token => Digest::SHA1.hexdigest([Time.now, rand].join)[0..9])
    end
  end
end

