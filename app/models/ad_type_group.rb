class AdTypeGroup < ActiveRecord::Base
  RICH_MEDIA_AD_UNITS = :rich_media_ad_units
  OTHER_RICH_MEDIA = :other_rich_media
  UAP = :uap
  OTHER_AD_UNITS = :other_ad_units
  
  has_many :ad_types

end
