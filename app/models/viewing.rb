class Viewing < ActiveRecord::Base
  belongs_to :viewable, :polymorphic => true
  belongs_to :user
  default_scope :order => 'created_at DESC'
  
  validates_presence_of :viewable_type, :viewable_id, :user_id
  
end
