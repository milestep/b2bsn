class AgencyBrand < ActiveRecord::Base
  belongs_to :agency
  belongs_to :brand

  attr_accessor :brand_name
  attr_accessor :agency_name

  alias :virtual_brand_name :brand_name
  def brand_name
    virtual_brand_name || brand.try(:name)
  end

  alias :virtual_agency_name :agency_name
  def agency_name
    virtual_agency_name || agency.try(:name)
  end
end
