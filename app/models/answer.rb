class Answer < ActiveRecord::Base
  DROPDOWN_LIMIT = 5

  acts_as_votable

  delegate :user_fullname, :to => :post

  belongs_to :user
  belongs_to :post
  has_many :timeline_events, :as => :subject, :dependent => :destroy
  has_one :timeline_event, :as => :subject, :dependent => :destroy

  validates_presence_of :user, :post, :answer

  attr_accessible :answer

  fires :question_answered, :on    => :create,
                            :actor => :user,
                            :to    => :answer,
                            :type  => proc { 'TimelineEvents::QuestionAnswered' }

  def vote_score
    cached_votes_up - cached_votes_down
  end

  def title
    self.answer
  end

  def post_timeline_event
    post.timeline_event
  end

end
