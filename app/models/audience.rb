class Audience < ActiveRecord::Base
  
  has_many :company_audiences
  has_many :companies, :through => :company_audiences

  scope :search, lambda{|s| where("name like ?", "%#{s}%")}

end
