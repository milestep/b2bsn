class IgnoreListUser < ActiveRecord::Base

  IGNORING_PERIOD = 3.months

  belongs_to :user
  belongs_to :ignored_user, :class_name => "User", :foreign_key => "ignored_user_id"

  before_create :establish_ignoring_period

  scope :expired, lambda {
    where("stop_ignoring_at <= ?", Date.today)
  }

  private

  def establish_ignoring_period
    self.stop_ignoring_at = Date.today + IGNORING_PERIOD
  end

end