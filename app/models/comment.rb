class Comment < ActiveRecord::Base
  DISPLAYED_LENGTH = 250
  DROPDOWN_LIMIT = 5

  acts_as_votable

  validates_presence_of :comment

  include ActsAsCommentable::Comment
  belongs_to :commentable, :polymorphic => true
  belongs_to :user
  default_scope :order => 'created_at ASC'
  has_many :timeline_events, :as => :subject
  has_one :timeline_event, :as => :subject

  after_save :increment_counters
  after_destroy :decrement_counters

  fires :comment_created, :on    => :create,
                          :actor => :user,
                          :to    => :comment,
                          :type  => proc { 'TimelineEvents::CommentCreated' }

  alias_method :scoped_user, :user
  def user
    scoped_user || User.unscoped.find(self.user_id)
  end

  def commentable_timeline_event
    commentable.timeline_event
  end

  alias_attribute :title, :comment

  private
  def increment_counters
    if commentable.respond_to? :comments_count
      commentable.class.increment_counter(:comments_count, commentable)
    end
  end

  def decrement_counters
    if commentable.respond_to? :comments_count
      commentable.class.decrement_counter(:comments_count, commentable)
    end
  end

  def truncated_comment
    self.comment.truncate(DISPLAYED_LENGTH)
  end

end
