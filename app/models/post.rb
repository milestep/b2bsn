class Post < ActiveRecord::Base

  acts_as_votable
  acts_as_commentable
  acts_as_viewable
  acts_as_clickable

  include Posts::UserProfiles
  include Posts::UserProperties
  include Posts::SearchFormatters
  MIN_TITLE_LEN = 3
  before_save :normalize_url

  belongs_to :user
  belongs_to :role
  has_many :answers, :order => "(answers.cached_votes_up - answers.cached_votes_down) DESC, answers.created_at DESC"
  has_many :recent_answers, :order => "created_at DESC", :class_name => "Answer"

  has_many :post_circles
  has_many :circles, :through => :post_circles
  has_one :timeline_event, :as => :subject
  has_many :timeline_events, :as => :subject
  has_many :posts_users, :dependent => :destroy
  has_many :mentioned_users, :through => :posts_users, :source => :user

  validates_length_of :title, :minimum => MIN_TITLE_LEN, :on => :create

  attr_accessible :company_type, :title, :description, :url, :who_with, :role_id, :who_asking, :post_type, :visibility, :mentioned_users_ids

  fires :question_asked, :on    => :create,
                         :actor => :user,
                         :to    => :title,
                         :type  => proc { 'TimelineEvents::QuestionAsked' },
                         :if    => lambda { |post| post.post_type == 'Question' }


  fires :status_update, :on    => :create,
                        :actor => :user,
                        :to    => :title,
                        :type  => proc { 'TimelineEvents::StatusUpdated' },
                        :if    => lambda { |post| post.post_type == 'Status' }

  searchable do
    text :title
    string :post_type
  end

  def is_question?
    post_type == 'Question'
  end

  alias_method :scoped_user, :user
  def user
    scoped_user || User.unscoped.find(self.user_id)
  end

  def user_fullname
    user.fullname
  end

  def notifiers
    [self.user]
  end

  def add_mentioned_users(ids)
    friends_ids = User.friend_summaries(self.user).uniq.map(&:id)
    mentioned_users_ids = friends_ids & ids
    mentioned_users = User.where(:id => mentioned_users_ids)
    self.mentioned_users = mentioned_users
  end

  private

  def normalize_url
    if self.url.present?
      self.url = "http://" + self.url unless self.url =~ /^http/
    end
  end
end
