class Experience < ActiveRecord::Base
  belongs_to :user_profile
  belongs_to :company

  mount_uploader :company_logo, CompanyUploader

  default_scope order("current desc, experiences.end_date desc, experiences.start_date desc")

  validates :end_date, :presence => { :message => :should_be_present_for_previous_position }, :unless => :current
  validates :start_date, :presence => true
  validate :end_date_absence
  validates_date :end_date, :after => :start_date,
                            :after_message => :after_start_date,
                            :if => lambda { start_date && end_date }
  validates_date :end_date, :on_or_before => lambda { Date.today },
                            :on_or_before_message => :on_or_before_today,
                            :allow_blank => true
  validates_date :start_date, :on_or_before => lambda { Date.today },
                              :on_or_before_message => :on_or_before_today,
                              :allow_blank => true

  def time_period
    s = (start_date.present? ? "#{start_date.strftime("%B, %Y")} to " : "")
    s << (end_date.present? ? "#{end_date.strftime("%B, %Y")}" : "present")
    s
  end

  def company_logo_url
    company_logo.url
  end

  def company_logo_file_name
    company_logo.file_name
  end

  def end_month
    if current
      I18n.t('user_profiles.profile_experience.today')
    elsif end_date
      end_date.localized_month
    end
  end

  def end_year
    if current
      Date.today.year
    elsif end_date
      end_date.year
    end
  end

  private

  def end_date_absence
    errors.add(:end_date, :should_be_empty_for_current_position) if current && end_date
  end

end
