class CompanySite < ActiveRecord::Base
  
  belongs_to :site
  belongs_to :company

  attr_accessor :site_name

  alias :virtual_site_name :site_name
  def site_name
    virtual_site_name || site.try(:name)
  end

end
