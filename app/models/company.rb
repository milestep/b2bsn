class Company < ActiveRecord::Base

  include Companies::SearchFormatters

  TYPES = {
    :agency => 'agency',
    :publisher => 'publisher',
    :brand => 'brand',
    :advertiser => 'advertiser'
  }

  TILES_NUMBER_FOR_PROFILE = 6
  ITEMS_NUMBER_FOR_DETAIL = 8

  mount_uploader :logo, CompanyUploader

  acts_as_paranoid

  has_many :locations, :class_name => 'CompanyLocation', :dependent => :delete_all
  has_many :addresses, :through => :locations
  has_many :company_brands
  has_many :brands, :through => :company_brands
  has_many :user_profiles
  has_many :users, :through => :user_profiles
  has_many :contacts
  belongs_to :owner, :polymorphic => true
  has_many :company_ad_types, :dependent => :destroy
  has_many :ad_types, :through => :company_ad_types
  has_many :company_audiences, :dependent => :destroy
  has_many :audiences, :through => :company_audiences
  has_many :company_categories, :dependent => :destroy
  has_many :categories, :through => :company_categories
  has_many :company_networks, :dependent => :destroy
  has_many :networks, :through => :company_networks
  has_many :company_sites, :dependent => :destroy
  has_many :sites, :through => :company_sites
  has_many :experiences

  validates :name, :presence => true, :uniqueness => true
  validates :email, :format => {:with => Formats::EMAIL, :if => :email?}
  validates :phone, :format => {:with => Formats::PHONE, :if => :phone?}
  validates :number_of_employees, :numericality => {:only_integer => true, :if => :number_of_employees}

  accepts_nested_attributes_for :locations
  accepts_nested_attributes_for :company_ad_types
  accepts_nested_attributes_for :company_audiences, :allow_destroy => true
  accepts_nested_attributes_for :company_categories, :allow_destroy => true
  accepts_nested_attributes_for :company_sites, :allow_destroy => true
  accepts_nested_attributes_for :company_networks, :allow_destroy => true

  scope :by_categories, lambda { |categories|
    includes(:categories).where('categories.id IN (?)', categories) if categories.present? && categories.exclude?('all')
  }

  scope :by_ad_types, lambda { |ad_types|
    includes(:ad_types).where('ad_types.id IN (?)', ad_types) if ad_types.present? && ad_types.exclude?('all')
  }

  scope :by_cities, lambda { |cities|
    includes(:addresses).where('addresses.city in (?)', cities) if cities.present? && cities.include?('all')
  }

  searchable do
    text :name
  end

  TYPES.each_value do |type|
    scope type.pluralize, where(:type => type.classify)
  end

  def self.expanded_company
    instance = self.new
    location = instance.locations.build(:name => I18n.t('company.by_area'))
    location.build_address
    instance
  end

  def self.fetch_by_linkedin_data(linkedin_company)
    company = Company.find_by_linkedin_id(linkedin_company["id"])
    company ? company : Company.create(:name => linkedin_company["name"], :linkedin_id => linkedin_company["id"])
  end

  def downcased_type
    type.downcase
  end

  [:publisher?, :agency?, :brand?, :advertiser?].each do |name|
    define_method(name) { false }
  end

  def brands_for_details
    brands.limit(Company::ITEMS_NUMBER_FOR_DETAIL)
  end

  def agencies_for_details
    agencies.limit(Company::ITEMS_NUMBER_FOR_DETAIL)
  end

  def advertisers_for_details
    advertisers.limit(Company::ITEMS_NUMBER_FOR_DETAIL)
  end

  def fields_changed
    changes = self.changes
    changes.delete("updated_at")
    changes
  end

  def first_location_name
    if locations.present?
      locations.first.name
    else
      I18n.t('.no_data')
    end
  end

end
