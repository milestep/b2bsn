class CompanyNetwork < ActiveRecord::Base
  
  belongs_to :network
  belongs_to :company

  attr_accessor :network_name

  alias :virtual_network_name :network_name
  def network_name
    virtual_network_name || network.try(:name)
  end

end
