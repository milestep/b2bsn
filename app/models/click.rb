class Click < ActiveRecord::Base
  belongs_to :clickable, :polymorphic => true
  belongs_to :user
  default_scope :order => 'created_at DESC'

  has_one :timeline_event, :as => :subject
  
  validates_presence_of :user_id

  fires :news_viewed, :on    => :create,
                      :actor => :user,
                      :type  => proc { 'TimelineEvents::ClickCreated' },
                      :if    => lambda { |click| click.clickable_type.to_s == 'NewsItem' }
end