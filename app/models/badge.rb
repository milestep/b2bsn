class Badge < ActiveRecord::Base
  mount_uploader :icon, IconUploader
  mount_uploader :icon_disabled, IconUploader

  belongs_to :role
  
  scope :by_role, lambda{|r| joins(:role).where("badges.role_id is null or roles.id = ?", r.id)}
  
end
