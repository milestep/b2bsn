class PackagePublisher < ActiveRecord::Base
  belongs_to :package
  belongs_to :publisher
end
