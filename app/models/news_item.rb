class NewsItem < ActiveRecord::Base
  DAYS_AMOUNT = 7
  TRUNCATION_LENGTH = 50
  NAMES_OF_DEFAULT_IMAGES = ["mf.gif"]

  acts_as_votable
  acts_as_commentable
  acts_as_clickable
  mount_uploader :image, NewsUploader

  include News::FormattedNews

  belongs_to :news_feed

  has_many :timeline_events, :through => :clicks, :source => :timeline_event

  scope :latest_news, where(:created_at => DAYS_AMOUNT.days.ago...Time.now).order("date_published desc").includes(:news_feed)
  scope :latest_deals, where(:news_feed_id => NewsFeed::DEAL_FEED_ID, :created_at => DAYS_AMOUNT.days.ago...Time.now).order("date_published desc")
  scope :newest, order("date_published desc")

  def timeline_event
    timeline_events.first
  end

  def notifiers
    self.clicks.map(&:user)
  end

  def tableized_class_name
    self.class.name.tableize
  end

  def default_image?
    File.basename(image_url).in?(NAMES_OF_DEFAULT_IMAGES)
  end
end
