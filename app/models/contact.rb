class Contact < ActiveRecord::Base
  belongs_to :user
  belongs_to :company
  belongs_to :address
    
  def fullname
    "#{first_name} #{last_name}"
  end
  
  def office_phone
    address.phones.find_by_phone_type("office").try(:number)
  end
  
  def company_name
    return self.company.name
  end
  
  def company_name=(company_name)
    # TODO: something
  end
  
end
