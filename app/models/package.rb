class Package < ActiveRecord::Base

  acts_as_viewable

  mount_uploader :logo, PackageUploader

  belongs_to :packagable, :polymorphic => true, :counter_cache => true
  has_many :package_publishers
  has_many :linked_publishers, :through => :package_publishers, :source => :publisher

  attr_accessible :brand_restriction, :cpm, :demo_targeting_available, :description, :dma_targeting_available, :flight_end_date, :flight_start_date, :name, :total_cost, :total_impressions

  validates :description, :flight_end_date, :flight_start_date, :name, :total_cost, :presence => true
  validates :total_cost, :inclusion => { :in => 0.01..99999999.99 }
  validates :total_impressions, :inclusion => { :in => 1..99999999999 }, :allow_blank => true
  validates :cpm, :inclusion => { :in => 0.01..999.99 }, :allow_blank => true
  validates :name, :length => { :maximum => 100 }
  validates :description, :length => { :maximum => 1000 }
  validates :brand_restriction, :length => { :maximum => 50 }
  validate :start_must_be_before_end_date

  fires :package_updated, :on    => :update,
                          :actor => :packagable,
                          :type  => proc { 'TimelineEvents::PackageUpdated' }

  def owner_publisher?
    packagable.is_a?(Publisher)
  end

  def owner_seller?
    packagable.is_a?(User) && packagable.seller?
  end

  def start_must_be_before_end_date
    return unless flight_start_date && flight_end_date
    errors.add(:flight_start_date, :should_be_less_than_flight_end_date) if self.flight_end_date < self.flight_start_date
  end

  def logo_url
    logo.url
  end

  def title
    I18n.t("models.package.was_updated")
  end

  def created_by?(object)
    self.packagable == object
  end
end
