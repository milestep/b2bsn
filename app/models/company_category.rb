class CompanyCategory < ActiveRecord::Base
  
  belongs_to :category
  belongs_to :company

  attr_accessor :category_name

  alias :virtual_category_name :category_name
  def category_name
    virtual_category_name || category.try(:name)
  end

end
