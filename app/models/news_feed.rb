class NewsFeed < ActiveRecord::Base
  DEAL_FEED_ID = 777

  has_many :news_items
  mount_uploader :image, ImageUploader

  def process_feed
    rss = FeedNormalizer::FeedNormalizer.parse(url)
    if rss
      rss.items.each do |r|
        news_items.find_or_create_by_url_and_title(r.url, r.title) do |n|
          n.date_published = r.date_published || Time.now
          n.permalink = r.id
          n.description = r.description
          n.copyright = r.copyright
          n.author = r.author
          n.content = r.content
          
          doc = Nokogiri::HTML(n.content)
          i = doc.xpath("/html/body//img[@src[contains(.,'://') and not(contains(.,'ads.') or contains(.,'ad.') or contains(.,'?') or contains(.,'share') or contains(.,'feedburner'))]][1]")
          n.remote_image_url = i[0]['src'] if i.present? && i[0]['src']
        end
      end
    end
  end

  def self.new_feed_from_url(url)
    f = FeedNormalizer::FeedNormalizer.parse(url)
    NewsFeed.create!(:title => f.title, :description => f.description, 
                     :url => f.url, :last_updated => f.last_updated, 
                     :copyright => f.copyright, :active => true,
                     :remote_image_url => f.image)
  end
  
  def self.process_all_feeds
    NewsFeed.where(:active => true).each{|nf| nf.process_feed}
  end

  def self.fetch_feeds(force = false)
    Rails.cache.fetch('news_feed', :force => force) {
      feed = []
      NewsItem.newest.each {|item| feed << Feeds::NewsReaderFeed.new(item)}
      TimelineEvent.all.each {|item| feed << Feeds::EventsFeed.new(item)}
      feed.sort_by! {|obj| obj.real_time}.reverse!
      feed
    }
  end
end

