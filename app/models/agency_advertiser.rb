class AgencyAdvertiser < ActiveRecord::Base
  belongs_to :agency
  belongs_to :advertiser
  
  attr_accessor :advertiser_name
  attr_accessor :agency_name

  alias :virtual_advertiser_name :advertiser_name
  def advertiser_name
    virtual_advertiser_name || advertiser.try(:name)
  end

  alias :virtual_agency_name :agency_name
  def agency_name
    virtual_agency_name || agency.try(:name)
  end
end
