# -*- coding: utf-8 -*-
# modeling very simple relationships and making sure that the
# Authorization has both a provider (e.g. “twitter” or “facebook”) and
# a uid (i.e. the external service ID) e.g. twitter's key for your
# account.
class Authorization < ActiveRecord::Base
  belongs_to :user
  belongs_to :social_network, :foreign_key => :provider
  delegate :name, :to => :social_network, :prefix => true

  validates_presence_of :user_id, :uid, :provider
  validates_uniqueness_of :uid, :scope => :provider
  validates_uniqueness_of :provider, :scope =>  :user_id

  def self.create_or_refresh_omniauth(auth, user)
    authorization = find_by_provider_and_uid(auth['provider'], auth['uid'])
    if (authorization.nil?)
      authorization = self.create_with_omniauth(auth, user)
    else
      authorization.refresh(auth)
    end
    authorization
  end

  # return the authorization object for this provider/user
  def self.find_with_omniauth(auth)
    find_by_provider_and_uid(auth[:provider], auth[:uid])
  end

  def self.create_with_omniauth(auth, user)
    name = auth[:info][:nickname] && "@"+auth[:info][:nickname] || auth[:info][:name] # twitter username or name.
    url = auth[:info][:urls].values.compact[0] if auth[:info][:urls] #twitter has one nil
    a=self.create(:user => user,
      :provider => auth[:provider],
      :username => name,
      :profile_url => url,
      :posting_default => true,
      :uid => auth['uid'],
      :oauth_token => auth['credentials']['token'],
      :oauth_token_secret => auth['credentials']['secret'])
    self.configure_gems(auth)
  end

  def refresh(auth)
    username= auth[:nickname]
    uid=auth['uid']
    oauth_token=auth['provider']['token']
    oauth_token_secret= auth['provider']['secret']
  end

  def self.providers(user)
    where(:user_id => user).map {|a| a.provider }
  end

  def self.default_providers(user)
    where(:user_id => user, :posting_default => true).map {|a| a.provider }
  end



  protected

  def self.configure_gems(auth)
    case auth['provider']
    when 'twitter'
      Twitter.configure do |config|
        config.oauth_token = auth['credentials']['token']
        config.oauth_token_secret = auth['credentials']['secret']
      end
    end
  end

end
