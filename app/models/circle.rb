class Circle < ActiveRecord::Base
  belongs_to :user
  has_and_belongs_to_many :users

  has_many :post_circles
  has_many :posts, :through => :post_circles
  
  validates :name, :presence => true

  scope :user_circles, lambda { |user,circles_names| where("user_id = ? and name in (?)", user, circles_names) }

  def add_user(user_id)
    new_member = User.find(user_id)
    return if self.users.exists?(new_member)
    if User.friends_of(self.user).include?(new_member)
      self.users << new_member
    end
  end

end
