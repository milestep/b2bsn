class UserStatus < ActiveRecord::Base
  belongs_to :user

  OFFLINE = "offline"
  INVISIBLE = "invisible"
  ONLINE = "online"
  AWAY = "away"
  DO_NOT_DISTURB = "do not disturb"

  STATUSES = [INVISIBLE, ONLINE, AWAY, DO_NOT_DISTURB]

  ONLINE_USER_TIME = 15
  OFFLINE_USER_TIME = 30
  AWAY_USER_TIME = 30

  def inactive_time
    (Time.now.utc - updated_at).to_minutes
  end

  def offline?
    status == OFFLINE || status == INVISIBLE || inactive_time > OFFLINE_USER_TIME
  end

  def online?
    (inactive_time <= ONLINE_USER_TIME) && (status == ONLINE || status == nil)
  end

  def away?
    (status == AWAY || status == ONLINE) && (inactive_time <= AWAY_USER_TIME)
  end

  def do_not_disturb?
    (status == DO_NOT_DISTURB) && (inactive_time < OFFLINE_USER_TIME)
  end

  def online!
    update_attributes(:status => ONLINE)
  end

  def away!
    update_attributes(:status => AWAY)
  end

  def do_not_disturb!
    update_attributes(:status => DO_NOT_DISTURB)
  end

  def offline!
    update_attributes(:status => OFFLINE)
  end

  def invisible!
    update_attributes(:status => INVISIBLE)
  end

  def reset_status!
    update_attributes(:status => ONLINE) if (status == ONLINE || status == OFFLINE)
    touch
  end

  def self.valid_status?(incom_status = OFFLINE)
    STATUSES.include?(incom_status.gsub("_", " "))
  end

end