class Skill < ActiveRecord::Base
  has_and_belongs_to_many :user_profiles, :uniq => true

  validates :name, :presence => true

end
