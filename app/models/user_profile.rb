class UserProfile < ActiveRecord::Base
  mount_uploader :avatar, UserUploader
  
  fires :profile_update, :on    => :update,
                         :actor => :user,
                         :to    => :fields_changed,
                         :type  => proc { 'TimelineEvents::ProfileUpdated' },
                         :if    => lambda { |profile| (%w(description avatar title company_id) & profile.changed_attributes.keys).present?}
         
  acts_as_viewable                                            
  acts_as_paranoid

  belongs_to :user
  belongs_to :company
  belongs_to :address
  has_many :experiences, :order => 'updated_at DESC'
  has_many :timeline_events, :as => :subject

  has_and_belongs_to_many :skills, :uniq => true
  
  accepts_nested_attributes_for :address
  accepts_nested_attributes_for :experiences, :reject_if => :all_blank

  def timeline_event
    timeline_events.last
  end
  
  def fullname
    return user.fullname.titleize
  end
  
  def company_name
    company.present? ? company.name : "N/A"
  end
  
  def location
    if address.present?
      [address.city, address.state_abbrev].rejoin
    end
  end

  # all can view
  def public_visibility?
    !(visibility =~ /public/).nil? 
  end
  # anyone connected, excludes non connected publishers/users
  def connection_visibility?
    !(visibility =~ /connections/).nil?
  end
  # all publishers (connected and not connected)
  def publisher_visibility?
    !(visibility =~ /publishers/).nil?
  end
  
  def fields_changed
    s = Array.new
    self.changed_attributes.keys.each do |k|
      s << "summary" if k == "description"
      s << "profile picture" if k == "avatar"
      s << "basic information" if %w(title company_id).include?(k)
    end
    s.uniq.join(', ')
  end

  def city_and_country
    if address.present?
      country_name = address.country_code && Country.new(address.country_code.upcase).try(:name)
      [address.city, country_name].rejoin
    end
  end
end
