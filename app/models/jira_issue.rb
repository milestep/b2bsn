class JiraIssue < ActiveRecord::Base
  belongs_to :user
  
  attr_accessible :description, :sent_at, :title, :type
  
  validates :description, :presence => true
  
  scope :not_sent, where(:sent_at => nil)
  
  [:feature_request?, :support_request?].each do |name|
    define_method(name) { false }
  end
  
  def summary
    I18n.t('jira_issues.summary', 
      :title => title || type.titleize,
      :user => user.fullname,
      :date => created_at.to_formatted_s(:short)
    )
  end
end
