class TimelineEvent < ActiveRecord::Base
  include Rails.application.routes.url_helpers
  include ActionView::Helpers::TextHelper

  ALL                = 'all'
  STATUS_UPDATE      = 'status_update'
  QUESTION_ASKED     = 'question_asked'
  QUESTION_ANSWERED  = 'question_answered'
  PROFILE_UPDATE     = 'profile_update'
  DEAL_UPDATED       = 'deal_updated'
  PACKAGE_UPDATED    = 'package_updated'

  TYPES = [
    ALL, STATUS_UPDATE, QUESTION_ASKED, QUESTION_ANSWERED, PROFILE_UPDATE, DEAL_UPDATED, PACKAGE_UPDATED
  ]

  EVENTS_PER_PAGE = 10
  EVENTS_FOR_POPUP = 5
  VISIBILITIES = ['public', 'private', 'connections']

  include TimelineEvents::ActivityFeed
  include TimelineEvents::UpdateBar

  default_scope :order => 'timeline_events.created_at DESC, timeline_events.id DESC'

  delegate :clickable, :to => :subject, :prefix => true

  serialize :fields_changes, Hash

  belongs_to :actor,              :polymorphic => true
  belongs_to :subject,            :polymorphic => true
  has_and_belongs_to_many :user_shares, :class_name => User, :join_table => :timeline_events_user_shares
  has_many :user_timeline_events
  has_many :users, :through => :user_timeline_events

  has_many :users_notices, :dependent => :destroy
  has_many :noticed_users, :through => :users_notices, :source => :user

  attr_accessible :type, :actor, :fields_changes, :subject, :event_type, :to, :actor_type, :actor_id, :subject_type, :subject_id

  after_create :async_create_user_timeline_events, :async_create_notices
  after_create :update_parent_timeline_event, :if => lambda { |timeline_event| timeline_event.event_type == QUESTION_ANSWERED }

  scope :midb, where("timeline_events.event_type like 'midb%'")
  scope :for_ticker, where(:event_type => ['status_update', 'question_asked', 'profile_update', 'package_updated', 'question_answered'])

  scope :for_user, lambda { |user|
    joins("
    left join posts p on timeline_events.subject_id = p.id
    left join post_circles pc on pc.post_id = p.id
    left join circles_users cu on pc.circle_id = cu.circle_id
    ").where(["
      (
        pc.circle_id is null
        or (timeline_events.actor_type='User' and timeline_events.actor_id = :user_id)
        or cu.user_id = :user_id
      )
      and (p.role_id = :user_role_id or p.user_id = :user_id or p.role_id is null)
    ", :user_id => user.id, :user_role_id => user.role_id]).includes([{:actor => [:user_profile]}, :subject])
  }

  scope :by_type, lambda { |type|
    where("timeline_events.event_type = :type", :type => type)
  }

  scope :by_actor, lambda { |actor|
    where("timeline_events.actor_id = ?", actor.id)
  }

  scope :public_events, lambda {
    joins("left join posts p on timeline_events.subject_id = p.id").where("timeline_events.subject_type = 'Post' AND p.visibility = 'public'")
  }

  scope :user_newest_timeline_events, lambda { |user|
    where(:actor_id => user.id, :actor_type => 'User').aggregated_timeline.limit(EVENTS_PER_PAGE)
  }

  scope :all_events_except_ignored_users_for, lambda { |user|
    ignored_users = User.mutual_ignored_users_for(user)
    where("actor_id not IN (?)", ignored_users.any? ? ignored_users : 0)
  }

  def source_url; end

  def description
    truncate(self.subject.title, :length => 450)
  end

  def self.most_recent(items = 10)
    order("timeline_events.created_at desc").limit(items)
  end

  def user_name
    self.actor.fullname if self.actor.is_a?(User)
  end
  alias_method :title, :user_name

  def company_name
    self.subject.name
  end

  def midb_activity?
    self.event_type.include?('midb')
  end

  def to_partial_path
    "activities/#{event_type}"
  end

  def appropriate_subject
    subject || subject_type.constantize.unscoped.find(subject_id)
  end

  def status
    I18n.t('.timeline_event.status')
  end

  def message
    I18n.t('.timeline_event.message')
  end

  def shared_subject_type
    subject_type.downcase
  end

  def author?(user)
    actor == user
  end

  alias_method :scoped_actor, :actor
  def actor
    scoped_actor || (actor_type && actor_type.constantize.unscoped.find(self.actor_id))
  end

  alias_method :scoped_subject, :subject
  def subject
    scoped_subject || (subject_type && subject_type.constantize.unscoped.find(self.subject_id))
  end

  def actor_fullname
    actor.fullname
  end

  def author_name
    nil
  end

  def create_user_timeline_events
    self.send("create_#{visibility}_events")
  end

  def created_date
    self.created_at.to_date
  end

  def create_notice_events
    self.noticed_users << collect_noticed_users if collect_noticed_users.present?
  end

  def collect_noticed_users
    []
  end

  def notice_for_user(user)
    self.users_notices.find_by_user_id(user.id)
  end

  def has_multiple_events?
    aggregated_events.present?
  end

  def aggregated_events
    @aggregated_events ||= updated_list.split(TimelineEvents::Aggregator::SEPARATOR).uniq if updated_list
  end

  def self.aggregated_timeline
    TimelineEvents::Aggregator.new(scoped).aggregate
  end

  private

  def async_create_user_timeline_events
    if actor.instance_of?(User)
      self.users << actor
      Resque.enqueue(UserTimelineEvents, id)
    end
  end

  def async_create_notices
    Resque.enqueue(NoticeEvents, id)
  end

  def update_parent_timeline_event
    post = subject.post
    post.timeline_event.touch if post.present? && post.timeline_event.present?
  end

end
