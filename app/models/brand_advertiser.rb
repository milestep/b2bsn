class BrandAdvertiser < ActiveRecord::Base
  belongs_to :brand
  belongs_to :advertiser

  attr_accessor :advertiser_name
  attr_accessor :brand_name

  alias :virtual_advertiser_name :advertiser_name
  def advertiser_name
    virtual_advertiser_name || advertiser.try(:name)
  end

  alias :virtual_brand_name :brand_name
  def brand_name
    virtual_brand_name || brand.try(:name)
  end
end
