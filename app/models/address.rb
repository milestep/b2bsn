class Address < ActiveRecord::Base
  US = 'us'

  has_many :phones
  has_many :company_locations

  before_create :set_default_country_code

  validates_format_of :city, :with => Formats::CITY

  def self.grouped_cities
    r = select([:city, :state_abbrev]).group_by(&:state_abbrev)
    r.each_pair { |k,v| r[k] = v.map(&:city) }
  end

  private

  def set_default_country_code
    self.country_code = US unless self.country_code
  end
end
