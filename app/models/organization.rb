class Organization < ActiveRecord::Base
  mount_uploader :logo, ImageUploader
  
  belongs_to :address
end
