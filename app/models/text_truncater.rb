class TextTruncater
  PARAGRAPH_REGEXP = /<p.*?>([\d\D]*?)<\/p>/
  DEFINE_TAGS_REGEXP = /(<(.|\n)*?>)|(<(.|\n)*?)$/
  DESCRIPTION_LENGTH = 500

  def initialize(description)
    @description = description
  end

  def sanitized_first_paragraph
    first_paragraph = @description.scan(PARAGRAPH_REGEXP)
    if first_paragraph.any?
      first_paragraph.each do |result|
        if result
          @truncated_result = truncate_description(result[0])
          return @truncated_result if @truncated_result.length > 0
        end
      end
    end
    @truncated_result.blank? ? truncate_description(@description) : @truncated_result
  end

  private

  def truncate_description(text)
    text.gsub(DEFINE_TAGS_REGEXP, "").truncate(DESCRIPTION_LENGTH).strip
  end
end