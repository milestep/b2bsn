class CompanyLocation < ActiveRecord::Base
  belongs_to :company
  belongs_to :address

  scope :by_type, lambda { |type| where("companies.type" => type) }
  scope :for_state_abbrev, lambda { |state| where("addresses.state_abbrev" => state) }
  scope :for_city, lambda { |city| where("addresses.city" => city) }

  accepts_nested_attributes_for :address

  scope :search, lambda { |search|
    where("LOWER(companies.name)  LIKE '%#{search.downcase}%'") if search.present?
  }

  def short_address
    [address.city, address.state_abbrev].rejoin
  end
  alias_method :discover_location, :short_address

  def fullname
    company.name
  end

  def discover_type
    company.type.downcase
  end

  def logo
    company.logo
  end

  def online_status
    nil
  end

  def item_type
    nil
  end

  def title
    nil
  end

  def city
    address ? address.city : ''
  end

  def state
    address ? address.state_abbrev : ''
  end

end
