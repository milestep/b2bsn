class Role < ActiveRecord::Base
  SELLER = "seller"
  PUBLISHER = "publisher"
  ADVERTISER = "advertiser"
  AGENCY = "agency"
  BUYER = "buyer"

  ROLES = [
    PUBLISHER, BUYER, SELLER
  ]

  attr_accessible :name

  has_many :users
  has_many :posts
  has_many :invitations
  has_many :badges

  def self.method_missing(method_name, *args)
    role = where(:name => method_name.to_s).first
    role ? role : super
  end
end
