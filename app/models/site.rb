class Site < ActiveRecord::Base
  
  has_many :company_sites
  has_many :companies, :through => :company_sites

  scope :search, lambda{|s| where("name like ?", "%#{s}%")}

end
