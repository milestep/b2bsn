require 'memoist'

class User < ActiveRecord::Base
  MESSAGES_FOR_DROPDOWN = 5

  extend Memoist

  include Users::ContactRequests
  include Users::Friends
  include Users::Sharable
  include Users::Profiles
  include Users::SearchFormatters
  include Users::Chats

  acts_as_voter
  acts_as_paranoid
  acts_as_messageable

  delegate :avatar, :title, :description, :experience, :experiences, :city_and_country, :to => :user_profile
  delegate :sentbox, :conversations, :inbox, :to => :mailbox
  delegate :online?, :offline?, :away?, :do_not_disturb?, :to => :user_status
  delegate :name, :to => :role, :prefix => true

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation,
    :remember_me, :first_name, :last_name, :role_id

  attr_accessor :password, :password_confirmation

  after_create :collect_initial_badges, :create_user_profile, :create_user_state

  belongs_to :role
  has_one :user_profile, :dependent => :destroy
  has_many :connections, :dependent => :destroy
  has_many :inverse_connections, :class_name => "Connection", :foreign_key => "friend_id"

  has_many :incoming_requests, :foreign_key => 'friend_id',
                               :class_name => 'Connection',
                               :conditions => { :status => 'pending' },
                               :order => 'created_at DESC'
  has_many :outgoing_requests, :class_name => 'Connection',
                               :order => 'created_at DESC'

  has_many :circles, :order => "name", :dependent => :destroy
  has_many :memberships
  has_many :organizations, :through => :memberships
  has_many :contacts
  has_many :user_timeline_events
  has_many :timeline_events, :through => :user_timeline_events, :as => :actor, :include => [:actor => [:user_profile]]
  has_many :own_timeline_events, :class_name => TimelineEvent, :as => :actor
  has_many :opportunities
  has_many :posts
  has_many :questions, :class_name => "Post", :conditions => { 'posts.post_type' => 'Question' }
  has_many :answers, :dependent => :destroy
  has_many :authorizations
  has_many :social_networks, :through => :authorizations
  has_many :user_badges
  has_many :badges, :through => :user_badges
  has_many :packages, :as => :packagable, :dependent => :destroy
  has_many :companies, :as => :owner
  has_one :user_status
  has_and_belongs_to_many :event_shares, :class_name => TimelineEvent, :join_table => :timeline_events_user_shares
  has_one :address, :through => :user_profile
  has_one :company, :through => :user_profile
  has_many :archived_messages
  has_many :users_notices, :dependent => :destroy
  has_many :notice_events, :through => :users_notices, :source => :timeline_event
  has_many :black_list_users
  has_many :blocked_users, :through => :black_list_users, :uniq => true
  has_many :ignore_list_users
  has_many :ignored_users, :through => :ignore_list_users, :uniq => true
  has_many :messages, :as => :sender
  has_many :receipts, :order => 'created_at DESC', :dependent => :destroy, :as => :receiver
  has_many :jira_issues

  has_many :posts_users, :dependent => :destroy
  has_many :mentioned_in, :through => :posts_users, :source => :post

  ################################################### VALIDATIONS ######################################################

  validates :email, :format => {:with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i, :if => :email?}

  ###################################################### SCOPES ########################################################
  scope :featured, joins(:user_profile).where('user_profiles.featured = ?', true)
  scope :by_type, lambda{|t| joins(:role).where("roles.id = ?", t.id)}
  scope :by_name, lambda{|s| where("first_name like ? or last_name like ?", "%#{s}%", "%#{s}%")}
  scope :newest, :order => "created_at desc"
  scope :since, lambda{|d| where("created_at > ?", d)}
  scope :search, lambda{ |name|
    where("CONCAT(LOWER(users.first_name), ' ', LOWER(users.last_name)) LIKE '%#{name.downcase}%'") if name.present?
  }

  scope :all_accepted_connections_of, lambda {|user|
    where("users.id IN (SELECT friend_id FROM connections WHERE connections.user_id = #{user.id} AND connections.status = 'accepted') OR users.id IN (SELECT user_id FROM connections WHERE connections.friend_id = #{user.id} AND connections.status = 'accepted')")
  }
  scope :friends_of, lambda {|user|
    where("(users.id IN
           (SELECT friend_id FROM connections WHERE connections.user_id = #{user.id} AND connections.status = 'accepted')
            AND users.id NOT IN( SELECT blocked_user_id FROM black_list_users WHERE user_id = #{user.id}))
            OR
           (users.id IN (SELECT user_id FROM connections WHERE connections.friend_id = #{user.id} AND connections.status = 'accepted')
            AND users.id NOT IN( SELECT blocked_user_id FROM black_list_users WHERE user_id = #{user.id}))")
  }

  scope :blockers_of, lambda {|user|
    where("users.id IN
          (SELECT user_id FROM black_list_users WHERE black_list_users.blocked_user_id = #{user.id})
           OR
          (SELECT user_id FROM ignore_list_users WHERE ignore_list_users.ignored_user_id = #{user.id})")
  }
  scope :order_by_name, lambda {|first_name_order, last_name_order|
    order("users.#{first_name_order ? 'first_name ' + first_name_order : 'last_name ' + (last_name_order || 'ASC' ) }")
  }
  scope :reject_with_id, lambda { |ids| where('id not in (?)', ids) }

  scope :by_role, lambda { |role| includes(:role).where('roles.id = ?', role.id).includes(:user_profile => [ :address, :company ]) }
  scope :by_roles, lambda { |role_ids| where('role_id IN (?)', role_ids) if role_ids.present? && role_ids.exclude?('all') }

  scope :similar_profiles_for, lambda { |user|
    joins(:address).where("(addresses.city = :city OR addresses.state_abbrev = :state OR addresses.country_code = :country)
                            AND users.id NOT IN (SELECT friend_id FROM connections WHERE connections.user_id = :user_id)
                            AND users.id NOT IN (SELECT user_id FROM connections WHERE connections.friend_id = :user_id)
                            AND users.id != :user_id
                            AND users.role_id != :role_id",
                            { :city => user.city, :state => user.state, :country => user.country, :user_id => user.id, :role_id => user.role_id }) }

  scope :co_workers, lambda {|user|
    joins(:user_profile).where("user_profiles.company_id = ? AND user_profiles.user_id != ?", user.company, user)
  }

  scope :by_cities, lambda { |cities|
    includes(:address).where('addresses.city in (?)', cities) if cities.present? && cities.exclude?('all')
  }

  scope :fine_search, lambda { |params| by_roles(params[:role_ids]).by_cities(params[:cities]) }

  def all_events_except_ignored_users
    self.timeline_events.all_events_except_ignored_users_for(self)
  end

  def self.mutual_ignored_users_for(user)
    where("(users.id IN
           (SELECT ignored_user_id FROM ignore_list_users WHERE ignore_list_users.user_id = #{user.id}))
            OR
           (users.id IN
           (SELECT user_id FROM ignore_list_users WHERE ignore_list_users.user_id = #{user.id}))")
  end

  searchable do
    text :first_name
    text :last_name
  end

  class << self

    def by_role(role)
      User.select(:all).includes(:role, :user_status).where('roles.id = ? AND user_statuses.user_id = users.id', role.id).includes(:user_profile => [ :address, :company ])
    end

  end

  def mutual_connections_for(friend)
    User.friends_of(self) & User.friends_of(friend)
  end

  def newest_timeline_events
    TimelineEvent.unscoped.user_newest_timeline_events(self)
  end

  def self.create_with_omniauth(auth, invite_id)
    invite = Invitation.find_by_token(invite_id)
    role = invite.role if invite && (invite.role == Role.seller || invite.role == Role.publisher)
    role_seller = Role.seller
    friend = create! do |user|
      user.provider = auth["provider"]
      user.uid = auth["uid"]
      user.first_name = auth["info"]["first_name"]
      user.last_name = auth["info"]["last_name"]
      user.role = role || role_seller
    end
    if invite && invite.sender_id
      Connection.create(:user_id => invite.sender_id, :friend => friend, :status => 'accepted')
    end
    friend
  end

  def fullname
    "#{first_name} #{last_name}"
  end

  def short_full_name(length = 18)
    fullname.truncate(length)
  end

  def abbrev_name
    "#{first_name} #{last_name[0]}."
  end

  def admin?
    role = Role.find_by_name("admin")
    role?(role)
  end

  def role?(role)
    self.role == role
  end

  def user_type
    self.role.name
  end

  def item_type
    user_type
  end

  def humanable_role
    user_type.capitalize
  end

  def find_circles(g = nil)
    if g.blank?
      circles
    else
      circles.where("name like ?", "%#{g}%")
    end
  end

  def inbox_messages
    inbox.map(&:messages).flatten
  end

  def find_active_connection(friend)
    active_connection = self.connections.where(:friend_id => friend.id).where(:status => 'accepted').first
    return active_connection ||= self.inverse_connections.where(:user_id => friend.id).where(:status => 'accepted').first
  end

  def self.search_with_params(params, limit = 10)
    collection = User.where("1=1")
    if params[:name].present?
      collection = collection.by_name(params[:name])
    end
    if params[:email].present?
      collection = collection.where(:email => params[:email])
    end
    if params[:title].present?
      collection = collection.joins(:user_profile).where("title like ?", "%#{params[:title]}%")
    end
    if params[:company].present?
      collection = collection.joins(:user_profile => :company).where("companies.name like ?", "%#{params[:company]}%")
    end
    if params[:phone].present?
      collection = collection.joins(:user_profile => {:address => :phones}).where("phones.number = ?", params[:phone])
    end
    if params[:user_type].present?
      collection = collection.where("role_id in (?)", params[:user_type])
    end
    if params[:except].present?
      collection = collection.reject_with_id(params[:except])
    end
    collection
  end

  def has_publishers?
    return @has_publishers if defined?(@has_publishers)
    # TODO: method should check friends from Publisher < Company, not from users
    # @has_publishers = friends.where(:role => PUBLISHER).any?
    @has_publishers = true
  end

  def seller?
    role?(Role.seller)
  end

  def connected_to_publisher?(publisher_id)
    # TODO: method should check friends from Publisher < Company, not from users
    #friends.find_by_id(publisher_id) || inverse_friends.find_by_id(publisher_id)
    true
  end
  memoize :connected_to_publisher?

  def should_see_add_package?
    seller? && has_publishers?
  end

  def short_address
    [user_profile.address.try(:city), user_profile.address.try(:state_abbrev)].rejoin
  end

  def discover_type
    "user"
  end

  def discover_location
    "#{user_profile.company_name} #{user_profile.title}"
  end

  def logo
    user_profile.avatar
  end

  def linked_packages
    packages
  end

  def new_users
    User.where("id not in (?)", my_friends.map(&:id) << self.id)
  end
  memoize :new_users

  def can_manage_comment?(comment, post_owner = nil)
    self.eql?(comment.user) || (post_owner && self.eql?(post_owner))
  end

  def self.stream_friends_for(activity_feed)
    statuses_questions = activity_feed.select { |t| ["status_update", "question_asked"].include?(t.event_type) }
    statuses_questions_owners_ids = statuses_questions.map { |s_q| s_q.subject.user_id }.uniq
    connections = Connection.by_friend_id_or_owner_id(statuses_questions_owners_ids)
    connections.map { |c| [c.friend, c.user] }.flatten.uniq.compact
  end

  def online_status
    return I18n.t("user.#{UserStatus::OFFLINE.underline}") if user_status.offline?
    return I18n.t("user.#{UserStatus::ONLINE.underline}") if user_status.online?
    return I18n.t("user.#{UserStatus::AWAY.underline}") if user_status.away?
    return I18n.t("user.#{UserStatus::DO_NOT_DISTURB.underline}") if user_status.do_not_disturb?
    I18n.t("user.#{UserStatus::OFFLINE.underline}")
  end

  alias_method :scoped_user_profile, :user_profile
  def user_profile
    scoped_user_profile || UserProfile.unscoped.find_by_user_id(self.id)
  end

  def logo_url
    logo.url
  end

  def city
    address ? address.city : ''
  end

  def state
    address ? address.state_abbrev : ''
  end

  def country
    address.country_code if address
  end

  def fetch_similar_profiles
    User.similar_profiles_for(self).limit(3)
  end

  def deletions_for_timeline_events(ids)
    timeline_event_user_deletions.for_timeline_events(ids)
  end

  def hide_events_from(user)
    ids = timeline_events.by_actor(user).map(&:id)
    user_timeline_events.where(:timeline_event_id => ids).delete_all
  end

  def unhide_events_from(user)
    timeline_events << TimelineEvent.by_actor(user)
  end

  def delete_user_from_circles(user)
    circles.each do |circle|
      circle.users.delete(user)
    end
  end

  def last_post_status
    posts.find_last_by_post_type("Status")
  end

  def last_status_event_id
    last_post_status.timeline_event.id
  end

  def last_status_likes_size
    last_post_status.likes.size
  end

  def unread_messages(conversations)
    unread_messages = []
    conversations.each { |conversation| unread_messages = unread_messages + unread_messages_for_conversation(conversation.messages.reverse) if conversation.is_unread?(self) }
    unread_messages
  end

  def mark_as_archived(conversation)
    ArchivedConversation.create(:user_id => self.id, :conversation_id => conversation.id)
  end

  def unarchive(conversation)
    ArchivedConversation.where(:user_id => self.id, :conversation_id => conversation.id).first.destroy
  end

  def is_archived?(conversation)
    Conversation.archived_for_user(self).include?(conversation)
  end

  def has_connection_for?(user)
    self.connections.accepted_or_pending.find_by_friend_id(user.id).present?
  end

  def add_to_blacklist(user)
    self.blocked_users << user
  end

  def add_to_ignore_list(user)
    self.ignored_users << user
  end

  def ignored_by?(user)
    user.ignored_users.include?(self)
  end

  def requests_union
    @requests_union ||= [incoming_requests, outgoing_requests].flatten
  end

  def to_requests_union_json
    requests_union.map do |r|
      {
        'created_at' => r.created_at,
        'request_type' => r.request_type(self),
        'actor' => {
          'id' => r.actor(self).id,
          'fullname' => r.actor(self).fullname,
          'user_type' => r.actor(self).user_type
        }
      }
    end.to_json
  end

  def friend_of?(user)
    User.friends_of(user).include?(self)
  end

  private

  def unread_messages_for_conversation(messages)
    messages.collect { |message| message if message.is_unread?(self) }.compact
  end

  def create_user_state
    create_user_status(:status => UserStatus::OFFLINE)
  end

  def collect_initial_badges
    # only one for now
    b = Badge.by_role(self.role).find_by_name("Early Adopter Achievement")
    user_badges.create(:badge => b, :percent_complete => 100, :completed_at => Time.now)
  end

  # for autocomplete
  def self.friend_summaries(user)
    friend_summaries = find_by_sql ["select u.id, concat(u.first_name,' ',u.last_name) as uname, up.title, " +
      "concat('/system/uploads/user_profile/avatar/', up.id,'/thumb_',  up.avatar) as avatar, " +
      "c.name as company "+
      " from connections conn "+
      " inner join  users u on conn.friend_id=u.id "+
      " left outer join user_profiles up on u.id=up.user_id "+
      " left outer join companies c on up.company_id=c.id "+
      " where conn.user_id=? order by u.last_name", user]
    friend_summaries << (find_by_sql ["select u.id, concat(u.first_name,' ',u.last_name) as uname, up.title, " +
      "concat('/system/uploads/user_profile/avatar/', up.id,'/thumb_',  up.avatar) as avatar, " +
      "c.name as company "+
      " from connections conn "+
      " inner join  users u on conn.user_id=u.id "+
      " left outer join user_profiles up on u.id=up.user_id "+
      " left outer join companies c on up.company_id=c.id "+
      " where conn.friend_id=? order by u.last_name", user])
    friend_summaries.flatten!
  end

end
