class SocialNetwork < ActiveRecord::Base
  
  attr_accessible :consumer_key, :consumer_secret, :name, :provider
  self.primary_key = "provider"
  
  has_many :authorizations, :foreign_key => :provider
  has_many :users, :through => :authorizations
  validates_presence_of :provider
  validates_uniqueness_of :provider
  
  @@messaging_active=false #disable the final send to social networks
  
  # share - a Post or NewsItem to send
  # user - send as
  # social_network_providers array of provider strings to send to
  # fmi_link - for more information typically link to share 
  def self.socialize(share, user, social_network_providers=[], fmi_link="http://beanstock.com")
    social_network_providers.each do |provider|
      begin
        case provider
        when 'twitter'
          self.socialize_via_twitter(share, user, fmi_link)
        when 'linkedin'
          self.socialize_via_linkedin(share, user, fmi_link)
        when 'facebook'
          self.socialize_via_facebook(share, user)
        end
      rescue => detail
        logger.error detail.backtrace.join("\n")
      end
      logger.debug "Requested share to: #{provider} #{share.title} #{Time.now} messaging_active: #{@@messaging_active}"
    end
  end
  
  def self.socialize_via_twitter(share, user, linkback) 
    Twitter.configure do |config|
      # config.consumer_key = YOUR_CONSUMER_KEY
      # config.consumer_secret = YOUR_CONSUMER_SECRET
      authorization = Authorization.find_by_user_id_and_provider(user, 'twitter')
      config.oauth_token = authorization.oauth_token
      config.oauth_token_secret = authorization.oauth_token_secret
    end
    if share.class == Post && share.is_question?
      body="#{share.title} #{share.description}"
    else
      body="#{share.title}"  
    end
    truncated = self.twitter_140izer(body, share.url, linkback)
    tweet=truncated.join
    Twitter.update tweet    if @@messaging_active
  end
  
  def self.socialize_via_linkedin(share, user, linkback) 

    client = LinkedIn::Client.new
    authorization = Authorization.find_by_user_id_and_provider(user, 'linkedin')
    client.authorize_from_access(authorization.oauth_token, authorization.oauth_token_secret)
    if share.class == Post && share.is_question?
      url = (share.url.nil? || share.url.length ==0)? linkback : share.url
      data={ 
        :comment => share.description, 
        :content => { 
          :title => share.title,
          :submitted_url => url, # not allowed to be nil
          :submitted_image_url => nil
        }
      }
    else
      data={:comment => share.title}
      unless (share.url.nil? || share.url.length ==0)
        data[:content] =  { 
          :title => nil,
          :submitted_url => share.url, 
          :submitted_image_url => nil
        }
      end
    end
    client.add_share(data) if @@messaging_active
  end 
  def self.socialize_via_facebook(share, user) 

    authorization = Authorization.find_by_user_id_and_provider(user, 'facebook')
    oauth_token = authorization.oauth_token # oauth2 has no oauth_token_secret
    graph = Koala::Facebook::API.new(oauth_token)
    
    if share.class == Post && share.is_question?
      graph.put_connections("me", "feed", :message => "#{share.title} #{share.description} #{share.url}") if @@messaging_active
    else
      graph.put_connections("me", "feed", :message => "#{share.title} #{share.url}")    if @@messaging_active
    end
  end
  
  # Twitter allows 140 characters, but insanely long URLs only count as 20/21.
  # if too much text, tweet truncate and link back to the post on beanstock via linkback
  def self.twitter_140izer(body, url, linkback)
    short_url_length=20 #https://dev.twitter.com/docs/api/1/get/help/configuration
    short_url_length_https=21 
    length=140
    twitterized=[body]
    unless (url.nil? || url.length==0)
      length -= (url =~ /https:/i)? short_url_length_https + 1 : short_url_length + 1
      twitterized.concat([" ", url])
    end
    # if shorten, link back 
    if body.length > length   
      fmi=" Read More: "
      shortened = body[0..(length-1-(fmi.length + short_url_length))]
      twitterized[0]=shortened
      twitterized.push(fmi)
      twitterized.push(linkback)
    end
    twitterized
  end
end
