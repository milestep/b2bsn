class Category < ActiveRecord::Base
  
  has_many :company_categories
  has_many :companies, :through => :company_categories

  scope :search, lambda{|s| where("name like ?", "%#{s}%")}

end
