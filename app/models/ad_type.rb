class AdType < ActiveRecord::Base

  belongs_to :ad_type_group
  has_many :company_ad_types
  has_many :companies, :through => :company_ad_types

  def unit_name
    I18n.t("ad_type.unit_name.#{ name }")
  end

  def content_category
    I18n.t("ad_type.content_category.#{ name }")
  end

end
