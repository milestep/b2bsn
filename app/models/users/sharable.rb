module Users::Sharable

  def allowed_to_share?(timeline_event)
    !(timeline_event.author?(self) || event_shares.include?(timeline_event))
  end

  def share_event!(timeline_event)
    event_shares << timeline_event if allowed_to_share?(timeline_event)
  end

end
