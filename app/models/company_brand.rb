class CompanyBrand < ActiveRecord::Base
  belongs_to :company
  belongs_to :brand
end
