class LinkedInMess
  @queue = :linked_queue

  def self.perform(user_id, token, secret, connections)
    curr_user = User.find_by_id(user_id)
    client_profile = curr_user.user_profile
    link_client = ApiClient::Linkedin.new(client_profile, token, secret)
    curr_user.send_invites_to_linkedin_accounts(connections, link_client)
  end
end
