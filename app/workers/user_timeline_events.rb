class UserTimelineEvents
  @queue = :timeline_events

  def self.perform(timeline_event_id)
    event = TimelineEvent.find(timeline_event_id)
    event.create_user_timeline_events
  end
end
