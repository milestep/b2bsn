class LinkedInCompany
  @queue = :fetch_linkedin_company_queue

  def self.perform(token, secret, linkedin_company_id)
    link_fetcher = LinkedInFetcher::CompanyAPI.new(token, secret, linkedin_company_id)
    link_fetcher.import_fields!
  end

end
