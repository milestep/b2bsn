require 'jira'

class JiraIssues
  JIRA_SITE = 'https://beanstockmedia.atlassian.net/'
  JIRA_USERNAME = 'beanstock_bot'
  JIRA_PASSWORD = 'b1nstock_b0t'
  JIRA_PROJECT = 'BNSTKPAAS'
  
  @queue = :jira_issues

  def self.perform
    issues = JiraIssue.not_sent
    
    if issues.present?
      client = JIRA::Client.new :username => JIRA_USERNAME, 
                                :password => JIRA_PASSWORD, 
                                :auth_type => :basic, 
                                :site => JIRA_SITE,
                                :context_path => ''
                              
      project = client.Project.find(JIRA_PROJECT)
      
      type_map = {
        'SupportRequest' => 18,
        'FeatureRequest' => 19
      }
    
      JiraIssue.not_sent.each do |issue|
        remote_issue = client.Issue.build
        remote_issue.save({
          "fields" => {
            "summary" => issue.summary,
            "description" => issue.description,
            "project" => {
              "id" => project.id
            },
            "issuetype" => {
              "id" => type_map[issue.type]
            }
          }
        })
        issue.update_attribute(:sent_at, Time.now)
      end
    end
  end
end
