class NoticeEvents
  @queue = :timeline_events

  def self.perform(timeline_event_id)
    event = TimelineEvent.find(timeline_event_id)
    event.create_notice_events
  end
end

