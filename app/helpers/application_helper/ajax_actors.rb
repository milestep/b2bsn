module ApplicationHelper::AjaxActors

  def inline_loader(opts={})
    klasses = []
    id = opts[:id] || rand(9999)
    style = 'display:none;'
    klasses << opts[:class] || ''
    klasses << 'inline_ajax_loader'
    content_tag :span, :id => id, :class => klasses.rejoin(' '), :style => style do
      tag :img, :src => '/assets/inline_loader.gif'
    end.html_safe
  end

end
