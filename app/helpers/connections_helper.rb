module ConnectionsHelper

  def render_connections_list(selected_tab)
    case selected_tab
    when 'incoming'
      connection_partial = 'incoming_requests'
    when 'outgoing'
      connection_partial = 'outgoing_requests'
    when 'all'
      connection_partial = 'list'
    end
    render :partial => connection_partial
  end

  def request_connection_link(item)
    request_connection_people_link(item) if item.is_a?(User)
  end

  def my_friends
    @curr_user_friends ||= current_user.my_friends
  end

  def my_requested_friends
    @curr_user_requested_friends ||= current_user.my_requested_friends
  end

  def blocked_users
    @my_blocked_users ||= current_user.blocked_users
  end

  def request_connection_user_link(user)
    return "" if my_friends.include?(user)
    return "" if my_requested_friends.include?(user)
    return "" if blocked_users.include?(user)
    return link_to t('shared.view_profile.connect'), new_connection_path(:friend_id => user), :class => 'button violet with_right_margin', :remote => true
  end

  def request_connection_people_link(user)
    return if current_user == user
    return link_to t('.connected'), '#', :class => 'btn btn-primary', :disabled => 'disabled', :onClick => "return false" if my_friends.include?(user)
    return link_to t('.pending_request'), '#', :class => 'btn btn-primary', :disabled => 'disabled', :onClick => "return false" if my_requested_friends.include?(user)
    return link_to t('tools.item.blocked'), '#', :class => 'btn btn-primary', :disabled => 'disabled', :onClick => "return false" if blocked_users.include?(user)
    return link_to t('tools.item.pending_request'), '#', :class => 'btn btn-primary', :disabled => 'disabled', :onClick => "return false" if user.ignored_users.include?(current_user)
    return link_to t('.connect'), new_connection_path(:friend_id => user), :class => "btn btn-primary", :remote => true
  end

  def indicator(opts={})
    opts[:class] ||= 'indicator'
    content_tag(:span, opts[:count], :class => opts[:class], :data => {:count => opts[:count]}) if opts[:count].present? && !opts[:count].zero?
  end

  def already_in_beanstock(connection)
    User.find_by_uid(connection.id)
  end

  def already_connected?(contact)
    current_user.friends.include?(contact)
  end

  def slider_user(connection)
    connection.user == current_user ? connection.friend : connection.user
  end

  def connection_invited?(connection_id)
    Invitation.find_by_sender_id_and_linkedin_id(current_user.id, connection_id).present?
  end

  def contact_invited?(contact)
    current_user.connections.find_by_friend_id(contact).present?
  end

  def max_page(connections_count)
    return 1 if connections_count <= Connection::CONNECTIONS_PER_PAGE
    integral_part = connections_count / Connection::CONNECTIONS_PER_PAGE
    modulo = connections_count % Connection::CONNECTIONS_PER_PAGE
    modulo.zero? ? integral_part : (integral_part + 1)
  end
end
