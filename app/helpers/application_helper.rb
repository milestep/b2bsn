module ApplicationHelper

  include AjaxActors

  def escape_new_line_symbols(text)
    text.gsub("\n", "<br/>").html_safe
  end

  def controller?(name)
    name = 'user' if name == 'home'
    name = 'inventories' if name == 'inventory'
    name = 'opportunities' if name == 'requests'
    params[:controller].include?(name)
  end

  def nav_link(name, path, display_name=nil, notifications=nil)
    css_class = controller?(name) ? 'active' : ''
    display_name ||= name
    display_name << " <span class=\"blue round label\">#{notifications}</span>" if notifications
    link_to raw(display_name), path, :class => css_class
  end

  def flash_helper
    fl = ''
    transform_to_class_name = {notice: 'success'}
    [:notice, :alert].each do |name|
      if flash[name]
        fl << %Q(<div class="alert alert-#{transform_to_class_name[name.to_sym]}">
                 <button type="button" class="close" data-dismiss="alert">&times;</button>#{flash[name]}</div>)
      end
      flash[name] = nil
    end
    fl.html_safe
  end

  def intelligence_map
    render :partial => "tools/#{@map}"
  end

  def demo_account_logos(n=1)
    logos = %w(apple bmw chevron cisco coach electronicarts expedia generalmills
      gm hp ikea kmart lego mars merck microsoft miller nike nissan oldnavy
      petco pfizer philips radioshack saks sears target toyota verizon yum).sample(n)
    return logos.map!{|logo| "demo/account_logos/#{logo}#{['','_off'].sample}.png"}
  end

  def likeable_object_name(likeable)
    likeable.is_a?(TimelineEvent) ? likeable.event_type.titleize : likeable.class
  end

  def render_activity_feed(activity_feed)
    render :partial => "activities/activity_feed", :object => activity_feed
  end

  def us_states
    [
      'AL', 'AK', 'AZ', 'AR', 'CA', 'CO', 'CT', 'DE', 'DC', 'FL', 'GA', 'HI', 'ID', 'IL', 'IN', 'IA', 'KS', 'KY', 'LA',
      'ME', 'MD', 'MA', 'MI', 'MN', 'MS', 'MO', 'MT', 'NE', 'NV', 'NH', 'NJ', 'NM', 'NY', 'NC', 'ND', 'OH', 'OK', 'OR',
      'PA', 'PR', 'RI', 'SC', 'SD', 'TN', 'TX', 'UT', 'VT', 'VA', 'WA', 'WV', 'WI', 'WY'
    ]
  end

  def element_identifier(element)
    "#{element.class.name.downcase}_#{element.id}"
  end

  def body_class
    params[:controller].gsub("/", "_")
  end

  def notice_created_at(time)
    now = Time.now
    case
    when time.between?(now.beginning_of_day, now.end_of_day)
      "#{time_ago_in_words(time)} ago"
    when time.between?(now.beginning_of_week, now.end_of_week)
      time.strftime("on %A")
    else
      time.strftime("%b %d")
    end
  end

  def tab_class(controller_name)
    "active" if params[:controller] == controller_name
  end

  def shared_by(timeline_event)
    %Q(
      #{render(:partial => 'activities/activity_author', :locals => {:timeline_event => timeline_event} )}
      #{render(:partial => 'activities/shared_by', :object => timeline_event) if timeline_event.user_shares.present? }
    ).html_safe
  end

  def present(object, klass = nil)
    presenter = generate_presenter(object, klass)
    if presenter
      yield presenter if block_given?
      presenter
    end
  end

  def generate_presenter(object, klass = nil)
    klass ||= "#{object.class}Presenter".constantize rescue nil
    if klass
      klass.new(object, self)
    end
  end

  def adjust_and_translate(key, opts={}, inline_class = 'label label-important')
    valid_opts = {}
    opts.each_key do |key|
      valid_opts[key] = (opts[key] == 0) ? nil : opts[key]
    end

    if valid_opts.values.any?
      content_tag :span, :class => inline_class do
        t(key, valid_opts)
      end
    end
  end

  def truncate_with_link(text, options = {:length => 0})
    link = options.delete(:link)
    need_to_add_link = text.size > options[:length]
    output = truncate(text, options)
    output << link if need_to_add_link
    output.html_safe
  end

  def receiving_date(object)
    object.created_at.today? ? object.created_at.strftime('%l:%M %P').lstrip : object.created_at.strftime("%B %e, %Y")
  end

  def render_view_profile(item)
    link_to t(".view_profile").html_safe, profile_link(item), :class => "btn btn-cancel"
  end

  def profile_link(item)
    item.is_a?(User) ? user_profile_path(item) : tool_path(item.company_id)
  end
end
