module ConversationsHelper
  def message_status_img(conversation)
    content_tag(:i, "", :class => "img-unread") if unread?(conversation)
  end

  def unread(conversation)
    "unread" if unread?(conversation)
  end

  def generate_chat_id(user_id, friend_id)
    if user_id < friend_id.to_i
      "/#{user_id}_#{friend_id}"
    else
      "/#{friend_id}_#{user_id}"
    end
  end

  def broadcast(channel, &block)
    message = {:channel => channel, :data => capture(&block), :ext => {:auth_token => FAYE_TOKEN}}
    uri = URI.parse("http://localhost:9292/faye")
    Net::HTTP.post_form(uri, :message => message.to_json)
  end

  def message_owner_class(chat_message)
    current_user == chat_message.sender ? 'self_message' : 'friend_message'
  end

  def recognize_controller_name_by_path(path)
    Rails.application.routes.recognize_path(path)[:controller]
  end

  def link_for_submit
    link ||= conversations_path
  end

  private

  def unread?(conversation)
    current_user.mailbox.inbox.unread(current_user).include?(conversation) ? true : false
  end

end
