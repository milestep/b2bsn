module UserProfilesHelper

  def period_and_location(experience)
    if experience.start_date && (experience.end_date || experience.current?) 
      start_date = "#{experience.start_date.localized_month} #{experience.start_date.year}"
      if experience.current?
        end_date = t('user_profiles.profile_experience.present')
        distance = distance_of_time_in_words(experience.start_date.to_time, Time.now)
      else
        end_date = "#{experience.end_date.localized_month} #{experience.end_date.year}"
        distance = distance_of_time_in_words(experience.start_date.to_time, experience.end_date.to_time)
      end
      period = "#{start_date} - #{end_date}"
      [period, "(#{distance})", experience.location].rejoin(" ")
    end
  end

  def send_message_button_for(recipient)
    link_to t(".send_message"), new_conversation_path(:recipient_id => recipient.id), :remote => true, :class => "btn btn-primary" unless recipient.id == current_user.id
  end
end
