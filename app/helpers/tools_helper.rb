module ToolsHelper

  def entry_types
    Company::TYPES.map{ |_, v| [v.capitalize, v] } << ['Person', 'user']
  end

  def visible_form_class(form_type)
    first_entry_type = entry_types.first[1]
    if form_type == first_entry_type
      " visible_form"
    end
  end

  def discover_item_info(item)
    if item.is_a?(User)
      render :partial => "item_company_section", :locals => { :item => item.company } if item.company
    else
      content_tag(:p, item.short_address)
    end
  end

  def render_first_slider(company, first_company_items)
    if company.publisher?
      render :partial => "publisher_first_company_items", :locals => { :items => first_company_items }
    else
      render :partial => "company_items", :locals => { :items => first_company_items }
    end
  end

  def render_second_slider(company, second_company_items)
    if company.publisher?
      render :partial => "publisher_second_company_items", :locals => { :items => second_company_items }
    else
      render :partial => "company_items", :locals => { :items => second_company_items }
    end
  end

  def render_update_status
    result = ""
    user_roles.each_with_index do |type, index|
      show_divider = (index + 1) < user_roles.size
      render :partial => "tools/render_dropdown_content", :locals => { :divider => show_divider, :type => type.name, :id => type.id, :result => result }
    end
    result.html_safe
  end

  def render_update_type_items
    result = ""
    TimelineEvent::TYPES.each_with_index do |type, index|
      show_divider = (index + 1) < TimelineEvent::TYPES.size
      render :partial => "tools/render_dropdown_content", :locals => { :divider => show_divider, :type => type, :id => nil, :result => result }
    end
    result.html_safe
  end

  def render_activity_items
    return if @activity_items.empty?
    result = ''
    @activity_items.each do |activity_item|
      present activity_item do |activity_item_presenter|
        result << activity_item_presenter.for_market_ticker
      end
    end
    result.html_safe
  end

  def render_notices
    result = ""
    @notices.present? && @notices.keys.each do |date|
      result << render(:partial => "layouts/notices/notice_date", :locals => { :date => date })
      @notices[date].each do |notice|
        present notice do |notice_presenter|
          result << notice_presenter.for_notices
        end
      end
    end
    result.html_safe
  end

  def notice_date_identifier(date)
    "date_" << date.strftime("%-d_%-m_%Y")
  end

  def render_notices_for_popup
    result = ""
    @notice_events.present? && @notice_events.each do |notice|
      present notice do |notice_presenter|
        result << notice_presenter.for_notices_popup
      end
    end
    content_tag(:table, result.html_safe)
  end

  def render_search_filter_options
    results = ""
    Constants::Search::FILTER.each_key do |key|
      link = content_tag(:a, t("searches.search_form_with_filter.#{key}"), :href => "javascript:Search.prepare_params('#{key}')")
      results << content_tag(:li, link, :class => key)
    end
    results.html_safe
  end

  def status_class(status)
    status.gsub(' ','-') + '-status'
  end

  def status_class_color(status)
    status.gsub(' ','-') + '_status' if status
  end

  def last_seen(user)
    I18n.t(".last_seen", :time => time_ago_in_words(user.user_status.updated_at))
  end

  def activity_feed_name(user)
    if user
      t('layouts.market_ticker.somebodys_activity', :user_name => user.first_name)
    else
      t('layouts.market_ticker.activity_feed')
    end
  end

end
