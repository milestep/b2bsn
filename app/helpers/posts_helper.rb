module PostsHelper

  def replace_user_name_with_link_in_post(post, friends)
    title = post.title
    if friends 
      matched_names = friends.map(&:fullname).find_all {|friends_name| title.index(friends_name)}.uniq
      matched_names.each do |name|
        title.gsub!(/#{name}/, link_to(name, user_profile_path(friends.detect { |f| f.fullname == name }.id)))
      end
    end
    title
  end

end
