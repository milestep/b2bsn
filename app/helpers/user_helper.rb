module UserHelper

  def avatar_link(user)
    return link_to image_tag(user.avatar.thumb.url), user_profile_path(user.user_profile), :class => 'user-profile-img'
  end

  def abbrev_name_link(user)
    return link_to user.abbrev_name, user_profile_path(user.user_profile), :class => 'user-profile'
  end

  def youtube_embed(youtube_url, width=560, height=315)
    if youtube_url[/youtu\.be\/([^\?]*)/]
      youtube_id = $1
    else
      youtube_url[/^.*((v\/)|(embed\/)|(watch\?))\??v?=?([^\&\?]*).*/]
      youtube_id = $5
    end

    %Q{<div class="smallVideo" style="text-align:center"><a href="http://www.youtube.com/watch?v=#{youtube_id}" target="_blank"><img src="http://i3.ytimg.com/vi/#{youtube_id}/mqdefault.jpg"></a></div>
    <div class="largeVideo" style="display:none; text-align:center;"><iframe title="YouTube Video Player" width="#{width}" height="#{height}" src="http://www.youtube.com/embed/#{youtube_id}" frameborder="0" allowfullscreen></iframe></div>}
  end

end
