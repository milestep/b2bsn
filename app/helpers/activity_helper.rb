module ActivityHelper

  def item_from_another_day?(item, i)
    (item.created_at.day != @activity_feed[i-1].created_at.day) && (i > 0)
  end

end
