module NewsHelper
  def news_excerpt(news)
    news.description.gsub(/(<(.|\n)*?>)|(<(.|\n)*?)$/, "").truncate(NewsItem::TRUNCATION_LENGTH) if news.description
  end

  def today_first_news?(date, index)
    date == Time.now.to_formatted_s(:day_week) && index.zero?
  end
end