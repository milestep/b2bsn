module PublishersHelper

  def render_ad_type_groups(ad_type_form, company)  
    groups_map = [
      [AdTypeGroup::RICH_MEDIA_AD_UNITS, AdTypeGroup::OTHER_RICH_MEDIA],
      [AdTypeGroup::UAP, AdTypeGroup::OTHER_AD_UNITS]
    ] 
    result = ""
    groups_map.each do |couple_groups|
      groups_column = ""
      couple_groups.each_with_index do |group_name, index|
        group_html_options = {:class => 'ad-type-accepted-cat'}
        group_html_options[:style] = 'clear: left;' if index.odd? 
        group = @ad_type_groups.select{|e| e.name == group_name.to_s}.first
        group_html = render(:partial => 'publishers/ad_type_group', :locals => {:group => group, :ad_type_form => ad_type_form, :company => company})
        groups_column << content_tag(:div, group_html, group_html_options)
      end
      result << content_tag(:div, groups_column.html_safe, :style => "float: left;")
    end
    result.html_safe
  end

end
