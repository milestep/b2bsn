module SearchesHelper
  def results_for(search_text)
    content_tag :p, t('searches.index.search_results_for', :search_for => search_text) unless search_text.empty?
  end
end
