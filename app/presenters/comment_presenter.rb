class CommentPresenter < BasePresenter
  include PresenterModules::LikeLink
  presents :comment

  def like_button_id
    "comment-like-#{comment.id}"
  end

  def likeable
    comment
  end

  def like_link_url
    h.comment_likes_path(comment, like_link_params)
  end

  def after_create_like_partial
    "comment_create"
  end

  def shared_link?
    false
  end
end
