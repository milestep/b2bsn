class ConnectionPresenter < BasePresenter
  presents :item

  def user
    item.actor(h.current_user)
  end

  def user_short_name
    user.fullname.truncate(25)
  end

  def user_type
    user.item_type.titleize if user.item_type
  end

  def block_button
    if item.incoming_for?(h.current_user)
      h.link_to("", h.connection_path(item, :commit => t("requests.options.block")), :class => "block_user", :method => :put, :remote => true)
    end
  end

  def created
    item.created_at.strftime(t('date.formats.middle'))
  end

  def render_buttons
    if item.incoming_for?(h.current_user)
      result = h.link_to(t("requests.item.accept"), h.connection_path(item, :commit => t("requests.options.accept")), :class => "btn btn-primary", :method => :put, :remote => true)
      result << h.link_to(t('requests.item.ignored'), h.ignored_connections_path(:connection_id => item, :commit => t("requests.options.ignore")), :class => "btn btn-cancel", :method => :post, :remote => true)
      result << h.link_to(t('requests.item.decline'), h.connection_path(item, :commit => t("requests.options.reject")), :class => "btn btn-cancel", :method => :put, :remote => true)
      result.html_safe
    else
      send("render_#{item.status}")
    end
  end

  def render_ignored
    h.link_to(t("requests.item.ignored"), "javascript:void(0)", :class => "btn btn-danger")
  end

  def render_pending
    result = h.link_to(t("requests.item.pending"), "javascript:void(0)", :class => "btn btn-primary disabled")
    result << h.link_to(t('requests.item.cancel'), h.connection_path(item, :commit => t("requests.options.cancel")), :class => "btn btn-cancel", :method => :put, :remote => true)
    result.html_safe
  end

  def render_accepted
    result = h.link_to(t("requests.item.accepted"), "javascript:void(0)", :class => "btn btn-primary disabled")
    result << h.link_to(t('requests.item.view_profile'), h.user_profile_path(item.friend.user_profile), :class => "btn btn-cancel", :method => :put, :remote => true)
    result.html_safe
  end

  def render_declined
    h.link_to(t("requests.item.rejected"), "javascript:void(0)", :class => "btn btn-danger")
  end

  def render_blocked
    h.link_to(t("requests.item.blocked"), "javascript:void(0)", :class => "btn btn-danger")
  end

  def render_deleted
    h.link_to(t("requests.item.canceled"), "javascript:void(0)", :class => "btn btn-danger")
  end

  def render_undo
    h.link_to(t('requests.item.undo'), h.connection_path(item, :commit => t("requests.options.undo")), :class => "btn btn-cancel", :method => :put, :remote => true)
  end

  def visibility
    item.incoming_for?(h.current_user) ? '' : 'display: none;'
  end
end

