module PresenterModules::LikeLink
  def like_link
    if like_link_is_displayable?
      text = h.current_user.voted_up_on?(likeable) ? t('likes.dont_like') : t('likes.like')
      h.link_to text, like_link_url, :method => :post, :remote => true
    end
  end

  def like_link_is_displayable?
    likeable.user != h.current_user
  end

  def like_link_params
    h.current_user.voted_up_on?(likeable) ? {:downvote => true} : {}
  end
end
