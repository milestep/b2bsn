module TimelineEvents
  class QuestionAskedPresenter < BaseTimelineEventsPresenter
    def icon
      h.content_tag(:i, '', :class => 'icon-question-sign')
    end

    def message
      link = h.link_to(t('layouts.market_ticker.see_more'), h.post_path(subject))
      h.truncate_with_link(subject.title, :length => truncate_length, :separator => ' ', :link => link)
    end

    def details_popup
      h.render :partial => "layouts/market_ticker/question_details_popup", :locals => {:presenter => self, :event_id => self.id}
    end

    def answers
      result = ""
      subject_recent_answers.take(Answer::DROPDOWN_LIMIT).each do |answer|
        h.present answer.timeline_event do |answer_presenter|
          result << answer_presenter.display
        end
      end
      result.html_safe
    end

    def action
      t('layouts.market_ticker.comment.asked_a_question')
    end

    def new_answer
      subject_answers.build
    end

    def see_more_link
      h.link_to t('layouts.market_ticker.see_more'), h.post_path(subject), :class => "see_more_link" if has_see_more_link?
    end

    def has_see_more_link?
      subject_recent_answers.size > Answer::DROPDOWN_LIMIT
    end

    def commented_on
      text = "#{subject.user.fullname}'s #{subject.post_type.titleize.downcase}"
      h.link_to text, h.post_path(subject)
    end

    def subject_recent_answers
      @subject_recent_answers ||= subject.recent_answers
    end

    def subject_answers
      @subject_answers ||= subject.answers
    end
  end
end
