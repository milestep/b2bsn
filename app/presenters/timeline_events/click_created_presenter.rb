module TimelineEvents
  class ClickCreatedPresenter < BaseTimelineEventsPresenter
    def icon
      h.content_tag(:i, '', :class => 'icon-tag')
    end

    def message
      text = h.replace_user_name_with_link_in_post(timeline_event.subject_clickable, user_friends)
      link = h.link_to(t('layouts.market_ticker.see_more'), h.post_path(subject_clickable))
      text = h.truncate_with_link(text, :length => truncate_length, :separator => ' ', :link => link)
      "\"#{text}\"".html_safe
    end

    def details_popup
      h.render :partial => "layouts/market_ticker/news_item_details_popup", :locals => {:presenter => h.generate_presenter(subject_clickable.timeline_event)}
    end

    def comments
      result = ""
      subject_comments.each do |comment|
        h.present comment.timeline_event do |comment_presenter|
          result << comment_presenter.display
        end
      end
      result.html_safe
    end

    def action
      t('layouts.market_ticker.comment.news_viewed')
    end

    def new_comment
      subject_comments.build
    end

    def title
      h.content_tag(:b, "\"#{subject_clickable.title}\"")
    end

    def like_link
      subject_presenter = h.generate_presenter(subject_clickable)
      return "" unless subject_presenter.like_link_is_displayable?
      h.link_to "", subject_presenter.like_link_url, :class => "like_action", :method => :post, :remote => true
    end

    def subject_likes_size
      subject_clickable.likes.size
    end

    def commented_on
      text = subject_clickable.title
      h.link_to text, h.news_item_path(subject_clickable)
    end

    def see_more_link
      h.link_to t('layouts.market_ticker.see_more'), h.news_item_path(subject_clickable), :class => "see_more_link" if has_see_more_link?
    end

    def has_see_more_link?
      subject_comments.size > Comment::DROPDOWN_LIMIT
    end

    def subject_comments
      @subject_comments ||= subject_clickable.comments
    end

  end
end
