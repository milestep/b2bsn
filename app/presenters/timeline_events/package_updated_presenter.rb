module TimelineEvents
  class PackageUpdatedPresenter < BaseTimelineEventsPresenter
    delegate :packagable, :created_by?, :to => :subject, :prefix => true

    def icon
      h.content_tag(:i, '', :class => 'icon-folder-close')
    end

    def message
      t("presenters.timeline_events.was_updated")
    end

    def header
      package = timeline_event.subject
      link = h.link_to(package.name, [package.packagable, package], :remote => true)
      h.content_tag(:div, link, :class => 'header')
    end

    def logo
      package = timeline_event.subject
      publisher = package.linked_publishers.first
      url = publisher ? publisher.logo.url : package.logo.url
      image = h.image_tag(url, :size => '38x38')
      h.content_tag(:div, image, :class => 'logo')
    end

    def details_popup
      h.render :partial => "layouts/market_ticker/package_details_popup", :locals => {:presenter => self}
    end
  end
end
