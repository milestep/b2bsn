module TimelineEvents
  class ProfileUpdatedPresenter < BaseTimelineEventsPresenter
    def icon
      h.content_tag(:i, '', :class => 'icon-user')
    end

    def message
      link_text = t("presenters.timeline_events.job_details")
      profile_link = h.link_to(link_text, h.user_profile_path(timeline_event.actor.user_profile), :class => "message")
      t("presenters.timeline_events.profile_updated", :profile_link => profile_link).html_safe
    end

    def details_popup
      h.render :partial => "layouts/market_ticker/profile_details_popup", :locals => {:presenter => self}
    end

    def popup_message
      subject.title
    end
  end
end
