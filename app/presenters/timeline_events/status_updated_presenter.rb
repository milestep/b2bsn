module TimelineEvents
  class StatusUpdatedPresenter < BaseTimelineEventsPresenter
    def icon
      h.content_tag(:i, '', :class => 'icon-tag')
    end

    def message
      text = h.replace_user_name_with_link_in_post(timeline_event.subject, user_friends)
      link = h.link_to(t('layouts.market_ticker.see_more'), h.post_path(subject))
      text = h.truncate_with_link(text, :length => truncate_length, :separator => ' ', :link => link)
      "\"#{text}\"".html_safe
    end

    def details_popup
      h.render :partial => "layouts/market_ticker/status_details_popup", :locals => {:presenter => self}
    end

    def comments
      result = ""
      subject_comments.take(Comment::DROPDOWN_LIMIT).each do |comment|
        h.present comment.timeline_event do |comment_presenter|
          result << comment_presenter.display
        end
      end
      result.html_safe
    end

    def action
      t('layouts.market_ticker.comment.changed_status')
    end

    def new_comment
      subject_comments.build
    end

    def title
      "\"#{subject.title}\""
    end

    def commented_on
      text = "#{subject.user.fullname}'s #{subject.post_type.titleize.downcase}"
      h.link_to text, h.post_path(subject)
    end

    def see_more_link
      h.link_to t('layouts.market_ticker.see_more'), h.post_path(subject), :class => "see_more_link" if has_see_more_link?
    end

    def has_see_more_link?
      subject_comments.size > Comment::DROPDOWN_LIMIT
    end

    def subject_comments
      @subject_comments ||= subject.comments
    end
  end
end
