module TimelineEvents
  class BaseTimelineEventsPresenter < BasePresenter
    TRUNCATE_LENGTH = 200
    USERNAME_TRUNCATE_LENGTH = 19

    presents :timeline_event
    delegate :id, :logo_url, :name, :description, :views_count, :packagable, :to => :subject, :prefix => true
    delegate :fullname, :logo_url, :user_profile, :to => :actor, :prefix => true
    instance_variable :user_friends

    def header
      actor = timeline_event.actor
      link = h.content_tag(:a, actor.fullname, :href => h.user_profile_path(actor.user_profile))
      h.content_tag(:div, link, :class => "header", :style => "float: left;")
    end

    def icon
      h.content_tag(:i, '', :class => 'icon-tag')
    end

    def logo
      ""
    end

    def message
      ""
    end

    def time_place_info
      "#{h.time_ago_in_words(timeline_event.created_at)} #{t('layouts.market_ticker.ago')}"
    end

    def created_at_time
      timeline_event.created_at.strftime("%I.%M %p")
    end

    def for_market_ticker
      if is_displayable_in_market_ticker?
        h.render :partial => "layouts/market_ticker/activity_item", :locals => {:presenter => self}
      else
        ""
      end
    end

    def for_notices
      h.render :partial => "layouts/notices/notice", :locals => { :presenter => self }
    end

    def for_notices_popup
      h.render :partial => "layouts/notices/notice_for_popup", :locals => {:presenter => self}
    end

    def is_displayable_in_market_ticker?
      true
    end

    def details_popup
      ""
    end

    def display
      h.render :partial => "layouts/market_ticker/comment", :locals => {:presenter => self}
    end

    def subject_likes_size
      subject.likes.size
    end

    def title
      "\"#{subject.title}\""
    end

    def popup_message
      link = h.link_to(t('layouts.market_ticker.see_more'), h.post_path(post))
      h.truncate_with_link(title, :length => truncate_length, :separator => ' ', :link => link)
    end

    def hide_confirm
      link = h.link_to(actor_fullname, h.user_profile_path(actor_user_profile))
      "<p>#{t('layouts.market_ticker.hide_confirm', :user_name => link)}</p>"
    end

    def truncate_length
      TRUNCATE_LENGTH
    end

    def usename_truncate_length
      USERNAME_TRUNCATE_LENGTH
    end

    def post
      subject
    end

    def like_link
      subject_presenter = h.generate_presenter(subject)
      return "" unless subject_presenter.like_link_is_displayable?
      h.link_to "", subject_presenter.like_link_url, :class => "like_action", :method => :post, :remote => true
    end

    def unread?
      notice_for_user(h.current_user).unread
    end

  end
end
