module TimelineEvents
  class CommentCreatedPresenter < BaseTimelineEventsPresenter
    def icon
      h.content_tag(:i, '', :class => 'icon-comment')
    end

    def action
      t('layouts.market_ticker.comment.wrote_comment')
    end

    def message
      "#{t("notices.commented_on")} #{post_presenter.commented_on}".html_safe
    end

    def details_popup
      post_presenter.details_popup
    end

    def post
      @post ||= subject.commentable
    end

    def is_displayable_in_market_ticker?
      false
    end

    def title
     "\"#{subject.comment}\""
    end

    def post_presenter
      @post_presenter ||= h.generate_presenter(post.timeline_event)
    end

  end
end
