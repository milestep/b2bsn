module TimelineEvents
  class QuestionAnsweredPresenter < BaseTimelineEventsPresenter
    def icon
      h.content_tag(:i, '', :class => 'icon-comment')
    end

    def message
      link_text = t("presenters.timeline_events.someones_question", :user_name => post.user_fullname)
      question_link = h.link_to(link_text, h.post_path(post), :class => "message")
      t("presenters.timeline_events.answer", :question_link => question_link).html_safe
    end

    def action
      t('layouts.market_ticker.comment.answered')
    end

    def details_popup
      h.render :partial => "layouts/market_ticker/question_details_popup", :locals => {:presenter => h.generate_presenter(post.timeline_event), :event_id => self.id}
    end

    def post
      subject.post
    end
  end
end
