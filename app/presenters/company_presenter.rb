class CompanyPresenter < BasePresenter
  presents :company

  def name
    company.name
  end

  def logo(options = {})
    size = options.fetch(:size, "130x130")
    h.image_tag(company.linkedin_logo, :size => size)
  end

  def location
    result = ""
    company.locations.each do |location|
      result << h.content_tag(:p, location.short_address)
    end
    result.html_safe
  end

  def about_me
    t('shared.view_profile.description')
  end

  def description
    h.escape_new_line_symbols(company.description)
  end

	[:nav_tabs, :render_tabs, :title, :type, :company_name].each do |name|
    define_method(name) { nil }
  end

  def header_buttons
    nil
  end

  def profile_path
    h.tool_path(company)
  end
end
