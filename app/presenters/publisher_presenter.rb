class PublisherPresenter < CompanyPresenter
  def nav_tabs
    %Q(
      <li class='active'><a data-toggle='tab' href='#profile_employees'>#{t('tools.show.organizations')}</a></li>
      <li><a data-toggle='tab' href='#profile_accepted_units'>#{t('tools.show.accepted_units')}</a></li>
      <li><a data-toggle='tab' href='#profile_traffic'>#{t('tools.show.traffic_profile')}</a></li>
    ).html_safe
  end

  def render_tabs
    result = ""
    result << h.render(:partial => "tools/profile_employees")
    result << h.render(:partial => "tools/profile_accepted_units")
    result << h.render(:partial => "tools/profile_traffic")
    result.html_safe
  end
end
