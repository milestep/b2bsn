class UserProfilePresenter < BasePresenter
  presents :user_profile

  def name
    user_profile.fullname
  end

  def logo(options = {})
    size = options.fetch(:size, "130x130")
    with_edit_buttons = options.fetch(:with_edit_buttons, false)
    result = ""
    result << h.render(:partial => "user_profiles/edit_photo_buttons", :locals => {:presenter => self}) if with_edit_buttons && user_profile.user == h.current_user
    result << h.image_tag(user_profile.avatar.url, :size => size)
    result.html_safe
  end

  def title
    h.content_tag :div, user_profile.title, :class => 'title'
  end

  def location
    user_profile.location
  end

  def about_me
    t('shared.view_profile.about_me')
  end

  def description
    h.escape_new_line_symbols(user_profile.description) if user_profile.description
  end

  def type
    user_profile.user.user_type
  end

  def header_buttons
    result = ""
    if h.current_user == user_profile.user
      result << h.link_to(t('shared.view_profile.edit_profile'), h.edit_base_info_user_profile_path(user_profile), :class => "button", :remote => true)
    else
      result << h.render(:partial => "user_profiles/authorizations")
      result << h.request_connection_user_link(user_profile.user) unless h.current_user.ignored_by?(user_profile.user)
      result << h.link_to(t('shared.view_profile.message'), h.new_conversation_path(:recipient_id => user_profile.user.id), :class => "button", :remote => true)
    end
    result.html_safe
  end

  def profile_path
    h.user_profile_path(user_profile)
  end

  def company_name
    h.content_tag :div, user_profile.company.try(:name), :class => 'company-name'
  end
end
