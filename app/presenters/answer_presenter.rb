class AnswerPresenter < BasePresenter
  include PresenterModules::LikeLink
  presents :answer

  def like_button_id
    "answer-like-#{answer.id}"
  end

  def like_link_url
    h.answer_likes_path(answer, like_link_params)
  end

  def likeable
    answer
  end

  def after_create_like_partial
    "answer_create"
  end
end
