class FeatureRequestPresenter < BasePresenter
  presents :feature_request
  
  def modal_title
    I18n.t('presenters.feature_request.modal_title')
  end
  
  def tip; nil; end
  def subject; nil; end
end
