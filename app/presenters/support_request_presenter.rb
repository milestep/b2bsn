class SupportRequestPresenter < BasePresenter
  presents :support_request
  
  def modal_title
    t('presenters.support_request.modal_title')
  end
  
  def tip
    h.content_tag :p, t('presenters.support_request.tip').html_safe, :class => 'tip'
  end
  
  def subject
    h.fields_for(:jira_issue, @object) do |f| 
      f.label(:title, t('presenters.support_request.subject')) << 
      f.text_field(:title)
    end
  end
end
