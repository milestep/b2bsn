class BasePresenter
  def initialize(object, template)
    @object = object
    @template = template
  end

  def to_param
    id
  end

private

  def self.presents(name)
    define_method(name) do
      @object
    end
  end

  def self.instance_variable(name)
    define_method(name) do
      @template.instance_variable_get("@#{name}")
    end
  end

  def h
    @template
  end

  def t(*args)
    I18n.t(*args)
  end

  def method_missing(*args, &block)
    @object.send(*args, &block)
  end

end
