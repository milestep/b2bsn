class AgencyPresenter < CompanyPresenter
  def nav_tabs
    %Q(
      <li class='active'><a data-toggle='tab' href='#profile_employees'>#{t('tools.show.organizations')}</a></li>
      <li><a data-toggle='tab' href='#profile_brands'>#{t('tools.show.brands')}</a></li>
    ).html_safe
  end

  def render_tabs
    result = ""
    result << h.render(:partial => "tools/profile_employees")
    result << h.render(:partial => "tools/profile_brands")
    result.html_safe
  end
end
