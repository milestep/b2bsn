class ClickPresenter < BasePresenter
  include PresenterModules::LikeLink
  presents :click

  def like_button_id
    "news-like-#{clickable.id}"
  end

  def like_link_is_displayable?
    true
  end

  def like_link_url
    h.news_item_likes_path(clickable, like_link_params)
  end

  def likeable
    clickable
  end

  def after_create_like_partial
    "news_item_create"
  end

  def clickable
    @clickable ||= click.clickable
  end
end
