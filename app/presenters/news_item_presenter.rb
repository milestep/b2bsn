class NewsItemPresenter < BasePresenter
  include PresenterModules::LikeLink
  presents :news_item

  def like_button_id
    "news-like-#{news_item.id}"
  end

  def like_link_is_displayable?
    true
  end

  def like_link_url
    h.news_item_likes_path(news_item, like_link_params)
  end

  def likeable
    news_item
  end

  def after_create_like_partial
    "news_item_create"
  end
end
