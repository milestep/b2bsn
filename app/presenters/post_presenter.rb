class PostPresenter < BasePresenter
  include PresenterModules::LikeLink
  presents :post

  def like_button_id
    "post-like-#{post.id}"
  end

  def like_link_url
    h.post_likes_path(post, like_link_params)
  end

  def likeable
    post
  end

  def after_create_like_partial
    "post_create"
  end

  def shared_link?
    true
  end
end
