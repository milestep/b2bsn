class ActivityFeedsController < ApplicationController
  before_filter :load_activity_items, :only => :index

  def index
    render :json => @activity_items.for_ticker.to_json(:only => [:event_type, :id, :actor_id, :created_at],
                                                       :methods => [:author_name, :actor_fullname, :event_title,
                                                         :profile_path, :event_path, :package_name, :status_update?,
                                                         :question_asked?, :question_answered?, :profile_update?,
                                                         :package_updated?, :updated_status, :posting_path])
  end

  private

  def load_activity_items
    return unless signed_in?
    if params[:profile_page] == "true"
      @user_profile = UserProfile.includes(:user).find(params[:id])
      @activity_items = TimelineEvent.unscoped.by_actor(@user_profile.user)
      @activity_items = @activity_items.public_events unless @user_profile.user.friend_of?(current_user)
    else
      @activity_items = TimelineEvent.unscoped{ current_user.all_events_except_ignored_users.includes(:subject) }
    end
    @user_friends = User.stream_friends_for(@activity_items) if @activity_items.present?
  end
end
