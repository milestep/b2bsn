module Signup
  class SkillsController < SignupController
    def index
      @skills = current_user.user_profile.skills
    end

    def destroy
      @skill = current_user.user_profile.skills.find(params[:id])
      @skill.destroy
    end

    def create
      user_profile = current_user.user_profile
      @skill = user_profile.skills.find_by_name(params['skill'])
      if @skill
        status = :existing
      else
        @skill = user_profile.skills.create(:name => params['skill'])
        status = @skill.valid? ? :new : :invalid
      end
      render :json => {:skill_id => @skill.id, :status => status}
    end
  end
end
