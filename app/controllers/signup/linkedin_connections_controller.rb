module Signup
  class LinkedinConnectionsController < SignupController
    def index
      @connections = linkedin_client.linkedin_connections
    end

    def create
      Resque.enqueue(LinkedInMess, current_user.id, linkedin_client.token, linkedin_client.secret, params[:connections])
      redirect_to root_path
    end
  end
end
