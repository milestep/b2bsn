module Signup
  class UsersController < SignupController
    skip_before_filter :authenticate_user
    skip_before_filter :verify_registration_completeness
    before_filter :find_user

    def update
      if @user.update_attributes(params[:user])
        session[:user_id] = @user.id

        linkedin_client.import_experiences!
        linkedin_client.import_skills!

        redirect_to signup_experiences_path(:id => @user.uid)
      else
        render :action => 'edit'
      end
    end

    private

    def find_user
      @user = User.find_by_uid(params[:id]) || render_not_found
    end

  end
end
