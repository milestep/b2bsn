module Signup
  class BeanstockConnectionsController < SignupController
    def index
      @connections = linkedin_client.beanstock_connections
    end

    def create
      current_user.send_contact_requests(params[:connections])
      redirect_to signup_linkedin_connections_path(:id => current_user.uid)
    end

  end
end
