module Signup
  class ExperiencesController < SignupController

    before_filter :get_user_profile, :except => [:index]
    before_filter :get_experience, :except => [:index]

    def index
      @experiences = current_user.experiences
    end

    def destroy
      @experience.destroy if @experience
    end

    def edit
      respond_to do |format|
        format.js
      end
    end

    def update
      if @experience.update_attributes(params[:experience])
        respond_to do |format|
          format.js
        end
      else
        render :action => "edit"
      end
    end

    private

    def get_user_profile
      @user_profile = current_user.user_profile
    end

    def get_experience
      @experience = @user_profile.experiences.find(params[:id])
    end
  end
end
