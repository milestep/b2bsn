class OpportunitiesController < ApplicationController
  
  def index
    # TODO: Replace this with real connections
    @connections = User.limit(9).order("rand()")
    if params[:new_opportunity]
      current_user.role?('buyer') ? @opportunities_count = 13 : @opportunities_count = 10
      session[:opportunities_count] = @opportunities_count
      session[:new_opportunity] = true
    end
  end
  
  def show
  end
  
  def new
    @next_page = params[:page].to_i + 1
    respond_to do |format|
      format.js
    end
  end
  
end
