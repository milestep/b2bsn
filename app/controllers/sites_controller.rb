class SitesController < ApplicationController

  def autocomplete
    @sites = Site.order(:name).search(params[:term])
    render :json => @sites.map{ |s| { :id => s.id, :value => s.name } }
  end

end
