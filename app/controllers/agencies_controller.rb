class AgenciesController < ApplicationController

  before_filter :find_agency, :only => [:show, :edit, :update, :destroy]

  def index
    @company = Company.find(params[:company]) if params[:company]
    @agencies = if @company
                  @company.agencies.search(params[:search]).order_by_name(params[:order])
                else
                  Agency.search(params[:search]).order_by_name(params[:order])
                end
  end

  def show
    @contacts = @agency.contacts

    respond_to do |format|
      format.html # show.html.erb
      format.json { render :json => @agency }
      format.js
    end
  end

  def new
    @agency = Agency.expanded_company
    respond_to do |format|
      format.js
    end
  end

  # GET /agencies/1/edit
  def edit
    respond_to do |format|
      format.js
      format.html # new.html.erb
    end
  end

  # POST /agencies
  # POST /agencies.json
  def create
    @agency = Agency.new(params[:agency])
    @agency.owner = current_user

    respond_to do |format|
      if @agency.save
        format.js
        format.html { redirect_to tools_path, :notice => I18n.t('agencies.create.success') }
        format.json { render :json => @agency, :status => :created, :location => @agency }
      else
        @agency.agency_brands.new if @agency.agency_brands.empty?
        @agency.agency_advertisers.new if @agency.agency_advertisers.empty?
        format.js
        format.html { render :action => "new" }
        format.json { render :json => @agency.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /agencies/1
  # PUT /agencies/1.json
  def update
    respond_to do |format|
      if @agency.update_attributes(params[:agency])
        format.js
        format.html { redirect_to @agency, :notice => I18n.t('agency_was_successfully_updated') }
        format.json { head :ok }
      else
        format.js
        format.html { render :action => "edit" }
        format.json { render :json => @agency.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /agencies/1
  # DELETE /agencies/1.json
  def destroy
    @agency.destroy

    respond_to do |format|
      format.js
      format.html { redirect_to agencies_url }
      format.json { head :no_content }
    end
  end

  def autocomplete
    @agencies = Agency.order(:name).where("name like ?", "%#{params[:term]}%")
    render :json => @agencies.map{|a| {:id => a.id, :value => a.name}}
  end

  private

  def find_agency
    @agency = Agency.find(params[:id])
  end
end
