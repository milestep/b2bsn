class LoginsController < ActionController::Base

  def index
    @users = User.all
  end

  def simulate_login
    session[:user_id] = params[:id]
    redirect_to root_path
  end

end
