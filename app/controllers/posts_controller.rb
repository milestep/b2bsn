class PostsController < ApplicationController

  def show
    @post = Post.find(params[:id])
    @post.viewed_by(current_user) unless @post.user == current_user
    @likes = @post.likes
    if @post.is_question?
      @other_questions = @post.user.questions.reject{|q| q.id == params[:id].to_i }
      render "show_question" and return
    end
    @other_posts = @post.user.posts.reject{|q| q.id == params[:id].to_i }

    respond_to do |format|
      format.html
      format.js
    end
  end

  def create
    params[:post][:who_asking] = params['auto-who-publishing'] if params[:post][:post_type] == "Status"
    @post = current_user.posts.build(params[:post])
    if params['auto-who-publishing'].present? && params['auto-who-publishing'].length  #at least one circle (existence implies status)
      circles_names = params['auto-who-publishing'].split ', '
      @post.circles = Circle.user_circles(current_user, circles_names)
    end
    unless params['mentioned_users_ids'].blank?
      mentioned_users_ids = params['mentioned_users_ids'].split(",").map(&:to_i)
      @post.add_mentioned_users(mentioned_users_ids);
    end
    @authorizations = current_user.authorizations
    @activity_feed = current_user.timeline_events.paginate(:page => 1, :per_page => 10)
    @stream_friends = User.stream_friends_for(@activity_feed)
    respond_to do |format|
      if @post.save
        host = host =~ /80/ ? request.host : request.host_with_port
        SocialNetwork.socialize(@post,current_user,Authorization.default_providers(current_user),host)
        @activity_item = @post.timeline_events.includes(:subject).to_json(:only => [:event_type, :id, :actor_id, :created_at],
                                                       :methods => [:actor_fullname, :event_title,
                                                         :profile_path, :event_path, :package_name, :status_update?,
                                                         :question_asked?, :question_answered?, :profile_update?,
                                                         :package_updated?, :updated_status, :posting_path])
        flash[:notice] = (@post.is_question?) ? I18n.t(".posts.create.question_posted") : I18n.t(".posts.create.status_posted")
        format.js
      else
        format.js
      end
    end
  end

  def new
    @user_profile = current_user.user_profile
    @post = Post.new
    @autocomplete = {
      :friend_summaries => User.friend_summaries(current_user).uniq,
      :circles => current_user.circles.select([:id, :user_id, :name])
    }
    respond_to do |format|
      format.js
    end
  end

end
