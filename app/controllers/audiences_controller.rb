class AudiencesController < ApplicationController

  def autocomplete
    @audiences = Audience.order(:name).search(params[:term])
    render :json => @audiences.map{ |a| { :id => a.id, :value => a.name } }
  end

end
