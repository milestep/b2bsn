class NewsController < ApplicationController
  layout "discover", :only => [:feeds, :slide_news, :show_news]
  before_filter :build_feed, :only => [:feeds, :slide_news]

  def index
    news = NewsItem.latest_news
    if params[:feed_id]
      news = news.where(:news_feed_id => params[:feed_id]).limit(16)
    end
    render "opps" unless news
    
    @news = news
  end

  def show
    @news = NewsItem.find(params[:id])
    @likes = @news.likes

    respond_to do |format|
      format.html
      format.js
    end
  end

  def slide_elements
    @news = NewsItem.newest.paginate(:page => params[:current_page].to_i.next, :per_page => 10)

    respond_to do |format|
      format.js
    end
  end

  def feeds
  end

  def show_news
    @feed = NewsFeed.fetch_feeds
    @item = @feed.find { |new| new.id == params[:id].to_i && new.obj.is_a?(NewsItem) }
    item_index = @feed.index(@item)
    @page_number = (item_index.to_f/Feeds::BaseFeed::FEED_NEWS_PER_PAGE).ceil
    @page_number = 1 if @page_number.zero?
    @feed = @feed.paginate(:page => 1, :per_page => @page_number * Feeds::BaseFeed::FEED_NEWS_PER_PAGE)
    @feed = @feed.group_by(&:feed_date)
  end

  def slide_news
    respond_to do |format|
      format.json{ render :json => @feed.to_json(:root => false) }
    end
  end

  private

  def build_feed
    page = params[:current_page] ? params[:current_page].to_i.next : 1
    @feed = NewsFeed.fetch_feeds
    filter_feeds if params[:feed_type] && params[:feed_type] != "all"
    @feed = @feed.paginate(:page => page, :per_page => Feeds::BaseFeed::FEED_NEWS_PER_PAGE)
    @feed = @feed.group_by(&:feed_date)
    @feed
  end

  def filter_feeds
    @feed = @feed.find_all { |news_item| news_item.feed_class == params[:feed_type] }
  end
end
