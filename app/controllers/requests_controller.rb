class RequestsController < ApplicationController
  layout "discover", :only => :index

  def index
    @requests = current_user.requests_union
    @data_for_autocomplete = current_user.to_requests_union_json
  end
end
