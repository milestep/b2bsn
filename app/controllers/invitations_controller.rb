class InvitationsController < ApplicationController
  skip_before_filter :authenticate_user, :only => :update

  def update
    redirect_to login_path(:invite_code => params[:token])
  end
end
