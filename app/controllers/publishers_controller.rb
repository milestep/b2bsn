class PublishersController < ApplicationController
  before_filter :find_publisher, :only => [:show, :edit, :update]

  def show
    @contacts = @publisher.contacts

    respond_to do |format|
      format.html # show.html.erb
      format.json { render :json => @publisher }
      format.js
    end
  end

  def create
    @publisher = Publisher.new(params[:publisher])
    @publisher.owner = current_user

    respond_to do |format|
      if @publisher.save
        format.js { render :action => "create_success" }
      else
        @publisher.initate_associations
        @ad_type_groups = AdTypeGroup.all
        format.js { render :action => "error" }
      end
    end
  end

  def edit
    @publisher.initate_associations
    @ad_type_groups = AdTypeGroup.all
  end

  def update
    respond_to do |format|
      if @publisher.update_attributes(params[:publisher])
        format.js { render :action => "update_success" }
      else
        @publisher.initate_associations
        @ad_type_groups = AdTypeGroup.all
        format.js { render :action => "error" }
      end
    end
  end

  private

  def find_publisher
    @publisher = Publisher.find(params[:id])
  end
end
