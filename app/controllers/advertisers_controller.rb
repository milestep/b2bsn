class AdvertisersController < ApplicationController

  def index
    @advertisers = Advertiser.all
  end

  def create
    @advertiser = Advertiser.new(params[:advertiser])
    @advertiser.owner = current_user

    respond_to do |format|
      if @advertiser.save
        format.js
        format.html { redirect_to tools_path, :notice => I18n.t('advertisers.create.success') }
        format.json { render :json => @advertiser, :status => :created, :location => @advertiser }
      else
        @advertiser.build_associations
        format.js
        format.html { render :action => "new" }
        format.json { render :json => @advertiser.errors, :status => :unprocessable_entity }
      end
    end
  end

  def autocomplete
    @advertisers = Advertiser.order(:name).where("name like ?", "%#{params[:term]}%")
    render :json => @advertisers.map{ |a| { :id => a.id, :value => a.name } }
  end

end
