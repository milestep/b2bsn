class SessionsController < ApplicationController
  before_filter :authenticate_user, :get_notifications, :except => [:new, :create, :destroy]
  before_filter :check_oauth_problems, :only => :create
  after_filter :add_to_authorizations, :only => :create
  before_filter :redirect_loggedin_user, :only => :new

  def new
    if params[:invite_code].present?
      invite = Invitation.validate_token(params[:invite_code])
      if invite
        invite.update_attribute(:used_at, Time.now)
        session[:invite_id] = invite.id
        flash[:warning] = "Invite accepted."
      else
        flash[:warning] = "Invalid invite code."
      end
    end
    render "login"
  end

  def create
    @user = User.find_by_provider_and_uid(@auth["provider"], @auth["uid"])

    if @user
      store_auth_keys
      session[:user_id] = @user.id
      redirect_to root_url, :notice => "#{@user.fullname} logged in"
    else
      @user = User.create_with_omniauth(@auth, session[:invite_id])
      store_auth_keys
      redirect_to edit_signup_user_path(@user.uid)
    end
  end


  def destroy
    current_user.user_status.offline! if current_user
    session[:user_id] = nil
    session[:invite_id] = nil
    redirect_to root_url
  end

  private

  def check_oauth_problems
    @auth = request.env["omniauth.auth"]
    if params[:oauth_problem].present?
      redirect_to login_url, :notice => "Problem authenticating via LinkedIn"
    end
  end

  def store_auth_keys
    if @auth["credentials"]
      session[:rtoken] = @auth["credentials"]["token"]
      session[:rsecret] = @auth["credentials"]["secret"]
    end
  end

  def add_to_authorizations
    Authorization.create_or_refresh_omniauth(@auth, @user) # add to authorizations too.
  end

  def redirect_loggedin_user
    redirect_to root_path if signed_in?
  end

end
