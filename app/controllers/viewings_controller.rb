class ViewingsController < ApplicationController
  before_filter :get_viewable, :only => [:index]
  
  def index
    @viewers = @viewable.viewers
  end

  private
  
  def get_viewable
    @viewable = UserProfile.find(params[:user_profile_id]) if params[:user_profile_id]
    @viewable = Post.find(params[:post_id]) if params[:post_id] 
  end
  
end
