class SettingsController < ApplicationController
  before_filter :admin_only, :only => [:index, :show]
  

  # GET /settings/1/edit
  def edit
    @setting = current_user.admin? ? Setting.find(params[:id]) : Setting.find(current_user)
    @authorizations = Authorization.find(:all, :conditions => {:user_id => @setting})
    @social_networks = SocialNetwork.all

    @social_defaults = @social_networks.map do |s| 
      auth=@authorizations.select do |a| 
        a.provider == s.provider
      end
      [s,auth.length>0,auth]
     end
     @social_defaults
  end

  # PUT /settings/1
  # PUT /settings/1.json
  def update
      
    @setting = current_user.admin? ? Setting.find(params[:id]) : Setting.find(current_user)
    @authorizations = @setting.authorizations
    @authorizations.each {|a| (params[a.provider] == "1")? a.posting_default=true : a.posting_default=false}
    decode_visibility params    
    respond_to do |format|
      if @setting.update_attributes(params[:setting]) && @setting.user_profile.save && @authorizations.each {|a| a.save}
        format.html { redirect_to edit_setting_path, :notice => 'Setting was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render :action => "edit" }
        format.json { render :json => @setting.errors, :status => :unprocessable_entity }
      end
    end
  end

  # ADMIN ONLY (convenience)
  # ================================================================
  # GET /settings
  # GET /settings.json
  def index
    @settings = Setting.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render :json => @settings }
    end
  end

  # GET /settings/1
  # GET /settings/1.json
  def show
    @setting = Setting.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render :json => @setting }
    end
  end
  
  private
  def decode_visibility params
    @setting.user_profile.visibility = [params[:public], params[:connections], params[:publishers]].compact.join("|")
  end

end
