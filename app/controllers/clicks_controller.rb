class ClicksController < ApplicationController
  def create
    @click = Click.new(params[:click])
    if @click.clickable_type
      @click.clickable.clicked_by(current_user)
    else
      @click.user = current_user
      @click.save!
    end
    
    render :nothing => true
  end
end
