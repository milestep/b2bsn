class PackagesController < ApplicationController

  before_filter :find_packagable
  before_filter :find_package, :only => [:show, :edit, :update, :destroy]

  def index
    @packages = @packagable.linked_packages
  end

  def new
    @package = current_user.packages.build
    respond_to do |format|
      format.js
    end
  end

  def create
    @package = current_user.packages.create(params[:package])
    respond_to do |format|
      format.js
    end
  end

  def edit
    render :action => :new
  end

  def update
    @package.update_attributes(params[:package])
  end

  def destroy
    @package.destroy
  end

  def show
    @package.viewed_by(current_user) unless @package.created_by?(current_user)
  end

  private

  def find_packagable
    resource, id = request.path.split('/')[1,2]
    @packagable = resource.classify.constantize.find(id)
  end

  protected

  def find_package
    @package = @packagable.packages.find(params[:id])
  end
end
