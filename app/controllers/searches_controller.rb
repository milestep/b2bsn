class SearchesController < ApplicationController

  layout 'search'

	helper_method :check_filter_params

  before_filter :ensure_params_provided, :only => :autocomplete
  before_filter :check_search_params

  def index
    search
  end

  def autocomplete
    @search_text = params[:term]
    render :json => Search::Results::Formatter.new(search).to_autocompete_json
  end

  private

  def check_search_params
    @filter_param = params[:filter] ? params[:filter].downcase.to_sym : :all
    @search_text = params[:search] || ''
    @limit = params[:rows] || 0
  end

  def ensure_params_provided
    render(:json => {:error => 'Bad request.'}, :status => :bad_request) unless params[:term].present?
  end

  def search
    if Constants::Search::FILTER.has_key?(@filter_param) && @search_text.present?
      @search ||= Sunspot::Searching.new(@filter_param, @search_text)
      @search_results ||= @search.produce_search(:limit => @limit)
    end
  end

	def check_filter_params
		params[:filter] || I18n.t(:all)
	end

end
