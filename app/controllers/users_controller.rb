class UsersController < ApplicationController
  skip_before_filter :authenticate_user, :only => :create
  # GET /users/1
  # GET /users/1.json
  def show
    @user = current_user
    @user_profile = @user.user_profile || @user.create_user_profile
    @post = Post.new
    @stats = Hash.new
    @stats[:total_users] = User.count
    @stats[:new_users] = User.where("created_at > ?", 7.days.ago).count
    @stats[:your_connections] = @my_friends.count
    @stats[:profile_views] = current_user.user_profile.viewings.count
    @news = NewsItem.latest_deals
    @activity_feed = TimelineEvent.aggregated_timeline.limit(TimelineEvent::EVENTS_PER_PAGE)
    @stream_friends = User.stream_friends_for(@activity_feed)
    @badges = current_user.badges
    @twitter_id = Authorization.find(:first, :conditions => {:user_id => current_user, :provider => "twitter"}).try(:username) || "beanstockmedia"
    @autocomplete={
      :friend_summaries => User.friend_summaries(current_user).uniq,
      :circles => current_user.circles.select([:id, :user_id, :name])
    }
  end

  def update_activity_feed
    @activity_feed = params[:user_id].present? ? User.find(params[:user_id]).own_timeline_events.scoped : TimelineEvent.scoped
    @activity_feed = @activity_feed.where("date(timeline_events.created_at) <= ?", params[:t]) if params[:t].present?
    @activity_feed = @activity_feed.where(:event_type => params[:event_type]) if params[:event_type].present?
    @activity_feed = @activity_feed.paginate(:page => params[:page] || 1, :per_page => TimelineEvent::EVENTS_PER_PAGE)
    @activity_feed = @activity_feed.aggregated_timeline
    @stream_friends = User.stream_friends_for(@activity_feed)

    respond_to do |format|
      format.js
      format.html { render :layout => false }
    end
  end

  def new_users_modal
    @users = User.since(7.days.ago).limit(10)
    count = User.since(7.days.ago).count
    @more_users = count - 10 if count > 10
  end

  def show_users
    @user_type = params[:t]

    case @user_type
    when "featured"
      @users = User.featured
    when "newest"
      @users = current_user.new_users.newest
    when "suggested"
      @users = current_user.suggested_friends
    when "incoming"
      @connections = current_user.incoming_requests
    when "outgoing"
      @connections = current_user.outgoing_requests
    end

    respond_to do |format|
      format.js
    end
  end

  def search_friends
    respond_to do |format|
      format.js
    end
  end

  def update_status
    current_user.user_status.send(params[:status] + "!") if UserStatus.valid_status?(params[:status])
    render :nothing => true
  end

end
