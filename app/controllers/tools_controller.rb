class ToolsController < ApplicationController
  def index
    @user = current_user
    @user_profile = @user.user_profile || @user.create_user_profile
    role = Role.seller
    @discover_items = User.by_role(role)
    Company::TYPES.each_key do |type|
      @discover_items.concat CompanyLocation.joins(:address, :company).by_type(type).includes(:company, :address)
    end
    @data_for_autocomplete = @discover_items.to_json(:only => :id, :methods => %w(fullname discover_location discover_type))
    render :layout => "discover"
  end

  def new_entry
    @agency = Agency.expanded_company
    @brand = Brand.expanded_company
    @publisher = Publisher.expanded_company
    @advertiser = Advertiser.expanded_company
    @ad_type_groups = AdTypeGroup.all
    respond_to { |format| format.js }
  end

  def nice_filter
    @ad_types = AdType.scoped
    @categories = Category.scoped
    @addresses = Address.grouped_cities
    @roles = Role.scoped
    respond_to { |format| format.js }
  end

  def fine_fetch
    @collection = FineFilter::Fetcher.new(params[:search]).fetch
    respond_to { |format| format.js }
  end

  def show
    @company = Company.find(params[:id])
    @user_profile = current_user.user_profile || current_user.create_user_profile
    @authorizations = current_user.authorizations
    render :layout => "activity_feed"
  end
end
