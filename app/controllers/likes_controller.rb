class LikesController < ApplicationController
  before_filter :get_likes, :only => [:index, :create]

  def index
    respond_to do |format|
      format.js
      format.html # index.html.erb
      format.json { render :json => @likes }
    end
  end

  def create
    if params[:downvote]
      @likeable.disliked_by current_user
    else
      @likeable.liked_by current_user
    end
  end

  private

  def get_likes
    @likeable = Post.find(params[:post_id]) if params[:post_id]
    @likeable = Comment.find(params[:comment_id]) if params[:comment_id]
    @likeable = Answer.find(params[:answer_id]) if params[:answer_id]
    @likeable = NewsItem.find(params[:news_item_id]) if params[:news_item_id]
    @likes = @likeable.likes.limit(5)
  end

end
