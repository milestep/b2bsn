class BrandsController < ApplicationController

  def index
    @company = Company.find(params[:company]) if params[:company]
    @brands = if @company
                  @company.brands.search(params[:search]).order_by_name(params[:order])
                else
                  Brand.search(params[:search]).order_by_name(params[:order])
                end
  end

  def new
    @brand = Brand.expanded_company
    respond_to do |format|
      format.js
    end
  end

  def create
    @brand = Brand.new(params[:brand])
    @brand.owner = current_user

    respond_to do |format|
      if @brand.save
        format.js
        format.html { redirect_to tools_path, :notice => I18n.t('brands.create.success') }
        format.json { render :json => @brand, :status => :created, :location => @brand }
      else
        @brand.agency_brands.new if @brand.agency_brands.empty?
        @brand.brand_advertisers.new if @brand.brand_advertisers.empty?
        format.js
        format.html { render :action => "new" }
        format.json { render :json => @brand.errors, :status => :unprocessable_entity }
      end
    end
  end

  def autocomplete
    @brands = Brand.joins(:brand_advertisers).where("companies.name like ? AND brand_advertisers.advertiser_id in (?)", "%#{params[:term]}%", params[:ids].split(",")).group("companies.id").order("companies.name")
    render :json => @brands.map{|b| {:id => b.id, :value => b.name}}
  end

end
