class ExperiencesController < ApplicationController

  before_filter :find_user_profile, :except => [:index, :new]
  before_filter :find_experience, :except => [:index, :create, :new]
  before_filter :find_companies, :only => [:edit, :new, :update, :create]

  def destroy
    @experience.destroy

    respond_to do |format|
      format.js
    end
  end

  def edit
    respond_to do |format|
      format.js
    end
  end

  def update
    if @experience.update_attributes(params[:experience])
      respond_to do |format|
        format.js { render :action => "update_success" }
      end
    else
      render :action => "edit"
    end
  end

  def new
    @experience = Experience.new

    respond_to do |format|
      format.js
    end
  end

  def create
    @experience = @user_profile.experiences.build(params[:experience])

    if @experience.save
      respond_to do |format|
        format.js { render :action => "create_success" }
      end
    else
      render :action => "new"
    end
  end

  private

  def find_companies
    @companies = Company.all.collect{|c| [c.name, c.id] }.sort
  end

  def find_user_profile
    @user_profile = current_user.user_profile
  end

  def find_experience
    @experience = @user_profile.experiences.find(params[:id])
  end
end
