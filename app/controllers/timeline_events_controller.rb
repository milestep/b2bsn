class TimelineEventsController < ApplicationController
  before_filter :find_actor, :only => [:bulk_hide, :undo_bulk_hide]

  def hide
    @timeline_event = TimelineEvent.find(params[:id])
    current_user.timeline_events.delete(@timeline_event) if @timeline_event

    respond_to do |format|
      format.js
    end
  end

  def bulk_hide
    current_user.hide_events_from(@actor)

    respond_to do |format|
      format.js
    end
  end

  def undo_bulk_hide
    current_user.unhide_events_from(@actor)
    @activity_items = current_user.timeline_events.by_actor(@actor)

    respond_to do |format|
      format.js
    end
  end

  def show
    @item = TimelineEvent.find(params[:id])
    render :layout => false
  end

  private

  def find_actor
    @actor = User.unscoped.find(params[:user_id])
  end
end
