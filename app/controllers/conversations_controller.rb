class ConversationsController < ApplicationController
  layout "discover", :only => [:index, :show]

  before_filter :fetch_inbox_messages, :only => [:index]
  before_filter :find_conversation, :only => [:show, :create_reply, :trash, :untrash, :archive, :unarchive, :forward, :new_reply, :quick_reply]
  before_filter :find_chat_conversation, :only => [:add_chat_message, :chat_history]

  def index
    @conversations = regularize_conversations(@conversations)
  end

  def show
    current_user.mark_as_read(@conversation)
  end

  def create
    recipients_ids = params[:message][:recipients_ids].split(',')
    recipients = User.find_all_by_id(recipients_ids)
    subject = params[:message][:subject].blank? ? I18n.t("conversation.no_subject") : params[:message][:subject]
    @message = current_user.create_message(recipients,
                                              params[:message][:body],
                                              subject,
                                              true,
                                              params[:message][:attachment]
                                             )
    @conversation = @message.deliver.conversation
    if @conversation
      respond_to do |format|
        format.js
      end
    else
      render :action => :new
    end
  end

  def add_chat_message
    @conversation = current_user.send_or_reply_to_conversation(@conversation, params[:message], @friend)
    @message = @conversation.messages.last
    @friend.mark_as_read(@conversation) if @friend.online?
  end

  def chat_history
    @messages = @conversation.messages
  end

  def forward
    @message = @conversation.forward_message(params[:message_id])
    respond_to do |format|
      format.js
    end
  end

  def new_reply
    @message = Message.new
    @recipient = @conversation.originator
    respond_to do |format|
      format.js
    end
  end

  def create_reply
    @message = current_user.reply_to_conversation(@conversation,
                                                 params[:message][:body],
                                                 params[:message][:subject],
                                                 true,
                                                 true,
                                                 params[:message][:attachment]
                                                 ).message
    if @message
      respond_to do |format|
        format.js
      end
    else
      render :action => :new_reply
    end
  end

  def quick_reply
    @message = current_user.reply_to_conversation(@conversation,
                                                 params[:message][:body],
                                                 params[:message][:subject],
                                                 true,
                                                 true,
                                                 params[:message][:attachment]
                                                 ).message
    if @message
      respond_to do |format|
        format.js
      end
    else
      render :action => :show
    end
  end

  def archive
    current_user.mark_as_archived(@conversation)
    respond_to do |format|
      format.js
    end
  end

  def unarchive
    current_user.unarchive(@conversation)
    @conversations = regularize_conversations(Conversation.archived_for_user(current_user))
    respond_to do |format|
      format.js
    end
  end

  def trash
    @conversation.move_to_trash(current_user)
    respond_to do |format|
      format.js
    end
  end

  def untrash
    @conversation.untrash(current_user)
  end

  def autocomplete
    friends = current_user.my_friends.search(params[:term])
    render :json => friends.map{ |b| { :id => b.id, :value => b.fullname } }
  end

  def new
    @message = Message.new
    @recipient = User.find_by_id(params[:recipient_id])
  end

  def window
    @friend = User.find_by_id(params[:friend_id])
    render :layout => false
  end

  def archived
    @conversations = regularize_conversations(Conversation.archived_for_user(current_user))
    respond_to do |format|
      format.js
    end
  end

  def friend_preview_popup
    @friend = User.find(params[:friend_id])
    render :partial => 'layouts/my_marketplace/friend_preview'
  end

  private

  def find_conversation
    @conversation ||= current_user.conversations.find(params[:id])
  end

  def find_chat_conversation
    @friend = User.find(params[:friend_id])
    @conversation = Conversation.chat_conversations_between(@friend, current_user).first
  end

  def fetch_inbox_messages
    @conversations = current_user.conversations.page(params[:page]).per(Constants::Messages::CONVERSATIONS_PER_PAGE)
  end

  def regularize_conversations(conversations)
    cnvs = conversations.select { |conversation| !conversation.is_trashed?(current_user) } #remove trashed conversations
    cnvs = cnvs.select { |conversation| !current_user.is_archived?(conversation) } if params[:action] != 'archived' && params[:action] != 'unarchive'
    if params[:sort].present? && params[:sort] == 'from_old_to_new'
      cnvs = cnvs.sort_by(&:updated_at)
    end
    cnvs = cnvs.select { |conversation| conversation.originator.role.id.to_s == params[:role_id] } if params[:role_id].present?
    cnvs
  end
end
