class AddressesController < ApplicationController
  def show
    @address = Address.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render :json => @address }
    end
  end

  # GET /addresses/new
  # GET /addresses/new.json
  def new
    @address = current_user.user_profile.build_address

    respond_to do |format|
      format.html # new.html.erb
      format.json { render :json => @address }
      format.js
    end
  end

  # GET /addresses/1/edit
  def edit
    @address = Address.find(params[:id])
  end

  # POST /addresses
  # POST /addresses.json
  def create
    @address = current_user.user_profile.build_address(params[:address])
    @address.country_code = 'US'

    respond_to do |format|
      if @address.save
        current_user.user_profile.update_attribute(:address_id, @address.id)
        format.js
        format.html { redirect_to @address, :notice => I18n.t('address_was_successfully_created') }
        format.json { render :json => @address, :status => :created, :location => @address }
      end
    end
  end

  # PUT /addresses/1
  # PUT /addresses/1.json
  def update
    @address = Address.find(params[:id])

    respond_to do |format|
      if @address.update_attributes(params[:address])
        format.html { redirect_to @address, :notice => I18n.t('address_was_successfully_updated') }
        format.json { head :ok }
      end
    end
  end

  # DELETE /addresses/1
  # DELETE /addresses/1.json
  def destroy
    @address = Address.find(params[:id])
    @address.destroy

    respond_to do |format|
      format.html { redirect_to addresses_url }
      format.json { head :no_content }
    end
  end
end
