class AuthorizationsController < ApplicationController
  #before_filter :admin_only, only: [:index, :show]


  # GET /authorizations/new
  # GET /authorizations/new.json
  def new
    @social_network = SocialNetwork.find(params[:social_network_id]) # aka :provider since provider is the id.
    @authorization = Authorization.new
    @authorization.provider = @social_network
    respond_to do |format|
      format.html # new.html.erb
      format.json { render :json => @authorization }
    end
  end

  # GET /authorizations/1/edit
  def edit
    @authorization = Authorization.find(params[:id])
  end

  # POST /authorizations
  # POST /authorizations.json

  def create
    auth = request.env['omniauth.auth']
    Authorization.create_or_refresh_omniauth(auth, current_user)
    respond_to do |format|
      format.html { redirect_to edit_setting_url(current_user), :notice => 'Authorized to ' + auth[:provider]}
      format.json { head :no_content }
    end

  end


  # PUT /authorizations/1

  # PUT /authorizations/1.json
  def update
    @authorization = Authorization.find(params[:id])

    respond_to do |format|
      if @authorization.update_attributes(params[:authorization])
        format.html { redirect_to [current_user, @authorization], :notice => 'Authorization was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render :action => 'edit' }
        format.json { render :json => @authorization.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /authorizations/1
  # DELETE /authorizations/1.json
  def destroy
    @authorization = Authorization.find(params[:id])
    message="Disconnected from #{@authorization.social_network_name}"
    @authorization.destroy

    respond_to do |format|
      format.html { redirect_to edit_setting_url(current_user), :notice => message }
      format.json { head :no_content }
    end
  end
  # ADMIN - convenience routines
  # GET /authorizations
  # GET /authorizations.json
  def index

      @authorizations = Authorization.all

      respond_to do |format|
        format.html # index.html.erb
        format.json { render :json => @authorizations }
      end
  end

  # GET /authorizations/1
  # GET /authorizations/1.json
  def show
      @authorization = Authorization.find(params[:id])

      respond_to do |format|
        format.html # show.html.erb
        format.json { render :json => @authorization }
      end
    end
end

