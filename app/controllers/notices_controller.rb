class NoticesController < ApplicationController
  layout "discover", :only => :index

  def index
    @notices = current_user.notice_events.group_by(&:created_date)
  end

  def destroy
    @notice = TimelineEvent.find(params[:id])
    current_user.notice_events.delete(@notice) if @notice

    all_notices = current_user.reload.notice_events
    if all_notices.blank?
      @all_notices_size = 0
    else
      @all_notices_size = all_notices.count
      @notices = all_notices.group_by(&:created_date)
      @notice_events = current_user.notice_events.first(TimelineEvent::EVENTS_FOR_POPUP)
    end

    respond_to do |format|
      format.js
    end
  end

  def destroy_all
    current_user.notice_events.clear

    respond_to do |format|
      format.js
    end
  end

end
