class Connections::MyConnectionsController < SignupController
  layout "discover", :only => [:index]

  def index
    @friends = User.friends_of(current_user)
  end

  def all_connections
    @friends = User.friends_of(current_user)
    respond_to do |format|
      format.js
    end
  end

  def full_user_profile
    @friend = User.find(params[:friend_id])
    @suggested_users = User.similar_profiles_for(@friend)
    @mutual_connections = current_user.mutual_connections_for(@friend)
    respond_to do |format|
      format.js
    end
  end
end