class Connections::BlockedConnectionsController < SignupController
  before_filter :find_user, :only => [:block_user, :block_autocomplete_user]

  def index
    @blocked_users = current_user.blocked_users
    respond_to do |format|
      format.js
    end
  end

  def block_user
    current_user.add_to_blacklist(@blocked_user) if @blocked_user
    render :nothing => true
  end

  def block_autocomplete_user
    current_user.add_to_blacklist(@blocked_user) if @blocked_user
    respond_to do |format|
      format.js
    end
  end

  def unblock_user
    user = User.find(params[:user_id])
    current_user.black_list_users.find_by_blocked_user_id(user).delete
    render :nothing => true
  end

  private

  def find_user
    @blocked_user = User.find(params[:friend_id])
  end
end