class Connections::ManageConnectionsController < SignupController
  layout "discover", :only => [:index]

  def index
    @co_workers_connections = user_co_workers
    @beanstock_connections = linkedin_client.beanstock_connections_for(current_user)
    @linked_in_connections = linkedin_client.linkedin_connections
  end

  def create
    @friend = User.find(params[:id])
    @connection = current_user.connections.create(:friend => @friend)
    redirect_to manage_connections_path
  end

  def invite_users
    Resque.enqueue(LinkedInMess, current_user.id, linkedin_client.token, linkedin_client.secret, [params[:connections]])
    redirect_to manage_connections_path
  end

  def co_workers
    @co_workers_connections = user_co_workers
    @co_workers_connections = @co_workers_connections.paginate(:page => params[:page], :per_page => Connection::CONNECTIONS_PER_PAGE)
    respond_to do |format|
      format.js
    end
  end

  def beanstock_connections
    @beanstock_connections = linkedin_client.beanstock_connections_for(current_user).paginate(:page => params[:page], :per_page => Connection::CONNECTIONS_PER_PAGE)
    respond_to do |format|
      format.js
    end
  end

  def linkedin_connections
    @linked_in_connections = linkedin_client.linkedin_connections.paginate(:page => params[:page], :per_page => Connection::CONNECTIONS_PER_PAGE)
    respond_to do |format|
      format.js
    end
  end

  private

  def user_co_workers
    current_user.company ? User.co_workers(current_user) : []
  end

end