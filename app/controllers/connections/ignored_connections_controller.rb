class Connections::IgnoredConnectionsController < ApplicationController
  before_filter :find_connection, :only => [:create]

  def create
    if @connection
      @ignored_user = @connection.user
      current_user.add_to_ignore_list(@ignored_user)
      @connection.fire_status_event(params[:commit])
    end
    respond_to do |format|
      format.js
    end
  end

  private

  def find_connection
    @connection = Connection.find(params[:connection_id])
  end
end