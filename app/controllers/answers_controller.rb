class AnswersController < ApplicationController

  # POST /answers
  # POST /answers.json
  def create
    @answer = Answer.new(params[:answer])
    @answer.post = Post.find(params[:post_id])
    @answer.user = current_user
    
    respond_to do |format|
      if @answer.save
        format.html { redirect_to post_path(@answer.post.id), :notice => I18n.t('.answers.create.answer_was_successfully_created')}
        format.js
      else
        format.html { redirect_to post_path(@answer.post.id), :notice => I18n.t('.answers.create.answer_can_not_be_blank') }
        format.js
      end
    end
  end

  def destroy
    @answer = current_user.answers.find(params[:id])
    post_id = @answer.post_id
    @answer.destroy
    redirect_to post_url(post_id)
  end
end
