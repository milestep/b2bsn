class NetworksController < ApplicationController

  def autocomplete
    @networks = Network.order(:name).search(params[:term])
    render :json => @networks.map{ |n| { :id => n.id, :value => n.name } }
  end

end
