class JiraIssuesController < ApplicationController
  
  def new
    @issue = jira_issue.new
  end

  def create
    @issue = current_user.jira_issues.create(params[:jira_issue])
    Resque.enqueue(JiraIssues)
  end
  
private

  def jira_issue
    params[:type].constantize
  end

end
