class UserProfilesController < ApplicationController
  layout "activity_feed", :only => :show
  before_filter :find_profile
  before_filter :find_companies, :only => [:edit_base_info, :update_base_info]
  before_filter :load_activity_items, :only => :show

  def show
    @user_profile.viewed_by(current_user) unless @user_profile.user == current_user
    @my_friends = @user_profile.user.my_friends.paginate(:page => 1, :per_page => 20)
    @authorizations = Authorization.find(:all, :conditions => {:user_id => @user_profile.user})
    if @user_profile.user == current_user && session[:rtoken].present?
      client = LinkedIn::Client.new
      client.authorize_from_access(session[:rtoken], session[:rsecret])
      @linked_in_profile = client.profile(
                      :fields => %w(first_name last_name headline phone_numbers
                                    primary_twitter_account summary main_address location))
      if current_user.user_profile.description.blank? && @linked_in_profile.summary.present?
        current_user.user_profile.update_attribute(:description, @linked_in_profile.summary)
      end
    end

    @unearned_badges = Badge.where('name in (?)', ['Validated Seller','Publisher Companion'])
    @activity_badges = Badge.where('name in (?)', ['Social Butterfly','Party Animal','Featured Writer'])

    respond_to do |format|
      format.html
      format.json { render :json => @user_profile }
      format.js
    end
  end

  def popup_profile
    render :layout => false
  end

  def edit_base_info
    @user_profile.build_address if @user_profile.address.blank?
  end

  def update_base_info
    respond_to do |format|
      if @user_profile.update_attributes(params[:user_profile])
        format.js { render :action => "success" }
      else
        @user_profile.build_address if @user_profile.address.blank?
        format.js { render :action => "edit_base_info" }
      end
    end
  end

  def update_photo
    @user_profile.update_attributes(params[:user_profile])
    render :json => [:file_url => @user_profile.avatar_url]
  end

  private

  def find_companies
    @companies = Company.all.collect{|c| [c.name, c.id] }.sort
  end

  def load_activity_items
    @activity_items = TimelineEvent.by_actor(@user_profile.user).order('timeline_events.created_at desc')
    @user_friends = User.stream_friends_for(@activity_items) if @activity_items.present?
  end

  def find_profile
    @user_profile = UserProfile.includes(:user).find(params[:id])
  end
end
