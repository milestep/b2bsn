class SignupController < ApplicationController
  before_filter :permit_signup_access

  private

  def permit_signup_access
    unless session[:rtoken] || session[:rsecret]
      redirect_to login_path
    end
  end

  def linkedin_client
    @linkedin_client ||= ApiClient::Linkedin.new(current_user.user_profile, session[:rtoken], session[:rsecret])
  end
end
