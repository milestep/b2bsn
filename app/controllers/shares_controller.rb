# Share ActivityFeed items
class SharesController < ApplicationController

  def create
    @user = (current_user.admin?)? User.find(params[:user]) : current_user
    @timeline_event = TimelineEvent.find(params[:event])
    if params[:type] && params[:type] == "NewsItem"
      @share = NewsItem.find(params[:activity])
      @linkback = @share.url
    else
      @share = Post.find(params[:activity])
      @linkback = post_url(@share)
    end
    @next=params[:next]

    #find social network to inform
    providers = Authorization.default_providers(@user)
    #inform them
    message="Shared on "
    SocialNetwork.where(:provider => providers).map {|s| message=message + s.name + " "}
    if (@share.class == Post && SocialNetwork.socialize(@share, @user, providers,@linkback))
      @share.shares_count+=1
      @share.save
    end
    if @share.class == NewsItem
      SocialNetwork.socialize(@share, @user, providers,@linkback)
    end
    @user.share_event!(@timeline_event)
    respond_to do |format|
        format.html { redirect_to root_url, :notice => message}
        format.json { render :json => @share, :status => :created, :location => @share }
        format.js
    end
  end
end


