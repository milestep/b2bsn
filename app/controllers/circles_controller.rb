class CirclesController < ApplicationController
  # GET /circles/new
  # GET /circles/new.json
  def new
    @circle = Circle.new

    respond_to do |format|
      format.js
      format.html # new.html.erb
      format.json { render :json => @circle }
    end
  end

  # GET /circles/1/edit
  def edit
    @circle = Circle.find(params[:id])
  end

  # POST /circles
  # POST /circles.json
  def create
    @circle = current_user.circles.build(params[:circle])

    respond_to do |format|
      if @circle.save
        format.js
        format.html { redirect_to connections_path, :notice => I18n.t('your_new_group_was_successfully_created') }
        format.json { render :json => @circle, :status => :created, :location => @circle }
      else
        format.js { render :action => "new" }
        format.html { render :action => "new" }
        format.json { render :json => @circle.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /circles/1
  # PUT /circles/1.json
  def update
    @circle = current_user.circles.find(params[:id])

    respond_to do |format|
      if @circle.update_attributes(params[:circle])
        format.html { redirect_to @circle, :notice => I18n.t('circle_was_successfully_updated') }
        format.json { head :ok }
      else
        format.html { render :action => "edit" }
        format.json { render :json => @circle.errors.full_messages, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /circles/1
  # DELETE /circles/1.json
  def destroy
    @circle = current_user.circles.find(params[:id])
    @circle.destroy
    @circles = current_user.find_circles

    respond_to do |format|
      format.js
      format.html { redirect_to connections_path, :notice => I18n.t("group_was_successfully_deleted" )}
    end
  end
  
  def add_user
    @circle = current_user.circles.find(params[:id])
    @circle.add_user(params[:user_id])
    @expanded = params[:expanded]
  end
  
  def delete_user
    @circle = current_user.circles.find(params[:id])
    @circle.users.delete(User.find(params[:user_id]))
    @expanded = params[:expanded]
  end
  
  def confirm_delete
    @circle = current_user.circles.find(params[:id])
    respond_to do |format|
      format.js
    end
  end
  
  def show_expanded
    @circle = current_user.circles.find(params[:id])
    @circles = current_user.find_circles.where("id <> #{@circle.id}")
    respond_to do |format|
      format.js
    end
  end
  
  def collapse_expanded
    @circles = current_user.circles.all
    respond_to do |format|
      format.js
    end
  end
  
end
