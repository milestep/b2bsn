class ApplicationController < ActionController::Base
  include Notificators::MainmenuCounterns

  protect_from_forgery
  layout "application"

  before_filter :authenticate_user, :get_notifications
  before_filter :verify_registration_completeness
  before_filter :prepare_analytic_data
  before_filter :prepare_notices_data
  before_filter :load_activity_items
  before_filter :load_news
  before_filter :load_friends
  before_filter :memorize_user_activity
  before_filter :load_suggested_users
  before_filter :prepare_messages_data

  helper_method :current_user, :admin?, :user_roles

  # http_basic_authenticate_with :name => "beanstock", :password => "helix101"

  rescue_from CanCan::AccessDenied do |exception|
    redirect_to root_url, :alert => exception.message
  end

  def admin_only
    raise CanCan::AccessDenied.new("Cannot access admin area") unless current_user.admin?
  end

  def signed_in?
    !!current_user
  end

  def get_notifications
    # TODO: remove demo data
    @opportunities_count = session[:opportunities_count] || current_user && current_user.role?(Role.buyer) ? 12 : 9
    @opportunities_label = current_user && current_user.role?(Role.buyer) ? 'requests' : 'opportunities'
    @connections_count = current_user && current_user.role?(Role.seller) ? '3' : '1'
  end

  protected

  def authenticate_user
    unless current_user
      redirect_to login_path
    end
  end

  def verify_registration_completeness
    if current_user && current_user.email.blank?
      redirect_to edit_signup_user_path(:id => current_user.uid)
    end
  end

  private

  def user_roles
    @user_roles ||= Role.all
  end

  def current_user
    @current_user ||= User.find_by_id(session[:user_id]) if session[:user_id]
  end

  def prepare_analytic_data
    if current_user
      @new_users_count ||= current_user.new_users.newest.count
    end
  end

  def prepare_notices_data
    if current_user
      @all_notices_size ||= current_user.notice_events.count
      @notice_events ||= current_user.notice_events.first(TimelineEvent::EVENTS_FOR_POPUP)
    end
  end

  def load_activity_items
    if signed_in?
      @activity_items = current_user.timeline_events.includes(:subject)
      @user_friends = User.stream_friends_for(@activity_items) if @activity_items.present?
    end
  end

  def load_news
    @slider_news = NewsItem.newest.includes(:news_feed).limit(10)
  end

  def load_friends
    return unless signed_in?
    @my_friends = User.friends_of(current_user).
                      by_name(params[:search_name]).
                      order_by_name(params[:first_name_order], params[:last_name_order]).includes(:user_profile, :user_status)
    @my_requested_friends = current_user.my_requested_friends
  end

  def memorize_user_activity
    current_user.user_status.reset_status! if signed_in?
  end

  def load_suggested_users
    @suggested_users ||= User.similar_profiles_for(current_user) if signed_in?
  end

  def render_not_found
    raise ActionController::RoutingError.new('Not Found')
  end

  def prepare_messages_data
    if signed_in?
      @mainmenu_header_unread_messages = current_user.unread_messages(current_user.inbox)
      @messages_for_dropdown = current_user.inbox_messages.first(User::MESSAGES_FOR_DROPDOWN)
    end
  end
end
