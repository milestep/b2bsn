class CommentsController < ApplicationController
  before_filter :find_comment, :except => [:create]

  def create
    commentable_type = params[:commentable_type].classify
    commentable = commentable_type.constantize.find(params[:commentable_id])
    @comment = commentable.comments.build(params[:comment])
    @comment.user = current_user

    respond_to do |format|
      format.html {
        obj = I18n.t :object, :scope => [:comments, :create]
        result = @comment.save ? :success : :failure
        redirect_to commentable, :notice =>  I18n.t(result, :obj => obj)
      }
      format.js { @comment.save; @subject = @comment.commentable }
    end
  end

  def destroy
    @comment.destroy if current_user.can_manage_comment?(@comment, @post_owner)
    respond_to do |format|
      format.js { @subject = @comment.commentable }
      format.html { redirect_to comments_url }
    end
  end

  def update
    @comment.update_attributes(params[:comment]) if current_user.can_manage_comment?(@comment, @post_owner)
    respond_to do |format|
      format.json { head :ok }
    end

  end

  private

  def find_comment
    @comment = Comment.find(params[:id])
    @post_owner = User.find_by_id(params[:post_owner_id])
  end

end
