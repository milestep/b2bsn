class SuggestionsController < ApplicationController
  layout 'discover'

  def index
    @suggested_users = User.includes(:user_profile).similar_profiles_for(current_user)
    @data_for_autocomplete = @suggested_users.to_json(:only => :id, :methods => %w(fullname role_id))
    @roles = Role.all
  end

end
