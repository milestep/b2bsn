class CategoriesController < ApplicationController

  def autocomplete
    @categories = Category.order(:name).search(params[:term])
    render :json => @categories.map{ |c| { :id => c.id, :value => c.name } }
  end

end
