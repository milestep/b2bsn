class ConnectionsController < ApplicationController

  before_filter :find_connectable, :only => [:new]

  # GET /connections
  # GET /connections.json
  def index
    if params[:g].present?
      @circles = current_user.find_circles(params[:g])
      render :action => "groups"
      return
    end

    @user_type = "newest"
    @users = current_user.new_users.newest
    #TODO: to change it in the future.
    #role = Role.find_by_id(params[:role_id])
    role = Role.find_by_name(params[:t])
    @my_friends = params[:search].present? ? current_user.find_friends(params[:search]) : current_user.my_friends(role)
    @my_friends = @my_friends.paginate(:page => 1, :per_page => 20)
    @circles = current_user.find_circles
    @incoming_requests_count = current_user.incoming_requests.count
    @outgoing_requests_count = current_user.outgoing_requests.count

    respond_to do |format|
      format.html # index.html.erb
      format.js
    end
  end

  def search
    params_to_fetch = params.merge(:except => [current_user.id])
    @results = User.search_with_params(params_to_fetch)
    render :action => "add_contacts"
  end

  # GET /connections/1
  # GET /connections/1.json
  def show
    @connection = Connection.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render :json => @connection }
    end
  end

  # GET /connections/new
  # GET /connections/new.json
  def new
    @connection = current_user.connections.build(:friend => @connectable)

    respond_to do |format|
      if @connectable.has_connection_for?(current_user)
        flash[:notice] = I18n.t("connections.new.double_friendship", :user_name => @connectable.fullname)
        format.js { render :action => "show_notice"}
      else
        format.html # new.html.erb
        format.json { render :json => @connection }
        format.js
      end
    end
  end

  # GET /connections/1/edit
  def edit
    @connection = Connection.find(params[:id])
  end

  # POST /connections
  # POST /connections.json
  def create
    connection = current_user.connections.find_by_friend_id(params[:connection][:friend_id])
    if connection
      @connection = connection
      @connection.fire_status_event('Undo')
    else
      @connection = current_user.connections.build(params[:connection])
    end

    respond_to do |format|
      if @connection.save
        ConnectNotificationMailer.connect_request(params[:connection]).deliver unless connection
        format.js { flash[:notice] = I18n.t("connections.create.successfully") }
        format.html { redirect_to connections_path(:c => "outgoing"), :notice => I18n.t("connections.create.successfully") }
        format.json { render :json => @connection, :status => :created, :location => @connection }
      else
        format.js
        format.html { render :action => "new" }
        format.json { render :json => @connection.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /connections/1
  # PUT /connections/1.json
  def update
    @connection = Connection.find(params[:id])
    
    respond_to do |format|
      if @connection.fire_status_event(params[:commit])
        if params[:commit] == "Delete"
          @deleted_friend = @connection.user == current_user ? @connection.friend : @connection.user
          current_user.delete_user_from_circles(@deleted_friend)
        end
        format.js
        format.html { redirect_to connections_path(:c => 'incoming'), :notice => I18n.t("connections.update.successfully", :params => params[:commit]) }
        format.json { head :no_content }
      end
    end
  end

  def add_contacts
    @suggestions = current_user.suggested_friends(8)

    respond_to do |format|
      format.js
    end
  end

  protected

  def find_connectable
    connectable_id = params[:friend_id] || params[:connection].try(:[], :friend_id)
    @connectable = User.find_by_id connectable_id
    render :nothing => true unless @connectable
  end

end
