class NewsUploader < CarrierWave::Uploader::Base
  include CarrierWave::MiniMagick
  storage :file

  def store_dir
    "system/uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  def default_url
    model.news_feed.image_url
  end

  process :resize_to_fill => [560, 315]

  version :medium do
    process :resize_to_fill => [320, 180]
  end

  version :small do
    process :resize_to_fill => [215, 120]
  end

end
