class CompanyUploader < ImageUploader

  def default_url
    "/assets/icons/#{model.class.to_s.underscore}.png"
  end

end
