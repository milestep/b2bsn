class PackageUploader < ImageUploader

  def default_url
    "/assets/icons/package.png"
  end

end
