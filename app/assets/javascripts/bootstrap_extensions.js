/**
 * Hack for Bootstrap modal dialog
 * to hide html body scrollbars if any.
*/

$(function() {
  $(document).on('show', 'div.modal', function() { Body.hideOverflow(); })
  $(document).on('hidden', 'div.modal', function() { Body.visibleOverflow(); })
})

var Body = {

  hideOverflow: function(html) {
    $('body').css('overflow', 'hidden');
  },

  visibleOverflow: function(html) {
    $('body').css('overflow', 'visible');
    $('div.modal-backdrop').hide();
  }
}
