function updateActivityFeed(items) {
  var $container = $('#activity-container');

  $container.isotope('remove', $('.isotope-item'));

  var $newItems = items.hide();
  var layout = $('.isotope-option-set a.active').attr('data-option-value');

  if (layout == 'masonry') {
    elements = onlyActivityItemElements($newItems);
    changeSpaceBetweenItems(elements);
  }
  formatActivityFeed(layout);

  $container.imagesLoaded(function(){
    $container.isotope('insert', $newItems.show());
    formatActivityFeed(layout);
    reLayout($container);
    isotopeSideDefinition($container);
  })
  initializeUserProfilePopups();
}

function formatActivityFeed(layout) {
  if (layout == 'straightDown') {
    $('#activity-container').css('background','none');
    $('i.spinePointer').css('background','none');
    $('.activity-item').css('width','865px');
    $('.activity-item').css('margin', '0 0 5px');
    $('.smallVideo').hide();
    $('.largeVideo').show();
  }
  if (layout == 'masonry') {
    $('.left-side').css('margin','0 20px 35px 0');
    $('.right-side').css('margin','0 0 35px 24px');
    $('#activity-container').css('background','url(/assets/activity_feed/background-line.png) repeat-y 50% 0');
    $('i.spinePointer').css('background', 'url(/assets/activity_feed/background-dot.png) repeat-x');
    $('.activity-item').css('width','410px');
    $('.activity-date').css('width','865px');
    $('.activity-date').css('margin','-30px 0 5px');
    $('.first-right-item').css('margin-top', '49px');
    $('.smallVideo').show();
    $('.largeVideo').hide();
  }
}

function changeSpaceBetweenItems(items) {
  var all_items = items || $('#activity-container .activity-item');
  all_items.removeClass('first-right-item');

  $.each(all_items, function(index, value) {
    var curr_element = $(value);
    //change margin for second element from AS
    if (index == 1 && !curr_element.hasClass('activity-date')) {
      curr_element.addClass('first-right-item');
    }
    //change margin for each second element from AS after date divider
    if (curr_element.hasClass('activity-date') && (index + 2) < all_items.size()) {
      var second_element = $(all_items[index+2]);
      if (!second_element.hasClass('activity-date')) {
        second_element.addClass('first-right-item');
      }
    }
  });
}

function isotopeSideDefinition(container, params) {
  params = typeof params !== 'undefined' ? params : {
    colWidth : 435
  };
  container.find('.activity-item').each(function(){
    var position = $(this).data('isotope-item-position');
    var dotted_class = 'left-side'
    if (position.x == params.colWidth) dotted_class = 'right-side';
    $(this).removeClass('left-side right-side').addClass(dotted_class);
  });
}

function onlyActivityItemElements(items) {
  return items.filter(function () {
    if ($(this).hasClass('activity-item')) return $(this)
  });
}

function isotopeLoaded(container, params) {
  params = typeof params !== 'undefined' ? params : {
    colWidth : 435
  };
  container.imagesLoaded(function(){
    container.isotope({
      itemSelector : '.activity-item',
      itemPositionDataEnabled: true,
      masonry : {
        columnWidth : params.colWidth
      }
    });
    isotopeSideDefinition(container);
  });
}

function isotopeInitScorll(container, params) {
  params = typeof params !== 'undefined' ? params : {
    colWidth : 435
  };
  container.infinitescroll({
    navSelector  : '#page_nav',       // selector for the paged navigation
    nextSelector : '#page_nav a',     // selector for the NEXT link (to page 2)
    itemSelector : '.activity-item',  // selector for all items you'll retrieve
    loading: {
      finishedMsg: 'No more Items to load',
      img: '/assets/spinner.gif'
    }
  },
  function( newElements ) {
    var $newElems = $(newElements).hide();
    $newElems.imagesLoaded(function(){
      $newElems.fadeIn();

      var layout = $('.isotope-option-set a.active').attr('data-option-value');
      if (layout == 'masonry') {
        changeSpaceBetweenItems();
      }

      container.isotope('appended', $newElems);
      initializeUserProfilePopups();
      isotopeSideDefinition(container);

      if (layout == 'straightDown') {
        formatActivityFeed(layout);
        reLayout(container);
      }
    });
  }
  );
}

function toggleComments(container) {
  if (container) {
    container.find('.hidden_comments').toggleClass('hidden');
    container.find('.view-all').toggleClass('hidden');
    container.find('.view-first').toggleClass('hidden');
    reLayout(container);
  }
}

function toggleInlineComment(container) {
  if (container) {
    container.find(".inline_comment_container").toggle();
    reLayout(container);
  }
}

function addToggleCommentsObserver() {
  $('body').on('click', 'a.view-all, a.view-first', function(event){
    var item = $(this).parents('.isotope-item');
    toggleComments(item);

    return false;
  });
}

function addToggleInlineCommentObserver() {
  $('body').on('click', 'a.show_inline_comment', function(event){
    var item = $(this).parents('.isotope-item');
    toggleInlineComment(item);

    return false;
  });
}

function reLayout(element) {
  if (element) {
    if (element.hasClass('isotope')) {
      element.isotope('reLayout');
    }
    else {
      element.parents('.isotope').isotope('reLayout');
      isotopeLoaded($('#activity-container'));
    }
  }
}

$(function(){
  var $container = $('#activity-container');

  isotopeLoaded($container);
  changeSpaceBetweenItems();

  isotopeInitScorll($container);

  var $optionSets = $('.isotope-option-set'),
  $optionLinks = $optionSets.find('a');

  $optionLinks.click(function(){
    var $this = $(this);
    if (!$this.hasClass('active')) {
      $optionLinks.removeClass('active');
      $this.addClass('active');
      var $optionSet = $this.parents('.isotope-option-set');
      var options = {}, key = $optionSet.attr('data-option-key'), value = $this.attr('data-option-value');
      value = value === 'false' ? false : value;
      options[ key ] = value;
      if (key == 'sortBy') {
        options['sortAscending'] = false
      }
      if (key == 'layoutMode' && value =='masonry') {
        changeSpaceBetweenItems();
      }
      if (key == 'layoutMode') {
        formatActivityFeed(value);
      }
      $container.isotope(options,
        function() {
          if (key == 'layoutMode' && value =='masonry') {
            isotopeSideDefinition($container);
            formatActivityFeed(value);
          }
        });
    }
    return false;
  });

  $("a.trackable").live("click", function() {
    params = {
      click: {
        url: this.href,
        clickable_type: $(this).attr('data-click-type'),
        clickable_id: $(this).attr('data-click-id')
      }
    };
    $.post("/clicks", params);
    return true;
  });

  $("#as-this-year").live("click", function() {
    $("#as-month-filter").toggle();
    return false;
  });

  addToggleCommentsObserver();
  addToggleInlineCommentObserver();

});

ActivityFeed = function() {}
ActivityFeed.attach_hide_item_popup = function(id, content) {
  var qtip_position = $('[data-qtip-position-hor]').data('qtip-position-hor') || 'right';
  var adjust_x_direction = (qtip_position == 'right') ? -6 : 6;
  $("#close_button_" + id).qtip({
    content: content,
    position: {
      my: 'middle left',
      at: 'middle right',
      viewport: $(window),
      adjust: {
        x: adjust_x_direction
      }
    },
    style: {
      classes: 'ui-tooltip-bootstrap'
    },
    hide: {
      event: 'mouseout',
      fixed: true
    },
    show: {
      event: 'click'
    },
    events: {
      show: function() {
        $("#timeline_event_" + id + " td").addClass("active-field");
        $("#close_button_" + id).css("visibility","visible");
        $("#timeline_event_" + id).qtip("api").elements.tooltip.hide();
      },
      hide: function() {
        $("#timeline_event_" + id + " td").removeClass("active-field");
        $("#close_button_" + id).removeAttr("style");
      }
    }
  });
}

ActivityFeed.attach_details_popup = function(id, url) {
  var qtip_position = $('[data-qtip-position-hor]').data('qtip-position-hor') || 'right';
  var adjust_x_direction = (qtip_position == 'right') ? -1 : 1;
  var sort_out_position = {
    left: {
      my: 'middle right',
      at: 'middle left'
    },
    right: {
      my: 'middle left',
      at: 'middle right'
    }
  };
  var $target = $("#timeline_event_" + id);
  $target.qtip({
    content: {
      ajax: {
        url: url,
        type: 'get'
      },
      text: "<div class='loading'>Loading...</div>"
    },
    position: {
      effect: false,
      viewport: $(window),
      adjust: {
        x: 14 * adjust_x_direction
      }
    },
    style: {
      classes: 'ui-tooltip-bootstrap no-padding min-width-250'
    },
    hide: {
      when: 'mouseout',
      fixed: true
    },
    events: {
      show: function() {
        $(".table-striped td").removeClass("active-field");
        var $tooltip = $("#timeline_event_" + id).qtip("api").elements.tooltip;
        $tooltip.mouseover(function() {
          $("#timeline_event_" + id + " td").addClass("active-field");
        });
      },
      hide: function() {
        var $textarea_focus = $(this).find('textarea.write_remote_comment').is(':focus');
        if (!$textarea_focus) {
          $("#timeline_event_" + id + " td").removeClass("active-field");
        }
        return !$textarea_focus;
      },
      hidden: function() {
        $("#timeline_event_" + id + " td").removeClass("active-field");
      }
    }
  });
  // update qtip relative position to it's target position
  $target.qtip('api').set({
    'position.my': sort_out_position[qtip_position].my,
    'position.at': sort_out_position[qtip_position].at
  });
}

ActivityFeed.renderItems = function(data) {
  $.each(data, function(index, item) {
    ActivityFeed.renderItem(item);
  });
  $(".timeago").timeago();
}

ActivityFeed.loadData = function(url, profile_page, id) {
  $.ajax({
    url: url,
    data: {
      profile_page: profile_page,
      id: id
    },
    type: 'get',
    success: function(data) {
      ActivityFeed.renderItems(data);
      $("#items_loader").hide();
    }
  });

}

$(document).on('focus', 'textarea.write_remote_comment', function(event) {
  var otherPopups = $('textarea.write_remote_comment:visible').not(this).closest('div.popup_content');
  $.each(otherPopups, function() {
    var timelineId = $(this).data('event').match(/event_popup_(\d.*)/)[1];
    $("#timeline_event_" + timelineId).qtip('api').hide();
  });
});

ActivityFeed.renderItem = function(item) {
    $("#market-stream .accordion-inner .table tbody").prepend(ich["activity_item_template"](item));
    ActivityFeed.attach_hide_item_popup(item.id, ich["hide_promt_template"](item));
    ActivityFeed.attach_details_popup(item.id, item.event_path);
    if (item["status_update?"] || item["question_asked?"]) {
      $("#timeline_event_" + item.id + " .truncated-text").dotdotdot({
        after: "<a href='" + item.posting_path + "'>see more</a>",
        watch: "window"
      });
    }
}