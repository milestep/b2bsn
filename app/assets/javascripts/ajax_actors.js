AjaxActors = function(){};

AjaxActors.show_inline_loader_by_id = function(actor) {
  $(actor).parent().find('span.inline_ajax_loader').show();
}

AjaxActors.hide_inline_loader_by_id = function(id) {
  $("span#"+id).hide();
}

AjaxActors.init_activity_listeners = function(){
  $('html').on('click', 'div.activity-item a.ajax', function() {
    AjaxActors.show_inline_loader_by_id(this);
  })
}

AjaxActors.init_activity_listeners();
