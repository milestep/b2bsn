// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//
//= require plugins
//= require jquery
//= require jquery_ujs
//= require jquery.purr
//= require best_in_place
//= require jquery-ui
//= require jquery.isotope.min
//= require jquery.social.stream.1.3.min
//= require jquery.qtip.min
//= require jquery.easing.1.3
//= require jquery.bxSlider
//= require jquery.infinitescroll.min
//= require jquery.nicescroll
//= require jquery.fileupload
//= require jquery.iframe-transport
//= require jquery.tmpl
//= require jquery.form
//= require underscore
//= require jquery.mentionsInput
//= require bootstrap
//= require bootstrap_init
//= require bootstrap-datepicker
//= require iconhaz.min
//= require jquery.chromatable
//= require jquery.dotdotdot-1.5.1
//= require jquery.fileupload
//= require jquery.form
//= require jquery.iframe-transport
//= require jquery.masonry.min
//= require jquery.nicescroll
//= require jquery.tablesorter.min
//= require jquery.timeago
//= require jquery.tmpl
//= require_directory .

Application = function() {}

$(function(){
  initializeUserProfilePopups();

  $('div.btn-group .btn').not('.users-menu-element, .filter-button').click(function(){
    var $btn = $(this);
    if ($btn.data("toggle") != "dropdown") {
      var $group = $btn.parents('.btn-group');
      $group.find('.btn').removeClass('active');
      $btn.addClass('active');
      if ($btn.data("remote-url")) {
        $.get($btn.data("remote-url"));
      }
    }
  });

  $('body').on('click', 'a', function(){
    var $link = $(this);
    if ($link.data("confirm-message")) {
      show_confirmation($link);
      return false;
    }
  });

  initializeBestInPlace();

  $('body').on('click', 'div.notice a.close', function(){
    $(this).parent().remove();
  });

  $.each(["addClass","removeClass"],function(i,methodname){
    var oldmethod = $.fn[methodname];
    $.fn[methodname] = function(){
      result = oldmethod.apply( this, arguments );
      this.trigger(methodname+"Change");
      return result;
    }
  });

  loadNiceScrollForDropdowns();

  $('body').on('ajax:aborted:file', 'form', function(){
    $(this).ajaxSubmit();
    return false;
  });
});

function closeModal() {
  $("#bootstrap_modal").modal('hide');
}

function initializeBestInPlace() {
  var best_in_place = $('.best_in_place');
  var isotope_container = $('.isotope');
  best_in_place.best_in_place();
  best_in_place.bind("best_in_place:activate", function(){ reLayout(isotope_container) });
  best_in_place.bind("best_in_place:abort", function(){ reLayout(isotope_container) });
  best_in_place.bind("best_in_place:update", function(){ reLayout(isotope_container) });
  best_in_place.bind("ajax:success", function(){
    best_in_place.data('originalContent') = best_in_place.html();
    var view_more = best_in_place.parent().find('.view-more');
    if (best_in_place.html().length > best_in_place.data('displayedLength')) { view_more.show(); }
    else { view_more.hide(); }
  });
}

$.ui.autocomplete.prototype._renderItem = function (ul, item) {
  var highlighted = item.label.split(this.term).join('<strong>' + this.term +  '</strong>');
  return $("<li></li>")
    .data("item.autocomplete", item)
    .append("<a>" + highlighted + "</a>")
    .appendTo(ul);
};

$(function(){
  Search.navigation_autocomplete_listener($('form#inline-navigation-search input#search'));
});

function show_confirmation($link){
  var data = [{url: $link.attr("href")}]
  var $modal = $("#confirmation_modal").tmpl(data);
  $modal.find('div.modal-body').html($($link.data('confirm-message')));
  $modal.appendTo("body");
  $modal.modal('show');
}

function loadNiceScrollForDropdowns(){
  var $btn_groups = $(".btn-group")
  $btn_groups.on("addClassChange", function(){
    var $btn_group = $(this);
    if ($btn_group.hasClass('open')) {
      $btn_group.find('.dropdown-menu').niceScroll({
        cursoropacitymax: 0.5,
        zindex: 1050
      });
    }
  })
  $btn_groups.on("removeClassChange", function(){
    var $btn_group = $(this);
    if (!$btn_group.hasClass('open')) {
      $btn_group.find('.dropdown-menu').getNiceScroll().remove();
    }
  })
}

Application.initializeCompanyDropdown = function() {
  loadNiceScrollForDropdowns();
  $("ul.dropdown-menu li a").click(function(){
    var $link = $(this);
    $btn_group = $link.parents('div.btn-group')
    $btn_group.find("div.current-value").html($link.html());
    $btn_group.find("input[type=hidden]").val($link.data("value"));
    $btn_group.removeClass("open");
    return false;
  });
}

Application.loadDatePickers = function(){
  $(".date-picker").datepicker();
}
