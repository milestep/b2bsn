/*
 * relplaces @somefriend with that user's full name
 */
Mentions = function() {}
Mentions.listUsers = function() {
  $('input#auto-title').mentionsInput({
    onDataRequest:function (mode, query, callback) {
      data = $('#auto-title').data('names');
      data = _.filter(data, function(item) {
        return item.uname.toLowerCase().indexOf(query.toLowerCase()) > -1
      });
      for(i = 0; i < data.length; i++) {
        data[i].name = data[i].uname
      }
      callback.call(this, data);
    },
    minChars : 2,
    showAvatars: false,
    templates     : {
      wrapper                    : _.template('<div class="mentions-input-box"></div>'),
      autocompleteList           : _.template('<div class="ui-autocomplete ui-menu ui-widget ui-widget-content ui-corner-all"  role="listbox" aria-activedescendant="ui-active-menuitem"></div>'),
      autocompleteListItem       : _.template('<li  class="ui-menu-item" role="menuitem" data-ref-id="<%= id %>" data-ref-type="user" data-display="<%= display %>"><%= content %></li>'),
      autocompleteListItemAvatar : _.template('<img src="<%= avatar %>" />'),
      autocompleteListItemIcon   : _.template('<div class="icon <%= icon %>"></div>'),
      mentionsOverlay            : _.template('<div class="mentions" style="display:none;"><div></div></div>'),
      mentionItemSyntax          : _.template('@[<%= value %>](user:<%= id %>)'),
      mentionItemHighlight       : _.template('<strong><span><%= value %></span></strong>'),
      enhancedContent            : _.template('<strong><%= name %></strong>')
    }
  });

}

Mentions.submitPostForm = function() {
  $('input#auto-title').mentionsInput('getMentions', function(data) {
    var mentioned_users = $('#post_mentioned_users_ids');
    var users_ids = $.map(data, function(_, i) { return data[i].id });
    $('#mentioned_users_ids').val(users_ids.join(','))
  });
}
