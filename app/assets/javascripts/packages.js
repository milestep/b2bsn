$(function(){

  var $package_container = $('#package_items');

  $package_container.imagesLoaded(function() {
    $package_container.isotope({
      itemSelector : '.package_item',
      filter: '*',
      masonry: { columnWidth: 130 }
    });
  });

  // filter items when filter link is clicked
  $('.package-filters button').click(function() {
    var selector = $(this).attr('data-filter');
    $package_container.isotope({
      filter: selector
    });
    return false;
  });

});
