Post = function() {};
Post.load_status_update_bar = function(path) {
  $.ajax({
    url: path,
    success: function(data) {
      Mentions.listUsers();
    }
  });
}

$(function() {
  $(document).on('click', '#post_visibility_input a', function(e) {
    $('input#post_visibility').val($(this).data('value'));
    var btn = $('#post_visibility_input button div');
    btn.text($(this).text());
    btn.attr('class', $(this).attr('class'));
    e.preventDefault();
  })

  $(document).on('click', '#update-post-status', function() {
    if ($('#auto-title').val()) {
      $("#update_module_loader").addClass('loader');
    } else {
      $(".alert").remove();
      return false;
    }
  });
})
