$(function() {
  $(document).on('click', '.friend_preview .chat_history', function() { Chat.history(this); });
  $(document).on('click', '.friend_chat .close-chat', function() { Chat.close(this); });
  $(document).on('click', '.friend_chat .to-window', function() { Chat.toWindow(this); });
  $(document).on('click', '.friend_chat .to-normal', function() { Chat.popIn(this); });
  $(document).on('click', '.friend_chat .minimize-chat', function() { Chat.minimize(this); });
  $(document).on('click', '.friend_chat .maximize-chat', function() {
    var friend_id = $(this).parents('.friend_chat').data('friend-id');
    Chat.maximize(friend_id);
  });
  $(document).on('click', '#my_marketplace .friend_message .message-icon, #my_marketplace .friend_message .message-icon-new', function() { Chat.openUsingEnvelope(this); });
  $(document).on('keypress', '.friend_chat #chat_message', function(e) { Chat.sendMessageFromChat(this, e); });
  $(document).on('keypress', '.friend_preview #message', function(e) { Chat.sendMessageFromPreview(this, e); });
});

var Chat = {
  count: 0,
  array: [],
  window_width: 285,
  max_chats_number: Math.floor($(window).width()/285),

  scroll_to_bottom: function(friend_id) {
    $("#friend_chat_" + friend_id + " .conversations_part").prop({ scrollTop: $("#friend_chat_" + friend_id + " .conversations_part").prop("scrollHeight") });
  },

  on_message: function(user_id, message_type) {
    var friend_row =  $("#friend_row_" + user_id);
    if ((!$("#friend_chat_" + user_id).is(":visible")) && ($("#friend_chat_" + user_id).length) && (message_type != "self")) {
      if ($("#select-user-status .current-value div:first").hasClass("do-not-disturb-status")) {
        friend_row.addClass("unread");
        friend_row.find('.message-icon').removeClass('message-icon').addClass('message-icon-new');
        var unread_messages_number = friend_row.find('.badge').data('badge') + 1;
        friend_row.find('.badge').data('badge', unread_messages_number);
        friend_row.find('.badge').html(unread_messages_number).show();
      }
      else {
        friend_row.removeClass('unread');
        friend_row.find('.message-icon-new').removeClass('message-icon-new').addClass('message-icon');
        friend_row.find('.badge').html("0").hide();
        friend_row.find('.badge').data("badge", 0);
        if (Chat.max_chats_number == Chat.count) { Chat.close_extra_chat(); }
        $('#friend_chat_' + user_id).css({ right: (Chat.window_width+1)*Chat.count }).show();
        Chat.array[Chat.count] = user_id;
        Chat.count++;
      }
    }
    else {
      if ((message_type == 'friend') && ($('#friend_chat_' + user_id + ' .operate_part div:last').hasClass('maximize-chat'))) {
        $('#friend_chat_' + user_id + ' .operate_part').css('background', '#9FADBF');
        var unread = $('#friend_chat_' + user_id + ' .unread-msg').data('unread') + 1;
        $('#friend_chat_' + user_id + ' .unread-msg').data('unread', unread);
        $('#friend_chat_' + user_id + ' .unread-msg').html(unread).show();
      }
      friend_row.removeClass("unread");
      friend_row.find('.message-icon-new').removeClass('message-icon-new').addClass('message-icon');
      friend_row.find('.badge').html("0").hide();
      friend_row.find('.badge').data("badge", 0);
    }
  },

  save_to_messages: function(friend_id, message, message_url) {
    $.ajax({
      url: message_url,
      type: 'get',
      data: {
        message: message,
        friend_id: friend_id
      }
    });
  },

  close_extra_chat: function() {
    Chat.count--;
    $('#friend_chat_' + Chat.array[0]).hide();
    Chat.removeHeaderHighlight(Chat.array[0]);
    $('#friend_chat_' + Chat.array[0] + ' > div').show();
    $('#friend_chat_' + Chat.array[0] + ' .operate_part .friend_name').hide();
    var i = 0;
    while (i < Chat.count) {
      Chat.array[i] = Chat.array[i+1];
      $('#friend_chat_' + Chat.array[i] ).css({ right: (Chat.window_width+1)*i });
      i++;
    }
  },

  removeHeaderHighlight: function(friend_id) {
    $('#friend_chat_' + friend_id + ' .operate_part').css('background-color', '#222');
    $('#friend_chat_' + friend_id + ' .unread-msg').data('unread', 0);
    $('#friend_chat_' + friend_id + ' .unread-msg').html("0").hide();
  },

  close: function(chat) {
    Chat.count--;
    var friend_conversation = $(chat).parents('.friend_chat');
    friend_conversation.hide();
    var friend_id = friend_conversation.data('friend-id');
    $('#friend_chat_' + friend_id + ' > div').show();
    $('#friend_chat_' + friend_id + ' .operate_part .friend_name').hide();
    $('#friend_chat_' + friend_id + ' .maximize-chat').removeClass('maximize-chat').addClass('minimize-chat');
    Chat.removeHeaderHighlight(friend_id);
    var i = 0;
    while (Chat.array[i] != friend_id) {
      i++;
    }
    while (i < Chat.count) {
      Chat.array[i] = Chat.array[i+1];
      $('#friend_chat_' + Chat.array[i]).css({ right: (Chat.window_width+1)*i });
      i++;
    }
  },

  maximize: function(friend_id) {
    $('#friend_chat_' + friend_id).removeClass('maximize-chat').addClass('minimize-chat');
    Chat.removeHeaderHighlight(friend_id);
    $('#friend_chat_' + friend_id + ' > div').show();
    $('#friend_chat_' + friend_id + ' .operate_part .friend_name').hide();
    $('#friend_chat_' + friend_id + ' .maximize-chat').removeClass('maximize-chat').addClass('minimize-chat');
  },

  minimize: function(element) {
    var friend_id = $(element).parents(".friend_chat").data('friend-id');
    $(element).removeClass('minimize-chat').addClass('maximize-chat');
    $('#friend_chat_' + friend_id + ' .header_part').hide();
    $('#friend_chat_' + friend_id + ' .conversations_part').hide();
    $('#friend_chat_' + friend_id + ' .messages_part').hide();
    $('#friend_chat_' + friend_id + ' .operate_part .friend_name').show();
  },

  toWindow: function(element) {
    Chat.close(element);
    var friend_id = $(element).parents(".friend_chat").data('friend-id');
    var generator= window.open('/messages/window?friend_id=' + friend_id, 'chat' + friend_id, 'height=288, width=285');
    generator.focus();
    return false;
  },

  history: function(element) {
    var friend_id = $(element).parents('.friend_preview').data('friend-id');
    $('#friend_row_' + friend_id + ' .message-icon').click();
  },

  openUsingEnvelope: function(element) {
    var friend_id = $(element).parents('.friend_row').data('friend-id');
    if (!$('#friend_chat_' + friend_id).is(':visible')) {
      if (Chat.max_chats_number == Chat.count) {
        Chat.close_extra_chat();
      }
      $('#friend_row_' + friend_id).removeClass('unread');
      $('#friend_row_' + friend_id + ' .message-icon-new').removeClass('message-icon-new').addClass('message-icon');
      $('#friend_chat_' + friend_id).css({ right: (Chat.window_width+1)*Chat.count }).show();
      $('#friend_row_' + friend_id + ' .badge').html("0").hide();
      $('#friend_row_' + friend_id + ' .badge').data("badge", 0);
      Chat.array[Chat.count] = friend_id;
      Chat.count++;
    }
    else {
      Chat.maximize(friend_id);
    }
    Chat.scroll_to_bottom(friend_id);
  },

  popIn: function(element) {
    var friend_id = $(element).parents(".friend_chat").data('friend-id');
    window.opener.Chat.openUsingEnvelope(window.opener.$("#friend_row_" + friend_id + " .message-icon"));
    self.close();
  },

  sendMessageFromChat: function(element, e) {
    var code = (e.keyCode ? e.keyCode : e.which);
    if (code == 13) {
      var message = $(element).val();
      if (message != '') {
        var friend_id = $(element).parents('.friend_chat').data('friend-id');
        var message_url = $(element).parents('.friend_chat').data('message-url');
        Chat.save_to_messages(friend_id, message, message_url);
        $(element).val('');
      }
      e.preventDefault();
    }
  },

  sendMessageFromPreview: function(element, e) {
    var code = (e.keyCode ? e.keyCode : e.which);
    var message = $(element).val();
    if ((code == 13) && (message != '')) {
      if (Chat.max_chats_number == Chat.count) {
        Chat.close_extra_chat();
      }
      $('#my_marketplace .friend_logo, .friend_preview').qtip('toggle', false);
      $(element).val('');
      var friend_id = $(element).parents('.friend_preview').data('friend-id');
      var message_url = $(element).parents('.friend_preview').data('message-url');
      Chat.save_to_messages(friend_id, message, message_url);
      $('#friend_row_' + friend_id).removeClass('unread');
      $('#friend_row_' + friend_id + ' .message-icon-new').removeClass('message-icon-new').addClass('message-icon');
      if (!$('#friend_chat_' + friend_id).is(":visible")) {
        $('#friend_chat_' + friend_id).css({ right: (Chat.window_width+1)*Chat.count }).show();
        Chat.array[Chat.count] = friend_id;
        Chat.count++;
      }
      else {
        Chat.maximize(friend_id);
      }
      Chat.scroll_to_bottom(friend_id);
      $('#friend_row_' + friend_id + ' .badge').html("0").hide();
      $('#friend_row_' + friend_id + ' .badge').data("badge", 0);
      e.preventDefault();
    }
  }, 
  
  attachScroll: function() {
    $("#my_marketplace .nav-list").niceScroll({
      cursoropacitymax: 0.5,
      zindex: 500
    });
  }
};
