Notices = function() {}

Notices.list = function() { return $('.notifications_list'); }
Notices.dropdown = function() { return $('.dropdown-menu.notifications'); }
Notices.badge = function() { return $('a[data-toggle=dropdown]#notification-dropdown span.counter'); }
Notices.dropdown_href = function() { return $('a[data-toggle=dropdown]#notification-dropdown'); }
Notices.hide_popups = function() { return $('div.hide_notification_popup'); }

Notices.clear = function() {
  this.list().find('tr').remove();
  this.list().getNiceScroll().hide();
  this.dropdown().find('li.message_item').remove();
  this.badge().remove();
}

Notices.addObservers = function(){
  //Notices.addDropdownObservers();
  Notices.addListObservers();
}

Notices.addDropdownObservers = function() {
  var hide_popup_selector = this.hide_popups().selector;
  this.dropdown_href().on('click', function(event) {
    $(event.target).closest('li.dropdown').toggleClass('open');
    return false;
  });

  $('body').on('click', this.dropdown().find('.destroy i').selector, function(event){
    var element = $(event.target);
    var popup = element.closest('.message_actions').find(hide_popup_selector);
    Notices.dropdown().find(hide_popup_selector).hide();
    popup.css({'top': element.position().top}).show();
    return false;
  });
}

Notices.addListObservers = function() {
  var hide_popup_selector = this.hide_popups().selector;
  this.list().niceScroll({
    cursoropacitymax: 0.5,
    zindex: 500
  });

  $('body').on('click', this.list().find('.close_button i').selector, function(event){
    var element = $(event.target);
    var popup = element.closest('.close_button').find(hide_popup_selector);
    Notices.list().find(hide_popup_selector).hide();
    popup.css({'top': element.position().top}).show();
    return false;
  });
}

Notices.update_list = function(html) {
  this.list().html(html);
}

Notices.update_dropdown = function(html) {
  this.dropdown().replaceWith(html)
}

Notices.update_badge = function(html) {
  this.badge().html(html);
}

Notices.attachHidePopup = function(id, content) {
  $("i[data-close-id=" + id + "]").qtip({
    content: content,
    position: {
      my: 'middle left',
      at: 'middle right',
      viewport: $(window),
      adjust: {
        x: -4
      }
    },
    style: {
      classes: 'ui-tooltip-bootstrap'
    },
    hide: {
      when: 'mouseout',
      fixed: true
    },
    show: {
      event: 'click'
    }
  });
}

Notices.attachQtipFor = function(target, url) {
  target.qtip({
    content: {
      ajax: {url: url, type: 'get'},
      text: "<div class='loading'>Loading...</div>"
    },
    position: {
      my: 'middle right',
      at: 'middle left',
      viewport: $(window),
      effect: false,
      adjust: {
        x: 10,
        y: 0
      }
    },
    style: {
      classes: 'ui-tooltip-bootstrap no-padding'
    },
    hide: {
      when: 'mouseout',
      fixed: true
    },
    events: {
      hide: function () {
        var $tooltip = target.qtip("api").elements.tooltip;
        return !$tooltip.find('textarea.write_remote_comment').is(':focus');
      },
      focus: function(event, api) {
        api.elements.target.closest('.dropdown-menu').addClass('stay-visible');
      },
      blur: function(event, api) {
        var $ddmenu = api.elements.target.closest('.dropdown-menu');
        $ddmenu.removeClass('stay-visible');
        if (!$ddmenu.is(':hover')) { $ddmenu.hide(); }

      }
    }
  });
}

Notices.attachDetailsPopup = function(id, url) {
  var $dropdown_target = $("[data-id=notification_popup_" + id + "]");
  var $list_target = $("[data-id=notification_" + id + "]");
  if(!$dropdown_target.qtip("api")) { Notices.attachQtipFor($dropdown_target, url); }
  if(!$list_target.qtip("api")) { Notices.attachQtipFor($list_target, url); }
}

$(document).ready(function() {
  $('#notification-dropdown').closest('.dropdown').hover(
    function() { $(this).find('.dropdown-menu').show(); },
    function() {
      if ($(this).find('.stay-visible').length) return;
      $(this).find('.dropdown-menu').hide();
    }
  );
});

