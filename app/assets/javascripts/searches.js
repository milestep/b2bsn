Search = function() {}

Search.prepare_params = function(key) {
  $('form#inline-search select#filter').val(key.capitalize());
  $('form#inline-search button.dropdown-toggle-search div.current-value').html(key.capitalize());
  $('form#inline-search input#search').focus();
}

Search.submit_form = function(event) {
  if (event.keyCode == 13) {
    $('#inline-search').submit();
    $('form#inline-search ul.dropdown-menu').hide();
  }
  return false
}

Search.submit_navigation_form = function(event) {
  if (event.keyCode == 13) {
    if ($('form#inline-navigation-search input#search').val().length < 1) { 
      $("div.navigation-autocomplete-results#error").show();
      event.preventDefault();
    } else { 
      $('#inline-navigation-search').submit(); 
    }
  } else { 
    $("div.navigation-autocomplete-results#error").hide(); 
  }
  return false;
};

Search.submit_suggestion = function(text) {
  $('form#inline-navigation-search input#search').val(text).focus();
  $('#inline-navigation-search').submit();
}

Search.initialise_search_form = function() {
  try {
    $('form#inline-navigation-search button.dropdown-toggle-search div.current-value').html($('form#inline-navigation-search select#filter').val().capitalize());
    $('form#inline-search button.dropdown-toggle-search div.current-value').html($('form#inline-search select#filter').val().capitalize());
  }
  catch(err) {}
}

Search.navigation_autocomplete_listener = function($input) {
  $input.autocomplete( {
    minLength: 3,
    source: function(request, response) {
      $.ajax({
        url: $input.data('autocomplete-source'),
        dataType: "json",
        beforeSend: function() { $("div.navigation-autocomplete-results#message").show(); },
        data: {
          term: request.term,
          filter: $input.parent().find('select#filter').val(),
          rows: 30
        },
        success: function(data) {
          response(data);
        },
        complete: function() { $("div.navigation-autocomplete-results#message").hide(); },
      })
    },
    select: function(event, ui) {
      var selectedObj = ui.item;
      $input.val(selectedObj.value);
    }
  });
}

Search.inline_autocomplete_listener = function($input) {
  $input.autocomplete( {
    minLength: 3,
    source: function(request, response) {
      $.ajax( {
        url: $input.data('autocomplete-source'),
        dataType: "json",
        data: {
          term: request.term,
          filter: $input.parent().find('select#filter').val(),
          rows: 30
        },
        success: function(data) {
          response(data);
        },
      })
    },
    select: function(event, ui) {
      var selectedObj = ui.item;
      $input.val(selectedObj.value);
    }
  });
}

String.prototype.capitalize = function() {
  return this.charAt(0).toUpperCase() + this.slice(1);
}
$(function() {
  Search.initialise_search_form();
  Search.inline_autocomplete_listener($('form#inline-search input#search'));
})
