$(document).ready(function() {
  $('#connection-container').masonry({
    itemSelector: '.connection-detail',
    isResizable: true,
    columnWidth: function( containerWidth ) {
      return (containerWidth / 2);
    },
    animationOptions: {
      duration: 400
    }
  });

  $('#group-list').masonry({
    itemSelector: '.group-box',
    isResizable: true,
    columnWidth: function( containerWidth ) {
      return (containerWidth / 2);
    },
    animationOptions: {
      duration: 400
    }
  });
});

$('.undo_connection').live("click", function(){
  var connection_id = $(this).attr('id').match(/\D+(\d+)/)[1];
  var $item = $('#'+connection_id);
  $('#connection-container').masonry( 'remove', $item).masonry( 'reload' );
});

$(function(){
  initGroupDeleteLink();
  resetDraggable();
});

function initGroupDeleteLink() {
  $('.group-box').hover(
    function () {
      $('#' + this.id + ' .delete-group').show();
    },
    function () {
      $('#' + this.id + ' .delete-group').hide();
    }
  );
}

function hide_all_tooltips() {
  $('.qtip.qtip-defaults.qtip-active').hide();
}

function resetDraggable() {
  $('.my-connection').draggable( {
    appendTo: '#connection-containment-area',
    containment: '#connection-containment-area',
    cursor: 'move',
    snap: '.group-box',
    stack: '.my-connection',
    helper: 'clone',
    zIndex: 500
  } );

  initGroupDraggable();
}

function initGroupDraggable() {

  $('.group-member').draggable( {
      appendTo: '#connection-containment-area',
      containment: '#connection-containment-area',
      cursor: 'move',
      snap: '.group-box',
      stack: '.group-member',
      start: function(event, ui) {
        hide_all_tooltips()
      }
    } );

  $('.expanded-group-member').draggable( {
      appendTo: '#connection-containment-area',
      containment: '#connection-containment-area',
      cursor: 'move',
      snap: '.group-box',
      stack: '.group-member'
    } );

  $('.group-box').droppable( {
      drop: function(event, ui) {
        var draggable = ui.draggable;
        if ($(this).attr('class').indexOf('expanded-group-box') >= 0 ) {
          $.getScript('/circles/' + this.id + '/add_user.js?expanded=true&user_id=' + draggable.attr('id'));
        } else {
          $.getScript('/circles/' + this.id + '/add_user.js?user_id=' + draggable.attr('id'));
        }
      },
      out: function(event, ui) {
        var draggable = ui.draggable;
        if (draggable.attr('class').indexOf('group-member') >= 0) {
          $.getScript('/circles/' + this.id + '/delete_user.js?user_id=' + draggable.attr('id'));
        }
        if (draggable.attr('class').indexOf('expanded-group-member') >= 0) {
          $.getScript('/circles/' + this.id + '/delete_user.js?expanded=true&user_id=' + draggable.attr('id'));
        }
      }
    } );
  $('#group-list').masonry('reload');
}

function toggleConnectionDescription(objectId) {
  $('#connection-description-' + objectId ).toggle();
  $('#connection-container').masonry('reload');
}

Connections = function() {}

Connections.update_incomming_messages_counter = function(increment) {
  $("div.incoming.indicator_container span.badge, ul.nav span.badge-info").each(function(index,element){
    var $indicator = $(element);
    var count = $indicator.data("count");
    if (increment) {
      count = count + 1;
    } else {
      count = count - 1;
    }
    $indicator.html(count);
    $indicator.data("count", count);
    $indicator.hide();
    if (count >= 1) $indicator.show();
  })
}

function initializeUsersSlider() {
  $usersSlider = $('#user-slider').bxSlider({
    controls: false,
    infiniteLoop: false
  });
  $('a#prev_user').click(function(){
    $usersSlider.goToPreviousSlide();
    return false;
  });
  $('a#next_user').click(function(){
    $usersSlider.goToNextSlide();
    return false;
  });
}

$(function(){
  initializeUsersSlider();
});

function leaf_connections($container, new_items, page) {
  var container_height = $container.find('.accordion-inner').height();
  var $current_page = $container.find('.paginator .current-page');
  var max_page = $current_page.data("max-page");
  var $connections_container = $container.find('.accordion-inner .container-fluid');
  var $accordion_container = $container.find('.accordion-inner');
  $accordion_container.css("height", container_height);
  $connections_container.fadeOut("fast", function(){
    $connections_container.find('.row-fluid').remove();
    $connections_container.prepend(new_items);
    $connections_container.fadeIn("fast");
    $current_page.data("page", page);
    $current_page.text("page " + page + " of " + max_page);
  })

  $accordion_container.imagesLoaded(function() {
    $connections_container.show();
    $accordion_container.css("height", "auto");
  });
}

$(function() {
  $('#connections_container .accordion-toggle').click(function() {
    var toggle_id = $(this).attr("href");
    var $arrow_image = $(this).find('div');
    if ($(toggle_id).hasClass('in')) {
      $arrow_image.removeClass("toggle-open");
      $arrow_image.addClass("toggle-close");
    } else {
      $arrow_image.removeClass("toggle-close");
      $arrow_image.addClass("toggle-open");
    }
  });

  $('.btn-toolbar a.btn').click(function(event) {
    event.preventDefault();
    var $paginator = $(this).parents('.paginator');
    var link_url = $(this).attr("href");
    var way = $(this).data("arrow");
    var curr_page = Math.ceil($paginator.find('.current-page').data("page"));
    var max_page = Math.ceil($paginator.find('.current-page').data("max-page"));
    var new_page;
    new_page = curr_page + (way == 'prev' ? -1 : 1)
    if (new_page >= 1 && new_page <= max_page) {
      $.ajax({
        url: link_url,
        type: 'get',
        data: {
          page: new_page
        },
        dataType: 'script',
        beforeSend: function () {
          $paginator.find('.connections_loader').show();
        },
        success: function() {
          $paginator.find('.connections_loader').hide();
        }
      });
    }
  });
});

function manage_connections() {
  $(".left_arrea").niceScroll({
    cursoropacitymax: 0.6,
    zindex: 500,
    autohidemode: false,
    cursorcolor: '#DBDBDB',
    railoffset: { left: -8 },
    cursorborder: "0px"
  });
}

function init_scroll_and_autocomplete() {

  $("#blocked_users_container").niceScroll({
    cursoropacitymax: 0.6,
    zindex: 500,
    autohidemode: false,
    cursorcolor: '#DBDBDB',
    railoffset: { left: -8 },
    cursorborder: "0px"
  });

  $("#blocked_user").autocomplete({
    source: $('input#blocked_user').data('autocomplete-source'),
    select: function(event, ui) {
      var selectedObj = ui.item;
      $("#blocked_users").val(selectedObj.value);
      $('#blocked_user_id').val(selectedObj.id);
    }
  });
}

function my_connections(profile_url) {

  $(document).on("click", 'a[data-action="unblock_user"]', function() {
    $(this).parents('.blocked_user').remove();
  });

  $(document).on("click", '#connections_container #block_choosen_user', function() {
    var url = $(this).attr('href');
    var $input_field = $('#blocked_user_id')
    if ($input_field.val() != "") {
      $.ajax({
        url: url,
        data: {
          friend_id: $input_field.val()
        },
        type: 'get',
        dataType: 'script',
        success: function() {
          $input_field.val("");
          $('#blocked_user').val("");
          $('#blocked_user').attr("placeholder", "Enter the Name of the User");
        }
      });
    }
    return false;
  })

  $(document).on("click", "#my_connections .connection", function() {
    var active_user = $(this);
    var user_id = active_user.data("connection-id")
    $.ajax({
      url: profile_url,
      data: {
        friend_id: user_id
      },
      type: 'get',
      dataType: 'script',
      beforeSend: function() {
        $('#my_connections tr').removeClass("active");
        $('.manage_connections .right_arrea').empty();
      },
      success: function() {
        active_user.addClass('active');
      }
    });
  })

  $(document).on("click", "#my_connections .block_user", function() {
    var connection_url = $(this).attr('href');
    var $connection = $(this).parents('.connection');
    $.ajax({
      url: connection_url,
      type: 'get',
      dataType: 'script',
      success: function() {
        $connection.remove();
      }
    });
    return false;
  })

  $(document).on("click", "#my_connections .new_message", function() {
    var new_mesage_url = $(this).attr("href");
    $.ajax({ url: new_mesage_url });
    return false;
  })
}
