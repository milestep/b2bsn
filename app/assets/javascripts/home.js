$(function() {
  $(document).on('click', "#new_post #update-status, #ask-question", function() {
    UpdateModule.addActiveClass(this);
    UpdateModule.postType(this);
    return false;
  });
})

var UpdateModule = {

  addActiveClass: function(link) {
    var links = $("#new_post #update-status, #ask-question")
      if (!$(link).hasClass('active')) {
        links.removeClass('active');
        links.addClass('not-active');
        $(link).removeClass('not-active');
        $(link).addClass('active');
      }
  },

  postType: function(id) {
    if ($(id).is("#ask-question")) {
      var post = "Question";
      $("#auto-who-publishing").val("");
    } else {
      var post = "Status";
      $("#auto-who-asking").val("");
    }
    $("[name='post[post_type]']").val(post);
  }
}
