$(function(){
  $('body.users ul.tabs').show();

  $('button[data-dismiss="alert"]').click(function(){
    $(this).parents('div.alert').fadeOut('1000');
    return false;
  });
});
