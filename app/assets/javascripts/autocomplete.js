/* Linked in style autocomplete */

function splitList( val ) {
  return val.split( /,\s*/ );
}

function extractLast( term ) {
  return splitList( term ).pop();
}

function filterNameList (cssId, requestTerm) {
  var resultsArray = [];
  if (extractLast(requestTerm)) {
    var names = [];
    $.each($(cssId).data('names'), function(i,item) { names.push(item.name) });
    var filteredNames = $.ui.autocomplete.filter(names, extractLast(requestTerm));
    $.each($(cssId).data('names'), function(i,item) {
      if ($.inArray(item.name, filteredNames) > -1) { resultsArray.push(item);}
    });
  }
  return resultsArray;
}

//renders appropriately for circle or user
function autoRenderLi( ul, item ) {
  var person;
  if (item.hasOwnProperty("title")){
    person="<a style='font-size: 10px;'><img style='float: left; margin: 4px 10px 4px 4px;height:35px;width:35px;' src='" + item.avatar + "' //><strong>" + item.name + "</strong><br/>" + item.title + "<br/><em>" + item.company + "<em/></a>"
  }else{
    person="<a style='font-size: 10px;'><strong>" + item.name + "</strong><br/></a>"
  }
  return $( "<li></li>" )
  .data( "item.autocomplete", item )
  .append( person)
  .appendTo(ul);
};

$(function() {
  if ($('#auto-who-with').length > 0) {
    $("#auto-who-with")
    // don't navigate away from the field on tab when selecting an item
    .bind( "keydown", function( event ) {
      if ( event.keyCode === $.ui.keyCode.TAB &&
          $( this ).data( "autocomplete" ).menu.active ) {
        event.preventDefault();
      }
    })
    .autocomplete({
      minLength: 0,    
      source: function( request, response ) {      
        response(filterNameList("#auto-who-with",request.term));
      },
      focus: function() {
        return false; //avoid deleting found items
      },
      select: function( event, ui ) {
          var terms = splitList( this.value );
          // remove the current input
          terms.pop();
          // add the selected item
          terms.push( ui.item.name );
          // add placeholder to get the comma-and-space at the end
          terms.push( "" );
          this.value = terms.join( ", " );
          return false;
      }
    }).data( "autocomplete" )._renderItem = function( ul, item ){autoRenderLi( ul, item )};
  }
  
  if ($('#auto-who-asking').length > 0) {
    $("#auto-who-asking")
    // don't navigate away from the field on tab when selecting an item
    .bind( "keydown", function( event ) {
      if ( event.keyCode === $.ui.keyCode.TAB &&
          $( this ).data( "autocomplete" ).menu.active ) {
        event.preventDefault();
      }
    })
    .autocomplete({
      minLength: 0,    
      source: function( request, response ) {
        response(filterNameList("#auto-who-asking",request.term));
      },
      focus: function() {
        return false; //avoid deleting found items
      },
      select: function( event, ui ) {
          var terms = splitList( this.value );
          // remove the current input
          terms.pop();
          // add the selected item
          terms.push( ui.item.name );
          // add placeholder to get the comma-and-space at the end
          terms.push( "" );
          this.value = terms.join( ", " );
          return false;
      }
    }).data( "autocomplete" )._renderItem = function( ul, item ){autoRenderLi( ul, item )};
  }
  
  if ($('#auto-who-publishing').length > 0) {
    $("#auto-who-publishing")
    // don't navigate away from the field on tab when selecting an item
    .bind( "keydown", function( event ) {
      if ( event.keyCode === $.ui.keyCode.TAB &&
          $( this ).data( "autocomplete" ).menu.active ) {
        event.preventDefault();
      }
    })
    .autocomplete({
      minLength: 0,    
      source: function( request, response ) {
        response(filterNameList("#auto-who-publishing",request.term));
      },
      focus: function() {
        return false; //avoid deleting found items
      },
      select: function( event, ui ) {
          var terms = splitList( this.value );
          // remove the current input
          terms.pop();
          // add the selected item
          terms.push( ui.item.name );
          // add placeholder to get the comma-and-space at the end
          terms.push( "" );
          this.value = terms.join( ", " );
          return false;
      }
    }).data( "autocomplete" )._renderItem = function( ul, item ){autoRenderLi( ul, item )}; 
  }
  
});