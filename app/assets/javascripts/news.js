$(function(){
  initializeNewsSlider();
  add_scroll_to_news_slider();

  $('#myCarousel').bind('slid', function() {
    add_scroll_to_news_slider();
  });
});

function add_scroll_to_news_slider() {
  $("#news .item.active .news_caption").niceScroll({
    cursoropacitymax: 0.5,
    zindex: 500,
    cursorcolor: '#7B7B7B',
    railoffset: { left: 4 },
    cursorborder: "0px"
  });
}

function initializeNewsSlider() {
  $newsSlider = $('#news-slider').bxSlider({
    displaySlideQty: 4,
    moveSlideQty: 4,
    controls: false
  });
  $('a#prev-news').click(function(){
    $newsSlider.goToPreviousSlide();
    return false;
  });
  $('a#next-news').click(function(){
    $newsSlider.goToNextSlide();
    return false;
  });
}

function news_slider(news_url) {
  $('#myCarousel').carousel({ interval: 4000 })
  $('.carousel-inner .item').first().addClass('active');
  $('#myCarousel').bind('slid', function() {
    var slides_count = $('#news .carousel-inner .item').size();
    $('#news .carousel-inner .item').each(function(index, Element) {
      if ($(Element).hasClass('active')) curr_index = index + 1;
    });
    $('#slider_page').val(curr_index);

    if (curr_index >= (slides_count - 5)) {
      $.ajax({
        url: news_url,
        type: 'get',
        data: { current_page: Math.ceil(curr_index/10) },
        dataType: 'html',
        success : function(items) { $('#myCarousel .carousel-inner').append(items) }
      });
    }
  })
}

function initNiceScroll() {
  $('#news_reader').niceScroll({
    cursoropacitymax: 0.5,
    zindex: 500,
    autohidemode: false,
    cursorcolor: '#DBDBDB',
    railoffset: { left: -8 },
    cursorborder: "0px"
  });
}

function newsContainerHeight() {
  var total_height = 0;
  $("#news_reader .news_day_block").each(function() {
    total_height += $(this).height();
  });
  return total_height;
}

Feed = function() {}

Feed.stripNews = function() {
  $('.news_item:odd').addClass('odd');
  $('.news_item:even').addClass('even');
}

Feed.addItem = function(news) {
  var $last_block = $('.news_day_block:last .news_container');
    $.each(news, function(item, value) {
      if (value.feed_class == "activities") { $last_block.append(ich.feed_activity_item_template(value)) }
      if (value.feed_class == "news") { $last_block.append(ich.news_item_template(value)) }
    })
}

Feed.addBlock = function(news) {
  $.each(news, function(item, value) {
    $('#news_reader').append(ich.block_for_news_template(value))
      return false;
    });
  return false
}

Feed.addNews = function(items) {
  var last_block_date = $('.news_day_block:last .news_date').attr("value");
  $.each(items, function(date, news) {
    if (date == last_block_date) {
      Feed.addItem(news);
    } else {
      Feed.addBlock(news)
      Feed.addItem(news);
    }
  });
  Feed.stripNews()
}

function news_page(news_url) {
  var scroll_state = "free";
  initNiceScroll();
  var total_height = newsContainerHeight();

  $("#news_reader").on("scroll", function(){
    if ($(this).scrollTop() >= ((total_height*70)/100)) {
      var curr_page = parseInt($('#news_current_page').val());
      if (scroll_state == "free" && curr_page > 0) {
        scroll_state = "block";
        $.ajax({
          url: news_url,
          type: 'get',
          data: {
            current_page: curr_page,
            feed_type: $('#feeds_type').val()
          },
          dataType: 'json',
          beforeSend: function() {
            $("#loader").show();
          },
          success : function(data) {
            if (jQuery.isEmptyObject(data) == false) {
              Feed.addNews(data);
              $('#news_current_page').val(curr_page + 1)
            } else {
              $('#news_current_page').val(undefined)
            }
          },
          complete: function() {
            $('#news_reader').hide();
            $('#news_reader').show();
            total_height = newsContainerHeight();
            scroll_state = "free";
            $("#loader").hide();
          }
        });
      }
    }
  });
}