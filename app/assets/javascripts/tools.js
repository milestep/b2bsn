$(function(){
  var $market_stream = $("#market-stream");
  var $activity_container = $("#market-stream .accordion-inner");
  $activity_container.data("normal_height", $activity_container.height());

  $("#add_new_entry").click(function(){
    $.get($(this).data('url'));
  });

  $("table#profile_list").tablesorter({
    sortList: [[0,0]]
  });
  $("table#profile_list").bind("sortEnd",function() {
    $("table#profile_list th:not(.section-search)").each(function(){
      var current_class = $(this).attr("class");
      $("#discover_items table._thead").find("th." + current_class.split(" ")[0])
      .attr("class", $(this).attr("class"));
    });
  });

  $("#profile_list").chromatable({
    width: "100%",
    height: "848px"
  });

  $("#discover_items table._thead th:not(.section-search)").click(function() {
    var current_class = $(this).attr("class").split(" ")[0];
    $("table#profile_list").find("th." + current_class).click();
  });

  $("#row-add-dropdown li").click(function(){
    var $li = $(this);
    var value = $li.find("a").html();
    var id = $li.attr("id");
    $("#row-add-dropdown .current-value").html(value);
    if ( id == "everyone" ) {
      $("#post_type").val(null);
    } else {
      $("#post_type").val(id);
    }
  });

  $("#select-update-type li").click(function(){
    var $li = $(this);
    var value = $li.find("a").html();
    var type = $li.attr("class");
    var $select = $("#select-update-type .current-value");
    $select.html(value);
    $select.data("type", type);
    var $all_types = $("#market-stream tr");
    $all_types.hide();
    if (type == "all") {
      $all_types.show();
    } else {
      $("#market-stream .accordion-inner table tr[class=" + type + "]").show();
    }
    set_activity_table_height($activity_container);
  });

  $activity_container.niceScroll({
    cursoropacitymax: 0.5,
    zindex: 500
  });

  Chat.attachScroll();

  $('input#discover-search').keyup(function() {
    var value = $(this).val();
    serch_profile(value);
    $("#profile_listwrapper").resize();
  });

  $("#profile_type ul.dropdown-menu a").click(function(e){
    var $discover_search_element = $('input#discover-search');
    var value = $.browser.msie ? fixPlaceholderForIe($discover_search_element) : $discover_search_element.val()
    var data_value =  $(this).data("value");
    var text = $(this).text();
    $("div#profile_type button.dropdown-toggle-search div.current-value").data("value", data_value).html(text);
    serch_profile(value);
    if ($("#profile_listwrapper").height() > $("#profile_list").height()) {
      $(".section-search").css("width", $("#discover_items").width());
    } else {
      $(".section-search").css("width", $("#profile_listwrapper").width()-16);
      $("._thead").resize();
    }
    e.preventDefault();
  });

  $("textarea.write_remote_comment").live("keydown", function(event){
    if (event.which == 13){
      event.preventDefault();
      $(this).parents("form").submit();
    }
  });

  Notices.addObservers();
});

function user_status(url) {
  $("#select-user-status li").click(function(){
    var $li = $(this);
    var user_status = $li.data('status');
    var value = $li.find("a").html();
    $("#select-user-status .current-value").html(value);
    $.ajax({
      url: url,
      type: 'get',
      data: {
        status: user_status
      },
      dataType: 'html'
    });
  });
}

function fixPlaceholderForIe(element) {
  return element.val() == element.attr("placeholder") ? "" : element.val();
}

function serch_profile(value) {
  var rgxp = new RegExp(value, 'i');
  var $container;
  var selected = $("div#profile_type button.dropdown-toggle-search div.current-value").data("value");
  $(Marketplace.data_items).each(function(k,v) {
    $container = $('tr#' + v['discover_type'] + '_profile_' + v['id']);
    var selected_options = (selected == '' || selected == v['discover_type']);
    var rgxp_options = (rgxp.test(v['fullname']) || rgxp.test(v['discover_location']));
    if (selected_options && rgxp_options) {
      $container.show();
    } else {
      $container.hide();
    }
  });
}

function get_advertiser_ids() {
  var advertisers = $("#add-associations-agency div.agency_advertiser.association:visible input.object_id[type='hidden']");
  return $.map(advertisers, function(e) {
    return $(e).val();
  });
}

function update_autocomplete_url(with_text) {
  url = $('#agency_agency_brands_attributes_0_brand_name').data('autocomplete-source');
  $('#agency_agency_brands_attributes_0_brand_name').live('keypress', function() {
    $(this).autocomplete({
      source: url + with_text
    });
  });
}

function add_autocomplete($input) {
  $input.autocomplete({
    source: $input.data('autocomplete-source'),
    appendTo: "#bootstrap_modal",
    select: function(event, ui){
      $input.parent().find('.object_id').val(ui.item.id);
      update_autocomplete_url('?ids=' + get_advertiser_ids());
    },
    search: function(event, ui){
      $input.parent().find('.object_id').val('');
      update_autocomplete_url('?ids=' + get_advertiser_ids());
    }
  });
}

function add_new_association(id){
  var $inputs_container = $("." + id).parents(".inputs");
  var associations = $inputs_container.find(".association");
  var $association = associations.first().clone();
  var associations_count = associations.size().toString();
  $association.find("input").val("");
  $association.removeClass("hidden");
  $association.find(".delete_association").click(function(){
    $(this).parent().remove();
    update_autocomplete_url();
  });
  $inputs_container.append($association);
  set_names($inputs_container);
  add_autocomplete($association.find(".object_name"));
}

function process_associations(association_types){
  $(association_types).each(function(index, id){
    $("." + id + " .object_name").each(function(index, input){
      process_data_input($(input));
    });
    $("#add_" + id).click(function(){
      add_new_association(id);
    });
    $("." + id + " .delete_association").click(function(){
      var $delete_link = $(this);
      var $inputs_container = $delete_link.parents(".inputs");
      var $association_template = $inputs_container.find(".association.hidden");
      var $association = $delete_link.parents('.association');
      $association.addClass('hidden');
      $association.find("input[data-type=destroy_input]").val("true");
      set_names($inputs_container);
      update_autocomplete_url();
    });
  });
}

function set_names($container){
  $container.find('.association').each(function(association_index,association){
    $(association).find('input').each(function(input_index,input){
      var $input = $(input);
      $input.attr("name", $input.attr("name").replace(/\[\d\]/, "[" + association_index.toString() + "]"));
    });
  });
}

function process_data_input($input){
  add_autocomplete($input);
  $input.on("keyup", function(){
    $destroy_input = $input.parent().find("input[data-type=destroy_input]");
    $destroy_input.val($input.val().length > 0);
  });
}

function show_company_summary(id) {
  $('#traffic_data, #audience_data, #profile_description').hide();
  $('#'+id).show();
}

function show_slider(class_name) {
  $('.first_slider, #first_slider_container, .second_slider, #second_slider_container').hide();
  $('.' + class_name + ', #' + class_name + '_container').show();
}

function set_activity_table_height(container) {
  var $activity_table = container.find("table");
  if ($activity_table.height() > container.data("normal_height")) {
    container.height(container.data("normal_height"));
  } else {
    container.height($activity_table.height());
  }
  container.getNiceScroll().resize();
  container.scrollTop(0);
}

function set_nice_scroll(container) {
}
