$(function(){

  $('#help-account-type').popover({
    animation: true,
    placement: 'right',
    trigger: 'hover'
  });

  var $sourceTypeahead = $("#add-skill-input").data('skills');

  $('.typeahead').typeahead({
    source: $sourceTypeahead,
    items: 8
  });

  $('#add-skill').click(function(){
    $('#no-skills-yet').hide();
    var skill = $('#add-skill-input').val();
    $.ajax({
        url: $("#add-skill-input").data('url'),
        type: 'post',
        data: {skill: skill},
        success: function(resp) {
          if (resp['status'] == 'new') {
            var template = $('#skill_template').children('.alert');
            var skill_node = template.clone();

            skill_node.attr('id', 'skill-' + resp['skill_id']);
            skill_node.children('strong').html(skill);
            skill_node.find('a.close').attr('href', '/signup/skills/' + resp['skill_id']);

            skill_node.appendTo('#skill-list');
          }
        }
      });

  });

  $('#signup_connections_search_box').keyup(function() {
      var value = $(this).val();
      var rgxp = new RegExp(value, 'ig');
      var container;
      var checkbox;
      $($("#connections").data('connections')).each(function(k,v) {
          checkbox = $('#user_' + v['id']);
          container = checkbox.parents('div.add-connection-tile');
          if(rgxp.test(v['first_name']) || rgxp.test(v['last_name'])) {
            container.show();
          } else {
            container.hide();
          }
      })
  })

  $("#bootstrap_modal").bind("ajaxComplete", function(){
      initializeDatepicker();
  })

});

function edit_experience(url) {
  $.ajax({
    url: url,
    success: $('#bootstrap_modal').modal('show')
  });
}

function initializeDatepicker() {
  $('.signup-experience-date-picker').datepicker({
      format: 'yyyy-mm-dd'
  });
}
