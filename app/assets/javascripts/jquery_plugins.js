(function( $ ){
  $.fn.align_by_center_by_width = function(width, height) {
    var self = $(this);
    self.css('width', width);
    self.css('margin-left', -width/2);
    self.css('height', height);
  };
})( jQuery );

