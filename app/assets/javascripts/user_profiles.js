UserProfile = function() {}

UserProfile.set_logo_url = function(file_url){
  $("div.profile_main_content div.user_logo img").attr("src", file_url);
}

$(document).ready(on_dom_ready);

function on_dom_ready() {
  $('#profile_connections .tab_cards_container').masonry({
    itemSelector: '.tab_card:visible',
    isResizable: true,
    animationOptions: {
      duration: 400
    }
  });

  $('#section-body .tabs .nav-tabs a').live('click', function() {
    $('#profile_connections .tab_cards_container').masonry('reload');
  });

  $("#user_profile_avatar").fileupload({
    dataType: 'json',
    done: function (e, data) {
      UserProfile.set_logo_url(data.result[0].file_url);
    }
  });

  $("#remove_profile_avatar").click(function(){
      $.post($(this).attr("href"), { user_profile: { remove_avatar: true } }, function(data){
        UserProfile.data = data;
      UserProfile.set_logo_url(data[0].file_url);
    });
    return false;
  });

  $("#bootstrap_modal").on("change", "input#experience_company_logo", function(){
    $("#experience_company_logo_file_name").val($(this).val());
  });

  $("div#profile_experience div.with_scroll").on("hover", function(){
    $(this).getNiceScroll().resize();
  });
}

$(window).load(function() {
  $("#early-adopter-progressbar").progressbar({ value: 100 });

  $("div.profile_main_content div.with_scroll").niceScroll({
    cursoropacitymax: 0.5,
    zindex: 500
  });

  var $role_links = $("div#profile_connections div.switcher a");
  $role_links.click(function(){
    var $link = $(this);
    $role_links.removeClass("active");
    $link.addClass("active");
    var $friends = $("div#profile_connections div.tab_card");
    $friends.hide();
    if ($link.data("role") == "all"){
      $friends.show();
    } else {
      $("div#profile_connections div.tab_card[data-role=" + $link.data("role") + "]").show();
    }
    $('#profile_connections .tab_cards_container').masonry('reload');
    $("div#profile_connections div.with_scroll").getNiceScroll().resize();
    return false;
  });

  $("div.profile_main_content ul.nav-tabs li").click(function(){
    $("div.profile_main_content div.with_scroll").getNiceScroll().show();
  });

  $("div#profile_achievements div.switcher a").click(function(){
    $("#all_achievements, #activity_achievements").hide();
    $('#' + $(this).data("id")).show();
    $("div#profile_achievements div.with_scroll").getNiceScroll().resize();
    return false;
  });
});

$(window).load(function() {
  $("#profile_achievements a").click(function() {
    $("#profile_achievements a").removeClass("active");
    $(this).addClass("active");
  });
});

function initializeUserProfilePopups() {
  $('a.user-profile, a.user-profile-img').each(function() {
     $(this).qtip({
        content: {
          ajax: {url: $(this).attr('href') + '/popup_profile', type: 'get'},
          text: 'Loading...'
        },
        show: 'mouseover',
        hide: { when: { event: 'mouseleave mouseout mousedown mouseup' } },
        style: {
          classes: 'ui-tooltip-bootstrap'
        },
       position: {
         my: 'middle left',
         at: 'middle right',
         viewport: $(window),
         adjust: {
           x: -8
         }
       }
     });
  });

  $('.new_connections').each(function() {
    $(this).qtip({
      content: $(this).parents('p').next(),
      show: 'mouseover',
      hide: 'mouseout',
      style: {
        width: 250,
        padding: 10,
        background: '#f3f3f3',
        color: 'gray',
        border: {
           color: '#f3f3f3'
        }
      },
      position: {
        my: 'right bottom ',
        at: 'bottom left',
        target: $(this)
      }
    });
  });
}

jQuery(function() {
  $("a.rest-events-wrapper").qtip({
    style: {
      padding: 10,
      background: 'white',
      tip: {
        corner: 'leftMiddle',
        color: 'white',
        size: {
          x: 10,
          y: 10
        }
      },
      border: {
        color: 'white'
      }
    },
    position: {
      corner: {
        target: 'rightMiddle',
        tooltip: 'leftMiddle'
      }
    }
  });
});
