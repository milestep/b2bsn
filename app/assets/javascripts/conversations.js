$(window).load(function() { Conversation.refreshNiceScroll() })

$(function() {
  $('textarea#message_body').keypress(function(e) {
    if ($('#quick-reply').is(':checked') && (e.which == 13)) {
      $('form#message').submit();
    }
  });

  $("#messages_menu .span9 a").click(function() {
    $("#messages_menu .span9 a").removeClass("active");
    $(this).addClass("active");
  });

  $("#sort_by_old ul.dropdown-menu a").on("click", function() {
    var data_value = $(this).data("value");
    var text = $(this).text();
    $("div#sort_by_old button.dropdown-toggle div.current-value").data("value", data_value).html(text);
  });

  $("#filter_by_user_role ul.dropdown-menu a").click(function() {
    var data_value = $(this).data("value");
    var text = $(this).text();
    $("div#filter_by_user_role button.dropdown-toggle div.current-value").data("value", data_value).html(text);
  });

  Conversation = function() {}

  Conversation.refreshNiceScroll = function() {
    $("#conversations_container .container-border").niceScroll({
      cursoropacitymax: 0.5,
      zindex: 500
    });
  }

  Conversation.addAutocomplete = function() {
    $("#message_recipients").autocomplete({
      source: $('#message_recipients').data('autocomplete-source'),
      appendTo: "#bootstrap_modal",
      select: function(event, ui) {
        Conversation.addReceipt(ui.item);
      },
      close: function(event, ui) {
        Conversation.clearReceiptNameField();
      }
    });
  }

  Conversation.addReceipt = function(selectedObj) {
    var receiptName = selectedObj.value;
    var receiptId = selectedObj.id.toString();
    var receiptsIds = Conversation.getIds();
    if (receiptsIds.length > 0) {
      if ($.inArray(receiptId, receiptsIds) == -1) {
        Conversation.addReceiptToReceiptsList(receiptId, receiptName);
        receiptsIds.push(receiptId);
        Conversation.refreshReceiptsIdsList(receiptsIds);
        $('#message_recipients_ids').val(receiptsIds.join(','));
      }
    }
    else
    {
      Conversation.addReceiptToReceiptsList(receiptId, receiptName);
      $('#message_recipients_ids').val(receiptId);
    }
    Conversation.clearReceiptNameField();
  }

  Conversation.addReceiptToReceiptsList = function(receiptId, receiptName) {
    var receiptsList = $('#receipts-list');
    receiptsList.addClass('show-receipts-list');
    receiptsList.removeClass('hide-receipts-list');
    receiptsList.append(
      "<p id='receipt-" +
        receiptId + "' class='receipt'><span class='receipt-name'>" +
        receiptName + "</span><i class='icon-remove' onclick='Conversation.removeReceipt(" +
        receiptId + ")'></i></p>"
    );
  }

  Conversation.removeReceipt = function(id) {
    var receiptsList = $('#receipts-list');
    $('#receipts-list p#receipt-' + id).remove();
    var receiptsIds = Conversation.getIds();
    receiptsIds.splice($.inArray(id.toString(), receiptsIds), 1);
    Conversation.refreshReceiptsIdsList(receiptsIds);
    if ($('#receipts-list p').length == 0) {
      receiptsList.removeClass('show-receipts-list');
      receiptsList.addClass('hide-receipts-list');
    }
  }

  Conversation.getIds = function() {
    var ids = [];
    if ($('#message_recipients_ids').val().length > 0) {
      var ids = $('#message_recipients_ids').val().split(",");
      $.each(ids, function(i, v) {
        ids[i] = v.toString();
      })
    };
    return ids;
  }

  Conversation.refreshReceiptsIdsList = function(receiptsIds) {
    $('#message_recipients_ids').val(receiptsIds.join(','));
  }

  Conversation.clearReceiptNameField = function() {
    $('#message_recipients').val('');
  }

  Conversation.newMessageClose = function() {
    $('#bootstrap_modal').modal('hide');
    Conversation.clearFields();
  }

  Conversation.clearFields = function() {
    $('#bootstrap_modal input[id], #bootstrap_modal textarea[id]').each(function() {
      switch(this.type) {
        case 'text':
	      case 'textarea':
	        $(this).val('');
      }
    });
  }

  Conversation.sendToConversation = function(url) {
    $.ajax({ url: url });
  }

  Conversation.updateFiltersLinks = function(url) {
    $('div#sort_by_old ul a, div#filter_by_user_role ul a[href]').each(function(i, element){
      var lnk = $(this).attr('href');
      if (lnk.indexOf('?') > 0) {
        $(this).attr('href', url + lnk.substring(lnk.indexOf('?')));
      } else {
        $(this).attr('href', url);
      }
    });
  }

  Conversation.addedFile = function() {
    var element = $('.reply-actions .img-attach, .send-message-actions .img-attach');
    var label = $('#message_attachment').val().substring(0,15) + "...";
    $('.attach-file label').text(label);
    element = element.removeClass('img-attach');
    element = element.addClass('icon-remove');
  }

  Conversation.removeFile = function() {
    var element = $('.send-message-actions .icon-remove');
    var label = 'Attach file';
    if (element.length > 0) {
      element = element.addClass('img-attach');
      element = element.removeClass('icon-remove');
      $('#message_attachment').val('');
      $('.attach-file label').text(label);
    }
  }

  Conversation.friendPreviewPopup = function() {
    $('#my_marketplace .friend_logo').each(function() {
      var target = $(this);
      target.qtip({
        content: {
          text: "<div class='loading'>Loading...</div>",
          ajax: {url: $('#friend_preview_popup_path').val() + '?friend_id=' + target.data('id'), type: 'get'}
        },
        position: {
          effect: false,
          my: 'middle right',
          at: 'middle left',
          viewport: $(window),
          adjust: {
            x: 8
          }
        },
        style: {
          classes: 'ui-tooltip-bootstrap no-padding friend-preview-pop-max-width min-width-250'
        },
        events: {
          hide: function() {
            var textareaHasFocus = $(this).find('textarea').is(':focus');
            return !textareaHasFocus;
          }
        },
        hide: {
          when: 'mouseout',
          fixed: true
        }
      });
    });
  }
});
