$(document).ready(function(){
  var suggestedUsersCarousel = $('#suggestedUsersCarousel'), suggestedUsersItems = suggestedUsersCarousel.find('.item'),
    suggestedUserCarousel = $('#suggestedUserCarousel'), suggestedUserItems = suggestedUserCarousel.find('.item');
  suggestedUsersCarousel.carousel({ interval: false })
  suggestedUsersItems.first().addClass('active').children('img:first').addClass('current');
  suggestedUserItems.first().show();

  $('.item img.ava').click(function(){
    $('.ava.current').removeClass('current');
    $(this).addClass('current');
    suggestedUserItems.hide();
    $('#suggested_user_' + $(this).data('user-id')).show();
  })

  suggestedUserCarousel.find('.carousel-control.left').click(function(){
    suggestedUserCarousel.find('.item:visible').hide(0, function(){
      ($(this).is(':first-child')) ? $(this).siblings('.item:last').show() : $(this).prev().show()
    });
    $('.item .ava.current').removeClass('current');
    ava = $('#user_ava_' + suggestedUserCarousel.find('.item:visible').data('user-id'));
    ava.addClass('current');
    if (ava.is(':last-child')) suggestedUsersCarousel.carousel('prev');
  });
  suggestedUserCarousel.find('.carousel-control.right').click(function(){
    suggestedUserCarousel.find('.item:visible').hide(0, function(){
      ($(this).is(':last-child')) ? $(this).siblings('.item:first').show() : $(this).next().show()
    });
    $('.item .ava.current').removeClass('current');
    ava = $('#user_ava_' + suggestedUserCarousel.find('.item:visible').data('user-id'));
    ava.addClass('current');
    if (ava.is(':first-child')) suggestedUsersCarousel.carousel('next');
  });

  function search_suggested_users(value, selected) {
    var rgxp = new RegExp(value, 'i');
    var $container;
    var selected = $("#profile_type button.dropdown-toggle-search div.current-value").data("value");
    $($("#actions_bar").data('items')).each(function(k,v) {
      $container = $('li#' + 'suggested_user_' + v['role_id'] + '_' + v['id']);
      var selected_options = (selected == '' || selected == v['role_id']);
      var rgxp_options = (rgxp.test(v['fullname']));
      (selected_options && rgxp_options) ? $container.show() : $container.hide()
    });
  }

  $('input#suggested_users_search').keyup(function() {
    var value = $(this).val();
    search_suggested_users(value);
  });

  $("#profile_type ul.dropdown-menu a").click(function(){
    var $search_element = $('input#suggested_users_search');
    var value = $.browser.msie ? fixPlaceholderForIe($discover_search_element) : $search_element.val()
    var data_value =  $(this).data("value");
    var text = $(this).text();
    $("div#profile_type button.dropdown-toggle-search div.current-value").data("value", data_value).html(text);
    search_suggested_users(value);
  });

});
