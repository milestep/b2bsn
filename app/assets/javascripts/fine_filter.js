$(function() {

  $(document).on('click', '.checkboxes .trigger', function() { FineFilter.toggle(this) })
  $(document).on('change', '.checkboxes :checkbox', function() { FineFilter.filter(this) });
  $(document).on('click', '#filter_type a', function(e) { FineFilter.submit(this); e.preventDefault() });
  $(document).on('keyup', '.inline-filter #search_name', function(e) { FineFilter.search(this.value, $('.search-results table tr')) });

});

var FineFilter = {
  toggle: function(element) {
    $('ul' + $(element).data('toggle') + ':first').slideToggle();
    $(element).toggleClass('icon-minus');
  },

  filter: function(element) {
    cssClass = $(element).parents('div').attr('class')
    type = $(element).parents('ul').attr('class')
    $('ul.' + element.id + ' :checkbox').attr('checked', $(element).is(':checked'))
    $('ul#filters li[data-parent*="' + element.id + '"]').remove();
    html = $('<li id="filter_'+ element.id +'" class="' + cssClass + '" data-parent="' + type + '">' + element.title + '<label for="' + element.id + '">×</label></li>')
    if ($(element).is(':checked')) {
      $('ul#filters:not(:has(#filter_'+ element.id +'))').append(html)
    } else {
      $('li#filter_'+ element.id).remove()
    }
    $('.filter_part form').submit();
  },

  submit: function(element) {
    val = $(element).data('type')
    $('input#search_type').val(val);
    $('#filter_type .current-value, span#type-label').html(val.capitalize());
    $('.checkboxes div, #filters li').show();
    $('.checkboxes div:not(.' + val + '), #filters li:not(.' + val + ')').hide();
    $('.filter_part form').submit();
  },

  search: function(val, elements) {
    try { var pattern = new RegExp(val, 'i'); }
    catch(err) { var pattern = '' }
    elements.hide();
    elements.each(function(index, node) {
      if ($(node).data('like').match(pattern) != null) { $(node).show(); }
    });
  }

}
