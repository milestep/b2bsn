$(document).ready(function() {
  Conversation.friendPreviewPopup();
  $(this).on('focus', 'textarea#message', function(event) {
    var otherPopups = $('.friend_preview textarea[name="message"]:visible').not(this).closest('.friend_preview');
    $.each(otherPopups, function() {
      var friendId =  $(this).data('friend-id');
      var $qtipTarget = $('#my_marketplace .friend_logo[data-id='+ friendId +']');
      $qtipTarget.qtip('api').hide();
    });
  });
});