$.tablesorter.addParser({
  id: "customDate",
  is: function(s) {
    return s.match(new RegExp(/^[A-Za-z]{3,10}\.? [0-9]{1,2}, [0-9]{4}|'?[0-9]{2}$/));
  },
  format: function(s) {
    return $.tablesorter.formatFloat(new Date(s).getTime());
  },
  type: "numeric"
});

$(function() {
  $("a.block_user").qtip({
    content: "Block this User",
    position: {
      my: 'middle left',
      at: 'middle right',
      viewport: $(window),
    },
    style: {
      classes: 'ui-tooltip-bootstrap button_popup'
    }
  });

  $(".mod-filter a.incoming, .mod-filter a.outgoing").click(function() {
    $("#requests-type").val($(this).data("value"));
    var $requestSearchElement = $('input#request-search');
    var value = $.browser.msie ? fixPlaceholderForIe($requestSearchElement) : $requestSearchElement.val()
    serchProfileInRequests(value);
    return false;
  });

  $('input#request-search').keyup(function() {
    var value = $(this).val();
    serchProfileInRequests(value);
  });

  $("#request-user-type ul.dropdown-menu a").click(function(e){
    var $requestSearchElement = $('input#request-search');
    var value = $.browser.msie ? fixPlaceholderForIe($requestSearchElement) : $requestSearchElement.val()
    var dataValue =  $(this).data("value");
    var text = $(this).text().toUpperCase();
    $("div#request-user-type button.dropdown-toggle-search div.current-value").data("value", dataValue).html(text);
    serchProfileInRequests(value);
    e.preventDefault();
  });

  $("#request-user-sorted ul.dropdown-menu a").click(function(e){
    var dataValue =  $(this).data("value");
    var text = $(this).text();
    $("div#request-user-sorted button.dropdown-toggle-search div.current-value").data("value", dataValue).html(text);
    var sorting;
    if (dataValue == "from_new_to_old") {
      sorting = [[1,1]];
    } else {
      sorting = [[1,0]];
    }
    $("table#requests").trigger("sorton", [sorting]);
    requestsRemapClass();
    e.preventDefault();
  });

  $("table#requests").tablesorter({
    sortList: [[1,1]],
    headers: {
      1: { sorter: 'customDate' },
      0: { sorter: false },
      1: { sorter: false },
      3: { sorter: false }
    }
  });

  requestsRemapClass();
});

function serchProfileInRequests(value) {
  var rgxp = new RegExp(value, 'i');
  var user;
  var request;
  var $container;
  var selected_role = $("div#request-user-type button.dropdown-toggle-search div.current-value").data("value");
  var selected_type = $("#requests-type").val();
  $(Requests.data_items).each(function(k,v) {
    user = v['actor'];
    $container = $('tr#user_profile_' + user['id']);
    var selected_options = ((selected_role == '' || selected_role == user['user_type']) && v['request_type'] == selected_type);
    var rgxp_options = rgxp.test(user['fullname']);
    if (selected_options && rgxp_options) {
      $container.show();
    } else {
      $container.hide();
    }
  });
  requestsRemapClass();
}

function requestsRemapClass() {
  $("table#requests tbody tr:visible:odd").removeClass("even odd").addClass("odd");
  $("table#requests tbody tr:visible:even").removeClass("even odd").addClass("even");
}
