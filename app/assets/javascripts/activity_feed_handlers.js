(function() {

  var TimelineEventShareItem = (function() {

    function TimelineEventShareItem(selector) {
      this.selector = selector;
    }

    TimelineEventShareItem.prototype.handleActivity = function() {
      this.selector.live("mouseenter", function() {
        return $(this).parent().find('span.popup_details').show();
      });
      this.selector.live("mouseleave", function() {
        return $(this).parent().find('span.popup_details').hide();
      });
      return this.selector.click(function() {
        return false;
      });
    };

    return TimelineEventShareItem;

  })();

  $(function() {
    var menu_item = new TimelineEventShareItem($('span.popup_container a'));
    return menu_item.handleActivity();
  });

}).call(this);
