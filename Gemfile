source 'https://rubygems.org'

gem 'rails', '3.2.8'
gem 'mysql2'

# Gems used only for assets and not required
# in production environments by default.
group :assets do
  gem 'sass-rails',   '~> 3.2.3'
  gem 'coffee-rails', '~> 3.2.1'
  gem 'uglifier', '>= 1.0.3'
  gem 'zurb-foundation', '= 2.2.0.2'
  # See https://github.com/sstephenson/execjs#readme for more supported runtimes
  # gem 'therubyracer'
end

gem 'memcached'
gem 'dalli'
gem 'fog'
gem 'mailboxer', :git => 'git://github.com/beanstock/mailboxer.git'
gem 'sunspot_rails'
gem 'sunspot_solr'
gem 'progress_bar'
gem 'resque'
gem 'resque_mailer'
gem 'jquery-rails'
gem 'cancan'
gem 'rails-boilerplate'
gem 'will_paginate'
gem 'nokogiri', '~> 1.5.4'
gem "whenever"
gem 'foreigner', '~> 1.1.4'
gem 'carrierwave', '~> 0.6.0'
gem 'feed-normalizer', '~> 1.5.2'
gem 'timeline_fu', '~> 0.4.4', :git => "git://github.com/onedesign/timeline_fu.git"

gem 'mini_magick', '~> 3.4'
gem 'best_in_place'
gem 'acts_as_commentable', '~> 3.0.1'
gem 'acts_as_votable', '~> 0.3.0'

gem 'omniauth', '~> 1.1'
gem 'omniauth-linkedin', '~> 0.0.6'
gem 'linkedin', :git => "git://github.com/pengwynn/linkedin.git"
gem 'omniauth-facebook', '~> 1.3.0'
gem 'omniauth-twitter', '~> 0.0.11'

gem 'twitter', '~> 2.5.0'
gem 'koala'                #facebook
gem 'exception_notification_rails3', :require => 'exception_notifier'
gem 'paranoia'
gem 'activeadmin'
gem 'state_machine'
gem 'countries'
gem 'faye'

group :assets, :test, :jenkins, :development do
  gem 'bootstrap-generators', '~> 2.0', :git => 'git://github.com/decioferreira/bootstrap-generators.git'
end

group :test, :jenkins, :development do
  gem 'jasmine'
  gem 'ey_cli'
  gem 'capybara-webkit'
  gem 'headless'
end

group :test, :jenkins do
  gem 'sunspot-rails-tester'
  gem 'minitest'
  gem 'capybara'
  gem 'turn'

  gem 'factory_girl'
  gem 'factory_girl_rails'
  gem 'simplecov'
  gem 'simplecov-rcov'
  gem 'database_cleaner'

  gem 'shoulda'
  gem 'rspec'
  gem 'test-unit'
  gem 'rspec-rails', :group => :development
  gem 'rack_session_access'
  gem 'rspec-html-matchers'
  gem 'launchy'
  gem 'fuubar'
end

group :development do
  gem 'railroady', :git => "git://github.com/ehwinter/railroady.git" 

  gem 'awesome_print'
  gem 'rack-bug', :require => 'rack/bug', :git => 'git://github.com/brynary/rack-bug.git', :branch => 'rails3'
  gem 'foreman'
  #gem 'rack-mini-profiler'
  #gem 'stacktrace'
end
gem 'faker', '~> 1.0.1'

gem 'thin'
gem 'haml' #,  '>= 3.1.4'
gem 'haml-rails'
gem 'debugger'
gem 'pry'
gem 'memoist'
gem 'fitter-happier'
gem 'validates_timeliness', '~> 3.0'
gem 'jira-ruby', :require => false

group :production do
  gem 'exception_notification'
  gem 'newrelic_rpm'
  gem 'ey_config'
end
