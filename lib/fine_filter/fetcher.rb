class FineFilter::Fetcher

  REFLECTED_CLASSES = {:publisher => Publisher, :brand => Brand, :agency => Agency, :person => User}

  attr_accessor :source_class, :params

  def initialize(params)
    fetch_type = params[:type].present? ? params[:type].downcase.to_sym : :publisher
    if REFLECTED_CLASSES.has_key?(fetch_type)
      @source_class = REFLECTED_CLASSES[fetch_type]
    end
    @params = params
  end

  def fetch
    if valid?
      source_class.fine_search(params)
    end
  end

  def valid?
    source_class.present?
  end

end
