module ApiClient
  class Linkedin
    attr_accessor :user_profile, :token, :secret, :client

    delegate :send_message, :profile, :to => :client

    def initialize(user_profile, token, secret)
      raise "arguments should be defined" unless [user_profile, token, secret].all?

      @user_profile = user_profile
      @token        = token
      @secret       = secret

      @client = LinkedIn::Client.new
      @client.authorize_from_access(token, secret)
    end

    def import_experiences!
      experiences = fetch_data('positions')
      user_experiences_eids = user_profile.experiences.map(&:eid)
      experiences.each do |exp|
        unless user_experiences_eids.include?(exp['id'].to_s)
          experience = user_profile.experiences.create({
            :company_name => exp['company']['name'],
            :position     => exp['title'],
            :description  => exp['summary'],
            :start_date   => convert_date(exp, 'start_date'),
            :end_date     => convert_date(exp, 'end_date'),
            :eid          => exp['id'],
            :current      => exp['end_date'].nil?
          })
          experience.company = Company.fetch_by_linkedin_data(exp['company'])
          experience.save
          Resque.enqueue(LinkedInCompany, token, secret, exp["company"]["id"])
        end
      end
    end

    def import_skills!
      skills = fetch_data('skills')
      skills.each do |node|
        skill_name = node['skill']['name']
        user_profile.skills << Skill.find_or_create_by_name(skill_name)
      end
    end

    def beanstock_connections
      lin_ids = connections.map(&:id)
      User.where("uid in (?)", lin_ids)
    end

    def beanstock_connections_for(user)
      beanstock_connections - User.all_accepted_connections_of(user)
    end

    def linkedin_connections
      beanstock_linkedin_ids = beanstock_connections.map(&:uid)
      connections.delete_if { |con| beanstock_linkedin_ids.include?(con.id) }
    end

    private

    def convert_date(exp, date)
      return nil unless exp[date]
      Date.new((exp[date]['year'] || Date.today.year).to_i, (exp[date]['month'] || 1).to_i)
    end

    def connections
      @connections ||= fetch_data('connections')
    end

    def fetch_data(attr)
      (client.profile(:fields => [attr])[attr]['all'] || []) rescue []
    end

  end
end
