class SearchError < StandardError ; end

class Search::Results::Formatter

  def initialize(opts={})
    raise SearchError, 'Invalid params provided for formatter' unless opts.present?
    @raw_results = opts
  end

  def to_json
    format.to_json
  end

  def to_autocompete_json
    format_autocompete.to_json
  end

  private

  def format
    formatted_results = {}
    @raw_results.delete(:search_text)
    @raw_results.each_with_index do |(key, values), index|
      if values.all? { |value| value.respond_to?(:search_title, :full_search_title) }
        formatted_results[key] = values.map{ |value| [value.search_title.html_safe, value.full_search_title.html_safe] } if values.present?
      end
    end
    formatted_results
  end

  def format_autocompete
    formatted_results = []
    @raw_results.delete(:search_text)
    @raw_results.each_with_index do |(key, values), index|
      if values.all? { |value| value.respond_to?(:search_title, :full_search_title) }
        formatted_results.concat(values.map{ |value| {:label => value.search_title.html_safe, :value => value.full_search_title.html_safe} }) if values.present?
      end
    end
    formatted_results
  end

end
