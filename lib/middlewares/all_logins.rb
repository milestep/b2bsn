class AllLogins
  def initialize(app)
    @app = app
  end

  def call(env)
    status, headers, response = @app.call(env)
    content_type = headers["Content-Type"]
    if content_type && content_type.include?("text/html")
      if response.respond_to? :body
        body = response.body
        if body.instance_of? String
          response.body = body.gsub("</body>", "
                                      <div id='all_logins' style='position:fixed; bottom:5px; float:right; padding:0 10px; right: 5px; background-color:green;'>
                                        <a href='/all_logins' style='color:white; font-weight:bold;'>
                                          All logins
                                        </a>
                                      </div>
                                    </body>")
        end
      end
    end
    [status, headers, response]
  end

end
