class String
  def underline
    self.gsub(' ', '_')
  end

  def replace_new_line_with_br
    self.gsub(/\r\n/, '<br/>').gsub(/\n/, '<br/>').html_safe
  end

  def remove_html_tags
    self.gsub(/(<[^>]+>|&nbsp;|\r|\n)/,"")
  end
end
