class Date
  def localized_month
    I18n.t("date.month_names")[self.month]
  end
end
