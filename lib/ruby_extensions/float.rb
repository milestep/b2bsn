class Float
  def to_minutes
    (self / 60).to_i
  end
end
