class Array
  def rejoin(divider = ", ")
    self.reject(&:blank?).join(divider)
  end
end
