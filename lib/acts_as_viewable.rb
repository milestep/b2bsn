module ActsAsViewable
  def self.included(base)
    base.extend(ClassMethods)
  end

  module ClassMethods
    def acts_as_viewable(options = {})
      include InstanceMethods      
      has_many :viewings, :as => :viewable, :dependent => :destroy
      has_many :viewers, :through => :viewings, :source => :user, :order => 'updated_at DESC'
    end
  end

  module InstanceMethods
    def viewed_by(user)
      if viewers.include?(user)
        viewings.find_by_user_id(user).touch
        return
      end
      unless Viewing.create(:viewable_id => id, :viewable_type => self.class.name, :user => user).new_record?
        if self.respond_to? :views_count
          self.increment!(:views_count)
        end
      end
    end
  end
end
