module MigrationHelpers
  def fk(from_table, from_column, to_table, constraint = nil)
    execute "alter table #{from_table} add constraint #{constraint_name(from_table, from_column)} foreign key (#{from_column}) references #{to_table}(id)"
  end
  def drop_fk(from_table, from_column, constraint = nil)
    execute "alter table #{from_table} drop foreign key #{constraint || constraint_name(from_table, from_column)}"
  end
  def constraint_name(table, column)
    "#{table}_#{column}_fk"
  end
end
