module AddRoles
  def self.create
    Role::ROLES.each {|name|  Role.find_or_create_by_name(name) }
  end
end
