module Constants
  module Search
    FILTER = {
      :all => 'All',
      :people => 'People',
      :companies => 'Companies',
      :brands => 'Brands',
      :agencies => 'Agencies',
      :posts => 'Posts'
    }
    MODELS = {
      :people => User,
      :companies => Company,
      :brands => Brand,
      :agencies => Agency,
      :posts => Post
    }
    DISTINCT_MODELS  = {
      :people => User,
      :companies => Company,
      :posts => Post
    }
  end

  module Messages
    SORT = {
      :from_new_to_old => 'From New to Old',
      :from_old_to_new => 'From Old to New'
    }
    FILTER_BY_USER_ROLE = {
      :seller => 'Seller',
      :publisher => 'Publisher',
      :buyer => 'Buyer'
    }
    SEARCH_BY = {
      :unread => 'Unread Messages',
      :archived => 'Archived Messages',
      :sent => 'Sent Messages'
    }
    CONVERSATIONS_PER_PAGE = 15
  end
end
