module LinkedInFetcher
  class CompanyAPI

    COMPANY_URL = "http://api.linkedin.com/v1/companies/"

    attr_accessor :linkedin_company_id, :access_token

    def initialize(token, secret, linkedin_company_id)
      raise "arguments should be defined" unless [token, secret, linkedin_company_id].all?

      token               = token
      secret              = secret
      @linkedin_company_id = linkedin_company_id

      consumer = OAuth::Consumer.new(LINKEDIN_CONSUMER_KEY, LINKEDIN_CONSUMER_SECRET)
      @access_token = OAuth::AccessToken.new(consumer, token, secret)
    end

    def import_fields!
      response = access_token.get("#{LinkedInFetcher::CompanyAPI::COMPANY_URL}#{linkedin_company_id}:(logo-url,description,website-url)?format=json")
      decoded_response = ActiveSupport::JSON.decode(response.body)
      company = Company.find_by_linkedin_id(linkedin_company_id)
      company.update_attributes(:description => decoded_response["description"],
                                :website => decoded_response["websiteUrl"],
                                :linkedin_logo => decoded_response["logoUrl"])
    end
  end
end
