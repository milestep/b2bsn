module ActsAsClickable
  def self.included(base)
    base.extend(ClassMethods)
  end

  module ClassMethods
    def acts_as_clickable(options = {})
      include InstanceMethods      
      has_many :clicks, :as => :clickable, :dependent => :destroy
      has_many :clickers, :through => :clicks, :source => :user, :order => 'updated_at DESC'
    end
  end

  module InstanceMethods
    def clicked_by(user)
      # ignore clicks on my own stuff
      return if self.respond_to?(:user) && self.user == user
      
      if clickers.include?(user)
        clicks.find_by_user_id(user).touch
        return
      end
      unless Click.create(:clickable_id => id, :clickable_type => self.class.name, :user => user, :url => self.url).new_record?
        if self.respond_to? :clicks_count
          self.increment!(:clicks_count)
        end
      end
    end
  end
end
