module Sunspot
  class Searching

    attr_reader :filter_param, :search_text
    attr_accessor :search_results

    def initialize(filter_param, search_text)
      @filter_param = filter_param.to_sym
      @search_text = search_text
      @search_results = {}
    end

    def produce_search(opts = { :limit => 0 })
      text = @search_text
      search_by(@filter_param).each do |model|
        results = model.solr_search do
          fulltext text
        end.results
        results = results.first(opts[:limit].to_i) if opts[:limit].to_i > 0
        self.search_results.merge!({ Constants::Search::MODELS.invert[model] => results })
      end
      self.search_results[:search_text] = @search_text if search_results
      search_results
    end

    def found?
      search_results.each do |key, value|
        return true if !value.empty? && key != :search_text
      end
      false
    end

    private

    def search_by(filter_param)
      models = filter_param == :all ? Constants::Search::DISTINCT_MODELS.values : [Constants::Search::MODELS[filter_param]]
      models.compact
    end
  end
end
