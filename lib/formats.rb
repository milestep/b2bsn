module Formats
  EMAIL = /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i
  PHONE = /\A((\(\d{3}\) ?)|(\d{3}[-\.]))?\d{3}[- \.]\d{4}(\s(x\d+)?){0,1}\Z/i
  CITY = /^[a-zA-Z\s-]*$/
end
