namespace :user_profile do
  desc "Remove avatar url for users if the avatar url not find file"
  task :remove_user_avatar => :environment do
    UserProfile.all.each do |profile|
      unless profile.avatar.file.try(:exists?)
        UserProfile.connection.execute("UPDATE user_profiles SET avatar = NULL WHERE id = #{profile.id}")
      end
    end
  end
end
