namespace :experiences do
  desc "Set current flag for current position"
  task :set_current_positions => :environment do
    if Experience.first.current.nil?
      UserProfile.all.each do |user_profile|
        user_profile.experiences.each{|e| e.update_attribute(:current, e.end_date.nil?)}
      end
    end
  end
end
