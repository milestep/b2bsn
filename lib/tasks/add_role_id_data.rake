namespace :add_role_id_data do
  hash = { 'Invitation' => 'invite_role', 'User' => 'role', 'Badge' => 'role' }
  desc "Set data in role_user_id for table user"
  task :create => :environment do
    hash.each do |key, value|
      if key.constantize.column_names.include?(value)
        key.constantize.all.each do |u|
          if u.read_attribute(value).present?
            role_id = Role.find_by_name(u.read_attribute(value)).id
            u.role_id = role_id
            u.save
          end
        end
      end
    end
  end
end
