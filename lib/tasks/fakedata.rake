namespace :db do
  desc "Generate fake data and load into db"
  task :generate_fake_data => :environment do
    18.times do
      case rand(3)
      when 0
        co = Agency.create(:name => Faker::Company.name, :description => Faker::Lorem.paragraph(5))
        role = Role.find_or_create_by_name(Role::SELLER)
      when 1
        co = Buyer.create(:name => Faker::Company.name, :description => Faker::Lorem.paragraph(5))
        role = Role.find_or_create_by_name(Role::BUYER)
      when 2
        co = Publisher.create(:name => Faker::Company.name, :description => Faker::Lorem.paragraph(5))
        role = Role.find_or_create_by_name(Role::PUBLISHER)
      end
      co.locations.create(
        :name => "Bay Area",
        :address => Address.new(
          :street => Faker::Address.street_address,
          :city => "San Francisco",
          :state_abbrev => "CA",
          :postal_code => "94105",
          :country_code => 'us',
          :phones => [Phone.new(:phone_type => 'Office', :number => Faker::PhoneNumber.phone_number)]
        )
      )
      6.times do
        c = co.contacts.create(
          :first_name => Faker::Name.first_name,
          :last_name => Faker::Name.last_name,
          :email => Faker::Internet.email,
          :title => Faker::Name.title,
          :address => Address.new(
            :street => Faker::Address.street_address,
            :city => Faker::Address.city,
            :state_abbrev => Faker::Address.state_abbr,
            :postal_code => Faker::Address.zip_code,
            :country_code => 'us',
            :phones => [Phone.new(:phone_type => 'Office', :number => Faker::PhoneNumber.phone_number)]
          )
        )
        if rand(3) == 0
          u = c.create_user!(
            :first_name => c.first_name,
            :last_name => c.last_name,
            :role_id => role.id,
            :email => c.email
          )
          UserProfile.create(
            :user_id => u.id,
            :company_id => co.id,
            :title => c.title,
            :description => Faker::Lorem.paragraph(10)
          )
        end
      end
    end
    User.all.each do |user|
      la = user.circles.create(:name => "LA Folks")
      pub = user.circles.create(:name => "Publisher Folks")
      nyc = user.circles.create(:name => "NYC")
      southeast = user.circles.create(:name => "Southeast Region")
      User.where(:id != user.id).order("rand()").limit(rand(12)).each do |friend|
        user.connections.create(:friend => friend, :status => %w(pending accepted accepted)[rand(3)], :responded_at => Time.now)
        la.users << friend if rand(3).zero?
        pub.users << friend if rand(4).zero?
        nyc.users << friend if rand(5).zero?
        southeast.users << friend if rand(6).zero?
      end
      Organization.order("rand()").limit(rand(7)).each do |org|
        user.memberships.create(:organization => org, :status => %w(pending validated)[rand(2)])
      end
    end
    40.times do
      case rand(2)
      when 0
        TimelineEvent.create(
          :event_type => 'midb_contact_update',
          :actor => User.order("rand()").limit(1).first,
          :subject => Contact.order("rand()").limit(1).first
        )
      when 1
        TimelineEvent.create(
          :event_type => 'midb_company_update',
          :actor => User.order("rand()").limit(1).first,
          :subject => Company.order("rand()").limit(1).first
        )
      end
    end
  end
end
