namespace :ignore_list do
  desc "Check and remove expired users from ignore list"
  task :remove_expired_users => :environment do
    expired_users = IgnoreListUser.expired
    expired_users.each do |eu|
      Connection.find_by_user_id_and_friend_id(eu.ignored_user_id, eu.user).destroy
      eu.destroy
    end
  end
end