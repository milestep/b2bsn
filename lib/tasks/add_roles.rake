namespace :add_roles do
  desc "Creates roles for users"
  task :create => :environment do
    AddRoles.create
  end
end
