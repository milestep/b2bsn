namespace :activity_items do
  desc "Update activity items"
  task :update => :environment do
    activity_types = {
      'status_update'  => 'TimelineEvents::StatusUpdated',
      'news_viewed' => 'TimelineEvents::ClickCreated',
      'question_asked' => 'TimelineEvents::QuestionAsked',
      'profile_update' => 'TimelineEvents::ProfileUpdated',
      'midb_agency_created' => 'TimelineEvents::MidbCompanyCreated'
    }
    TimelineEvent.find_each do |event|
      event_type = activity_types[event.event_type]
      begin
        if event_type && event.type.blank?
          event.update_attribute(:type, event_type)
          print ('.').green
        elsif event_type.blank?
          puts "undefined type '#{event.event_type}'".red
        end
      rescue NoMethodError
        break puts ("Please run 'rake db:migrate' before task").red
      end
    end
    puts 'completed'.green
  end
end
