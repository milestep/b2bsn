task :ci => :environment do
  Rake::Task['spec'].invoke
  Rake::Task['jasmine:ci'].invoke
end
