namespace :db do
  desc "Contains all scripts for patching production database"
  task :patch => :environment do
    Rake::Task['experiences:set_current_positions'].invoke
  end

  task :revert_patch_changes => :environment do
  end
end
