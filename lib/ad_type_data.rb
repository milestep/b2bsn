module AdTypeData
  def self.create
    data = [
      {:group_name => :rich_media_ad_units, :ad_types => [
        :billboard_970_250, :filmstrip_300_600, :portrait_300_1050, :pushdown_970_90, :sidekick_300_250, :sidekick_300_600,
        :sidekick_970_250, :pushdown_970_90
      ]},
      {:group_name => :other_rich_media, :ad_types => [
        :in_banner_video_300_250, :in_banner_video_180_160, :in_banner_video_160_600, :in_banner_video_728_90, :in_banner_video_300_600,
        :wide_skyscraper_160_600, :leaderboard_728_90 
      ]},
      {:group_name => :uap, :ad_types => [
        :medium_rectangle_300_250, :rectangle_180_150, :wide_skyscraper_160_600, :leaderboard_728_90 
      ]},
      {:group_name => :other_ad_units, :ad_types => [
        :super_leaderboard_970_90, :half_page_300_600, :button2_120_60, :micro_bar_88_31
      ]}
    ]
    data.each do |group_data|
      group_name = group_data[:group_name]
      group = AdTypeGroup.where(:name => group_name).first || AdTypeGroup.create(:name => group_name)
      group_data[:ad_types].each{|ad_type| group.ad_types.create(:name => ad_type) if group.ad_types.where(:name => ad_type).first.blank?}
    end
  end

  def self.destroy
    AdType.destroy_all
    AdTypeGroup.destroy_all
  end
end
