require 'spec_helper'

feature 'sign up page' do

  before do
    visit root_path
  end

  specify 'I should see main poster' do
    page.should have_content("Your Private Collaborative Marketplace")
  end

  specify 'I should see link to log in' do
    page.should have_link("Log In", :href => "/auth/linkedin")
  end

  specify 'I should see link to sign up with LinkedIn account' do
    page.should have_link("Sign Up with a LinkedIn account", :href => "/auth/linkedin")

  end

end
