# This file is copied to spec/ when you run 'rails generate rspec:install'
require 'simplecov'
require 'simplecov-rcov'
SimpleCov.formatter = SimpleCov::Formatter::RcovFormatter
SimpleCov.start 'rails'

ENV["RAILS_ENV"] ||= 'test'
require File.expand_path("../../config/environment", __FILE__)
require 'rspec/rails'
require 'rspec/autorun'
require 'database_cleaner'
require "rack_session_access/capybara"
require 'capybara/rspec'
require 'capybara/rails'
require 'capybara/webkit'
require 'headless'

# Requires supporting ruby files with custom matchers and macros, etc,
# in spec/support/ and its subdirectories.
Dir[Rails.root.join("spec/support/**/*.rb")].each {|f| require f}

RSpec.configure do |config|
  config.before do
    Sunspot.session = Sunspot::Rails::StubSessionProxy.new($original_sunspot_session)
    FactoryGirl.create(:role)
    FactoryGirl.create(:buyer_role)
    FactoryGirl.create(:publisher_role)
  end

  config.before :each, :solr => false do
    Sunspot::Rails::Tester.start_original_sunspot_session
    Sunspot.session = $original_sunspot_session
    Sunspot.remove_all!
  end

  Rails.cache.clear

  # ## Mock Framework
  #
  # If you prefer to use mocha, flexmock or RR, uncomment the appropriate line:
  #
  # config.mock_with :mocha
  # config.mock_with :flexmock
  # config.mock_with :rr

  # Remove this line if you're not using ActiveRecord or ActiveRecord fixtures
  config.fixture_path = "#{::Rails.root}/spec/fixtures"

  # If you're not using ActiveRecord, or you'd prefer not to run each of your
  # examples within a transaction, remove the following line or assign false
  # instead of true.
  config.use_transactional_fixtures = true

  # If true, the base class of anonymous controllers will be inferred
  # automatically. This will be the default behavior in future versions of
  # rspec-rails.
  config.infer_base_class_for_anonymous_controllers = false
  config.include ActionView::TestCase::Behavior, example_group: {file_path: %r{spec/presenters}}
  config.include ActionView::TestCase::Behavior, example_group: {file_path: %r{spec/lib}}

  if ENV['JS'].present? || ENV['JSONLY'].present? || ENV["RAILS_ENV"] == 'jenkins'
    # Capybara for tests with JS functionality
    Capybara.javascript_driver = :webkit

    if ENV["RAILS_ENV"] == 'jenkins'
      headless = Headless.new
      headless.start
    end

    DatabaseCleaner.strategy = :truncation
    config.use_transactional_fixtures = false
    config.before(:each) { DatabaseCleaner.start }
    config.after(:each) { DatabaseCleaner.clean }
    config.before(:suite) { DatabaseCleaner.strategy = :truncation }
    if ENV['JSONLY'].present?
      config.filter_run_including :js => true if ENV['JSONLY'].present?
      puts 'Running JavaScript specs only...'
    else
      puts 'Running all specs...'
    end
    
  else
    puts 'Hey!'
    puts 'By default RSpec runs all except JavaScript Capybara based specs. To run ALL specs please provide \'JS=true\' command line option. To run ONLY JavaScript related specs please provide \'JSONLY=true\' option.'
    puts
    config.filter_run_excluding :js => true
    puts 'Skipping JavaScript Capybara related specs...'
  end

  OmniAuth.config.test_mode = true
end

FactoryGirl.duplicate_attribute_assignment_from_initialize_with = false
