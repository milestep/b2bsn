require "spec_helper"

feature "Company Profiles" do

  let(:user) { FactoryGirl.create(:user) }
  let(:agency) { FactoryGirl.create(:agency, :description => "Test message") }
  let(:advertiser) { FactoryGirl.create(:advertiser, :description => "Test message") }
  let(:publisher) { FactoryGirl.create(:publisher, :description => "Test message") }

  before(:each) { visit(simulate_login_path(user.id)) }

  scenario "User should company name" do
    visit(tools_path)
    find_link("View Profile").click
    visit(tool_path(agency))
    find(".profile_name").should have_content(agency.name)
  end

  scenario "User should see description" do
    visit(tools_path)
    find_link("View Profile").click
    visit(tool_path(agency))
    find(".description").should have_content(agency.description)
  end

  scenario "User should see description" do
    visit(tools_path)
    find_link("View Profile").click
    visit(tool_path(agency))
    find(".user_logo").should have_css("img", "//img[@alt=#{agency.type}]")
  end

  scenario "User should see tabs navigation for agency" do
    visit(tools_path)
    find_link("View Profile").click
    visit(tool_path(agency))
    find(".nav-tabs").should have_link("Brands")
    find(".nav-tabs").should have_link("Organization")
  end

  scenario "User should see tabs navigation for advertiser" do
    visit(tools_path)
    find_link("View Profile").click
    visit(tool_path(advertiser))
    find(".nav-tabs").should have_link("Brands")
    find(".nav-tabs").should have_link("Organization")
    find(".nav-tabs").should have_link("Agencies")
  end

  scenario "User should see tabs navigation for publisher" do
    visit(tools_path)
    find_link("View Profile").click
    visit(tool_path(publisher))
    find(".nav-tabs").should have_link("Accepted Units")
    find(".nav-tabs").should have_link("Organization")
    find(".nav-tabs").should have_link("Traffic Profile")
  end
end
