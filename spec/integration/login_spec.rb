require "spec_helper"

feature "Login" do

  let(:user) { FactoryGirl.create(:user) }
  let!(:user_profile) { FactoryGirl.create(:user_profile, :user => user) }

  scenario "user should see links on the navigation bar if login" do
    visit(simulate_login_path(user.id))
    visit(tools_path)
    expect(find('.group-menu.nav-collapse')).to have_link("Connections")
    expect(find('.group-menu.nav-collapse')).to have_link("Orders")
    expect(find('.group-menu.nav-collapse')).to have_link("Inventory")
    expect(find('.group-menu.nav-collapse')).to have_link("Reports")
    expect(find('.group-menu.nav-collapse')).to have_link("Feeds")
  end

  scenario "user should not see links on the navigation bar if not login" do
    visit(login_path)
    expect(find('.container-fluid')).not_to have_link("Connections")
    expect(find('.container-fluid')).not_to have_link("Orders")
    expect(find('.container-fluid')).not_to have_link("Inventory")
    expect(find('.container-fluid')).not_to have_link("Reports")
    expect(find('.container-fluid')).not_to have_link("Feeds")
  end

end
