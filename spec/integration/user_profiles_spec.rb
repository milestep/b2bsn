require "spec_helper"

feature "User Profiles" do

  let(:user) { FactoryGirl.create(:user) }
  let(:user2) { FactoryGirl.create(:user) }
  let!(:badge) { FactoryGirl.create(:badge, :name => "Social Butterfly") }

  before(:each) { visit(simulate_login_path(user.id)) }

  scenario "User should see bar status for achievement" do
    pending("Need to define Achievements")
    visit(tools_path)
    find_link('beanstock_logo').click
    find_link("Achievements").click
    page.html.should have_css('.bar')
  end

  scenario "User should see badge name on the tab achievements" do
    pending("Need to define Achievements")
    visit(tools_path)
    find_link('beanstock_logo').click
    find_link("Achievements").click
    find(".name").should have_content(badge.name)
  end

  scenario "User should see two links on the tab achievements" do
    pending("Need to define Achievements")
    visit(tools_path)
    find_link('beanstock_logo').click
    find_link("Achievements").click
    page.html.should have_link("Activity")
    page.html.should have_link("All")
  end

  scenario "User should see active link 'all' on the tab achievements" do
    pending("Need to define Achievements")
    visit(tools_path)
    find_link('beanstock_logo').click
    find_link("Achievements").click
    page.html.should have_xpath("//a[@class='active']")
  end

  scenario "User should see user full name on the tab achievements" do
    pending("Need to define Achievements")
    visit(tools_path)
    find_link('beanstock_logo').click
    find_link("Achievements").click
    find(".name_activity").should have_content("#{user2.first_name}'s")
  end

end
