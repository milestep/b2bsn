require "spec_helper"

feature "Feeds" do

  let(:user) { FactoryGirl.create(:user) }
  let(:news_item) { FactoryGirl.create(:news_item) }

  before(:each) { visit(simulate_login_path(user.id)) }

  scenario "User should see feeds" do
    Rails.cache.clear and news_item
    visit(tools_path)
    find_link('Feeds').click
    page.html.should have_css('.main_news_container')
    page.html.should include('Feeds')
  end

  scenario "User should see feeds link in navigation bar" do
    visit(tools_path)
    page.html.should have_css('.navbar-inner')
    page.html.should have_link('Feeds')
  end
end
