require "spec_helper"

feature "Connections" do
  let!(:role) { FactoryGirl.create(:role) }
  let(:user) { FactoryGirl.create(:user, :role => role) }
  let(:user_profile) { FactoryGirl.create(:user_profile, :user => user) }
  let!(:company) { FactoryGirl.create(:company, :user_profiles => [user_profile]) }

  before(:each) { visit(simulate_login_path(user.id)) }

  scenario "User should see company logo of his company", :js => true do
    visit(tools_path)
    expect(page.html.include?(user.company.linkedin_logo)).to be_true
  end
end
