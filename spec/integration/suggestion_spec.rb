require "spec_helper"

feature "Suggested Users" do

  let(:user) { FactoryGirl.create(:user) }

  before(:each) { visit(simulate_login_path(user.id)) }

  scenario "User should not see suggestions widget" do
    visit(suggestions_path)
    page.html.should_not include('suggested_users_widget')
  end

end
