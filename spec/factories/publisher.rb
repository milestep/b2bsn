FactoryGirl.define do

  factory :publisher do
    sequence :name do |n|
      "Publisher Company ##{n}"
    end
  end

end
