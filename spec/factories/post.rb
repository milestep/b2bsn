FactoryGirl.define do

  factory :post do
    title "PostTitle"
    association :role, :factory => :role
    association :user, :factory => :user
  end

  factory :question, :parent => :post do
    post_type 'Question'
  end

  factory :status, :parent => :post do
    post_type 'Status'
  end

  factory :private_question, :parent => :post do
    post_type 'Question'
    visibility 'private'
  end

  factory :public_question, :parent => :post do
    post_type 'Question'
    visibility 'public'
  end

end
