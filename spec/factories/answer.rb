FactoryGirl.define do

  factory :answer do
    answer "I like it."
    association :user, :factory => :user, :first_name => "UserForAnswer", :last_name => "UserForAnswer"
    association :post, :factory => :post, :title => "Post in answer."
  end

end
