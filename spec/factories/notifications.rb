FactoryGirl.define do
  factory :notification do
    association :sender, :factory => :user
    subject     "Notification Subject"
    body        "Notification Body"
  end
end
