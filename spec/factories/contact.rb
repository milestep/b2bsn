FactoryGirl.define do

  factory :contact do
    association :company
    association :address
    association :user
    first_name "John"
    last_name "Smith"
    email Faker::Internet.email
    title "title"
  end

end
