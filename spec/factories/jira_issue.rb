FactoryGirl.define do
  factory :feature_request, :class => FeatureRequest do
    title 'Fix it ASAP'
    description 'smth goes wrong'
    
    user
  end
  
  factory :support_request, :class => SupportRequest do
    title 'Implement this ASAP'
    description 'amazing feature'
    
    user
  end
end
