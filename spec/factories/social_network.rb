FactoryGirl.define do

  factory :social_network do
    name 'Twitter'
    provider 'twitter'
    consumer_key 'key'
    consumer_secret 'secret'
  end

end
