FactoryGirl.define do

  factory :package do
    brand_restriction Faker::Lorem.words(1).first
    cpm 10.0
    demo_targeting_available true
    description Faker::Lorem.sentence
    dma_targeting_available true
    flight_end_date Date.tomorrow
    flight_start_date Date.today
    name Faker::Lorem.words(1).first
    total_cost 10
    total_impressions 10
    association :packagable, :factory => :user
  end

end
