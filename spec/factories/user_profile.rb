FactoryGirl.define do

  factory :user_profile do
    association :user, :factory => :user, :first_name => "UserFirstName", :last_name => "UserLastName"
    association :company, :factory => :company, :name => "UserCompany"
    title "Lead Operations Technician"
  end

  factory :another_user_profile, :parent => :user_profile do
    association :company, :factory => :company, :name => Faker::Company.name
  end

  factory :comment_user_profile, :parent => :user_profile do
    association :company, :factory => :company, :name => Faker::Company.name
  end

end
