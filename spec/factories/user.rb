FactoryGirl.define do

  factory :user do
    first_name "John"
    last_name "Smith"
    email Faker::Internet.email
    password "password"
    password_confirmation "password"
    uid "GVdfT45Dc"
    association :role, :factory => :role, :name => "user"

    after(:build) { |user| user.class.skip_callback(:create, :after, :create_user_profile) }
  end

  factory :admin, :class => User do
    first_name "Admin"
    last_name "Boss"
    email Faker::Internet.email
    password "password"
    password_confirmation "password"
    association :role, :factory => :role, :name => "admin"
  end

  factory :publisher_user, :class => User do
    first_name Faker::Name.first_name
    last_name Faker::Name.last_name
    email Faker::Internet.email
    password "password"
    password_confirmation "password"
    association :role, :factory => :publisher_role
  end

  factory :user_params, :class => User do
    email Faker::Internet.email
  end
end
