FactoryGirl.define do
  factory :message do
    association :conversation, :factory => :conversation
    association :sender, :factory => :user
    type        "Message"
    body        "Some test message body."
    subject     "Subject"
  end
end
