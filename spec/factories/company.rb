FactoryGirl.define do

  factory :company do
    name Faker::Company.name
    type "Publisher"
    description Faker::Lorem.sentence
    phone '(123) 456-7890'
    email Faker::Internet.email
    website { "#{name.gsub(/[\W_]+/, '-').downcase}.com" }
    number_of_employees 100
    annual_revenue '$10000'
    linkedin_logo 'http://image.png'
    linkedin_id 592431
  end

end
