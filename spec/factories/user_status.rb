FactoryGirl.define do

  factory :user_status do
    status UserStatus::OFFLINE
    association :user
  end

  factory :online_status, :parent => :user_status do
    status UserStatus::ONLINE
  end

  factory :away_status, :parent => :user_status do
    status UserStatus::AWAY
  end

  factory :do_not_disturb_status, :parent => :user_status do
    status UserStatus::DO_NOT_DISTURB
  end

  factory :offline_status, :parent => :user_status do
    status UserStatus::OFFLINE
  end

  factory :invisible_status, :parent => :user_status do
    status UserStatus::INVISIBLE
  end
end