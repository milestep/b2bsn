FactoryGirl.define do
  factory :receipt do
    association :notification
    association :receiver
    association :message
  end
end
