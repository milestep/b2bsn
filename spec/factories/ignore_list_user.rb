FactoryGirl.define do

  factory :ignore_list_user do
    association :user
    association :ignored_user, :factory => :user
  end

end