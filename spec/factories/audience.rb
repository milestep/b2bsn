FactoryGirl.define do

  factory :audience do
    name Faker::Name.name
  end

end
