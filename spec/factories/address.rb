FactoryGirl.define do

  factory :address do
    street Faker::Address.street_name
    street2 Faker::Address.secondary_address
    city 'San Francisco'  
    state_abbrev 'CA'
    postal_code Faker::Address.zip_code
    country_code Faker::Address.uk_postcode
  end

  factory :address_without_state_abbrev, :parent => :address do
    state_abbrev nil
  end

  factory :address_without_city, :parent => :address do
    city nil
  end

end
