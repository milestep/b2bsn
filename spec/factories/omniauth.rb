FactoryGirl.define do

  factory :omniauth, :class => OmniAuth::AuthHash do
    provider 'linkedin'
    uid '1'
    info {{ :first_name => Faker::Name.first_name , :last_name => Faker::Name.last_name }}
    credentials {{ :token => 'token', :secret => 'secret' }}
  end

end
