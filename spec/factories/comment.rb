FactoryGirl.define do

  factory :comment do
    comment Faker::Lorem.sentence
    association :commentable, :factory => :post
    association :user
  end

end

