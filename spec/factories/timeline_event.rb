FactoryGirl.define do

  factory :timeline_event do
    association :actor, :factory => :user
    association :subject, :factory => :post
    event_type "status_update"
  end

  factory :timeline_event_click_created, :class => TimelineEvents::ClickCreated do
    association :actor, :factory => :user
    event_type 'news_viewed'
  end

  factory :timeline_event_profile_updated, :class => TimelineEvents::ProfileUpdated do
    association :actor, :factory => :user
    association :subject, :factory => :user_profile
    event_type 'profile_updated'
    
    factory :timeline_events_with_created_at do
      created_at { 1.day.ago }
      event_type 'profile_update'
    end
  end

  factory :timeline_event_package_updated, :class => TimelineEvents::PackageUpdated do
    association :actor, :factory => :user
    association :subject, :factory => :package
    event_type 'package_updated'
  end

  factory :timeline_event_question_asked, :class => TimelineEvents::QuestionAsked do
    association :actor, :factory => :user
    association :subject, :factory => :post
    event_type 'question_asked'
  end

  factory :public_timeline_event_question_asked, :class => TimelineEvents::QuestionAsked do
    association :actor, :factory => :user
    association :subject, :factory => :public_question
    event_type 'question_asked'
  end

  factory :private_timeline_event_question_asked, :class => TimelineEvents::QuestionAsked do
    association :actor, :factory => :user
    association :subject, :factory => :private_question
    event_type 'question_asked'
  end

  factory :timeline_event_question_answered, :class => TimelineEvents::QuestionAnswered do
    association :actor, :factory => :user
    association :subject, :factory => :answer
    event_type 'question_answered'
  end

  factory :timeline_event_status_updated, :class => TimelineEvents::StatusUpdated do
    association :actor, :factory => :user
    association :subject, :factory => :post
    event_type 'status_updated'
  end

  factory :timeline_event_midb_company_created, :class => TimelineEvents::MidbCompanyCreated do
    association :actor, :factory => :user
    event_type 'midb_company_created'
  end

  factory :timeline_event_midb_company_updated, :class => TimelineEvents::MidbCompanyUpdated do
    association :actor, :factory => :user
    event_type 'midb_company_updated'
  end

  factory :timeline_event_midb_company_deleted, :class => TimelineEvents::MidbCompanyDeleted do
    association :actor, :factory => :user
    event_type 'midb_company_deleted'
  end

  factory :timeline_event_comment_created, :class => TimelineEvents::CommentCreated do
    association :actor, :factory => :user
    association :subject, :factory => :comment
    event_type 'comment_created'
  end

end
