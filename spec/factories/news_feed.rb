FactoryGirl.define do

  factory :news_feed do
    title Faker::Lorem.words(1).first
    description Faker::Lorem.sentence
  end

end
