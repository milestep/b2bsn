FactoryGirl.define do

  factory :badge do
    name Faker::Name.name
    association :role, :factory => :role
  end

end
