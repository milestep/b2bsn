FactoryGirl.define do

  factory :company_location do
    name Faker::Company.name
    association :company, :factory => :company
    association :address, :factory => :address
  end

end

