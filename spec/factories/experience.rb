FactoryGirl.define do

  factory :experience do
    end_date Date.today
    start_date Date.yesterday
    company_name Faker::Company.name
    location "Kyiv"
    association :user_profile
    association :company
  end

end
