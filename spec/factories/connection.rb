FactoryGirl.define do

  factory :connection do
    association :user, :factory => :user, :first_name => "UserName", :last_name => "UserLastName"
    association :friend, :factory => :user, :first_name => "FriendName", :last_name => "FriendLastName"
    status "accepted"
    message "Add me please to your friends list!"
  end

  factory :pending_connection, :parent => :connection do
    status "pending"
  end

end
