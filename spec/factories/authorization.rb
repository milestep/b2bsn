FactoryGirl.define do

  factory :authorization do
    association :user
    association :social_network
    provider 'twitter'
    profile_url 'http://www.twitter.com/TestUser'
    uid '1'
  end

end
