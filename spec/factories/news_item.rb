FactoryGirl.define do

  factory :news_item do
    title Faker::Lorem.words(1).first
    description Faker::Lorem.sentence(100)
    author Faker::Name.name
    date_published Time.now

    association :news_feed
  end

end
