FactoryGirl.define do

  factory :phone do
    number "4444444444"
    phone_type "Office"
  end

end
