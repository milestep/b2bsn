FactoryGirl.define do

  factory :brand_advertiser do
    association :brand
    association :advertiser
  end

end
