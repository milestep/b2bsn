FactoryGirl.define do

  factory :timeline_event_user_deletion do
    association :user, :factory => :user
    association :timeline_event, :factory => :timeline_event
  end


end
