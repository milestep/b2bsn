FactoryGirl.define do

  factory :role do
    name Role::SELLER
    initialize_with { Role.find_or_create_by_name(name) }
  end

  factory :buyer_role, :parent => :role do
    name Role::BUYER
    initialize_with { Role.find_or_create_by_name(name) }
  end

  factory :publisher_role, :parent => :role do
    name Role::PUBLISHER
    initialize_with { Role.find_or_create_by_name(name) }
  end
end
