FactoryGirl.define do

  factory :location, :class => CompanyLocation do
    name Faker::Lorem.words(1).first
  end

  factory :location_for_agency, :class => CompanyLocation do
    name Faker::Lorem.words(2).join
    association :company, :factory => :company, :type => "Agency"
    association :address, :factory => :address
  end

end
