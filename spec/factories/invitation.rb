FactoryGirl.define do

  factory :invitation do
    token "e4988ca257"
    association :role, :factory => :role, :name => Role::SELLER
  end

end
