FactoryGirl.define do

  factory :click do
    url Faker::Internet.domain_name
    association :clickable, :factory => :news_item
    association :user
  end

end
