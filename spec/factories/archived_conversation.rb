FactoryGirl.define do
  factory :archived_conversation do
    association :conversation
    association :user
  end
end
