require "spec_helper"

describe NoticeEvents do
  let(:user) { FactoryGirl.create(:user) }
  let(:another_user) { FactoryGirl.create(:user) }

  let(:question) { FactoryGirl.create(:question, :user => user) }
  let(:answer) { FactoryGirl.create(:answer, :post => question, :user => another_user) }
  

  it "should success again" do
    TimelineEvent.stub(:find).and_return(answer.timeline_event)
    answer.timeline_event.should_receive(:create_notice_events)
    NoticeEvents.perform(answer.timeline_event.id)
  end
end

