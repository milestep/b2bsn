require "spec_helper"

describe UserTimelineEvents do
  let(:actor) { FactoryGirl.create(:user) }
  let(:user) { FactoryGirl.create(:user) }
  let(:status) { FactoryGirl.create(:status, :role => user.role) }

  it "should success again" do
    TimelineEvent.stub(:find).and_return(status.timeline_event)
    status.timeline_event.should_receive(:create_user_timeline_events)
    UserTimelineEvents.perform(status.timeline_event.id)
  end
end
