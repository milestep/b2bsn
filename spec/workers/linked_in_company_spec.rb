require "spec_helper"

describe LinkedInCompany do
  let(:company) { FactoryGirl.create(:company) }
  let(:token) { "some token" }
  let(:secret) { "some secret" }
  let(:link_fetcher){ double('LinkedInFetcher::CompanyAPI') }

  it "should call import_fields!" do
    LinkedInFetcher::CompanyAPI.any_instance.should_receive(:import_fields!)
    LinkedInCompany.perform(token, secret, company.linkedin_id)
  end
end
