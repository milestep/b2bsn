require "spec_helper"

describe LinkedInMess do
  let(:user_profile) { FactoryGirl.create(:user_profile) }
  let(:token) { "some token" }
  let(:secret) { "some secret" }
  let(:connections) { ["6SfgQgyLKp"] }
  let(:linkedin_client){ double('ApiClient::Linkedin') }

  it "should success again" do
    User.any_instance.should_receive(:send_invites_to_linkedin_accounts)

    LinkedInMess.perform(user_profile.user.id, token, secret, connections)
  end
end