require "spec_helper"
require "capybara_helper"

feature "Fine Filter Dialog", :js => true  do

  scenario "as a registered user I want to be able to change my current position title" do
    user = FactoryGirl.create(:user, :first_name => 'John', :last_name => 'Dou')
    login_as(user)

    visit(user_profile_path(user))
    expect(find('div.profile_main_content div.profile_name')).to have_content('John Dou')
    find('div.profile_main_content').click_link('EDIT PROFILE')
    expect(find('#bootstrap_modal div.modal-header h3')).to have_content('Edit Profile Information')
    within("form.edit_user_profile") do
      fill_in 'user_profile_title', :with => 'ROR Developer'
      click_button('SAVE')
    end
    expect(find('div#base_profile_info div.title')).to have_content('ROR Developer')
  end

end