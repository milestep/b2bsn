require "spec_helper"
require "capybara_helper"

feature "Fine Filter Dialog", :js => true  do

  scenario "as a registered user I want to find contacts via detailed filter" do
    login_as(FactoryGirl.create(:user))
    visit(tools_path)
    expect(page).to have_content('Add Connections')
    click_link('Advanced Search')
    expect(page).to have_content('Fine Filter Options')
  end
  
end