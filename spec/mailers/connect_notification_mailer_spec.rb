require 'spec_helper'

describe ConnectNotificationMailer do
  describe '#connect_request' do
    let!(:user) { FactoryGirl.create(:user) }
    let!(:company) { FactoryGirl.create(:company) }
    let(:connection) { {"friend_id" => user.id, "message" => "This connection was created for test."} }
    let(:mail) { ConnectNotificationMailer.connect_request(connection) }
    let(:email) { "beanstock.marketplace@gmail.com" }

    it 'should renders the subject' do
      mail.subject.should == "User connection request for #{user.fullname}"
    end

    it 'renders the receiver email' do
      mail.to.should == [email]
    end

    it 'renders the sender email' do
      mail.from.should == [email]
    end
  end
end
