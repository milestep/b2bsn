require 'spec_helper'

describe Brand do

  let(:brand) { FactoryGirl.create(:brand) }
  let(:new_brand) { FactoryGirl.build(:brand, :agency_brands_attributes => [{:agency_name => 'Brand'}], :brand_advertisers_attributes => [{:advertiser_name => 'Advertiser'}]) }
  let(:advertiser) { FactoryGirl.create(:advertiser) }
  let(:agency) { FactoryGirl.create(:agency) }
  let(:brand_with_advertiser_and_agency) { FactoryGirl.create(:brand, :advertisers => [advertiser], :agencies => [agency]) }

  context "associations" do
    subject { brand }
    it { should have_many(:agency_brands) }
    it { should have_many(:agencies).through(:agency_brands) }
    it { should have_many(:brand_advertisers) }
    it { should have_many(:advertisers).through(:brand_advertisers) }
  end

  context "validations" do
    it "should validate agency presence" do
      invalid_brand = FactoryGirl.build(:brand, :agency_brands_attributes => [{ :agency_id => nil, :agency_name => 'Brand' }])
      invalid_brand.save
      invalid_brand.should have(1).error_on(:agencies)
    end

    it "should validate advertiser presence" do
      invalid_brand = FactoryGirl.build(:brand, :brand_advertisers_attributes => [{ :advertiser_id => nil, :advertiser_name => 'Advertiser' }])
      invalid_brand.save
      invalid_brand.should have(1).error_on(:advertisers)
    end

  end

  it "should create an instance with brand and advertiser" do
    b = Brand.expanded_company
    b.agency_brands.should_not be_blank
    b.brand_advertisers.should_not be_blank
  end

  it "should return company items for profile" do
    brand_with_advertiser_and_agency.company_items_for_profile.should_not be_blank
  end

end
