require 'spec_helper'

describe CompanySite do

  let(:company) { FactoryGirl.create(:company) }
  let(:site) { FactoryGirl.create(:site)}
  let(:company_site) { CompanySite.create(:company => company, :site => site) }

  context "associations" do
    subject { company_site }
    it { should belong_to(:company) }
    it { should belong_to(:site) }
  end

  it "should return site name" do
    company_site.site_name.should eql(site.name)
  end

end
