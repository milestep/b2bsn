require 'spec_helper'

describe TimelineEvent do

  let(:author) { FactoryGirl.create(:user) }
  let(:author_profile) { FactoryGirl.create(:user_profile, :user => author) }
  let(:another_user) { FactoryGirl.create(:user) }
  let(:another_profile) { FactoryGirl.create(:user_profile, :user => another_user) }
  let!(:timeline_event) { FactoryGirl.create(:timeline_event, :actor => author, :subject => author_profile) }
  let!(:status_updated) { FactoryGirl.create(:timeline_event_status_updated) }
  let!(:question_asked) { FactoryGirl.create(:timeline_event_question_asked, :actor => author) }
  let!(:question_answered) { FactoryGirl.create(:timeline_event_question_answered, :actor => author) }
  let!(:another_question_asked) { FactoryGirl.create(:timeline_event_question_asked, :actor => another_user) }
  let(:deleted_timeline_event) { FactoryGirl.create(:timeline_event, :actor => author, :subject => author_profile) }
  let(:alien_timeline_event) { FactoryGirl.create(:timeline_event, :actor => another_user) }
  let(:bunch_of_timeline_events) { 30.times { FactoryGirl.create(:timeline_event, :actor => author)} }
  let(:timeline_event_profile_updated) { FactoryGirl.create(:timeline_event_profile_updated, :actor => author, :subject => author_profile) }
  let(:public_timeline_event_question_asked) { FactoryGirl.create(:public_timeline_event_question_asked) }
  let(:private_timeline_event_question_asked) { FactoryGirl.create(:private_timeline_event_question_asked) }

  let(:question) { FactoryGirl.create(:question, :user => author) }
  let(:answer) { FactoryGirl.create(:answer, :post => question, :user => another_user) }
  let(:post) { FactoryGirl.create(:post) }
  let(:connection) { FactoryGirl.create(:connection, :user => timeline_event_profile_updated.actor, :friend => another_user) }
  let(:ignore_list_user) { FactoryGirl.create(:ignore_list_user, :user => another_user, :ignored_user => author) }

  context 'validations' do
    subject { timeline_event }
    it { should be_valid }
    it { should have_many(:users_notices) }
    it { should have_many(:noticed_users).through(:users_notices) }
  end

  context 'scopes' do
    it "should return events by type" do
      TimelineEvent.by_type(status_updated.event_type).should include(status_updated)
    end

    it "should not return events by another type" do
      TimelineEvent.by_type(status_updated.event_type).should_not include(question_asked)
    end

    it "should return events by actor" do
      TimelineEvent.by_actor(author).should include(question_asked)
    end

    it "should not return events by another actor" do
      TimelineEvent.by_actor(author).should_not include(another_question_asked)
    end

    it "should return last timeline events only for choosen user" do
      TimelineEvent.unscoped.user_newest_timeline_events(author).should_not include(alien_timeline_event)
    end

    it "should not return events from ignoring users" do
      timeline_event and ignore_list_user
      expect(TimelineEvent.all_events_except_ignored_users_for(another_user)).not_to include(author)
    end

    it "should return newest timeline events only for choosen user" do
      # convert to_a because we can't use COUNT(*) for aggregated_timeline query
      # it because we are using GROUP BY instruction
      bunch_of_timeline_events and TimelineEvent.unscoped.user_newest_timeline_events(author).to_a.count.should eql(TimelineEvent::EVENTS_PER_PAGE)
    end

    it "should return only public events" do
      private_timeline_event_question_asked and public_timeline_event_question_asked
      expect(TimelineEvent.public_events).to include(public_timeline_event_question_asked)
      expect(TimelineEvent.public_events).not_to include(private_timeline_event_question_asked)
    end
  end

  context "class methods" do
    describe "#aggregated_timeline" do
      let(:timeline_events_that_we_can_aggregate) do
        FactoryGirl.create :timeline_events_with_created_at, :actor => author,
                                                             :subject => author_profile,
                                                             :created_at => 1.day.ago,
                                                             :to => 'upload photo'
        FactoryGirl.create :timeline_events_with_created_at, :actor => author,
                                                             :subject => author_profile,
                                                             :created_at => 2.days.ago,
                                                             :to => 'basic information'
      end

      it "should aggregate consecutive events" do
        timeline_events_that_we_can_aggregate
        result = TimelineEvent.where(:event_type => 'profile_update').aggregated_timeline.first
        result.type.should eql 'TimelineEvents::AggregatedEvent'
        result.aggregated_events.should include('upload photo', 'basic information')
      end
    end
  end

  describe "#autor" do
    it "should return creator" do
      timeline_event.reload.actor.should eql(author)
    end

    it "should return deleted creator" do
      author.destroy
      timeline_event.reload.actor.should eql(author)
    end
  end

  describe "#scoped_autor" do
    it "should return creator" do
      timeline_event.reload.scoped_actor.should eql(author)
    end

    it "should not return deleted creator" do
      author.destroy
      timeline_event.reload.scoped_actor.should be_nil
    end
  end

  describe "#subject" do
    it "should return author profile" do
      timeline_event.reload.subject.should eql(author_profile)
    end

    it "should return deleted author_profile" do
      author_profile.destroy
      timeline_event.reload.subject.should eql(author_profile)
    end
  end

  describe "#scoped_subject" do
    it "should return author profile" do
      timeline_event.reload.scoped_subject.should eql(author_profile)
    end

    it "should not return deleted author_profile" do
      author_profile.destroy
      timeline_event.reload.scoped_subject.should be_nil
    end
  end

  describe "#actor_fullname" do
    it "should return actor fullname" do
      timeline_event.actor_fullname.should eql(author.fullname)
    end
  end

  describe "#author_name" do
    it "should be nil" do
      expect(timeline_event.author_name).to be_nil
    end
  end

  describe "#create_user_timeline_events" do
    it "should create public user_timeline_event" do
      status_updated.create_user_timeline_events
      expect(another_user.timeline_events).not_to be_blank
    end

    it "should create private user_timeline_event" do
      status_updated.subject.visibility = 'private'
      status_updated.create_user_timeline_events
      expect(another_user.timeline_events).not_to include(status_updated)
    end

    it "should create connections user_timeline_event" do
      status_updated.subject.visibility = 'connections'
      status_updated.create_user_timeline_events
      connection
      expect(another_user.timeline_events).not_to be_blank
    end

  end

  describe "#aggregated_events" do
    before(:each) do
      timeline_event.
        stub!(:updated_list).
        and_return("basic information#{TimelineEvents::Aggregator::SEPARATOR}profile picture")
    end
    subject { timeline_event.aggregated_events }

    it { should include('basic information', 'profile picture') }
  end

  describe "#create_notice_events" do
    it "should create users_notice" do
      answer.timeline_event.create_notice_events
      answer.timeline_event.noticed_users.should_not be_blank
    end
  end

  describe "#notice_for_user" do
    it "should return user_notice for given user" do
      answer_event = answer.timeline_event
      answer_event.create_notice_events
      answer_event.notice_for_user(author).should_not be_nil
      answer_event.notice_for_user(author).unread.should be_true
    end
  end

  describe "#collect_noticed_users" do
    it "should return list of user inside conversation" do
      answer.timeline_event.collect_noticed_users.should eql([author])
    end
  end

  describe "callbacks" do

    it "should initiate callback on question answer" do
      timeline_event_for_question_answer = FactoryGirl.build(:timeline_event, :event_type => 'question_answered')
      timeline_event_for_question_answer.should_receive(:update_parent_timeline_event)
      timeline_event_for_question_answer.save
    end

    it "shouldn't initiate callback on some other event_type" do
      timeline_event_for_question_answer = FactoryGirl.build(:timeline_event, :event_type => 'question_asked')
      timeline_event_for_question_answer.should_not_receive(:update_parent_timeline_event)
      timeline_event_for_question_answer.save
    end

    it "should receive touch on callback" do
      some_other_timeline_event = FactoryGirl.create(:timeline_event)
      timeline_event_for_question_answer = FactoryGirl.build(:timeline_event, :event_type => 'question_answered')
      post.stub(:timeline_event).and_return(some_other_timeline_event)
      timeline_event_for_question_answer.stub_chain(:subject, :post).and_return(post)
      some_other_timeline_event.should_receive(:touch)
      timeline_event_for_question_answer.save
    end

  end

  describe "#posting_path" do
    it "should return path to post of timeline_event_for_question_answer" do
      expect(question_answered.posting_path).to eq("/posts/" + question_answered.subject.post_id.to_s)
    end

    it "should return path to post of timeline_event_for_status_updated" do
      expect(status_updated.posting_path).to eq("/posts/" + status_updated.subject.id.to_s)
    end
  end

  describe "#event_title" do
    it "should return title of timeline_event's subject'" do
      expect(status_updated.event_title).to eq("PostTitle")
    end
  end

  describe "#updated_status" do
    it "should return blank string for question_asked timeline_event" do
      expect(question_asked.updated_status).to eq('')
    end

    it "should return new status of status_updated timeline_event" do
      expect(status_updated.updated_status).to eq(status_updated.to)
    end
  end

end
