require 'spec_helper'

describe JiraIssue do
  context 'associations' do
    subject { FactoryGirl.create(:feature_request) }
    it { should belong_to(:user) }
  end
  
  context 'validations' do
    it 'should validate presece of description' do
      expect(FactoryGirl.build(:feature_request, :description => nil)).not_to be_valid
    end
  end
  
  context 'scopes' do
    describe 'not_sent scope' do
      it 'should return not sent jira issues' do
        FactoryGirl.create_list(:feature_request, 10, :sent_at => nil)
        FactoryGirl.create(:feature_request, :sent_at => 1.day.ago)
        
        expect(JiraIssue.not_sent).to have(10).items
      end
    end
  end
  
  describe "#feature_request?" do
    it "should detect if issues is a feature request" do
      expect(FactoryGirl.create(:feature_request)).to be_feature_request
      expect(FactoryGirl.create(:feature_request)).not_to be_support_request
    end
  end
  
  describe "#support_request?" do
    it "should detect if issues is a support request" do
      expect(FactoryGirl.create(:support_request)).not_to be_feature_request
      expect(FactoryGirl.create(:support_request)).to be_support_request
    end
  end
  
  describe "#subject" do
    it "should use title for generation if exists" do
      expect(FactoryGirl.create(:feature_request, :title => 'My mega title').summary).to match(/My mega title/)
    end
    
    it "should use type for generation if title doesn\'t exists" do
      expect(FactoryGirl.create(:feature_request, :title => nil).summary).to match(/feature request/i)
    end
  end
end
