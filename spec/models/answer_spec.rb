require 'spec_helper'

describe Answer do

  let(:question) { FactoryGirl.create(:question) }
  let(:answer) { FactoryGirl.create(:answer, :post => question) }
  let(:answer_timeline_event) { FactoryGirl.create(:timeline_event_question_answered, :subject => answer) }

  subject { answer }

  context "associations" do
    it { should belong_to(:user) }
    it { should belong_to(:post) }
    it { should have_many(:timeline_events) }
    it { should have_one(:timeline_event) }
  end

  context "validations" do
    it { should validate_presence_of(:user) }
    it { should validate_presence_of(:post) }
    it { should validate_presence_of(:answer)}
  end

  it "should vote score" do
    answer.vote :voter => FactoryGirl.create(:user), :vote => 'like'
    answer.vote :voter => FactoryGirl.create(:user), :vote => 'like'
    answer.vote_score.should eql(2)
  end

  it "should return a zero vote_score" do
    answer.vote_score.should eql(0)
  end

  it "should destroy dependent timeline event" do
    answer_timeline_event
    answer.timeline_events.should_not be_empty
    timeline_events_ids = answer.timeline_events.map(&:id)
    answer.destroy
    TimelineEvent.where(:id => timeline_events_ids).should be_empty
  end

  describe "#title" do
    it "should return a text of answer" do
      answer.title.should eql(answer.answer)
    end
  end

  describe "#post_timeline_event" do
    it "should return event for appropriate question" do
      answer.post_timeline_event.should eq(question.timeline_event)
    end
  end

end
