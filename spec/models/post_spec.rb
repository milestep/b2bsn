require 'spec_helper'

describe Post do

  let(:user) { FactoryGirl.create(:user) }
  let(:user_profile) { FactoryGirl.create(:user_profile, :user => user) }
  let(:post) { FactoryGirl.create(:post, :user => user) }
  let(:first_friend) { FactoryGirl.create(:user) }
  let(:second_friend) { FactoryGirl.create(:user) }

  context 'validations' do
    it "should validate min title length and fail" do
      expect { FactoryGirl.create(:post, :user => user, :title => '') }.to raise_error(ActiveRecord::RecordInvalid)
    end
    it "should validate min title length and pass" do
      expect { FactoryGirl.create(:post, :user => user, :title => 'a' * Post::MIN_TITLE_LEN) }.to_not raise_error(ActiveRecord::RecordInvalid)
    end
  end

  context 'associations' do
    subject { post }
    it { should belong_to(:user) }
    it { should have_many(:answers) }
    it { should have_many(:recent_answers) }
    it { should have_many(:post_circles) }
    it { should have_many(:circles).through(:post_circles) }
    it { should have_many(:timeline_events) }
    it { should have_one(:timeline_event) }
    it { should respond_to(:user_profile_avatar) }
    it { should respond_to(:user_fullname) }
    it { should respond_to(:user_profile) }
    it { should respond_to(:online_status) }
    it { should have_many(:posts_users) }
    it { should have_many(:mentioned_users).through(:posts_users) }
  end

  it "should return true if post is a question" do
    post.post_type = "Question"
    post.is_question?.should be_true
  end

  it "should return false if post is not a question" do
    post.post_type = "Update"
    post.is_question?.should be_false
  end

  it "should normilize the url attribute" do
    expect { post.update_attributes(:url => 'google.com') }.to change(post, :url).to('http://google.com')
  end

  describe "#user" do
    it "should return user" do
      post.reload.user.should eql(user)
    end

    it "should return deleted user" do
      user.destroy
      post.reload.user.should eql(user)
    end
  end

  describe "#scoped_user" do
    it "should return user" do
      post.reload.scoped_user.should eql(user)
    end

    it "should not return deleted user" do
      user.destroy
      post.reload.scoped_user.should be_nil
    end

  end

  describe "user association" do

    it "should delegate fullname to user" do
      post.user_fullname.should eq(user.fullname)
    end

    it "should delegate online status to user" do
      post.online_status.should eq(user.online_status)
    end

    it "should delegate profile to user" do
      user.user_profile = user_profile
      post.user_profile.should eq(user_profile)
    end

  end

  describe "#user_fullname" do
    it "should return user fullname" do
      post.user_fullname.should eql(user.fullname)
    end
  end

  describe "search formatters" do

    subject { post }
    it { should respond_to(:search_title) }
    it { should respond_to(:full_search_title) }

  end

  describe "#add_mentioned_users" do
    it "should assign mentioned users to post" do
      User.stub(:friend_summaries).with(user).and_return([first_friend, second_friend])
      post.add_mentioned_users([second_friend.id])
      post.reload.mentioned_users.should include(second_friend)
    end
  end

end
