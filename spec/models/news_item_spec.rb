require 'spec_helper'

describe NewsItem do
  let(:news_feed) { FactoryGirl.create(:news_feed) }
  let(:news_item) { FactoryGirl.create(:news_item, :news_feed => news_feed) }
  let!(:first_click) { FactoryGirl.create(:click, :clickable => news_item, :created_at => 1.minute.ago ) }
  let!(:second_click) { FactoryGirl.create(:click, :clickable => news_item) }
  let(:old_news_item) { FactoryGirl.create(:news_item, :created_at => (NewsItem::DAYS_AMOUNT + 1).days.ago) }
  let(:old_deals_item) { FactoryGirl.create(:news_item, :created_at => (NewsItem::DAYS_AMOUNT + 1).days.ago, :news_feed_id => NewsFeed::DEAL_FEED_ID) }

  context 'associations' do
    subject { news_item }
    it { should belong_to(:news_feed) }
    it { should have_many(:timeline_events).through(:clicks) }
  end

  context "scopes" do
    it "excludes news that oldest than #{NewsItem::DAYS_AMOUNT} days" do
      NewsItem.latest_news.should_not include(:old_news_item)
    end

    it "excludes deals that oldest than #{NewsItem::DAYS_AMOUNT} days" do
      NewsItem.latest_deals.should_not include(:old_deals_item)
    end

    it "should return latest news" do
      news_item
      old_news_item
      NewsItem.newest.first.should eq(news_item)
    end

    it "should return formatted date" do
      news_item.update_attributes(:date_published => DateTime.parse("2012-01-01 10:10:10"))
      news_item.formatted_date.should eq("01 Sun")
    end

    it "should return formatted description, description include simple text" do
      TextTruncater.any_instance.should_receive(:sanitized_first_paragraph).once
      news_item.formatted_description
    end
  end

  describe "#timeline_event" do
    it "should return last timeline event" do
      news_item.timeline_event.should eq(second_click.timeline_event)
    end
  end

  it "should return true if news item image is default" do
    NewsItem.any_instance.should_receive(:image_url).once.and_return(NewsItem::NAMES_OF_DEFAULT_IMAGES.first)
    expect(news_item.default_image?).to be_true
  end

  it "should return false if news item image is realy existing" do
    NewsItem.any_instance.should_receive(:image_url).once.and_return("some.png")
    expect(news_item.default_image?).to be_false
  end
end