require 'spec_helper'

describe Package do
  let(:user_profile) { FactoryGirl.create(:user_profile) }
  let(:skill) { FactoryGirl.create(:skill, :user_profiles => [user_profile]) }

  subject { skill }

  context 'associations' do
    it { should have_and_belong_to_many(:user_profiles) }
  end

  context 'validations' do
    it { should be_valid }
    it { should validate_presence_of(:name) }
  end

end
