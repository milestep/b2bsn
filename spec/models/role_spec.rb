require 'spec_helper'

describe Role do

  let(:role) { FactoryGirl.create(:role) }

  context 'associations' do
    subject { role }
    it { should have_many(:users) }
    it { should have_many(:posts) }
    it { should have_many(:invitations) }
    it { should have_many(:badges) }
  end
end
