require 'spec_helper'

describe AdType do

  let(:ad_type_group) { FactoryGirl.create(:ad_type_group) }

  context "associations" do
    subject { ad_type_group }
    it { should have_many(:ad_types) }
  end

end
