require 'spec_helper'

describe Site do

  let!(:site) { FactoryGirl.create(:site) }

  context "associations" do
    subject { site }
    it { should have_many(:company_sites) }
    it { should have_many(:companies) }
  end

  context "scopes" do
    it "should search category with term" do
      Site.search(site.name[0..2]).should include(site)
    end
  end
end
