require 'spec_helper'

describe CompanyAdType do

  let(:ad_type) { FactoryGirl.create(:ad_type) }
  let(:company) { FactoryGirl.create(:company) }
  let(:company_ad_type) { CompanyAdType.create(:ad_type => ad_type, :company => company) }

  context "associations" do
    subject { company_ad_type }
    it { should belong_to(:company) }
    it { should belong_to(:ad_type) }
  end

end
