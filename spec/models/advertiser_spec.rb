require 'spec_helper'

describe Advertiser do

  let(:advertiser) { FactoryGirl.create(:advertiser) }
  let(:new_advertiser) { FactoryGirl.build(:advertiser, :agency_advertisers_attributes => [{:agency_name => 'Agency'}], :brand_advertisers_attributes => [{:brand_name => 'Brand'}]) }
  let(:brand) { FactoryGirl.create(:brand) }
  let(:agency) { FactoryGirl.create(:agency) }
  let(:advertiser_with_brand_and_agency) { FactoryGirl.create(:advertiser, :brands => [brand], :agencies => [agency]) }

  context "associations" do
    subject { advertiser }
    it { should have_many(:agency_advertisers) }
    it { should have_many(:agencies).through(:agency_advertisers) }
    it { should have_many(:brand_advertisers) }
    it { should have_many(:brands).through(:brand_advertisers) }
  end

  context "validations" do
    it "should validate agency presence" do
      invalid_advertiser = FactoryGirl.build(:advertiser, :agency_advertisers_attributes => [{ :agency_id => nil, :agency_name => 'Agency' }])
      invalid_advertiser.save
      invalid_advertiser.should have(1).error_on(:agencies)
    end

    it "should validate brand presence" do
      invalid_advertiser = FactoryGirl.build(:advertiser, :brand_advertisers_attributes => [{ :brand_id => nil, :brand_name => 'Brand' }])
      invalid_advertiser.save
      invalid_advertiser.should have(1).error_on(:brands)
    end

  end

  it "should create an instance with agency and brand" do
    b = Advertiser.expanded_company
    b.agency_advertisers.should_not be_blank
    b.brand_advertisers.should_not be_blank
  end

  it "should build agency and brand associations if they are empty" do
    advertiser.agency_advertisers.should be_blank
    advertiser.brand_advertisers.should be_blank
    advertiser.build_associations
    advertiser.agency_advertisers.should_not be_blank
    advertiser.brand_advertisers.should_not be_blank
  end

  it "should return company items for profile" do
    advertiser_with_brand_and_agency.company_items_for_profile.should_not be_blank
  end

end
