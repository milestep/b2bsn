require 'spec_helper'

describe Company do

  let(:company) { FactoryGirl.create(:company) }
  let(:category) { FactoryGirl.create(:category) }
  let(:ad_type) { FactoryGirl.create(:ad_type) }
  let(:address) { FactoryGirl.create(:address) }
  let(:location) { FactoryGirl.create(:location, :company => company, :address => address) }
  let(:publisher) { FactoryGirl.create(:publisher) }
  let(:agency) { FactoryGirl.create(:agency) }
  let(:brand) { FactoryGirl.create(:brand) }
  let(:advertiser) { FactoryGirl.create(:advertiser) }
  let(:brand_with_advertiser_and_agency) { FactoryGirl.create(:brand, :advertisers => [advertiser], :agencies => [agency]) }
  let(:agency_with_brand) { FactoryGirl.create(:agency, :brands => [brand]) }
  let(:bare_locations) do
    locations = []
    3.times { |i| locations << FactoryGirl.create(:location, :name => "Location #{i}") }
    locations
  end


  context 'validations' do
    subject { company }
    it { should be_valid }
    it { should have_many(:locations) }
    it { should have_many(:company_brands) }
    it { should have_many(:brands).through(:company_brands) }
    it { should have_many(:user_profiles) }
    it { should have_many(:users).through(:user_profiles) }
    it { should have_many(:contacts) }
    it { should have_many(:company_ad_types) }
    it { should have_many(:ad_types) }
    it { should have_many(:company_audiences) }
    it { should have_many(:audiences) }
    it { should have_many(:company_categories) }
    it { should have_many(:categories) }
    it { should have_many(:company_sites) }
    it { should have_many(:sites) }
    it { should have_many(:company_networks) }
    it { should have_many(:networks) }
    it { should have_many(:experiences) }

    it { should validate_presence_of(:name) }
    it { should_not allow_value('test').for(:number_of_employees) }
    it { should allow_value(20).for(:number_of_employees) }
    it { should_not allow_value('test').for(:email) }
    it { should allow_value('test@email.com').for(:email) }
    it { should_not allow_value('test').for(:phone) }
    it { should allow_value('(123) 456-7890').for(:phone) }
  end

  context 'scopes' do

    it 'should return company if it has categories' do
      publisher.categories << category
      Company.by_categories([category.id]).should include(publisher)
    end

    it 'should return company if it has ad types' do
      publisher.ad_types << ad_type
      Company.by_ad_types([ad_type.id]).should include(publisher)
    end

    it 'should return company if it has cities' do
      publisher.addresses << address
      Company.by_cities([address.city]).should include(publisher)
    end

  end

  it "should return fields changes" do
    company.email = "new@email.com"
    company.fields_changed.keys.should include('email')
  end

  describe "search formatters" do

    subject { company }
    it { should respond_to(:search_title) }
    it { should respond_to(:full_search_title) }

  end

  context "check company type" do

    it "should return true if company is publisher" do
      publisher.publisher?.should be_true
    end

    it "should return false if company is not publisher" do
      agency.publisher?.should be_false
    end

    it "should return true if company is agency" do
      agency.agency?.should be_true
    end

    it "should return false if company is not publisher" do
      publisher.agency?.should be_false
    end

    it "should return true if company is brand" do
      brand.brand?.should be_true
    end

    it "should return false if company is not brand" do
      agency.brand?.should be_false
    end

    it "should return true if company is advertiser" do
      advertiser.advertiser?.should be_true
    end

    it "should return false if company is not advertiser" do
      agency.advertiser?.should be_false
    end
  end

  it "should return brands for details" do
    agency_with_brand.brands_for_details.should_not be_blank
  end

  it "should return agencies for details" do
    brand_with_advertiser_and_agency.agencies_for_details.should_not be_blank
  end

  it "should return advertisers for details" do
    brand_with_advertiser_and_agency.advertisers_for_details.should_not be_blank
  end

  describe "#locations" do
    it "should fetch first location name" do
      subject.stub(:locations).and_return(bare_locations)
      subject.first_location_name.should eq(bare_locations.first.name)
    end

    it "should reflect no_data location name when empty" do
      subject.stub(:locations).and_return([])
      subject.first_location_name.should eq(I18n.t('.no_data'))
    end
  end

  describe "#search_formatters" do

    subject { company }
    it_behaves_like 'an object with search_formatters'

    it { should respond_to :first_location_name }

    it "should reflect search_location" do
      subject.stub(:first_location_name).and_return('New York, NY')
      subject.search_location.should eq('New York, NY')
    end

    it "should reflect search_site_name" do
      subject.stub(:name).and_return('Microsoft')
      subject.search_site_name.should eq('Microsoft')
    end

    it "should reflect short_search_site_name" do
      subject.stub(:name).and_return('Microsoft company with long name')
      subject.short_search_site_name.should eq('Microsoft comp...')
    end

    it "should reflect search_category" do
      category = FactoryGirl.create(:category)
      category.stub(:category_name).and_return('IT Industry')
      subject.stub(:company_categories).and_return([category])
      subject.search_category.should eq('IT Industry')
    end

    it "should reflect search_category when empty" do
      subject.stub(:company_categories).and_return(nil)
      subject.search_category.should eq(I18n.t('.no_data'))
    end

    it "should reflect search_like" do
      subject.stub(:search_site_name).and_return('Microsoft')
      subject.stub(:search_location).and_return('New York, NY')
      subject.stub(:search_category).and_return('IT Industry')
      subject.search_like.should eq("Microsoft New York, NY IT Industry")
    end

  end

end
