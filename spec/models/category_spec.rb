require 'spec_helper'

describe Category do

  let!(:category) { FactoryGirl.create(:category) }

  context "associations" do
    subject { category }
    it { should have_many(:company_categories) }
    it { should have_many(:companies) }
  end

  context "scopes" do
    it "should search category with term" do
      Category.search(category.name[0..2]).should include(category)
    end
  end

end
