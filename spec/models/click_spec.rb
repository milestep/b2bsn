require 'spec_helper'

describe Click do

  let(:click) { FactoryGirl.create(:click) }

  context 'associations' do
    subject { click }
    it { should belong_to(:user) }
    it { should belong_to(:clickable) }
    it { should have_one(:timeline_event) }
  end

  context "validations" do
    it { should validate_presence_of(:user_id) }
  end

end
