require 'spec_helper'

describe CompanyLocation do
  let(:company) { FactoryGirl.create(:company, :name => "StrongCompany") }
  let(:address) { FactoryGirl.create(:address, :city => "San Francisco") }
  let!(:company_location) { FactoryGirl.create(:company_location, :company => company, :address => address) }

  context "scopes" do

    it "should get a list of all company location with types #{Company::TYPES}" do
      CompanyLocation.joins(:company, :address).by_type(:publisher).should have(1).item
    end

    it "should get a list of all company location with address state abbrev 'ca'" do
      CompanyLocation.joins(:company, :address).for_state_abbrev("ca").should have(1).item
    end

    it "should get a list of all company location with address city 'San Francisco'" do
      CompanyLocation.joins(:company, :address).for_city("San Francisco").should have(1).item
    end

    it "should get a list of all company with mathed name" do
      CompanyLocation.joins(:company, :address).search("company").should have(1).item
    end
  end

  context "associations" do
    subject { company_location }
    it { should belong_to(:company) }
    it { should belong_to(:address) }
  end

  context "short address" do
    let(:address_without_state_abbrev) { FactoryGirl.create(:address_without_state_abbrev, :city => 'San Francisco') }
    let(:address_without_city) { FactoryGirl.create(:address_without_city, :state_abbrev => 'CA') }

    it "should return short address consisting of city and state abbrev" do
      company_location.short_address.should eql('San Francisco, CA')
    end

    it "should return short address only consisting of city" do
      company_location.update_attributes(:address => address_without_state_abbrev)
      company_location.short_address.should eql('San Francisco')
    end

    it "should return short address only consisting of state abbrev" do
      company_location.update_attributes(:address => address_without_city)
      company_location.short_address.should eql('CA')
    end
  end

  it "should return full company name" do
    company_location.fullname.should eql(company.name)
  end

  it "should return discover_type" do
    company_location.discover_type.should eql(company.type.downcase)
  end

  it "should return discover_location" do
    company_location.discover_location.should eql('San Francisco, CA')
  end

  it "should return company logo" do
    company_location.logo.should eql(company.logo)
  end

  it "should not return item_type" do
    expect(company_location.item_type).to eq(nil)
  end

  it "should not return title" do
    expect(company_location.title).to eq(nil)
  end

  describe '#city' do
    it "should return city" do
      company_location.city.should eql(address.city)
    end

    it "should return empty string if there is no address" do
      company_location.address = nil
      company_location.city.should be_empty
    end
  end

  describe '#state' do
    it "should return state" do
      company_location.state.should eql(address.state_abbrev)
    end

    it "should return empty string if there is no address" do
      company_location.address = nil
      company_location.state.should be_empty
    end
  end

end
