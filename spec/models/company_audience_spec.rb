require 'spec_helper'

describe CompanyAudience do

  let(:company) { FactoryGirl.create(:company) }
  let(:audience) { FactoryGirl.create(:audience)}
  let(:company_audience) { CompanyAudience.create(:company => company, :audience => audience) }

  context "associations" do
    subject { company_audience }
    it { should belong_to(:company) }
    it { should belong_to(:audience) }
  end

  it "should return audience name" do
    company_audience.audience_name.should eql(audience.name)
  end

end
