require 'spec_helper'

describe User do

  let!(:role) { FactoryGirl.create(:role) }
  let!(:publisher_role) { FactoryGirl.create(:publisher_role) }
  let!(:role_admin) { FactoryGirl.create(:role, :name => "admin") }
  let(:user) { FactoryGirl.create(:user) }
  let(:another_user) { FactoryGirl.create(:user) }
  let!(:profile) { FactoryGirl.create(:user_profile, :user => user) }
  let(:seller) { FactoryGirl.create(:user, :role => role) }
  let(:new_user) { FactoryGirl.build(:user) }
  let!(:publisher) { FactoryGirl.create(:publisher_user) }
  let!(:admin_user) { FactoryGirl.create(:admin, :role => role_admin) }
  let(:friend1) { FactoryGirl.create(:user, :first_name => "Super", :last_name => "Man", :role => role) }
  let(:friend2) { FactoryGirl.create(:user, :first_name => "Bat", :last_name => "Man", :role => role) }
  let(:friend3) { FactoryGirl.create(:user, :first_name => "Spider", :last_name => "Man", :role => role) }
  let(:friend4_inversed) { FactoryGirl.create(:user, :first_name => "Inversed", :last_name => "Friend", :role => role) }
  let!(:connection_accepted_friend1) { FactoryGirl.create(:connection, :user => user, :friend => friend1) }
  let!(:connection_accepted_friend2) { FactoryGirl.create(:connection, :user => user, :friend => friend2) }
  let!(:connection_pending_friend3) { FactoryGirl.create(:connection, :user => user, :friend => friend3, :status => "pending") }
  let!(:connection_friend4_inversed) { FactoryGirl.create(:connection, :user => friend4_inversed, :friend => user) }
  let(:company) { FactoryGirl.create(:company) }
  let(:post) { FactoryGirl.create(:post, :user => user) }
  let(:status_post) { FactoryGirl.create(:status, :user => user) }
  let!(:timeline_event) { FactoryGirl.create(:timeline_event, :subject => post, :actor => user) }
  let!(:timeline_event2) { FactoryGirl.create(:timeline_event, :subject => status_post, :actor => user) }
  let(:timeline_event_deletion) { FactoryGirl.create(:timeline_event_user_deletion, :timeline_event => timeline_event, :user => user) }
  let(:user_profile) { FactoryGirl.create(:another_user_profile, :user => user) }
  let(:address) { FactoryGirl.create(:address) }
  let(:ignore_list_user) { FactoryGirl.create(:ignore_list_user, :user => user, :ignored_user => friend1)}

  context 'associations' do
    subject { user }
    it { should have_one(:user_profile) }
    it { should have_many(:connections) }
    it { should have_many(:inverse_connections) }
    it { should have_many(:friends).through(:connections) }
    it { should have_many(:inverse_friends).through(:inverse_connections) }
    it { should have_many(:requested_friends).through(:connections) }
    it { should have_many(:inverse_requested_friends).through(:inverse_connections) }
    it { should have_many(:incoming_requests) }
    it { should have_many(:outgoing_requests) }
    it { should have_many(:circles) }
    it { should have_many(:memberships) }
    it { should have_many(:organizations).through(:memberships) }
    it { should have_many(:contacts) }
    it { should have_many(:own_timeline_events).class_name(TimelineEvent) }
    it { should have_many(:timeline_events).through(:user_timeline_events) }
    it { should have_many(:opportunities) }
    it { should have_many(:posts) }
    it { should have_many(:questions) }
    it { should have_many(:answers) }
    it { should have_many(:authorizations) }
    it { should have_many(:social_networks).through(:authorizations) }
    it { should have_many(:user_badges) }
    it { should have_many(:badges).through(:user_badges) }
    it { should have_many(:packages) }
    it { should have_and_belong_to_many(:event_shares) }

    it { should have_many(:user_timeline_events) }
    it { should have_one(:address) }

    it { should have_many(:users_notices) }
    it { should have_many(:notice_events).through(:users_notices) }

    it { should have_many(:black_list_users) }
    it { should have_many(:blocked_users).through(:black_list_users) }

    it { should have_many(:ignore_list_users) }
    it { should have_many(:ignored_users).through(:ignore_list_users) }
    
    it { should have_many(:jira_issues) }
  end

  context "callbacks" do

    it "should collect initial badges" do
      b = FactoryGirl.create(:badge, :role => role, :name => "Early Adopter Achievement")
      seller.send :collect_initial_badges
      seller.user_badges.where(:badge_id => b.id).should_not be_empty
    end

  end

  context "scopes" do

    it "should find featured users" do
      profile.update_attributes(:featured => true)
      another_user = FactoryGirl.create(:user, :first_name => "First", :last_name => "Last")
      user_profile = FactoryGirl.create(:another_user_profile, :user => another_user, :featured => false)
      User.featured.should include(user)
      User.featured.should_not include(another_user)
    end

    it "should find users by role" do
      User.by_type(publisher_role).should include(publisher)
      User.by_type(publisher_role).should_not include(user, admin_user)
    end

    it "should find users by roles" do
      User.by_roles([publisher_role.id]).should include(publisher)
    end

    it "should find users by name" do
      admin_user.update_attributes(:first_name => "Admin", :last_name => "Boss")
      publisher.update_attributes(:first_name => "Publisher", :last_name => "User")
      User.by_name(user.first_name).should include(user)
      User.by_name(user.first_name).should_not include(admin_user, publisher)
    end

    it "should find newest users" do
      newest_user_reg_date = Time.now + 2.days
      newest_user = FactoryGirl.create(:user, :first_name => "Newest", :last_name => "User", :created_at => newest_user_reg_date)
      User.newest.first.should eql(newest_user)
    end

    it "should find all users registered after specified date" do
      admin_user.update_attribute(:created_at, 10.days.ago)
      publisher.update_attribute(:created_at, 5.days.ago)
      User.since(7.days.ago).should include(user, publisher)
      User.since(7.days.ago).should_not include(admin_user)
    end

  end

  it "should receive user_last_timeline_events scope" do
    TimelineEvent.should_receive(:user_newest_timeline_events).once
    user.newest_timeline_events
  end

  it "should have fullname" do
    user.fullname.should eq("John Smith")
  end

  it "should have abbreviation name" do
    user.abbrev_name.should eq("John S.")
  end

  context "short address" do
    let(:address) { FactoryGirl.create(:address, :city => 'San Francisco', :state_abbrev => 'CA') }
    let(:address_without_state_abbrev) { FactoryGirl.create(:address_without_state_abbrev, :city => 'San Francisco') }
    let(:address_without_city) { FactoryGirl.create(:address_without_city, :state_abbrev => 'CA') }
    let(:user_with_right_address) { FactoryGirl.create(:user) }
    let(:user_without_city) { FactoryGirl.create(:user) }
    let(:user_without_state_abbrev) { FactoryGirl.create(:user) }
    let(:user_profile) { FactoryGirl.create(:another_user_profile, :user => user_with_right_address, :address => address) }
    let(:user_profile_without_state_abbrev) { FactoryGirl.create(:another_user_profile, :user => user_without_state_abbrev, :address => address_without_state_abbrev) }
    let(:user_profile_without_city) { user_profile = FactoryGirl.create(:another_user_profile, :user => user_without_city, :address => address_without_city) }

    it "should return short address consisting of city and state abbrev" do
      user_profile.user.short_address.should eql('San Francisco, CA')
    end

    it "should return short address only consisting of city" do
      user_profile_without_state_abbrev.user.short_address.should eql('San Francisco')
    end

    it "should return short address only consisting of state abbrev" do
      user_profile_without_city.user.short_address.should eql('CA')
    end
  end

  context "can_manage_comment?" do
    let(:comment) { FactoryGirl.create(:comment, :user => user) }
    let(:another_comment) { FactoryGirl.create(:comment, :user => admin_user) }
    it "should be true for the same user" do
      user.can_manage_comment?(comment).should be_true
      user.can_manage_comment?(comment, user).should be_true
    end

    it "should be false for another user" do
      user.can_manage_comment?(another_comment, admin_user).should_not be_true
    end

  end

  context "roles" do

    it "should be an admin" do
      admin_user.admin?.should be_true
    end

    it "should not be an admin" do
      user.admin?.should be_false
    end

    it "should have a role" do
      seller.role?(role).should be_true
      admin_user.role?(role_admin).should be_true
    end

    it "should display user's role" do
      expect(seller.user_type).to eq("seller")
      expect(seller.item_type).to eq("seller")
      expect(admin_user.user_type).to eq("admin")
    end

    it "should detect whether the user is a seller" do
      seller.seller?.should be_true
      user.seller?.should be_false
    end

    it "should respond to humanable_role" do
      expect(seller.humanable_role).to eq('Seller')
    end

  end

  context "profiles" do

    it "should reflect title with length of 5 characters only" do
      user.user_profile.title = 'This is my finest profile'
      expect(user.user_profile_title(10)).to eql('This is...')
    end

    it "should reflect company name with 7 characters only" do
      user.user_profile.company = company
      user.user_profile.company.name = 'CocaCola Corp.'
      expect(user.company_name(7)).to eql('Coca...')
    end

    it "should display shorten full_name with length of 12 characters only" do
      user.first_name = 'Jerry'
      user.last_name = 'Abbraham'
      expect(user.short_full_name(12)).to eql('Jerry Abb...')
    end

  end

  context "friends" do

    it "should display his friends" do
      user.my_friends.should have(3).items
      user.my_friends(role).should have(3).items
      user.my_friends(role_admin).should be_empty
    end

    it "should display suggested friends" do
      new_user = FactoryGirl.create(:user, :first_name => "Kind", :last_name => "Roger")
      user.suggested_friends.should include(new_user)
      user.suggested_friends.should_not include(user.my_friends, user)
      friend_of_friend = FactoryGirl.create(:user, :first_name => "Friend", :last_name => "OfFriend")
      friend_of_friend_connection = FactoryGirl.create(:connection, :user => user.my_friends.first, :friend => friend_of_friend)
      user.reload
      user.suggested_friends.should include(friend_of_friend)
      user.suggested_friends.should_not include(new_user)
    end

    it "should display requested_friends" do
      user.my_requested_friends.should have(1).items
      another_pending_user = FactoryGirl.create(:user, :first_name => "Another", :last_name => "PendingUser")
      another_pending_connection = FactoryGirl.create(:connection, :user => user, :friend => another_pending_user, :status => "pending")
      user.reload
      user.my_requested_friends.should have(2).items
      another_pending_reverse_user = FactoryGirl.create(:user, :first_name => "Another", :last_name => "PendingReverseUser")
      another_pending_reverse_connection = FactoryGirl.create(:connection, :user => another_pending_reverse_user, :friend => user, :status => "pending")
      user.reload
      user.my_requested_friends.should have(3).items
    end

    it "should find friends" do
      user.find_friends().should have(3).items
      user.find_friends("Man").should have(2).items
      user.find_friends("NotExistingName").should have(0).items
    end

    it "should find circles" do
      circle = FactoryGirl.create(:circle, :name => "TestCircle", :user => user)
      another_circle = FactoryGirl.create(:circle, :name => "AnotherCircle", :user => user)
      another_user_circle = FactoryGirl.create(:circle, :name => "TestCircle", :user => admin_user)
      user.find_circles.should have(2).items
      user.find_circles("Test").should have(1).items
      user.find_circles("NotExistingCircleName").should have(0).items
    end

    it "should find active connection" do
      user.find_active_connection(user.friends.first).should eq(user.connections.first)
      new_user_not_friend = FactoryGirl.create(:user, :first_name => "Not", :last_name => "Friend")
      user.find_active_connection(new_user_not_friend).should be_nil
      user.find_active_connection(friend4_inversed).should eq(connection_friend4_inversed)
    end

    it "should display new users" do
      first_new_user = FactoryGirl.create(:user, :first_name => "New", :last_name => "User")
      second_new_user = FactoryGirl.create(:user, :first_name => "Another", :last_name => "User")
      user.new_users.should include(first_new_user, second_new_user)
      user.new_users.should_not include(user.my_friends, user)
    end

    it "should return new friends of user for last seven days" do
      user.friends_for_seven_days.should include(connection_accepted_friend1)
    end

    it "should return name of last added friend" do
      user.last_added_friend_name_for_seven_days.should eq(friend2.fullname)
    end

    it "should check if user has new friends" do
      user.has_new_friends?.should be_true
    end

    it "should return friends for seven days without last" do
      user.friends_for_seven_days_without_last.should_not include(friend2)
    end

  end

  describe "search formatters" do

    subject { user }
    it { should respond_to(:search_title) }
    it { should respond_to(:full_search_title) }

  end

  describe "search behavior" do

    before do
      @user1_for_searching = FactoryGirl.create(:user, :first_name => "Cool", :last_name => "Guy", :email => "cool_guy@email.com", :role => role)
      @user1_company = FactoryGirl.create(:company, :name => "FirstUserCompany")
      @user1_profile = FactoryGirl.create(:user_profile, :user => @user1_for_searching, :company => @user1_company, :title => "profile1")
      @user2_for_searching = FactoryGirl.create(:user, :first_name => "Smart", :last_name => "Guy", :email => "smart_guy@email.com", :role => role)
      @user2_company = FactoryGirl.create(:company, :name => "SecondUserCompany")
      @user2_address = FactoryGirl.create(:address)
      @user2_profile = FactoryGirl.create(:user_profile, :user => @user2_for_searching, :company => @user2_company, :address => @user2_address, :title => "profile2")
      @user2_phone = FactoryGirl.create(:phone, :number => "123456789", :address => @user2_address)
    end

    it "should search with params" do
      User.search_with_params({:name => "Cool"}).should include(@user1_for_searching)
      User.search_with_params({:name => "Guy"}).should include(@user1_for_searching, @user2_for_searching)
      User.search_with_params({:email => "cool_guy@email.com"}).should include(@user1_for_searching)
      User.search_with_params({:title => "profile2"}).should include(@user2_for_searching)
      User.search_with_params({:company => "FirstUserCompany"}).should include(@user1_for_searching)
      User.search_with_params({:phone => "123456789"}).should include(@user2_for_searching)
      User.search_with_params({:user_type => role}).should include(@user1_for_searching, @user2_for_searching)
    end

    it "should fetch collection of users but without the current_user" do
      User.search_with_params({:user_type => role, :except => [@user1_for_searching.id]}).should_not include(@user1_for_searching)
    end

  end

  it "should create itself with omniauth" do
    invitation = FactoryGirl.create(:invitation)
    auth = {"provider" => "twitter",
      "uid" => rand(1000),
      "info" => {"first_name" => "FNameFromOAuth", "last_name" => "LNameFromOAuth"}}
    User.create_with_omniauth(auth, invitation.token)
    User.find_by_first_name("FNameFromOAuth").should_not be_nil
  end

  it "should return stream_friends for activity feed" do
    timeline_event
    User.stream_friends_for([timeline_event]).should include(post.user)
  end

  context "should see add package link" do

    it "should not see add package link" do
      user.should_see_add_package?.should be_false
    end

    it "should see add package link" do
      FactoryGirl.create(:connection, :user => seller, :friend => publisher)
      seller.should_see_add_package?.should be_true
    end

  end

  context "connected to publisher" do

    it "should detect that user is connected to a publisher" do
      publisher_user_connection = FactoryGirl.create(:connection, :user => user, :friend => publisher)
      user.connected_to_publisher?(publisher.id).should be_true
    end

    it "should detect that user is connected to a publisher" do
      publisher_user_connection = FactoryGirl.create(:connection, :user => publisher, :friend => user)
      user.connected_to_publisher?(publisher.id).should be_true
    end

  end

  it "should return discover_type of user" do
    user.discover_type.should eq("user")
  end

  it "should return discover_location of user" do
    user.discover_location.should eq("UserCompany Lead Operations Technician")
  end

  it "should return avatar of user" do
    user.reload.logo.should eql(user.user_profile.avatar)
  end

  describe "#user_profile" do
    it "should return profile" do
      user.reload.user_profile.should eql(profile)
    end

    it "should return deleted profile" do
      profile.destroy
      user.reload.user_profile.should eql(profile)
    end
  end

  describe "#scoped_user_profile" do
    it "should return profile" do
      user.reload.scoped_user_profile.should eql(profile)
    end

    it "should not return deleted profile" do
      profile.destroy
      user.reload.scoped_user_profile.should be_nil
    end
  end

  context "should return user status" do
    it "should return offline status" do
      user.user_status.update_attributes(:status => UserStatus::OFFLINE)
      user.online_status.should eq(UserStatus::OFFLINE)
    end

    it "should return away status" do
      user.user_status.update_attributes(:status => UserStatus::AWAY)
      user.online_status.should eq(UserStatus::AWAY)
    end

    it "should return busy status" do
      user.user_status.update_attributes(:status => UserStatus::DO_NOT_DISTURB)
      user.online_status.should eq(UserStatus::DO_NOT_DISTURB)
    end

    it "should return offline status, because user invisible" do
      user.user_status.update_attributes(:status => UserStatus::INVISIBLE)
      user.online_status.should eq(UserStatus::OFFLINE)
    end
  end

  describe "#logo_url" do
    it "should return logo url" do
      user.logo_url.should eql(user.user_profile.avatar.url)
    end
  end

  describe '#city' do
    it "should return city" do
      user.address = address
      user.city.should eql(address.city)
    end

    it "should return empty string if there is no address" do
      user.city.should be_empty
    end
  end

  describe '#state' do
    it "should return state" do
      user.address = address
      user.state.should eql(address.state_abbrev)
    end

    it "should return empty string if there is no address" do
      user.state.should be_empty
    end
  end

  describe '#country' do
    it "should return country" do
      user.address = address
      user.country.should eql(address.country_code)
    end

    it "should return nil if there is no address" do
      user.country.should be_nil
    end
  end

  describe '#fetch_similar_profiles' do
    it "should return similar profiles" do
      user.address = address
      publisher.role = publisher_role
      publisher.address = address
      user.fetch_similar_profiles.should include(publisher)
    end

    it "should not return similar profiles" do
      user.fetch_similar_profiles.should be_empty
    end
  end

  context 'timeline events' do
    let(:actor) { FactoryGirl.create(:user) }
    let(:another_user) { FactoryGirl.create(:user) }
    let(:status) { FactoryGirl.create(:status, :role => user.role) }
    let(:another_status) { FactoryGirl.create(:status, :role => user.role) }
    let(:question) { FactoryGirl.create(:question, :role => user.role) }
    let!(:status_updated) { FactoryGirl.create(:timeline_event_status_updated, :actor => actor, :subject => status) }
    let!(:question_asked) { FactoryGirl.create(:timeline_event_question_asked, :actor => actor, :subject => question) }
    let!(:another_status_updated) { FactoryGirl.create(:timeline_event_status_updated, :actor => another_user, :subject => another_status) }

    describe '#hide_events_from' do

      before { user.timeline_events << [status_updated, question_asked, another_status_updated]; user.hide_events_from(actor) }

      it "should hide events from actor" do
        user.reload.timeline_events.should_not include(status_updated, question_asked)
      end

      it "should not hide events from another user" do
        user.timeline_events.should include(another_status_updated)
      end
    end

    describe '#unhide_events_from' do
      before { user.hide_events_from(actor); user.unhide_events_from(actor) }

      it "should unhide events from actor" do
        user.timeline_events.should include(status_updated, question_asked)
      end
    end
  end

  describe '#delete_user_from_circles' do
    it "should delete disconnected friend from all users group" do
      new_circle = FactoryGirl.create(:circle, :name => "New_Circle", :user => user)
      new_circle.add_user(friend1.id)
      new_circle.users.should include(friend1)
      user.delete_user_from_circles(friend1)
      new_circle.reload
      new_circle.users.should_not include(friend1)
    end
  end

  describe "#search_formatters" do
    subject { user }
    it_behaves_like "an object with search_formatters"

    it "should reflect search_location" do
      subject.stub_chain(:user_profile, :location).and_return('New York, NY')
      subject.search_location.should eq('New York, NY')
    end

    it "should reflect search_site_name" do
      subject.stub(:fullname).and_return('Bill Gates')
      subject.search_site_name.should eq('Bill Gates')
    end

    it "should reflect short_search_site_name" do
      subject.stub(:fullname).and_return('This is a Bill Gates user with long name')
      subject.short_search_site_name.should eq('This is a Bill...')
    end

    it "should reflect search_like" do
      subject.stub(:search_site_name).and_return('Bill Gates')
      subject.stub(:search_location).and_return('New York, NY')
      subject.stub(:search_category).and_return('Commerce')
      subject.search_like.should eq("Bill Gates New York, NY Commerce")
    end
  end

  describe '#last_post_status' do
    it "should return last post with status type" do
      user.last_post_status.should eq(status_post)
    end
  end

  describe '#last_status_event_id' do
    it "should return timeline_event id of last post with status type" do
      user.last_status_event_id.should eq(status_post.timeline_event.id)
    end
  end

  describe '#last_status_likes_size' do
    it "should return likes size of last post with status type" do
      user.last_status_likes_size.should eq(0)
    end
  end

  describe '#has_connection_for?' do
    let(:requested_friend) { FactoryGirl.create(:user) }
    let(:not_friend) { FactoryGirl.create(:user) }
    let!(:connection) { FactoryGirl.create(:connection, :user => user, :friend => requested_friend) }
    it "should return true" do
      expect(user.has_connection_for?(requested_friend)).to be_true
    end

    it "should return false" do
      expect(user.has_connection_for?(not_friend)).to be_false
    end

    it "should return false there is only declined connection" do
      connection.fire_status_event("Reject")
      expect(user.has_connection_for?(requested_friend)).to be_false
    end

    it "should return false there is only deleted connection" do
      connection.fire_status_event("Delete")
      expect(user.has_connection_for?(requested_friend)).to be_false
    end
  end

  it "should block choosen user" do
    expect(user.add_to_blacklist(friend1)).to be_true
    expect(user.blocked_users).to include(friend1)
  end
  
  describe "#to_requests_union_json" do
    it "should generate correct json" do
      json = user.to_requests_union_json
      parsed = JSON::parse(json)
      expect(parsed.first.keys).to include('created_at', 'request_type', 'actor')
      expect(parsed.first['actor'].keys).to include('id', 'fullname', 'user_type')
    end
  end

  it "should call chain of scopes related to the timeline events " do
    User.any_instance.stub_chain(:timeline_events, :all_events_except_ignored_users_for).and_return([])
    expect(user.all_events_except_ignored_users).to be_empty
  end

  it "should return both users from ignore list" do
    ignore_list_user
    expect(User.mutual_ignored_users_for(user)).to include(user)
    expect(User.mutual_ignored_users_for(user)).to include(friend1)
  end

  it "should add user to ignore list" do
    expect(user.add_to_ignore_list(friend1)).to be_true
    expect(user.ignored_users).to include(friend1)
  end

  it "should check if user ignored by another user" do
    ignore_list_user and expect(friend1.ignored_by?(user)).to be_true
  end

  it "should return false if users are not friends" do
    expect(user.friend_of?(another_user)).to be_false
  end

  it "should return true if users are friends" do
    expect(user.friend_of?(friend1)).to be_true
  end
end
