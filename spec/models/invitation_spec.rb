require 'spec_helper'

describe Invitation do

  let(:role) { FactoryGirl.create(:role) }
  let(:invitation) { FactoryGirl.create(:invitation) }

  context "scopes" do
    it "should get a list of all available invitations" do
      invitation
      Invitation.available.should have(1).item
    end
  end

  context 'associations' do
    subject { invitation }
    it { should belong_to(:sender) }
  end

  it "should return invitation if token is valid" do
    Invitation.validate_token(invitation.token).should eql(invitation)
  end

  it "should create an invitation" do
    Invitation.generate_tokens(role)
    Invitation.should have(1).item
  end

end
