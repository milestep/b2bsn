require 'spec_helper'

describe Comment do

  let(:user) { FactoryGirl.create(:user) }
  let(:post) { FactoryGirl.create(:post) }
  let(:comment) { FactoryGirl.create(:comment, :commentable => post, :user => user) }

  context 'associations' do
    subject { comment }
    it { should belong_to(:user) }
    it { should belong_to(:commentable) }
    it { should have_many(:timeline_events) }
    it { should have_one(:timeline_event) }
  end

  it "should increment comments counter" do
    expect { FactoryGirl.create(:comment, :commentable => post); post.reload }.to change(post, :comments_count).by(1)
  end

  it "should decrement comments counter" do
    post = comment.commentable.reload
    expect { comment.destroy; post.reload}.to change(post, :comments_count).by(-1)
  end

  describe "#user" do
    it "should return user" do
      comment.reload.user.should eql(user)
    end

    it "should return deleted user" do
      user.destroy
      comment.reload.user.should eql(user)
    end
  end

  describe "#scoped_user" do
    it "should return user" do
      comment.reload.scoped_user.should eql(user)
    end

    it "should not return deleted user" do
      user.destroy
      comment.reload.scoped_user.should be_nil
    end
  end

  describe "#commentable_timeline_event" do
    it "should return timeline event for commentable" do
      comment.commentable_timeline_event.should eq(post.timeline_event)
    end
  end

  context 'alias_attribute' do
    subject { comment }

    before(:each) { subject.stub(:comment).and_return("some comment") }

    its("comment") { should eq("some comment") }
    its("title") { should eq("some comment") }
  end
end
