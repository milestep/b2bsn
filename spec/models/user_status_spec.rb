require 'spec_helper'

describe UserStatus do
  let(:user) { FactoryGirl.create(:user) }
  let(:user_status) { FactoryGirl.create(:user_status) }
  let(:online_status) { FactoryGirl.create(:online_status) }
  let(:away_status) { FactoryGirl.create(:away_status) }
  let(:busy_status) { FactoryGirl.create(:do_not_disturb_status) }
  let(:offline_status) { FactoryGirl.create(:offline_status) }
  let(:invisible_status) { FactoryGirl.create(:invisible_status) }

  context 'associations' do
    it { should belong_to(:user) }
  end

  context 'should return current status' do
    it "should return true if status offline" do
      user_status.offline?.should be_true
    end

    it "should return true if status online" do
      online_status.online?.should be_true
    end

    it "should return true if status away" do
      away_status.away?.should be_true
    end

    it "should return true if status busy" do
      busy_status.do_not_disturb?.should be_true
    end
  end

  context 'should change status' do
    it "should change status on online" do
      expect {
        offline_status.online!
      }.to change(offline_status, :status).from(UserStatus::OFFLINE).to(UserStatus::ONLINE)
    end

    it "should change status on away" do
      expect {
        offline_status.away!
      }.to change(offline_status, :status).from(UserStatus::OFFLINE).to(UserStatus::AWAY)
    end

    it "should change status on busy" do
      expect {
        offline_status.do_not_disturb!
      }.to change(offline_status, :status).from(UserStatus::OFFLINE).to(UserStatus::DO_NOT_DISTURB)
    end

    it "should change status on invisible" do
      expect {
        offline_status.invisible!
      }.to change(offline_status, :status).from(UserStatus::OFFLINE).to(UserStatus::INVISIBLE)
    end

    it "should change status on offline" do
      expect {
        online_status.offline!
      }.to change(online_status, :status).from(UserStatus::ONLINE).to(UserStatus::OFFLINE)
    end
  end

  context 'updated user status on user activity' do
    it "should update user status on online status" do
      online_status.reset_status!.should be_true
      online_status.status.should eq(UserStatus::ONLINE)
      online_status.online?.should be_true
    end

    it "should update user status on away status" do
      offline_status.reset_status!.should be_true
      offline_status.status.should eq(UserStatus::ONLINE)
      offline_status.online?.should be_true
    end

    it "should update user status on away status" do
      offline_by_time_status(away_status)
      away_status.reset_status!.should be_true
      away_status.status.should eq(UserStatus::AWAY)
    end

    it "should update user status on busy status" do
      offline_by_time_status(busy_status)
      busy_status.reset_status!.should be_true
      busy_status.status.should eq(UserStatus::DO_NOT_DISTURB)
    end

    it "should update user status on invisible status" do
      offline_by_time_status(invisible_status)
      invisible_status.reset_status!.should be_true
      invisible_status.status.should eq(UserStatus::INVISIBLE)
    end
  end

  context "should check if incoming status is correct" do
    it "should return false, because status is incorrect" do
      UserStatus.valid_status?("incorrect_status").should be_false
    end

    it "should return true, because status is correct" do
      UserStatus.valid_status?(UserStatus::ONLINE).should be_true
    end
  end

  it "should return user inactive time in minutes" do
    offline_by_time_status(offline_status, 1.hour)
    offline_status.inactive_time.should >= 60
  end

  def offline_by_time_status(status, time_ago = 1.hour)
    UserStatus.record_timestamps = false
    status.update_attributes :updated_at => time_ago.seconds.ago
    UserStatus.record_timestamps = true
  end
end