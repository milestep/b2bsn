require 'spec_helper'

describe Address do

  let(:address) { FactoryGirl.create(:address) }
  let(:address_attrs) { FactoryGirl.attributes_for(:address) }
  let(:address_without_country_code) {FactoryGirl.create(:address, :country_code => nil)}
  let(:address_with_country_code) {FactoryGirl.create(:address, :country_code => 'ua')}

  context "associations" do
    subject { address }
    it { should have_many(:phones) }
    it { should have_many(:company_locations) }
  end

  context 'validations' do
    subject { address }
    it { should be_valid }
    it { should validate_format_of(:city).with("San-Francisco") }
    it { should validate_format_of(:city).not_with("San-Francisco12") }
  end

  it "should set default value for country code" do
    address_without_country_code.country_code.should eql(Address::US)
  end

  it "should not set defalt value for country code if it is specified" do
    address_with_country_code.country_code.should eql('ua')
  end

  it "should set the default country code if country code blank" do
    address = Address.create(address_attrs.merge(:country_code => nil))
    address.country_code.should eql(Address::US)
  end

  it "should return cities grouped by state" do
    address
    cities = Address.grouped_cities
    cities.keys.should include(address.state_abbrev)
    cities.values.flatten.should include(address.city)
  end

end
