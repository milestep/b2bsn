require "spec_helper"

describe Circle do

  let(:user) { FactoryGirl.create(:user) }
  let(:circle) { FactoryGirl.create(:circle, :user => user, :users => [user]) }

  context "associations" do
    subject { circle }
    it { should belong_to(:user) }
    it { should have_and_belong_to_many(:users) }
    it { should have_many(:post_circles) }
    it { should have_many(:posts).through(:post_circles) }
  end

  context "validations" do
    it { should validate_presence_of(:name) }
  end

  context "scopes" do

    it "should find circles by user_id and circle name" do
      Circle.user_circles(user.id,[circle.name]).should include(circle)
    end

  end

  describe '#add_user' do
    let(:friend) { FactoryGirl.create(:user, :first_name => 'Bat', :last_name => 'Man') }
    let!(:connection) { FactoryGirl.create(:connection, :user => user, :friend => friend) }
    let(:another_user) { FactoryGirl.create(:user, :first_name => 'Super', :last_name => 'Man') }

    it "should add user" do
      circle.add_user(friend.id)
      circle.users.should include(friend)
    end

    it "should not add user to circle if user is already in circle" do
      circle.users.should have(1).item
      circle.add_user(friend.id)
      circle.add_user(friend.id)
      circle.users.should have(2).item
    end

    it "should not add user to circle if user is not a friend of circle owner" do
      circle.add_user(another_user.id)
      circle.users.should_not include(another_user)
    end
  end

end
