require 'spec_helper'

describe Agency do

  let(:agency) { FactoryGirl.create(:agency) }
  let(:brand) { FactoryGirl.create(:brand) }
  let(:advertiser) { FactoryGirl.create(:advertiser) }
  let(:agency_with_brand) { FactoryGirl.create(:agency, :brands => [brand]) }

  context "associations" do
    subject { agency }
    it { should have_many(:agency_brands) }
    it { should have_many(:brands).through(:agency_brands) }
    it { should have_many(:agency_advertisers) }
    it { should have_many(:advertisers).through(:agency_advertisers) }
  end

  context "validations" do

    it "should validate brand unique" do
      a = FactoryGirl.build(:agency, :agency_brands_attributes => [{ :brand => brand, :brand_name => 'Brand' }, { :brand => brand, :brand_name => 'Brand' }])
      a.save
      a.should have(1).error_on(:brands)
    end

    it "should validate brand presence" do
      a = FactoryGirl.build(:agency, :agency_brands_attributes => [{ :brand_id => nil, :brand_name => 'Brand' }])
      a.save
      a.should have(1).error_on(:brands)
    end

    it "should validate advertisers unique" do
      a = FactoryGirl.build(:agency, :agency_advertisers_attributes => [{:advertiser => advertiser, :advertiser_name => 'Advertiser'}, {:advertiser => advertiser, :advertiser_name => 'Advertiser'}])
      a.save
      a.should have(1).error_on(:advertisers)
    end

    it "should validate advertiser presence" do
      a = FactoryGirl.build(:agency, :agency_advertisers_attributes => [{:advertiser_id => nil, :advertiser_name => 'Advertiser'}])
      a.save
      a.should have(1).error_on(:advertisers)
    end

  end

  it "should create an instance with brand and advertiser" do
    a = Agency.expanded_company
    a.agency_advertisers.should_not be_blank
    a.agency_brands.should_not be_blank
  end

  it "should return company items for profile" do
    agency_with_brand.company_items_for_profile.should_not be_blank
  end

end
