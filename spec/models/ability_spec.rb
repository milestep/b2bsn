require 'spec_helper'
require "cancan/matchers"

describe Ability do

  let!(:role_admin) { FactoryGirl.create(:role, :name => "admin") }
  let!(:role) { FactoryGirl.create(:role, :name => Role::SELLER) }
  let(:user) { FactoryGirl.create(:user, :role => role) }
  let(:admin) { FactoryGirl.create(:admin, :role => role_admin) }
  let(:ability) { Ability.new(user) }
  let(:admin_ability) { Ability.new(admin) }

  it "should allow user to read all" do
    ability.should be_able_to(:read, :all)
  end

  it "should allow user to write all" do
    ability.should_not be_able_to(:write, :all)
  end

  it "should allow user to read all" do
    admin_ability.should be_able_to(:read, :all)
  end

  it "should allow admin user to write all" do
    admin_ability.should be_able_to(:write, :all)
  end

end
