require 'spec_helper'

describe CompanyCategory do

  let(:company) { FactoryGirl.create(:company) }
  let(:category) { FactoryGirl.create(:category)}
  let(:company_category) { CompanyCategory.create(:company => company, :category => category) }

  context "associations" do
    subject { company_category }
    it { should belong_to(:company) }
    it { should belong_to(:category) }
  end

  it "should return category name" do
    company_category.category_name.should eql(category.name)
  end

end
