require 'spec_helper'

describe Audience do

  let!(:audience) { FactoryGirl.create(:audience) }

  context "associations" do
    subject { audience }
    it { should have_many(:company_audiences) }
    it { should have_many(:companies) }
  end

  context "scopes" do
    it "should search category with term" do
      Audience.search(audience.name[0..2]).should include(audience)
    end
  end
end
