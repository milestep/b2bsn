require 'spec_helper'
require "cancan/matchers"

describe IgnoreListUser do

  let(:user) { FactoryGirl.create(:user) }
  let(:friend) { FactoryGirl.create(:user, :first_name => "Ilia", :last_name => "Muromec") }
  let(:ignore_list_user) { FactoryGirl.create(:ignore_list_user, :user => user, :ignored_user => friend) }
  let(:new_ignore_list_user) { FactoryGirl.build(:ignore_list_user, :user => user, :ignored_user => friend) }

  it "should esteblish ignoring period" do
    expect{new_ignore_list_user.save}.to change{new_ignore_list_user.stop_ignoring_at}.from(nil).to(Date.today+ IgnoreListUser::IGNORING_PERIOD)
  end

  it "should return expired records" do
    ignore_list_user.update_attribute(:stop_ignoring_at, Date.today)
    expect(IgnoreListUser.expired).to include(ignore_list_user)
  end

end
