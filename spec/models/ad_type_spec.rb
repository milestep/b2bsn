require 'spec_helper'

describe AdType do

  let(:ad_type) { FactoryGirl.create(:ad_type, :name => 'micro_bar_88_31') }

  context "associations" do
    subject { ad_type }
    it { should belong_to(:ad_type_group) }
    it { should have_many(:company_ad_types) }
    it { should have_many(:companies) }
  end

  it "should return unit_name" do
    expect(ad_type.unit_name).to eql('Micro Bar')
  end

  it "should return content_category" do
    expect(ad_type.content_category).to eql('88x31')
  end

end
