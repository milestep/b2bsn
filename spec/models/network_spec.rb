require 'spec_helper'

describe Network do

  let!(:network) { FactoryGirl.create(:network) }

  context "associations" do
    subject { network }
    it { should have_many(:company_networks) }
    it { should have_many(:companies) }
  end

  context "scopes" do
    it "should search category with term" do
      Network.search(network.name[0..2]).should include(network)
    end
  end
end
