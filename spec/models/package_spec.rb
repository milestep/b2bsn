require 'spec_helper'

describe Package do
  let!(:role) { FactoryGirl.create(:role) }
  let(:user) { FactoryGirl.create(:user, :role => role) }
  let(:publisher) { FactoryGirl.create(:publisher) }
  let(:package) { FactoryGirl.create(:package, :packagable => user) }
  let(:publisher_package) { FactoryGirl.create(:package, :packagable => publisher) }

  context 'validations' do
    subject { package }
    it { should belong_to(:packagable) }
    it { should be_valid }
    it { should validate_presence_of(:description) }
    it { should validate_presence_of(:flight_end_date) }
    it { should validate_presence_of(:flight_start_date) }
    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:total_cost) }
    it { should_not allow_value(0).for(:total_cost) }
    it { should_not allow_value(0).for(:total_impressions) }
    it { should_not allow_value(0).for(:cpm) }
    it { should_not allow_value("n"*101).for(:total_cost) }
    it { should_not allow_value("d"*1001).for(:total_impressions) }
    it { should_not allow_value("b"*51).for(:cpm) }

    it "validates that flight start date should be <= flight end date" do
      package = FactoryGirl.build(:package, :flight_end_date => Date.yesterday)
      package.should be_invalid
    end
  end

  it "should check if package owner is publisher" do
    publisher_package.owner_publisher?.should be_true
  end

  it "should check if package owner is not publisher" do
    package.owner_publisher?.should be_false
  end

  it "should check if package owner is seller" do
    package.owner_seller?.should be_true
  end

  it "should check if package owner is not seller" do
    publisher_package.owner_seller?.should be_false
  end

  it "should return logo url" do
    package.logo_url.should eql(package.logo.url)
  end

  it "should return title" do
    expect(package.title).to eql("was updated")
  end

  describe "#created_by?" do
    let(:another_package) { FactoryGirl.create(:package) }

    it "should return true" do
      package.created_by?(user).should be_true
    end

    it "should return false" do
      another_package.created_by?(user).should be_false
    end
  end
end
