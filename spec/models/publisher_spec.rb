require 'spec_helper'

describe Publisher do

  let(:publisher) { FactoryGirl.create(:publisher) }
  let(:audience) { FactoryGirl.create(:audience) }
  let(:category) { FactoryGirl.create(:category) }
  let(:site) { FactoryGirl.create(:site) }
  let(:network) { FactoryGirl.create(:network) }

  context "associations" do
    subject { publisher }
  end

  it "should return company items for profile" do
    publisher.company_items_for_profile.should_not be_blank
  end

  describe "#initate_associations" do
    it "should build company audience" do
      expect {
        publisher.initate_associations
      }.to change{publisher.company_audiences.size}.by(1)
    end

    it "should not build company audience if it has one" do
      publisher.company_audiences.build
      expect {
        publisher.initate_associations
      }.to change{publisher.company_audiences.size}.by(0)
    end

    it "should build company category" do
      expect {
        publisher.initate_associations
      }.to change{publisher.company_categories.size}.by(1)
    end

    it "should not build company category if it has one" do
      publisher.company_categories.build
      expect {
        publisher.initate_associations
      }.to change{publisher.company_categories.size}.by(0)
    end

    it "should build company site" do
      expect {
        publisher.initate_associations
      }.to change{publisher.company_sites.size}.by(1)
    end

    it "should not build company site if it has one" do
      publisher.company_sites.build
      expect {
        publisher.initate_associations
      }.to change{publisher.company_sites.size}.by(0)
    end

    it "should build company network" do
      expect {
        publisher.initate_associations
      }.to change{publisher.company_networks.size}.by(1)
    end

    it "should not build company site if it has one" do
      publisher.company_networks.build
      expect {
        publisher.initate_associations
      }.to change{publisher.company_networks.size}.by(0)
    end
  end

  describe "#check_audience" do
    it "should create new audience" do
      publisher.company_audiences.build(:audience_name => "new audience")
      expect {
        publisher.save
      }.to change{Audience.count}.by(1)
    end

    it "should use exisiting audience" do
      publisher.company_audiences.build(:audience_id => audience.id)
      expect {
        publisher.save
      }.to change{Audience.count}.by(0)
    end
  end

  describe "#check_category" do
    it "should create new category" do
      publisher.company_categories.build(:category_name => "new category")
      expect {
        publisher.save
      }.to change{Category.count}.by(1)
    end

    it "should use exisiting category" do
      publisher.company_categories.build(:category_id => category.id)
      expect {
        publisher.save
      }.to change{Category.count}.by(0)
    end
  end

  describe "#check_site" do
    it "should create new site" do
      publisher.company_sites.build(:site_name => "new site")
      expect {
        publisher.save
      }.to change{Site.count}.by(1)
    end

    it "should use exisiting site" do
      publisher.company_sites.build(:site_id => site.id)
      expect {
        publisher.save
      }.to change{Site.count}.by(0)
    end
  end

  describe "#check_network" do
    it "should create new network" do
      publisher.company_networks.build(:network_name => "new network")
      expect {
        publisher.save
      }.to change{Network.count}.by(1)
    end

    it "should use exisiting network" do
      publisher.company_networks.build(:network_id => network.id)
      expect {
        publisher.save
      }.to change{Network.count}.by(0)
    end
  end
end
