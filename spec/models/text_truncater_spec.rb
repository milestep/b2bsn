require 'spec_helper'

describe TextTruncater do
  let(:news_feed) { FactoryGirl.create(:news_feed) }
  let(:news_item) { FactoryGirl.create(:news_item, :news_feed => news_feed) }
  let(:text_truncater) { TextTruncater.new(news_item.description) }

  it "should return formatted description, simple text" do
    news_item.update_attributes(:description => "test")
    text_truncater.sanitized_first_paragraph.should eq("test")
  end

  it "should return formatted description, simple text with tags" do
    news_item.update_attributes(:description => "<p>test</p>")
    text_truncater.sanitized_first_paragraph.should eq("test")
  end

  it "should return only first paragraph" do
    news_item.update_attributes(:description => "<p>test</p><p>test2</p>")
    text_truncater.sanitized_first_paragraph.should eq("test")
  end

  it "should return formatted description, simple text without tags" do
    news_item.update_attributes(:description => "<p>test</p>")
    text_truncater.sanitized_first_paragraph.should eq("test")
  end

  it "should return simple text without tags" do
    news_item.update_attributes(:description => "<p></p>test")
    text_truncater.sanitized_first_paragraph.should eq("test")
  end
end