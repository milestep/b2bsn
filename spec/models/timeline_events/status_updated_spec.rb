require 'spec_helper'

describe TimelineEvents::StatusUpdated do
  subject { FactoryGirl.create(:timeline_event_status_updated) }

  it "should return valid message" do
    subject.message.should eq(I18n.t('.timeline_events.status_updated_message', :to => subject.to))
  end
end