require 'spec_helper'

describe TimelineEvents::CommentCreated do
  subject { FactoryGirl.create(:timeline_event_comment_created) }

  it "should return valid message" do
    subject.message.should eq(I18n.t('.timeline_events.comment_created_message'))
  end
end
