require 'spec_helper'

describe TimelineEvents::MidbCompanyDeleted do
  subject { FactoryGirl.create(:timeline_event_midb_company_deleted) }

  it "should return valid message" do
    subject.stub(:company_name).and_return('Some name')
    subject.stub_chain(:subject, :type, :capitalize).and_return('Type')
    message = I18n.t('.timeline_events.removed_entry', :user_name => subject.user_name, :company_name => 'Some name', :company_type => 'Type')
    subject.message.should eq(message)
  end

  it "should return valid status" do
    subject.status.should eq(I18n.t('.timeline_events.deleted_status'))
  end
end