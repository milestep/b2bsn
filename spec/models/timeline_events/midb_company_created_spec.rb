require 'spec_helper'

describe TimelineEvents::MidbCompanyCreated do
  subject { FactoryGirl.create(:timeline_event_midb_company_created) }

  it "should return valid message" do
    subject.stub(:company_name).and_return('Some name')
    subject.stub_chain(:subject, :type, :capitalize).and_return('Type')
    message = I18n.t('.timeline_events.created_entry', :user_name => subject.user_name, :company_name => 'Some name', :company_type => "Type")
    subject.message.should eq(message)
  end

  it "should return valid status" do
    subject.status.should eq(I18n.t('.timeline_events.created_status'))
  end

  it "should return title" do
    TimelineEvents::MidbCompanyCreated.any_instance.stub_chain(:subject, :name).and_return('Some name')
    expect(subject.title).to eql('Some name')
  end

  it "should return empty description" do
    expect(subject.description).to be_nil
  end
end