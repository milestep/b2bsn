require 'spec_helper'

describe TimelineEvents::QuestionAnswered do
  let(:author) { FactoryGirl.create(:user) }
  let(:author_profile) { FactoryGirl.create(:user_profile, :user => author) }
  let(:another_user) { FactoryGirl.create(:user) }
  let(:another_profile) { FactoryGirl.create(:user_profile, :user => another_user) }
  let!(:timeline_event) { FactoryGirl.create(:timeline_event, :actor => author, :subject => author_profile) }
  let(:question) { FactoryGirl.create(:question, :user => author) }
  let(:answer) { FactoryGirl.create(:answer, :post => question, :user => another_user) }

  subject { FactoryGirl.create(:timeline_event_question_answered) }

  it "should return valid message" do
    expect(subject.message).to eq(I18n.t('.timeline_events.question_answered_message'))
  end

  it "should return user fullname" do
    expect(subject.author_name).to eql(answer.user_fullname)
  end

  describe "#question_answered?" do
    it "should return true" do
      expect(subject.question_answered?).to be_true
    end
  end
end
