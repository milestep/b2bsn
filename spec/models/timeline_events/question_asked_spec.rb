require 'spec_helper'

describe TimelineEvents::QuestionAsked do
  subject { FactoryGirl.create(:timeline_event_question_asked) }

  it "should return valid message" do
    subject.message.should eq(I18n.t('.timeline_events.question_asked_message'))
  end

  describe "#question_asked?" do
    it "should return true" do
      expect(subject.question_asked?).to be_true
    end
  end
end
