require 'spec_helper'

describe TimelineEvents::ProfileUpdated do
  subject { FactoryGirl.create(:timeline_event_profile_updated) }

  it "should return valid message" do
    subject.message.should eq(I18n.t('.timeline_events.profile_updated_message', :to => subject.to))
  end

  describe "#profile_update?" do
    it "should return true" do
      expect(subject.profile_update?).to be_true
    end
  end
end
