require 'spec_helper'

describe TimelineEvent do

  describe 'assosiations' do

    subject { FactoryGirl.create(:timeline_event) }

    it { should have_and_belong_to_many(:user_shares) } 

  end

end
