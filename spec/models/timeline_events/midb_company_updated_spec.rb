require 'spec_helper'

describe TimelineEvents::MidbCompanyUpdated do
  subject { FactoryGirl.create(:timeline_event_midb_company_updated) }

  it "should return valid message if changes count = 1" do
    subject.stub(:company_name).and_return('Some name')
    subject.stub_chain(:fields_changes, :keys, :count).and_return(1)
    subject.stub_chain(:subject, :type, :capitalize).and_return('Type')
    subject.stub_chain(:fields_changes, :keys, :first).and_return('email')
    subject.stub_chain(:fields_changes, :values, :first, :last).and_return('test@test.com')
    message = I18n.t('.timeline_events.single_update', :user_name => subject.user_name, :company_name => 'Some name', :field_name => 'email', :field_value => 'test@test.com')
    subject.message.should eq(message)
  end

  it "should return valid message if changes count > 1" do
    subject.stub(:company_name).and_return('Some name')
    subject.stub_chain(:fields_changes, :keys, :count).and_return(2)
    subject.stub_chain(:subject, :type, :capitalize).and_return('Type')
    message = I18n.t('.timeline_events.multi_update', :user_name => subject.user_name, :company_type => 'Type')
    subject.message.should eq(message)
  end

  it "should return valid status" do
    subject.status.should eq(I18n.t('.timeline_events.updated_status'))
  end

  it "should return title" do
    TimelineEvents::MidbCompanyUpdated.any_instance.stub_chain(:subject, :name).and_return('Some name')
    expect(subject.title).to eql('Some name')
  end

  it "should return empty description" do
    expect(subject.description).to be_nil
  end
end