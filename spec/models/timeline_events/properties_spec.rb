require 'spec_helper'

describe TimelineEvent do

  describe 'properties' do
    
    let(:author) { FactoryGirl.create(:user) }
    let(:some_other_event) { FactoryGirl.create(:timeline_event) }
    subject { FactoryGirl.create(:timeline_event, :subject_type => 'Post', :actor => author) }

    it "should reflect shared_subject_type" do
      subject.shared_subject_type.should eq('post')
    end

    it "should be aware of the timeline author" do
      subject.author?(author).should be_true
    end

    it "should not allow user to share event" do
      author.allowed_to_share?(subject).should be_false
    end

    it "should allow user to share event" do
      author.allowed_to_share?(some_other_event).should be_true
    end

    it "should share event for particular user" do
      author.share_event!(some_other_event)
      author.event_shares.should include(some_other_event)
    end

  end
  
end
