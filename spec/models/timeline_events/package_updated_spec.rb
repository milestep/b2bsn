require 'spec_helper'

describe TimelineEvents::PackageUpdated do
  let(:package) { FactoryGirl.create(:package, :name => 'package_test_name') }
  let(:event) { FactoryGirl.create(:timeline_event_package_updated, :subject => package) }

  it "should return valid message" do
    event.message.should eq(I18n.t('.timeline_events.package_updated_message', :to => event.to))
  end

  describe "#package_updated?" do
    it "should return true" do
      expect(event.package_updated?).to be_true
    end
  end

  describe "#package_name" do
    it "should return name of package" do
      expect(event.package_name).to eq('package_test_name')
    end
  end

  describe "#package_path" do
    it "should return path to package" do
      expect(event.package_path).to eq("/users/#{package.packagable.id}/packages/#{package.id}")
    end
  end
end
