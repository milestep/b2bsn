require 'spec_helper'

describe TimelineEvents::ClickCreated do
  subject { FactoryGirl.create(:timeline_event_click_created) }

  it "should return valid message" do
    subject.message.should eq(I18n.t('.timeline_events.click_created_message'))
  end
end