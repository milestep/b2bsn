require 'spec_helper'

describe BrandAdvertiser do

  let(:brand) { FactoryGirl.create(:brand) }
  let(:advertiser) { FactoryGirl.create(:advertiser) }
  let(:brand_advertiser) { FactoryGirl.create(:brand_advertiser, :brand => brand, :advertiser => advertiser) }

  context "associations" do
    subject { brand_advertiser }
    it { should belong_to(:brand) }
    it { should belong_to(:advertiser) }
  end

  it "should return advertiser name" do
    brand_advertiser.advertiser_name.should eql(advertiser.name)
  end

  it "should return brand name" do
    brand_advertiser.brand_name.should eql(brand.name)
  end

end
