require 'spec_helper'

describe AgencyBrand do

  let(:agency) { FactoryGirl.create(:agency) }
  let(:brand) { FactoryGirl.create(:brand)}
  let(:agency_brand) { AgencyBrand.create(:agency => agency, :brand => brand) }

  context "associations" do
    subject { agency_brand }
    it { should belong_to(:agency) }
    it { should belong_to(:brand) }
  end

  it "should return brand name" do
    agency_brand.brand_name.should eql(agency_brand.brand.name)
  end

  it "should return agency name" do
    agency_brand.agency_name.should eql(agency_brand.agency.name)
  end

end
