require 'spec_helper'

describe Connection do

  let(:user) { FactoryGirl.create(:user, :first_name => "UserName", :last_name => "UserLastName") }
  let(:friend) { FactoryGirl.create(:user, :first_name => "FriendName", :last_name => "FriendLastName") }
  let!(:connection) { FactoryGirl.create(:connection, :user => user, :friend => friend) }

  context "associations" do

    subject { connection }
    it { should belong_to(:user) }
    it { should belong_to(:friend) }

  end

  context "validations" do

    subject { connection }
    it { should_not allow_value("m" * 252).for(:message) }

  end

  context "scopes" do

    subject { connection }
    it "should find all connections by user_id or friend_id" do
      Connection.by_friend_id_or_owner_id([user.id]).should include(connection)
    end

    it "should find friends within seven days" do
      user.connections.within_seven_days.should include(connection)
    end

    context "accepted_or_pending" do
      let(:some_user) { FactoryGirl.create(:user) }
      let!(:accepted_connection) { FactoryGirl.create(:connection, :user => some_user, :status => "accepted") }
      let!(:pending_connection) { FactoryGirl.create(:connection, :user => some_user, :status => "pending") }
      let!(:declined_connection) { FactoryGirl.create(:connection, :user => some_user, :status => "declined") }
      let!(:deleted_connection) { FactoryGirl.create(:connection, :user => some_user, :status => "deleted") }

      it "should return only accepted or pending connections" do
        expect(some_user.connections.accepted_or_pending).to eq([accepted_connection, pending_connection])
      end
    end
  end

  describe "#friend" do
    it "should return friend" do
      connection.reload.friend.should eql(friend)
    end

    it "should return deleted creator" do
      friend.destroy
      connection.reload.friend.should eql(friend)
    end
  end

  describe "#scoped_friend" do
    it "should return friend" do
      connection.reload.scoped_friend.should eql(friend)
    end

    it "should not return deleted friend" do
      friend.destroy
      connection.reload.scoped_friend.should be_nil
    end
  end

  describe "#update_status" do
    subject { connection }

    it "should have declined status" do
      subject.fire_status_event("Accept")
      subject.accepted?.should be_true
    end

    it "should have declined status" do
      subject.fire_status_event("Reject")
      subject.declined?.should be_true
    end

    it "should have deleted status" do
      subject.fire_status_event("Delete")
      subject.deleted?.should be_true
    end

    it "should have pending status" do
      subject.fire_status_event("Undo")
      subject.pending?.should be_true
    end

    it "should have blocked status" do
      subject.fire_status_event("Block")
      expect(subject.blocked?).to be_true
    end

    it "should have ignored status" do
      subject.fire_status_event("Ignore")
      expect(subject.ignored?).to be_true
    end
  end

  describe "#user_and_friend_difference" do
    it "should create an error if user and friend is the same user" do
      connection.update_attributes(:user => user, :friend => user).should be_false
      connection.errors[:friend].should include(I18n.t('activerecord.errors.models.connection.attributes.friend.should_be_different_from_user'))
    end

    it "should not create an error if user and friend are different users" do
      connection.update_attributes(:user => user, :friend => friend).should be_true
      connection.errors[:friend].should be_empty
    end
  end

  describe "#requests" do
    it "should return incoming_request" do
      expect(connection.incoming_for?(friend)).to be_true
    end

    it "should return outgoing_request" do
      expect(connection.incoming_for?(user)).to be_false
    end

    context "#actor" do
      it "should return connection user" do
        expect(connection.actor(user)).to eq friend
      end
    end

    context "#actor" do
      it "should return connection user" do
        expect(connection.actor(user)).to eq friend
      end
    end
  end
end
