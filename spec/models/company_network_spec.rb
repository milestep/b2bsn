require 'spec_helper'

describe CompanyNetwork do

  let(:company) { FactoryGirl.create(:company) }
  let(:network) { FactoryGirl.create(:network)}
  let(:company_network) { CompanyNetwork.create(:company => company, :network => network) }

  context "associations" do
    subject { company_network }
    it { should belong_to(:company) }
    it { should belong_to(:network) }
  end

  it "should return network name" do
    company_network.network_name.should eql(network.name)
  end

end
