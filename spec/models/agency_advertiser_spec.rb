require 'spec_helper'

describe AgencyAdvertiser do

  let(:agency) { FactoryGirl.create(:agency) }
  let(:advertiser) { FactoryGirl.create(:advertiser)}
  let(:agency_advertiser) { AgencyAdvertiser.create(:agency => agency, :advertiser => advertiser) }

  context "associations" do
    subject { agency_advertiser }
    it { should belong_to(:agency) }
    it { should belong_to(:advertiser) }
  end

  it "should return advertiser name" do
    agency_advertiser.advertiser_name.should eql(agency_advertiser.advertiser.name)
  end

  it "should return agency name" do
    agency_advertiser.agency_name.should eql(agency_advertiser.agency.name)
  end

end
