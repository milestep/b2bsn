require 'spec_helper'

describe Contact do

  let(:phone) { FactoryGirl.create(:phone) }
  let(:address) { FactoryGirl.create(:address, :phones => [phone]) }
  let(:contact) { FactoryGirl.create(:contact, :address => address) }

  context 'associations' do
    subject { contact }
    it { should belong_to(:user) }
    it { should belong_to(:company) }
    it { should belong_to(:address) }
  end

  it "should return fullname" do
    contact.fullname.should eql("#{contact.first_name} #{contact.last_name}")
  end

  it "should return office phone" do
    contact.office_phone.should eql(phone.number)
  end

  it "should return company name" do
    contact.company_name.should eql(contact.company.name)
  end

end
