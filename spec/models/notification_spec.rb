require 'spec_helper'

describe Notification do
  let(:user) { FactoryGirl.create(:user) }
  let(:friend) { FactoryGirl.create(:user) }
  let(:receipt) { user.send_message(friend, "body", "subject", true, nil )}

  it "should check recipients for black list" do
    friend.blocked_users << user
    expect(receipt.errors[:"notification.recipients"]).to be_true
  end
end