require 'spec_helper'
include ActionDispatch::TestProcess

describe Experience do

  let(:experience) { FactoryGirl.create(:experience) }

  context 'associations' do
    subject { experience }
    it { should belong_to(:user_profile) }
    it { should belong_to(:company) }
  end

  context 'validations' do
    subject { experience }
    it "validates that start date should be <= end date" do
      experience = FactoryGirl.build(:experience, :end_date => (Date.today - 3.days))
      experience.should be_invalid
      experience.should have(1).error_on(:end_date)
    end

    it "validates that start date should be <= today" do
      Date.stub(:today).and_return(Date.new(2012, 1, 1))
      experience = FactoryGirl.build(:experience, :start_date => Date.new(2012, 1, 2))
      experience.should be_invalid
      expect(experience).to have(1).error_on(:start_date)
    end

    it "validates that end date should be <= today" do
      Date.stub(:today).and_return(Date.new(2012, 1, 1))
      experience = FactoryGirl.build(:experience, :end_date => Date.new(2012, 1, 2), :start_date => Date.new(2012, 1, 1))
      experience.should be_invalid
      expect(experience).to have(1).error_on(:end_date)
    end

    it "validates that start date is present" do
      experience = FactoryGirl.build(:experience, :end_date => Date.new(2012, 1, 2), :start_date => nil)
      experience.should be_invalid
      expect(experience).to have(1).error_on(:start_date)
    end

    it "validates that end date is present for previuos position" do
      experience = FactoryGirl.build(:experience, :end_date => nil, :start_date => Date.new(2012, 1, 1), :current => false)
      experience.should be_invalid
      expect(experience).to have(1).error_on(:end_date)
    end

    it "validates that end date is absent for current position" do
      experience = FactoryGirl.build(:experience, :end_date => Date.new(2012, 1, 2), :start_date => Date.new(2012, 1, 1), :current => true)
      experience.should be_invalid
      expect(experience).to have(1).error_on(:end_date)
    end
  end

  it "should return time period string" do
    experience.time_period.should be_a_kind_of(String)
  end

  describe "#company_logo_url" do
    it "should return company logo url" do
      subject.company_logo_url.should eql(subject.company_logo.url)
    end
  end

  describe "#company_logo_file_name" do
    it "should return company logo file name" do
      subject.update_attribute(:company_logo, fixture_file_upload('/files/test_image.png', 'image/png'))
      expect(subject.company_logo_file_name).to eql("test_image.png")
    end
  end

  describe "#end_month" do
    it "should return Today word for current experience" do
      experience.stub(:current).and_return(true)
      expect(experience.end_month).to eql("Today")
    end

    it "should return month name for previous exprience" do
      experience.stub(:current).and_return(false)
      experience.stub(:end_date).and_return(Date.new(2011,12,1))
      expect(experience.end_month).to eql("December")
    end

    it "should return nil for previous experience with empty end date" do
      experience.stub(:current).and_return(false)
      experience.stub(:end_date).and_return(nil)
      expect(experience.end_month).to be_nil
    end
  end

  describe "#end_year" do
    it "should return present year for current experience" do
      experience.stub(:current).and_return(true)
      expect(experience.end_year).to eql(Date.today.year)
    end

    it "should return year value for previous exprience" do
      experience.stub(:current).and_return(false)
      experience.stub(:end_date).and_return(Date.new(2011,12,1))
      expect(experience.end_year).to eql(2011)
    end

    it "should return nil for previous exprience with empty end date" do
      experience.stub(:current).and_return(false)
      experience.stub(:end_date).and_return(nil)
      expect(experience.end_year).to be_nil
    end
  end

  describe "default order scope" do
    let(:user_profile) { FactoryGirl.create(:user_profile) }
    let(:company) { FactoryGirl.create(:company) }
    let!(:first_experience) { FactoryGirl.create(:experience, :user_profile => user_profile, :company => company) }
    let!(:second_experience) { FactoryGirl.create(:experience, :user_profile => user_profile, :company => company) }

    it "should sort by current field firstly" do
      first_experience.update_attribute(:current, true)
      second_experience.update_attribute(:current, false)
      expect(user_profile.experiences.first).to eql(first_experience)
      expect(user_profile.experiences.last).to eql(second_experience)
    end

    it "should sort by end date if current fields are the same" do
      first_experience.update_attributes({:current => false, :start_date => Date.new(2011, 1, 1), :end_date => Date.new(2012, 1, 1)})
      second_experience.update_attributes({:current => false, :start_date => Date.new(2011, 1, 1), :end_date => Date.new(2011, 12, 31)})
      expect(user_profile.experiences.first).to eql(first_experience)
      expect(user_profile.experiences.last).to eql(second_experience)
    end

    it "should sort by start date if current and end date fields are the same and current position is false" do
      first_experience.update_attributes({:current => false, :start_date => Date.new(2011, 1, 3), :end_date => Date.new(2012, 1, 1)})
      second_experience.update_attributes({:current => false, :start_date => Date.new(2011, 1, 1), :end_date => Date.new(2012, 1, 1)})
      expect(user_profile.experiences.first).to eql(first_experience)
      expect(user_profile.experiences.last).to eql(second_experience)
    end

    it "should sort by start date if current and end date fields are the same and current position is true" do
      first_experience.update_attributes({:current => true, :start_date => Date.new(2011, 1, 3), :end_date => nil})
      second_experience.update_attributes({:current => true, :start_date => Date.new(2011, 1, 1), :end_date => nil})
      expect(user_profile.experiences.first).to eql(first_experience)
      expect(user_profile.experiences.last).to eql(second_experience)
    end
  end
end
