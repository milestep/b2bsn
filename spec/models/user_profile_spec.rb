require 'spec_helper'

describe UserProfile do

  let(:author) { FactoryGirl.create(:user) }
  let(:address) { FactoryGirl.create(:address) }
  let(:user_profile) { FactoryGirl.create(:user_profile) }
  let(:timeline_event) { FactoryGirl.create(:timeline_event, :actor => author, :subject => user_profile) }
  let(:another_timeline_event) { FactoryGirl.create(:timeline_event, :actor => author, :subject => user_profile) }
  let(:valid_attrs) { FactoryGirl.attributes_for(:user_profile, :title => 'title', :description => 'description') }

  context 'associations' do
    subject { user_profile }
    it { should belong_to(:user) }
    it { should belong_to(:company) }
    it { should belong_to(:address) }
    it { should have_many(:experiences) }
    it { should have_many(:timeline_events) }
    it { should have_and_belong_to_many(:skills) }
  end

  it 'should return fullname' do
    user_profile.fullname.should eql(user_profile.user.fullname.titleize)
  end

  it 'should return last event' do
    user_profile.timeline_event.should eq(user_profile.timeline_events.last)
  end

  it 'should return company name' do
    user_profile.company_name.should eql(user_profile.company.name)
  end

  it 'should return location' do
    user_profile.address = address
    user_profile.save
    user_profile.location.should be_a_kind_of(String)
  end

  it "should return location with state" do
    address.stub(:state_abbrev).and_return 'NY'
    user_profile.address = address
    user_profile.location.should eq("#{address.city}, NY")
  end

  it "shouldn't return location with state if state is blank" do
    address.stub(:state_abbrev).and_return nil
    user_profile.address = address
    user_profile.location.should eq(address.city)
  end

  it "should return nil as profile lication value if profile address doesn't exist" do
    user_profile.stub(:address).and_return nil
    user_profile.location.should eq(nil)
  end

  it 'should not return location' do
    user_profile.location.should be_nil
  end

  it 'should return true if visibility is public' do
    user_profile.visibility = 'public'
    user_profile.public_visibility?.should be_true
  end

  it 'should return true if visibility is connections' do
    user_profile.visibility = 'connections'
    user_profile.connection_visibility?.should be_true
  end

  it 'should return true if visibility is publishers' do
    user_profile.visibility = 'publishers'
    user_profile.publisher_visibility?.should be_true
  end

  it 'should return the summary in list of updated fields' do
    user_profile.description = 'description'
    user_profile.fields_changed.should include('summary')
  end

  it 'should return the basic information in list of updated fields' do
    user_profile.title = 'title'
    user_profile.fields_changed.should include('basic information')
  end

  describe "#city_and_country" do
    it 'should return location' do
      address.stub(:country_code).and_return 'ua'
      user_profile.address = address
      user_profile.save
      user_profile.city_and_country.should be_a_kind_of(String)
    end

    it "should return city and country" do
      address.stub(:country_code).and_return 'ua'
      user_profile.address = address
      user_profile.city_and_country.should eq("#{address.city}, Ukraine")
    end

    it "shouldn't include country if country code is blank" do
      address.stub(:country_code).and_return nil
      user_profile.address = address
      user_profile.city_and_country.should eq(address.city)
    end

    it "shouldn't include city if city is blank" do
      address.stub(:city).and_return nil
      address.stub(:country_code).and_return "ua"
      user_profile.address = address
      user_profile.city_and_country.should eq("Ukraine")
    end

    it "should return nil as profile lication value if profile address doesn't exist" do
      user_profile.stub(:address).and_return nil
      user_profile.city_and_country.should eq(nil)
    end
  end
end
