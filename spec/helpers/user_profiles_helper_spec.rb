require 'spec_helper'

describe UserProfilesHelper do
  let(:user) { FactoryGirl.create(:user) }
  let(:another_user) { FactoryGirl.create(:user) }
  let(:experience) { FactoryGirl.create(:experience) }

  describe "#period_and_location" do
    it "should return nil if start date is missing" do
      experience.stub(:current?).and_return(true)
      experience.stub("start_date").and_return(nil)
      expect(helper.period_and_location(experience)).to be_nil 
    end

    it "should return nil if end date is missing for previous experience" do
      experience.stub(:current?).and_return(false)
      experience.stub("end_date").and_return(nil)
      expect(helper.period_and_location(experience)).to be_nil 
    end

    it "should return period and location for current experience" do
      experience.stub(:current?).and_return(true)
      experience.stub(:start_date).and_return(Date.new(2012, 1, 1))
      Time.stub(:now).and_return(Time.new(2012,6,20))
      expect(helper.period_and_location(experience)).to eql("January 2012 - Present (6 months) Kyiv") 
    end

    it "should return period and location for previous experience" do
      experience.stub(:current?).and_return(false)
      experience.stub(:start_date).and_return(Date.new(2010, 6, 1))
      experience.stub(:end_date).and_return(Date.new(2011, 12, 31))
      expect(helper.period_and_location(experience)).to eql("June 2010 - December 2011 (over 1 year) Kyiv") 
    end
  end

  describe "#send_message_button_for" do
    before(:each) do
      helper.stub(:current_user).and_return(user)
      helper.stub(:t).and_return("Send Message")
    end

    it "should not show the button to the same user" do
      expect(helper.send_message_button_for(user)).to be_nil
    end

    it "should show the button to another user" do
      expect(helper.send_message_button_for(another_user)).not_to be_nil
    end
  end
end

