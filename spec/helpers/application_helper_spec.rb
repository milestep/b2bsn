require 'spec_helper'

describe ApplicationHelper do

  describe "#adjust_and_translate" do

    ['some string', 7].each do |value|

      it "should render correct I18iz`ed notification value depending on the value. Value is #{value}" do
        expect(adjust_and_translate('shared.users_right_menu.messages', {:count => value}, 'badge')).to eql("<span class=\"badge\">#{I18n.t('shared.users_right_menu.messages', :count => value)}</span>")
      end

    end

    it "should render correct I18iz`ed notification value at least one key is valid" do
      expect(adjust_and_translate('shared.users_right_menu.messages', {:count => 0, :some_other_key => 'hey'}, 'badge')).to eql("<span class=\"badge\">#{I18n.t('shared.users_right_menu.messages', :count => nil, :some_other_key => 'hey')}</span>")
    end

    it "should render correct I18iz`ed notification value depending on the value. Value is 0" do
      expect(adjust_and_translate('shared.users_right_menu.messages', {:count => 0}, 'badge')).to eql(nil)
    end

  end

  describe "#specific_symbol" do
    let(:company) { FactoryGirl.create(:company) }

    it "should escape specific symbol at the description" do
      escape_new_line_symbols(company.description)
    end
  end

  describe "#truncate_with_link" do
    it "should add link if text is long" do
      link = content_tag("a", "some link")
      truncate_with_link("verrrry loooong text", :length => 12, :link => link).should have_tag(:a)
    end

    it "should not add link if text is short" do
      link = content_tag("a", "some link")
      truncate_with_link("short text", :length => 15, :link => link).should_not have_tag(:a)
    end
  end

  describe 'receiving_date' do
    let(:user) { FactoryGirl.create(:user) }
    let(:conversation) { FactoryGirl.create(:conversation) }
    let(:message) { FactoryGirl.create(:message, :conversation => conversation, :sender => user) }

    before(:each) { helper.stub(:current_user).and_return(user) }

    it 'should return date' do
      conversation.created_at = '13.06.2012'.to_datetime + 7.hour
      helper.receiving_date(conversation).should eq('13.06.2012'.to_datetime.utc.strftime("%B %e, %Y"))
    end

    it 'should return date' do
      helper.receiving_date(conversation).should eq(conversation.created_at.strftime('%l:%M %P').lstrip)
    end
  end

  describe "ajax loading actors" do

    it "should render inline loader" do
      helper.inline_loader({:id => 555}).should eq("<span class=\"inline_ajax_loader\" id=\"555\" style=\"display:none;\"><img src=\"/assets/inline_loader.gif\" /></span>")
    end

    it "should render inline loader with class & randon ID" do
      helper.inline_loader({:id => 555, :class => 'some_class'}).should eq("<span class=\"some_class inline_ajax_loader\" id=\"555\" style=\"display:none;\"><img src=\"/assets/inline_loader.gif\" /></span>")
    end

  end

end
