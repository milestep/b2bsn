require 'spec_helper'

describe ToolsHelper do
  let(:user_profile) { FactoryGirl.create(:user_profile) }
  let(:user) { FactoryGirl.create(:user, :user_profile => user_profile) }
  let(:user_status) { FactoryGirl.create(:user_status, :user => user) }
  let(:company_location) { FactoryGirl.create(:company_location) }

  describe "#discover_item_info" do
    it "should return p with short_address" do
      helper.discover_item_info(company_location).should eq("<p>#{company_location.short_address}</p>")
    end
  end

  describe "#last_seen" do

    it "should return last seen time" do
      helper.last_seen(user).should eq(I18n.t(".last_seen", :time => time_ago_in_words(user.user_status.updated_at)))
    end

  end

  describe "#profile_link" do
    it "should return user profile link" do
      helper.profile_link(user).should eq(user_profile_path(user))
    end

    it "should return link to show company page" do
      helper.profile_link(company_location).should eq(tool_path(company_location.company_id))
    end
  end

  describe "#activity_feed_name" do
    it "should return 'user's activity' string if user is specified" do
      helper.activity_feed_name(user).should eql("#{user.first_name}'s Activity")
    end

    it "should return 'activity feed' string if user is not specified" do
      helper.activity_feed_name(nil).should eql(I18n.t('layouts.market_ticker.activity_feed'))
    end
  end

end
