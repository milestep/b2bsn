require 'spec_helper'

describe PostsHelper do
  let(:user) { FactoryGirl.create(:user) }
  let(:friend) { FactoryGirl.create(:user, :first_name => "Name", :last_name => "Surname") }
  let!(:connection) { FactoryGirl.create(:connection, :user => user, :friend => friend) }
  let(:post) { FactoryGirl.create(:post, :user => user, :title => friend.fullname) }

  describe "#replace_user_name_with_link_in_post" do
    it "should return post title with links to friends user profiles, instead of friends names" do
      helper.replace_user_name_with_link_in_post(post, [friend]).should eq("#{link_to friend.fullname, user_profile_path(friend)}")
    end
  end

end
