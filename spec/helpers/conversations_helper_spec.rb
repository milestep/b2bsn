require 'spec_helper'

describe ConversationsHelper do
  let(:user) { FactoryGirl.create(:user) }
  let(:friend) { FactoryGirl.create(:user) }
  let(:conversation) { FactoryGirl.create(:conversation) }
  let(:message) { FactoryGirl.create(:message, :conversation => conversation, :sender => user) }
  let(:send_message) { user.send_message(friend, "body", "subject", true, nil )}
  let(:receipt_with_incorrect_message) { user.send_message(friend, "", "", true, nil )}

  before(:each) { helper.stub(:current_user).and_return(user) }

  describe 'unread' do
    it 'should return nil' do
      helper.unread(conversation).should be_nil
    end

    it 'should return unread' do
      helper.stub(:unread?).and_return(true)
      helper.unread(conversation).should eq('unread')
    end
  end

  describe 'message_owner_class' do
    it 'should return self_message' do
      expect(helper.message_owner_class(message)).to eq("self_message")
    end
  end

  it "should return controller name by pathname" do
    expect(helper.recognize_controller_name_by_path('/messages')).to eql('conversations')
  end
end
