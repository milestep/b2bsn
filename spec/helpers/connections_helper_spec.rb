require 'spec_helper'

describe ConnectionsHelper do
  let(:user) { FactoryGirl.create(:user) }
  let(:friend) { FactoryGirl.create(:user, :first_name => "Name", :last_name => "Surname") }
  let(:connection) { FactoryGirl.create(:connection, :user => user, :friend => friend) }
  let(:reverse_connection) { FactoryGirl.create(:connection, :user => friend, :friend => user) }
  
  before(:each) { helper.stub(:current_user).and_return(user) }

  describe "#indicator" do
    it "should return span with class indicator" do
      helper.indicator(:count => 3).should eq("<span class=\"indicator\" data-count=\"3\">3</span>")
    end

    it "should return nothing" do
      helper.indicator(:count => 0).should be_nil
    end

    it "should return friend if current user is connection user" do
      helper.stub(:current_user).and_return(user)
      helper.slider_user(connection).should eq(friend)
    end

    it "should return connection user if current user is not connection user" do
      helper.stub(:current_user).and_return(friend)
      helper.slider_user(connection).should eq(user)
    end

    context "check for invites" do
      let!(:another_user) { FactoryGirl.create(:user) }
      let!(:invitation) { FactoryGirl.create(:invitation, :sender_id => user.id, :linkedin_id => friend.uid) }

      before do
        helper.stub(:current_user).and_return(user)
        connection
      end

      it "should return true if connection was already invited via linkedin" do
        helper.connection_invited?(friend.uid).should be_true
      end

      it "should return false if connection is new via linkedin" do
        helper.connection_invited?("unknown connection").should be_false
      end

      it "should return true if invitation to contact was already sent" do
        helper.contact_invited?(friend).should be_true
      end

      it "should return false if invitation to contact was not sent" do
        helper.contact_invited?(another_user).should be_false
      end
    end
  end

  it "should return correct pages count" do
    helper.max_page(0).should eq(1)
    helper.max_page(Connection::CONNECTIONS_PER_PAGE - 1).should eq(1)
    helper.max_page(Connection::CONNECTIONS_PER_PAGE).should eq(1)
    helper.max_page(Connection::CONNECTIONS_PER_PAGE + 1).should eq(2)
  end

  describe "#request_connection_user_link" do
    it "should return nil if user is already friend" do
      connection
      expect(helper.request_connection_user_link(friend)).to be_empty
    end

    it "should return nil if user is already requested friend" do
      reverse_connection
      expect(helper.request_connection_user_link(friend)).to be_empty
    end

    it "should return nil if user in black list" do
      user.blocked_users << friend
      expect(helper.request_connection_user_link(friend)).to be_empty
    end

    it "should return connect link for new user" do
      expect(helper.request_connection_user_link(friend)).to have_tag(:a, :with => {:href => new_connection_path(:friend_id => friend)})
    end

    it "should return current user friends" do
      User.any_instance.should_receive(:my_friends).once.and_return([])
      expect(helper.my_friends).to be_empty
    end

    it "should return requested friends" do
      User.any_instance.should_receive(:my_requested_friends).once.and_return([])
      expect(helper.my_requested_friends).to be_empty
    end

    it "should return blocked users of current user" do
      User.any_instance.should_receive(:blocked_users).once.and_return([])
      expect(helper.blocked_users).to be_empty
    end

    it "should return status blocked if user in black list" do
      user.blocked_users << friend
      expect(helper.request_connection_people_link(friend)).to include("Blocked")
    end

    it "should return status pending if user ignored" do
      friend.ignored_users << user
      expect(helper.request_connection_people_link(friend)).to include("Pending")
    end
  end
end
