require 'spec_helper'

describe NewsHelper do
  let(:item_discription) { "some test description" }
  let(:news) { FactoryGirl.create(:news_item) }
  let(:news_with_html_tags) { FactoryGirl.create(:news_item, :description => "<p></br></p><") }
  let(:news_with_mixed_description) { FactoryGirl.create(:news_item, :description => "<p>#{item_discription}</br></p><") }
  let(:news_with_nil_description) { FactoryGirl.create(:news_item, :description => nil) }

  it "should return truncated news description" do
    helper.news_excerpt(news).length.should == NewsItem::TRUNCATION_LENGTH
  end

  it "should return nil, because description is empty" do
    helper.news_excerpt(news_with_nil_description).should be_nil
  end

  it "should return empty truncated news description, because it includes only html tags" do
    helper.news_excerpt(news_with_html_tags).should be_empty
  end

  it "should return only truncated text without html tags" do
    helper.news_excerpt(news_with_mixed_description).should eq(item_discription)
  end

  it "should return true if news is latest" do
    helper.today_first_news?(Time.now.to_formatted_s(:day_week), 0).should be_true
  end

  it "should return false if news is old" do
    helper.today_first_news?((Time.now - 3.days).to_formatted_s(:day_week), 0).should be_false
  end

  it "should return false if news is not first" do
    helper.today_first_news?(Time.now.to_formatted_s(:day_week), 2).should be_false
  end

end
