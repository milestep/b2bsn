require 'spec_helper'

describe SearchesHelper do

  describe "#results_for" do

    it "should return valid search title" do
      expect(results_for('search for string')).to eq("<p>#{t('searches.index.search_results_for', :search_for => 'search for string')}</p>")
    end

  end

end
