require 'spec_helper'

module Devise

  def log_in user
    page.set_rack_session(:user_id => user.id)
  end

end

RSpec.configure do |config|
  config.include Devise, type: :request
end
