shared_examples_for "an object with search_formatters" do

  %w(search_location search_site_name short_search_site_name logo search_category search_like).each do |method_name|
    it "should respond to" do
      subject.should respond_to(method_name)
    end
  end

end
