require 'spec_helper'

describe Feeds::BaseFeed do
  let(:news_feed) { FactoryGirl.create(:news_feed) }
  let(:news_item) { FactoryGirl.create(:news_item, :news_feed => news_feed) }
  let(:news_item_feed) { Feeds::BaseFeed.new(news_item) }

  it "should create new object based on news item" do
    expect(news_item_feed.obj).to eql(news_item)
    expect(news_item_feed.image_url).to be_nil
    expect(news_item_feed.feed_status).to be_nil
    expect(news_item_feed.url).to be_nil
  end
end