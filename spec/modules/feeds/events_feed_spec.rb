require 'spec_helper'

describe Feeds::EventsFeed do
  let(:author) { FactoryGirl.create(:user) }
  let(:author_profile) { FactoryGirl.create(:user_profile, :user => author) }
  let(:timeline_event) { FactoryGirl.create(:timeline_event, :actor => author, :subject => author_profile) }
  let(:activity_item_feed) { Feeds::EventsFeed.new(timeline_event) }

  it "should create new object based on tirmeline event" do
    expect(activity_item_feed.obj).to eql(timeline_event)
  end

  it "should return basic field values uses in json" do
    expect(activity_item_feed.feed_title).not_to be_nil
    expect(activity_item_feed.description).not_to be_nil
    expect(activity_item_feed.feed_time).not_to be_nil
    expect(activity_item_feed.feed_type).to eql("Activity")
    expect(activity_item_feed.feed_class).to eql("activities")
    expect(activity_item_feed.real_time).not_to be_nil
  end
end