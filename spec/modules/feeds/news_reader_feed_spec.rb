require 'spec_helper'

describe Feeds::NewsReaderFeed do
  let(:news_feed) { FactoryGirl.create(:news_feed) }
  let(:news_item) { FactoryGirl.create(:news_item, :news_feed => news_feed) }
  let(:news_item_feed) { Feeds::NewsReaderFeed.new(news_item) }

  it "should create new object based on news item" do
    expect(news_item_feed.obj).to eql(news_item)
  end

  it "should return basic field values uses in json" do
    expect(news_item_feed.feed_title).not_to be_nil
    expect(news_item_feed.description).not_to be_nil
    expect(news_item_feed.feed_time).not_to be_nil
    expect(news_item_feed.feed_type).to eql("News")
    expect(news_item_feed.feed_class).to eql("news")
    expect(news_item_feed.real_time).not_to be_nil
  end
end