require 'spec_helper'

describe AnswerPresenter do
  let(:user) { FactoryGirl.create(:user) }
  let(:answer) { FactoryGirl.create(:answer, :user => user) }
  subject { AnswerPresenter.new(answer, view) }

  before(:each) { view.stub(:current_user).and_return(user) }

  describe "#like_button_id" do
    it "should return appropriate id" do
      subject.like_button_id.should eql("answer-like-#{answer.id}")
    end
  end

  describe "#like_link_url" do
    it "should return like link url" do
      subject.like_link_url.should eql(answer_likes_path(answer))
    end

    it "should return like link url with downvote true" do
      answer.liked_by user
      subject.like_link_url.should eql(answer_likes_path(answer, :downvote => true))
    end
  end

  describe "#likeable" do
    it "should return answer" do
      subject.likeable.should eql(answer)
    end
  end

  describe "#after_create_like_partial" do
    it "should return appropriate partial" do
      subject.after_create_like_partial.should eql("answer_create")
    end
  end
end
