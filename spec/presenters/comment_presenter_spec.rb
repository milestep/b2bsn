require 'spec_helper'

describe CommentPresenter do
  let(:user) { FactoryGirl.create(:user) }
  let(:comment) { FactoryGirl.create(:comment, :user => user) }
  subject { CommentPresenter.new(comment, view) }

  before(:each) { view.stub(:current_user).and_return(user) }

  describe "#like_button_id" do
    it "should return appropriate id" do
      subject.like_button_id.should eql("comment-like-#{comment.id}")
    end
  end

  describe "#like_link_url" do
    it "should return like link url" do
      subject.like_link_url.should eql(comment_likes_path(comment))
    end

    it "should return like link url with downvote true" do
      comment.liked_by user
      subject.like_link_url.should eql(comment_likes_path(comment, :downvote => true))
    end
  end

  describe "#likeable" do
    it "should return comment" do
      subject.likeable.should eql(comment)
    end
  end

  describe "#after_create_like_partial" do
    it "should return appropriate partial" do
      subject.after_create_like_partial.should eql("comment_create")
    end
  end

  describe "#shared_link?" do
    it "should return false" do
      subject.shared_link?.should be_false
    end
  end
end
