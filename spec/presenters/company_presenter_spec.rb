require 'spec_helper'

describe CompanyPresenter do
  let(:user) { FactoryGirl.create(:user) }
  let(:company) { FactoryGirl.create(:company, :name => "Test") }
  let(:address) { FactoryGirl.create(:address, :city => 'San Francisco', :state_abbrev => 'CA') }
  let!(:company_location) { FactoryGirl.create(:company_location, :company => company, :address => address) }

  subject { CompanyPresenter.new(company, view) }

  before(:each) { view.stub(:current_user).and_return(user) }

  describe "#name" do
    it "should return company name" do
      subject.name.should eql("Test")
    end
  end

  describe "#logo" do
    it "should return logo company" do
      subject.logo.should include(subject.logo)
    end

    it "should include logo company" do
      expect(subject.logo).to have_tag("img", :with => { :src => company.linkedin_logo })
    end

    it "should include logo company with specified size" do
      expect(subject.logo(:size => "50x50")).to have_tag("img", :with => {:src => company.linkedin_logo, :width => "50", :height => "50"})
    end
  end

  describe "#location" do
    it "should return location" do
      subject.location.should eql("<p>#{company_location.short_address}</p>")
    end
  end

  describe "#about_me" do
    it "should render label 'Description'" do
      subject.about_me.should eql("Description")
    end
  end

  describe "#description" do
    it "should return text description" do
      subject.description.should eql(company.description)
    end
  end
	describe "#nav_tabs" do
    it "should return nil" do
      subject.nav_tabs.should be_nil
    end
  end

  describe "#render_tabs" do
    it "should return nil" do
      subject.render_tabs.should be_nil
    end
  end

  [:title, :type].each do |name|
    describe "\##{name}" do
      it "should be nil" do
        subject.send(name).should be_nil
      end
    end
  end

  describe "#header_buttons" do
    it "should return nil" do
      subject.header_buttons.should be_nil
    end
  end

  describe "#profile_path" do
    it "should path to show company page" do
      expect(subject.profile_path).to eql("/connections/#{company.id}")
    end
  end
end
