require 'spec_helper'

describe PostPresenter do
  let(:user) { FactoryGirl.create(:user) }
  let(:post) { FactoryGirl.create(:post, :user => user) }
  subject { PostPresenter.new(post, view) }

  before(:each) { view.stub(:current_user).and_return(user) }

  describe "#like_button_id" do
    it "should return appropriate id" do
      subject.like_button_id.should eql("post-like-#{post.id}")
    end
  end

  describe "#like_link_url" do
    it "should return like link url" do
      subject.like_link_url.should eql(post_likes_path(post))
    end

    it "should return like link url with downvote true" do
      post.liked_by user
      subject.like_link_url.should eql(post_likes_path(post, :downvote => true))
    end
  end

  describe "#likeable" do
    it "should return post" do
      subject.likeable.should eql(post)
    end
  end

  describe "#after_create_like_partial" do
    it "should return appropriate partial" do
      subject.after_create_like_partial.should eql("post_create")
    end
  end

  describe "#shared_link?" do
    it "should return true" do
      subject.shared_link?.should be_true
    end
  end
end
