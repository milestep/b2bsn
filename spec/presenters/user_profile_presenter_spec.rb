require 'spec_helper'

describe UserProfilePresenter do
  let(:user) { FactoryGirl.create(:user) }
  let(:another_user) { FactoryGirl.create(:user) }
  let(:address) { FactoryGirl.create(:address, :city => 'San Francisco', :state_abbrev => 'CA') }
  let(:user_profile) { FactoryGirl.create(:user_profile, :user => user, :address => address) }

  subject { UserProfilePresenter.new(user_profile, view) }

  before(:each) { view.stub(:current_user).and_return(user) }

  describe "#name" do
    it "should return user name" do
      subject.name.should eql(user.fullname)
    end
  end

  describe "#logo" do
    it "should include logo user" do
      expect(subject.logo).to have_tag("img", :with => {:src => user_profile.avatar.url})
    end

    it "should include logo user with specified size" do
      expect(subject.logo(:size => "50x50")).to have_tag("img", :with => {:src => user_profile.avatar.url, :width => "50", :height => "50"})
    end

    it "should include upload photo button" do
      expect(subject.logo(:with_edit_buttons => true)).to have_tag("span", :text => "UPLOAD PHOTO", :with => {:class => "button violet"})
    end

    it "should include remove logo button" do
      expect(subject.logo(:with_edit_buttons => true)).to have_tag("a", :text => "X REMOVE", :with => {:href => update_photo_user_profile_path(user_profile)})
    end

    it "should not include remove logo button if with_edit_buttons is not specified" do
      expect(subject.logo).to_not have_tag("a", :text => "X REMOVE", :with => {:href => update_photo_user_profile_path(user_profile)})
    end

    it "should not include upload photo button for another user" do
      view.stub(:current_user).and_return(another_user)
      expect(subject.logo(:with_edit_buttons => true)).to_not have_tag("span", :text => "UPLOAD PHOTO", :with => {:class => "button violet"})
    end
  end

  describe "#title" do
    it "should return user title" do
      subject.title.should have_tag('div.title', :text => user_profile.title)
    end
  end

  describe "#location" do
    it "should return user location" do
      subject.location.should eql(user_profile.location)
    end
  end

  describe "#about_me" do
    it "should render label 'about me'" do
      subject.about_me.should eql("About Me")
    end
  end

  describe "#description" do
    it "should return text description" do
      subject.description.should eql(user_profile.description)
    end
  end
  
  describe "#type" do
    it "should return user type" do
      subject.type.should eql(user.user_type)
    end
  end

  describe "#header_buttons" do
    let!(:authorization) { FactoryGirl.create(:authorization, :user => user) }
    let(:ignore_list_user) { FactoryGirl.create(:ignore_list_user, :user => user, :ignored_user => another_user)}

    before(:each) { view.instance_variable_set("@authorizations", [authorization]) }

    it "should include edit profile button for current user" do
      expect(subject.header_buttons).to have_tag(:a, :with => {:href => edit_base_info_user_profile_path(user_profile)})
    end

    context "for another user" do
      let(:another_user) { FactoryGirl.create(:user) }
      before(:each) { view.stub(:current_user).and_return(another_user) }

      it "should include connect button" do
        expect(subject.header_buttons).to have_tag(:a, :with => {:href => new_connection_path(:friend_id => user.id)})
      end

      it "should include message button" do
        expect(subject.header_buttons).to have_tag(:a, :with => {:href => new_conversation_path(:recipient_id => user.id)})
      end

      it "should include link to social network" do
        authorization.stub(:profile_url).and_return("http://www.network.com/SomeUser")
        expect(subject.header_buttons).to have_tag(:a, :with => {:href => "http://www.network.com/SomeUser"})
      end

      it "should include image for social network" do
        authorization.stub(:provider).and_return("twitter")
        expect(subject.header_buttons).to have_tag(:img, :with => {:src => "/assets/social_icons/twitter-strong-square.png"})
      end

      it "should not include connection button because user in ignore list" do
        ignore_list_user
        expect(subject.header_buttons).not_to have_tag(:a, :with => {:href => new_conversation_path(:recipient_id => another_user.id)})
      end
    end
  end

  describe "#profile_path" do
    it "should return path to user profile" do
      expect(subject.profile_path).to eql("/user_profiles/#{user_profile.id}")
    end
  end

  describe "#company_name" do
    it "should return company name" do
      subject.company_name.should have_tag('div.company-name', :text => user_profile.company.name)
    end
  end
end
