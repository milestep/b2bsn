require 'spec_helper'

describe PresenterModules::LikeLink do
  class TestPresenter < BasePresenter
    include PresenterModules::LikeLink
  end

  let(:user) { FactoryGirl.create(:user) }
  let(:another_user) { FactoryGirl.create(:user) }
  let(:post) { FactoryGirl.create(:post, :user => user) }
  subject { TestPresenter.new(post, view) }

  before do
    subject.stub(:likeable).and_return(post)
    subject.stub(:like_link_url).and_return(post_likes_path(post))
    subject.stub(:h).and_return(view)
    subject.stub_chain(:h, :current_user).and_return(another_user)
  end

  describe "#like_link" do
    it "should return like link" do
      link = link_to I18n.t('likes.like'), post_likes_path(post), :method => :post, :remote => true
      subject.like_link.should eql(link)
    end

    it "should include dont like link" do
      post.liked_by another_user
      link = link_to I18n.t('likes.dont_like'), post_likes_path(post), :method => :post, :remote => true
      subject.like_link.html_safe.should eql(link)
    end

    it "should return nil for own post" do
      subject.stub_chain(:h, :current_user).and_return(user)
      subject.like_link.should be_nil
    end
  end

  describe "#like_link_is_displayable?" do
    it "should return true" do
      subject.like_link_is_displayable?.should be_true
    end

    it "should return false" do
      subject.stub_chain(:h, :current_user).and_return(user)
      subject.like_link_is_displayable?.should be_false
    end
  end

  describe "#like_link_params" do
    it "should return downvote param set to true" do
      post.liked_by another_user
      subject.like_link_params.should eql({:downvote => true})
    end

    it "should return empty hash" do
      subject.like_link_params.should be_empty
    end
  end
end
