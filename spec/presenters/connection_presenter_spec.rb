require 'spec_helper'

describe ConnectionPresenter do
  let(:user) { FactoryGirl.create(:user) }
  let(:friend) { FactoryGirl.create(:user, :first_name => "Name", :last_name => "Surname") }
  let!(:friend_profile) { FactoryGirl.create(:user_profile, :user => friend) }
  let(:connection) { FactoryGirl.create(:connection, :user => user, :friend => friend) }
  let(:incoming_connection) { FactoryGirl.create(:connection, :user => friend, :friend => user) }

  subject { ConnectionPresenter.new(connection, view) }

  before(:each) { view.stub(:current_user).and_return(user) }

  describe "#render_undo" do
    it "should render correct link" do
      expect(subject.render_undo).to have_tag("a", :with => {:class => "btn btn-cancel", :"data-method" => :put, :"data-remote" => true, :href => connection_path(connection, :commit => t("requests.options.undo"))}, :text => "UNDO")
    end
  end

  describe "#render_block" do
    let(:presenter) { ConnectionPresenter.new(incoming_connection, view) }

    it "should render correct link #incoming_request" do
      expect(presenter.block_button).to have_tag("a", :with => {:class => "block_user", :"data-method" => :put, :"data-remote" => true, :href => connection_path(incoming_connection, :commit => t("requests.options.block"))}, :text => "")
    end

    it "should return nothing" do
      expect(subject.block_button).to be_nil
    end
  end

  describe "#render_buttons" do
    context "#incoming_request" do
      let(:presenter) { ConnectionPresenter.new(incoming_connection, view) }

      it "should render accept and decline buttons" do
        expect(presenter.render_buttons).to have_tag("a", :with => {:class => "btn btn-cancel", :"data-method" => :put, :"data-remote" => true, :href => connection_path(incoming_connection, :commit => "Reject")}, :text => "DECLINE")
        expect(presenter.render_buttons).to have_tag("a", :with => {:class => "btn btn-primary", :"data-method" => :put, :"data-remote" => true, :href => connection_path(incoming_connection, :commit => "Accept")}, :text => "ACCEPT")
      end
    end

    context "#outgoing_request" do
      it "should return pending and cancel buttons #pending_status" do
        connection.update_attributes(:status => :pending)
        expect(subject.render_buttons).to have_tag("a", :with => {:class => "btn btn-primary disabled", :href => "javascript:void(0)"}, :text => "PENDING")
        expect(subject.render_buttons).to have_tag("a", :with => {:class => "btn btn-cancel", :"data-method" => :put, :"data-remote" => true, :href => connection_path(connection, :commit => "Delete")}, :text => "CANCEL")
      end

      it "should return accepted and view_profile buttons #accepted_status" do
        connection.update_attributes(:status => :accepted)
        expect(subject.render_buttons).to have_tag("a", :with => {:class => "btn btn-primary disabled", :href => "javascript:void(0)"}, :text => "ACCEPTED")
        expect(subject.render_buttons).to have_tag("a", :with => {:class => "btn btn-cancel", :"data-method" => :put, :"data-remote" => true, :href => user_profile_path(connection.friend.user_profile)}, :text => "VIEW PROFILE")
      end

      it "should return declined button #rejected_status" do
        connection.update_attributes(:status => :declined)
        expect(subject.render_buttons).to have_tag("a", :with => {:class => "btn btn-danger", :href => "javascript:void(0)"}, :text => "REJECTED")
      end

      it "should return declined button #blocked_status" do
        connection.update_attributes(:status => :blocked)
        expect(subject.render_buttons).to have_tag("a", :with => {:class => "btn btn-danger", :href => "javascript:void(0)"}, :text => "BLOCKED")
      end

      it "should return declined button #deleted_status" do
        connection.update_attributes(:status => :deleted)
        expect(subject.render_buttons).to have_tag("a", :with => {:class => "btn btn-danger", :href => "javascript:void(0)"}, :text => "CANCELED")
      end

      it "should return declined button #ignored_status" do
        connection.update_attributes(:status => :ignored)
        expect(subject.render_buttons).to have_tag("a", :with => {:class => "btn btn-danger", :href => "javascript:void(0)"}, :text => "IGNORE")
      end
    end
  end

  describe "#visibility" do
    context "#incoming_request" do
      let(:presenter) { ConnectionPresenter.new(incoming_connection, view) }

      it "should display incoming requests" do
        expect(presenter.visibility).to eq ""
      end
    end

    context "#outgoing_request" do
      it "should hide outgoing requests" do
        expect(subject.visibility).to eq "display: none;"
      end
    end
  end

  describe "#created" do
    it "should display created_at" do
      expect(subject.created).to eq connection.created_at.strftime(t('date.formats.middle'))
    end
  end

  describe "#user_short_name" do
    it "should return truncated fullname" do
      friend.update_attributes(:first_name => "TrancatedFirstNameForUser")
      expect(subject.user_short_name).to eq connection.friend.fullname.truncate(25)
    end
  end

  describe "#user_type" do
    it "should return correct user_type" do
      expect(subject.user_type).to eq connection.friend.user_type.titleize
    end
  end

  describe "#user" do
    it "should return connection friend" do
      expect(subject.user).to eq friend
    end
  end
end
