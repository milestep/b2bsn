require 'spec_helper'

describe FeatureRequestPresenter do
  let(:feature_request) { FactoryGirl.create(:feature_request) }
  
  subject { FeatureRequestPresenter.new(feature_request, view) }

  describe "#modal_title" do
    it "should contain right text" do
      expect(subject.modal_title).to eql(I18n.t('presenters.feature_request.modal_title'))
    end
  end
  
  [:tip, :subject].each do |name|
    describe "#{name}" do
      it "should be nil" do
        expect(subject.send(name)).to be_nil
      end
    end
  end
end
