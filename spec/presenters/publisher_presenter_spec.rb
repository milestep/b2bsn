require 'spec_helper'

describe PublisherPresenter do
  let(:user) { FactoryGirl.create(:user) }
  let(:publisher) { FactoryGirl.create(:publisher) }
  subject { PublisherPresenter.new(publisher, view) }

  before(:each) { view.stub(:current_user).and_return(user) }

  describe "#nav_tabs" do
    it "contain should be have links" do
      subject.nav_tabs.should have_tag("a", :with => {:href => "#profile_employees"})
      subject.nav_tabs.should have_tag("a", :with => {:href => "#profile_accepted_units"})
      subject.nav_tabs.should have_tag("a", :with => {:href => "#profile_traffic"})
    end
  end

  describe "#render_tabs" do
    it "contain should be have partials" do
      # needed for view rendering
      view.instance_variable_set(:@company, publisher)
      subject.render_tabs.should have_tag("div", :with => {:id => "profile_employees", :class => "tab-pane active"})
      subject.render_tabs.should have_tag("div", :with => {:id => "profile_accepted_units", :class => "tab-pane"})
      subject.render_tabs.should have_tag("div", :with => {:id => "profile_traffic", :class => "tab-pane"})
    end
  end
end
