require 'spec_helper'

describe AdvertiserPresenter do
  let(:user) { FactoryGirl.create(:user) }
  let(:advertiser) { FactoryGirl.create(:advertiser) }
  subject { AdvertiserPresenter.new(advertiser, view) }

  before(:each) { view.stub(:current_user).and_return(user) }

  describe "#nav_tabs" do
    it "contain should be have links" do
      subject.nav_tabs.should have_tag("a", :with => {:href => "#profile_employees"})
      subject.nav_tabs.should have_tag("a", :with => {:href => "#profile_brands"})
      subject.nav_tabs.should have_tag("a", :with => {:href => "#profile_agencies"})
    end
  end

  describe "#render_tabs" do
    it "contain should be have partials" do
      # needed for view rendering
      view.instance_variable_set(:@company, advertiser)
      subject.render_tabs.should have_tag("div", :with => {:id => "profile_employees", :class => "tab-pane active"})
      subject.render_tabs.should have_tag("div", :with => {:id => "profile_brands", :class => "tab-pane"})
      subject.render_tabs.should have_tag("div", :with => {:id => "profile_agencies", :class => "tab-pane"})
    end
  end
end
