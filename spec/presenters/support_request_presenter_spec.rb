require 'spec_helper'

describe SupportRequestPresenter do
  let(:support_request) { FactoryGirl.create(:support_request) }
  
  subject { SupportRequestPresenter.new(support_request, view) }

  describe "#modal_title" do
    it "should contain right text" do
      expect(subject.modal_title).to eql(I18n.t('presenters.support_request.modal_title'))
    end
  end
  
  describe "#tip" do
    it "should contain right text" do
      expect(subject.tip).to match(/If you experienced a technical issue/)
    end
  end
  
  describe "#subject" do
    it "should contain label tag" do
      expect(subject.subject).to have_tag("label", :text => I18n.t('presenters.support_request.subject'))
    end
    
    it "should contain input tag" do
      expect(subject.subject).to have_tag("input")
    end
  end
end
