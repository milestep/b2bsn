require 'spec_helper'

describe AgencyPresenter do
  let(:user) { FactoryGirl.create(:user) }
  let(:agency) { FactoryGirl.create(:agency) }
  subject { AgencyPresenter.new(agency, view) }

  before(:each) { view.stub(:current_user).and_return(user) }

  describe "#nav_tabs" do
    it "contain should be have links" do
      subject.nav_tabs.should have_tag("a", :with => {:href => "#profile_employees"})
      subject.nav_tabs.should have_tag("a", :with => {:href => "#profile_brands"})
    end
  end

  describe "#render_tabs" do
    it "contain should be have partials" do
      # needed for view rendering
      view.instance_variable_set(:@company, agency)
      subject.render_tabs.should have_tag("div", :with => {:id => "profile_employees", :class => "tab-pane active"})
      subject.render_tabs.should have_tag("div", :with => {:id => "profile_brands", :class => "tab-pane"})
    end
  end
end
