require 'spec_helper'

describe TimelineEvents::CommentCreatedPresenter do
  let(:user) { FactoryGirl.create(:user) }
  let!(:profile) { FactoryGirl.create(:user_profile, :user => user) }
  let(:comment) { FactoryGirl.create(:comment, :user => user) }
  let(:timeline_event) { FactoryGirl.create(:timeline_event_comment_created, :actor => user, :subject => comment) }
  subject { TimelineEvents::CommentCreatedPresenter.new(timeline_event, view) }

  describe "#action" do
    it "should return appropriate action" do
      subject.action.should eql(I18n.t('layouts.market_ticker.comment.wrote_comment'))
    end
  end

  describe "#icon" do
    it "should contain i tag with appropriate class" do
      subject.icon.should have_tag("i", :with => {:class => "icon-comment"})
    end
  end

  describe "#title" do
    it "should return comment text" do
      subject.title.should eql("\"#{comment.comment}\"")
    end
  end

  describe "#post" do
    it "should return commentable" do
      subject.post.should eql(comment.commentable)
    end
  end

  describe "#is_displayable_in_market_ticker?" do
    it "should return false" do
      subject.is_displayable_in_market_ticker?.should be_false
    end
  end

end
