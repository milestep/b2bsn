require 'spec_helper'

describe TimelineEvents::QuestionAskedPresenter do
  let(:user) { FactoryGirl.create(:user) }
  let!(:user_profile) { FactoryGirl.create(:user_profile, :user => user) }
  let(:answer_user) { FactoryGirl.create(:user) }
  let!(:another_user_profile) { FactoryGirl.create(:another_user_profile, :user => answer_user) }
  let(:question) { FactoryGirl.create(:question, :user => user) }
  let!(:first_answer) { FactoryGirl.create(:answer, :user => answer_user, :post => question) }
  let!(:second_answer) { FactoryGirl.create(:answer, :user => answer_user, :post => question) }
  let(:timeline_event) { FactoryGirl.create(:timeline_event_question_asked, :actor => user, :subject => question) }
  subject { TimelineEvents::QuestionAskedPresenter.new(timeline_event, view) }

  before(:each) { view.stub(:current_user).and_return(user) }

  describe "#icon" do
    it "should contain i tag with appropriate class" do
      subject.icon.should have_tag("i", :with => {:class => "icon-question-sign"})
    end
  end

  describe "#message" do
    it "should return question title" do
      subject.message.should eql(question.title)
    end

    it "should include see more link if question is too long" do
      question.stub(:title).and_return(Faker::Lorem.sentence(100))
      subject.message.should have_tag(:a, :with => {:href => post_path(question)})
    end

    it "should not include see more link if question is short" do
      subject.message.should_not have_tag(:a)
    end
  end

  describe "#display" do
    it "should include user avatar image" do
      subject.display.should have_tag("img", :with => {:src => question.user.logo.url})
    end

    it "should include user full name" do
      subject.display.should include(user.fullname)
    end

    it "should include answer" do
      subject.display.should include(question.title)
    end

    it "should include comment link" do
      subject.display.should have_tag("a", :with => {:href => post_path(question)})
    end

    it "should include share link" do
      subject.display.should have_tag("a", :with => {:href => shares_path(:user => user, :activity => question, :event => timeline_event, :next => "someurl")})
    end

    context "like link" do
      let(:another_user) { FactoryGirl.create(:user) }
      let(:another_question) { FactoryGirl.create(:question, :user => another_user) }
      let(:question_event) { FactoryGirl.create(:timeline_event_question_asked, :actor => another_user, :subject => another_question) }
      let(:presenter) { TimelineEvents::QuestionAskedPresenter.new(question_event, view) }

      before(:each) { another_user_profile.update_attributes(:user => another_user) }

      it "should not include like link for own question" do
        subject.display.should_not have_tag("a", :with => {:href => post_likes_path(question)})
      end

      it "should include like link for any not own question" do
        presenter.display.should have_tag("a", :with => {:href => post_likes_path(another_question)})
      end
    end
  end

  describe "#action" do
    it "should return appropriate action" do
      subject.action.should eql(I18n.t('layouts.market_ticker.comment.asked_a_question'))
    end
  end

  describe "#details_popup" do
    it "should include content of popup" do
      subject.details_popup.should have_tag("div", :with => {"data-event" => "event_popup_#{timeline_event.id}"})
    end

    it "should include question" do
      subject.details_popup.should include(subject.display)
    end

    it "should include list of answers" do
      subject.details_popup.should include(subject.answers)
    end

    it "should include form for answer" do
      subject.details_popup.should have_tag("form", :with => {:action => post_answers_path(question)})
    end

    it "should include textarea for answer" do
      subject.details_popup.should have_tag("textarea", :with => {:class => "write_remote_comment", :name => "answer[answer]"})
    end
  end

  describe "#answers" do
    let!(:first_answer_presenter) { TimelineEvents::QuestionAnsweredPresenter.new(first_answer.timeline_event, view) }
    let!(:second_answer_presenter) { TimelineEvents::QuestionAnsweredPresenter.new(second_answer.timeline_event, view) }

    it "should render list of answers" do
      answers_list = first_answer_presenter.display + second_answer_presenter.display
      subject.answers.should eql("#{answers_list}")
    end
  end


  describe "#new_answer" do
    it "should return new answer object" do
      subject.new_answer.should eql(question.answers.last)
    end
  end

  describe "#see_more_link" do
    it "should return see more link if it has see more link" do
      subject.stub(:has_see_more_link?).and_return(true)
      subject.see_more_link.should have_tag("a", :with => {:href => post_path(question), :class => "see_more_link"})
    end

    it "should return noting if it does not have see more link" do
      subject.stub(:has_see_more_link?).and_return(false)
      subject.see_more_link.should be_nil
    end
  end

  describe "#has_see_more_link?" do
    let(:question_with_many_answers) { FactoryGirl.create(:question, :user => user) }
    let(:timeline_event) { FactoryGirl.create(:timeline_event_question_asked, :actor => user, :subject => question_with_many_answers) }
    let(:presenter) { TimelineEvents::QuestionAskedPresenter.new(timeline_event, view) }

    it "should return true if answers count more than 5" do
      6.times{FactoryGirl.create(:answer, :user => answer_user, :post => question_with_many_answers)}
      question_with_many_answers.reload
      presenter.has_see_more_link?.should be_true
    end

    it "should return false if answers count less or equal 5" do
      5.times{FactoryGirl.create(:answer, :user => answer_user, :post => question_with_many_answers)}
      question_with_many_answers.reload
      presenter.has_see_more_link?.should be_false
    end
  end

end
