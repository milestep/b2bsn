require 'spec_helper'

describe TimelineEvents::ProfileUpdatedPresenter do
  let(:user) { FactoryGirl.create(:user) }
  let!(:profile) { FactoryGirl.create(:user_profile, :user => user) }
  let(:timeline_event) { FactoryGirl.create(:timeline_event_profile_updated, :actor => user, :subject => profile) }
  subject { TimelineEvents::ProfileUpdatedPresenter.new(timeline_event, view) }

  describe "#icon" do
    it "should contain i tag with appropriate class" do
      subject.icon.should have_tag("i", :with => {:class => "icon-user"})
    end
  end

  describe "#message" do
    it "should include link to user profile" do
      subject.message.should have_tag("a", :with => {:class => "message", :href => user_profile_path(timeline_event.actor.user_profile)})
    end
  end

  describe "#details_popup" do

    before(:each) do
      view.stub(:current_user).and_return(user)
    end

    it "should include content of popup" do
      subject.details_popup.should have_tag("div", :with => {"data-event" => "event_popup_#{timeline_event.id}"})
    end

    it "should include user avatar image" do
      subject.details_popup.should have_tag("img", :with => {:src => user.logo_url})
    end

    it "should include user full name" do
      subject.details_popup.should include(user.fullname)
    end

    it "should include user profile title" do
      subject.details_popup.should include(profile.title)
    end

    it "should include user profile link" do
      subject.details_popup.should have_tag("a", :with => {:href => user_profile_path(profile)})
    end
  end

  describe "#popup_message" do
    it "should return user profile title " do
      subject.popup_message.should eql(profile.title)
    end
  end
end
