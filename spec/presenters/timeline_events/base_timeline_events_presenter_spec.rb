require 'spec_helper'

describe TimelineEvents::BaseTimelineEventsPresenter do
  let(:user) { FactoryGirl.create(:user) }
  let!(:user_profile) { FactoryGirl.create(:user_profile, :user => user) }
  let(:question) { FactoryGirl.create(:question, :user => user) }
  let(:timeline_event) { FactoryGirl.create(:timeline_event_question_asked, :actor => user, :subject => question) }
  let!(:user_notice) { FactoryGirl.create(:users_notice, :user => user, :timeline_event => timeline_event) }
  subject { TimelineEvents::BaseTimelineEventsPresenter.new(timeline_event, view) }

  before(:each) { view.stub(:current_user).and_return(user) }

  it { should respond_to(:icon) }

  it "should present timeline_event object" do
    subject.timeline_event.should eql(timeline_event)
  end

  it "should have access to user friends instance variable" do
    view.instance_variable_set("@user_friends", ["first friend", "second friend"])
    subject.user_friends.should eql(view.instance_variable_get("@user_friends"))
  end

  describe "#icon" do
    it "should contain i tag" do
      subject.icon.should have_tag("i")
    end
  end

  describe "#logo" do
    it "should return empty string" do
      subject.logo.should be_empty
    end
  end

  describe "#message" do
    it "should return empty string" do
      subject.message.should be_empty
    end
  end

  describe "#is_displayable_in_market_ticker?" do
    it "should return true" do
      subject.is_displayable_in_market_ticker?.should be_true
    end
  end

  describe "#for_market_ticker" do
    before do
      subject.stub(:icon).and_return('test icon')
      subject.stub(:message).and_return('test message')
    end

    it "should contain tr with appropriate class" do
      subject.for_market_ticker.should have_tag("tr", :with => {:class => timeline_event.event_type})
    end

    it "should include icon" do
      subject.for_market_ticker.should include(subject.icon)
    end

    it "should include logo" do
      subject.for_market_ticker.should include(subject.logo)
    end

    it "should include header" do
      subject.for_market_ticker.should include(subject.header)
    end

    it "should include message" do
      subject.for_market_ticker.should include(subject.message)
    end

    it "should include time_place_info" do
      subject.for_market_ticker.should include(subject.time_place_info)
    end

    it "should contain close button" do
      subject.for_market_ticker.should have_tag("td", :with => {:class => "close_button"})
    end
  end

  describe "#for_notices" do
    it "should render notice" do
      subject.for_notices.should include(subject.icon)
      subject.for_notices.should include(subject.header)
      subject.for_notices.should include(subject.message)
      subject.for_notices.should include(subject.created_at_time)
      subject.for_notices.should have_tag("i", :with => {:class => "icon-remove"})
    end
  end

  describe "#for_notices_popup" do
    it "should render notice for popup" do
      subject.for_notices_popup.should include(subject.icon)
      subject.for_notices_popup.should include(subject.message)
      subject.for_notices_popup.should have_tag("i", :with => {:class => "icon-remove"})
    end
  end

  describe "#time_place_info" do
    it "should return time and place information" do
      subject.time_place_info.should eql("#{time_ago_in_words(timeline_event.created_at)} #{I18n.t('layouts.market_ticker.ago')}")
    end
  end

  describe "#header" do
    it "should contain div with header class" do
      subject.header.should have_tag("div", :with => {:class => "header"})
    end

    it "should contain user profile link" do
      subject.header.should have_tag("a", :with => {:href => user_profile_path(timeline_event.actor.user_profile)})
    end

    it "should contain user full name" do
      subject.header.should include(timeline_event.actor.fullname)
    end
  end

  describe "#subject_likes_size" do
    it "should return subject likes size" do
      subject.subject_likes_size.should eql(question.likes.size)
    end
  end

  describe "#title" do
    it "should return question title" do
      subject.title.should eql("\"#{question.title}\"")
    end
  end

  describe "#truncate length" do
    it "should return 200" do
      subject.truncate_length.should eql(200)
    end
  end

  describe "#popup_message" do
    it "should include see more link if question is too long" do
      question.stub(:title).and_return(Faker::Lorem.sentence(100))
      subject.popup_message.should have_tag(:a, :with => {:href => post_path(question)})
    end

    it "should include title of the question" do
      subject.popup_message.should eql("\"#{question.title}\"")
    end
  end

  describe "#post" do
    it "should return question" do
      subject.post.should eql(question)
    end
  end

  describe "#hide_confirm" do
    it "should return hide comfirm message" do
      link = link_to(user.fullname, user_profile_path(user_profile))
      subject.hide_confirm.should include(I18n.t('layouts.market_ticker.hide_confirm', :user_name => link))
    end
  end

  describe "#like_link" do
    let(:another_user) { FactoryGirl.create(:user) }
    let!(:another_user_profile) { FactoryGirl.create(:another_user_profile, :user => another_user) }
    let(:another_question) { FactoryGirl.create(:question, :user => another_user) }
    let(:question_event) { FactoryGirl.create(:timeline_event_question_asked, :actor => another_user, :subject => another_question) }
    let(:presenter) { TimelineEvents::BaseTimelineEventsPresenter.new(question_event, view) }

    it "should not return like link for own post" do
      subject.like_link.should be_empty
    end

    it "should return like link for any not own post" do
      presenter.like_link.should have_tag(:a, :with => {:href => post_likes_path(another_question)})
    end
  end
end
