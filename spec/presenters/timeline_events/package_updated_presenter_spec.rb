require 'spec_helper'

describe TimelineEvents::PackageUpdatedPresenter do
  let(:user) { FactoryGirl.create(:user) }
  let(:package) { FactoryGirl.create(:package, :packagable => user) }
  let(:timeline_event) { FactoryGirl.create(:timeline_event_package_updated, :subject => package) }
  subject { TimelineEvents::PackageUpdatedPresenter.new(timeline_event, view) }

  before(:each) { view.stub(:current_user).and_return(user) }

  describe "#icon" do
    it "should contain i tag with appropriate class" do
      subject.icon.should have_tag("i", :with => {:class => "icon-folder-close"})
    end
  end

  describe "#header" do
    it "should contain div tag with header class" do
      subject.header.should have_tag("div", :with => {:class => "header"})
    end

    it "should contain remote package link" do
      subject.header.should have_tag("a", :with => {:href => user_package_path(package.packagable, package), "data-remote" => true})
    end
  end

  describe "#message" do
    it "should return message" do
      subject.message.should eql(I18n.t("presenters.timeline_events.was_updated"))
    end
  end

  describe "#logo" do
    it "should contain div tag with logo class" do
      subject.logo.should have_tag("div", :with => {:class => "logo"})
    end

    it "should contain logo image" do
      subject.logo.should have_tag("img", :with => {:src => package.logo.url, :height => "38", :width => "38"})
    end
  end

  describe "#details_popup" do
    it "should include content of popup" do
      subject.details_popup.should have_tag("div", :with => {"data-event" => "event_popup_#{timeline_event.id}"})
    end

    it "should include package avatar image" do
      subject.details_popup.should have_tag("img", :with => {:src => package.logo_url})
    end

    it "should include package name" do
      subject.details_popup.should include(package.name)
    end

    it "should include package description" do
      subject.details_popup.should include(package.description)
    end

    it "should include show package link" do
      subject.details_popup.should have_tag("a", :with => {:href => user_package_path(user, package)})
    end

    it "should include link to collaborate page" do
      subject.details_popup.should have_tag("a", :with => {:href => connections_path})
    end
  end
end
