require 'spec_helper'

describe TimelineEvents::QuestionAnsweredPresenter do
  let(:user) { FactoryGirl.create(:user, :first_name => "test", :last_name => "user") }
  let!(:user_profile) { FactoryGirl.create(:user_profile, :user => user) }
  let(:question) { FactoryGirl.create(:question, :user => user) }
  let(:answer) { FactoryGirl.create(:answer, :post => question, :user => user) }
  let(:timeline_event) { FactoryGirl.create(:timeline_event_question_answered, :subject => answer, :actor => user) }
  subject { TimelineEvents::QuestionAnsweredPresenter.new(timeline_event, view) }

  before(:each) { view.stub(:current_user).and_return(user) }

  describe "#icon" do
    it "should contain i tag with appropriate class" do
      subject.icon.should have_tag("i", :with => {:class => "icon-comment"})
    end
  end

  describe "#message" do
    it "should include link to question" do
      subject.message.should have_tag("a", :with => {:class => "message", :href => post_path(answer.post)})
    end

    it "should include fullname of question owner" do
      subject.message.should include(answer.post.user.fullname)
    end
  end

  describe "#display" do
    it "should include user avatar image" do
      subject.display.should have_tag("img", :with => {:src => user.logo_url})
    end

    it "should include user full name" do
      subject.display.should include(user.fullname)
    end

    it "should include answer" do
      subject.display.should include(answer.title)
    end

    it "should include comment link" do
      subject.display.should have_tag("a", :with => {:href => post_path(question)})
    end

    it "should include share link" do
      subject.display.should have_tag("a", :with => {:href => shares_path(:user => user, :activity => answer, :event => timeline_event, :next => "someurl")})
    end

    context "like link" do
      let(:another_user) { FactoryGirl.create(:user) }
      let!(:another_user_profile) { FactoryGirl.create(:another_user_profile, :user => another_user) }
      let(:another_answer) { FactoryGirl.create(:answer, :post => question, :user => another_user) }
      let(:answer_event) { FactoryGirl.create(:timeline_event_question_answered, :actor => another_user, :subject => another_answer) }
      let(:presenter) { TimelineEvents::QuestionAnsweredPresenter.new(answer_event, view) }

      it "should not include like link for own answer" do
        subject.display.should_not have_tag("a", :with => {:href => answer_likes_path(answer)})
      end

      it "should include like link for any not own answer" do
        presenter.display.should have_tag("a", :with => {:href => answer_likes_path(another_answer)})
      end
    end
  end

  describe "#action" do
    it "should return appropriate action" do
      subject.action.should eql(I18n.t('layouts.market_ticker.comment.answered'))
    end
  end

  describe "#details_popup" do
    let(:question_event) { FactoryGirl.create(:timeline_event_question_asked, :subject => question, :actor => user) }
    let(:question_presenter) { TimelineEvents::QuestionAskedPresenter.new(question.timeline_event, view) }
    
    it "should include content of popup" do
      subject.details_popup.should have_tag("div", :with => {"data-event" => "event_popup_#{question.timeline_event.id}"})
    end

    it "should include question" do
      subject.details_popup.should include(question_presenter.display)
    end

    it "should include list of answers" do
      subject.details_popup.should include(question_presenter.answers)
    end

    it "should include form for answer" do
      subject.details_popup.should have_tag("form", :with => {:action => post_answers_path(question)})
    end

    it "should include textarea for answer" do
      subject.details_popup.should have_tag("textarea", :with => {:class => "write_remote_comment", :name => "answer[answer]"})
    end
  end

  describe "#post" do
    it "should return question" do
      subject.post.should eql(question)
    end
  end
end
