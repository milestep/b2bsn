require 'spec_helper'

describe TimelineEvents::StatusUpdatedPresenter do
  let(:user) { FactoryGirl.create(:user) }
  let!(:user_profile) { FactoryGirl.create(:user_profile, :user => user) }
  let(:comment_user) { FactoryGirl.create(:user) }
  let!(:comment_user_profile) { FactoryGirl.create(:comment_user_profile, :user => comment_user) }
  let(:post) { FactoryGirl.create(:post, :user => user) }
  let!(:first_comment) { FactoryGirl.create(:comment, :user => comment_user, :commentable => post) }
  let!(:second_comment) { FactoryGirl.create(:comment, :user => comment_user, :commentable => post) }
  let(:timeline_event) { FactoryGirl.create(:timeline_event_status_updated, :actor => user, :subject => post) }
  subject { TimelineEvents::StatusUpdatedPresenter.new(timeline_event, view) }

  before(:each) { view.stub(:current_user).and_return(user) }

  describe "#icon" do
    it "should contain i tag with appropriate class" do
      subject.icon.should have_tag("i", :with => {:class => "icon-tag"})
    end
  end

  describe "#message" do
    it "should return post title" do
      subject.message.should eql("\"#{post.title}\"")
    end

    it "should include see more link if post is too long" do
      post.stub(:title).and_return(Faker::Lorem.sentence(100))
      subject.message.should have_tag(:a, :with => {:href => post_path(post)})
    end

    it "should not include see more link if post is short" do
      subject.message.should_not have_tag(:a)
    end
  end

  describe "#display" do
    it "should include user avatar image" do
      subject.display.should have_tag("img", :with => {:src => user.logo.url})
    end

    it "should include user full name" do
      subject.display.should include(user.fullname)
    end

    it "should include answer" do
      subject.display.should include(post.title)
    end

    it "should include comment link" do
      subject.display.should have_tag("a", :with => {:href => post_path(post)})
    end

    it "should include share link" do
      subject.display.should have_tag("a", :with => {:href => shares_path(:user => user, :activity => post, :event => timeline_event, :next => "someurl")})
    end

    context "like link" do
      let(:another_user) { FactoryGirl.create(:user) }
      let!(:another_user_profile) { FactoryGirl.create(:another_user_profile, :user => another_user) }
      let(:another_post) { FactoryGirl.create(:post, :user => another_user) }
      let(:post_event) { FactoryGirl.create(:timeline_event_status_updated, :actor => another_user, :subject => another_post) }
      let(:presenter) { TimelineEvents::StatusUpdatedPresenter.new(post_event, view) }

      it "should not include like link for own post" do
        subject.display.should_not have_tag("a", :with => {:href => post_likes_path(post)})
      end

      it "should include like link for any not own post" do
        presenter.display.should have_tag("a", :with => {:href => post_likes_path(another_post)})
      end
    end
  end

  describe "#details_popup" do
    it "should include content of popup" do
      subject.details_popup.should have_tag("div", :with => {"data-event" => "event_popup_#{timeline_event.id}"})
    end

    it "should include update post" do
      subject.details_popup.should include(subject.display)
    end

    it "should include list of comments" do
      subject.details_popup.should include(subject.comments)
    end

    it "should include form for comment" do
      subject.details_popup.should have_tag("form", :with => {:action => post_comments_path(post)})
    end

    it "should include textarea for comment" do
      subject.details_popup.should have_tag("textarea", :with => {:class => "write_remote_comment", :name => "comment[comment]"})
    end
  end

  describe "#comments" do
    let!(:first_comment_presenter) { TimelineEvents::CommentCreatedPresenter.new(first_comment.timeline_event, view) }
    let!(:second_comment_presenter) { TimelineEvents::CommentCreatedPresenter.new(second_comment.timeline_event, view) }

    it "should render list of comments" do
      comments_list = first_comment_presenter.display + second_comment_presenter.display
      subject.comments.should eql(comments_list)
    end
  end

  describe "#action" do
    it "should return appropriate action" do
      subject.action.should eql(I18n.t('layouts.market_ticker.comment.changed_status'))
    end
  end

  describe "#title" do
    it "should include status update text" do
      subject.title.should include(post.title)
    end
  end

  describe "#new_comment" do
    it "should return new comment" do
      subject.new_comment.should eql(post.comments.last)
    end
  end

  describe "#see_more_link" do
    it "should return see more link if it has see more link" do
      subject.stub(:has_see_more_link?).and_return(true)
      subject.see_more_link.should have_tag("a", :with => {:href => post_path(post), :class => "see_more_link"})
    end

    it "should return noting if it does not have see more link" do
      subject.stub(:has_see_more_link?).and_return(false)
      subject.see_more_link.should be_nil
    end
  end

  describe "#has_see_more_link?" do
    let(:status_with_many_comments) { FactoryGirl.create(:status, :user => user) }
    let(:timeline_event) { FactoryGirl.create(:timeline_event_status_updated, :actor => user, :subject => status_with_many_comments) }
    let(:presenter) { TimelineEvents::StatusUpdatedPresenter.new(timeline_event, view) }

    it "should return true if number of comments is more than 5" do
      (Comment::DROPDOWN_LIMIT + 1).times{FactoryGirl.create(:comment, :commentable => status_with_many_comments)}
      status_with_many_comments.reload
      presenter.has_see_more_link?.should be_true
    end

    it "should return false if number of comments is less or equal 5" do
      Comment::DROPDOWN_LIMIT.times{FactoryGirl.create(:comment, :commentable => status_with_many_comments)}
      status_with_many_comments.reload
      presenter.has_see_more_link?.should be_false
    end
  end

end
