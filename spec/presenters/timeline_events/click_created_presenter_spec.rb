require 'spec_helper'

describe TimelineEvents::ClickCreatedPresenter do
  let(:user) { FactoryGirl.create(:user) }
  let!(:profile) { FactoryGirl.create(:user_profile, :user => user) }
  let(:news_item) { FactoryGirl.create(:news_item) }
  let(:click) { FactoryGirl.create(:click, :user => user, :clickable => news_item) }
  let(:comment) { FactoryGirl.create(:comment, :user => user, :commentable => news_item) }
  let(:another_comment) { FactoryGirl.create(:comment, :user => user, :commentable => news_item) }

  let(:timeline_event) { FactoryGirl.create(:timeline_event_click_created, :actor => user, :subject => click) }
  subject { TimelineEvents::ClickCreatedPresenter.new(timeline_event, view) }

  before(:each) { view.stub(:current_user).and_return(user) }

  describe "#action" do
    it "should return appropriate action" do
      subject.action.should eql(I18n.t('layouts.market_ticker.comment.news_viewed'))
    end
  end

  describe "#icon" do
    it "should contain i tag with appropriate class" do
      subject.icon.should have_tag("i", :with => {:class => "icon-tag"})
    end
  end

  describe "#title" do
    it "should return comment text" do
      subject.title.should have_tag(:b, :value => news_item.title)
    end
  end

  describe "#answers" do
    let!(:first_comment_presenter) { TimelineEvents::CommentCreatedPresenter.new(comment.timeline_event, view) }
    let!(:second_comment_presenter) { TimelineEvents::CommentCreatedPresenter.new(another_comment.timeline_event, view) }

    it "should render list of comments" do
      comments_list = first_comment_presenter.display + second_comment_presenter.display
      subject.comments.should eql(comments_list)
    end
  end

  describe "#new_comment" do
    it "should return new comment object" do
      subject.new_comment.should eql(news_item.comments.last)
    end
  end

  describe "#commented_on" do
    it "should return link to subject" do
      subject.commented_on.should have_tag(:a, :with => {:href => news_item_path(news_item)}, :value => news_item.title)
    end
  end

  describe "#see_more_link" do
    it "should return see more link if it has see more link" do
      subject.stub(:has_see_more_link?).and_return(true)
      subject.see_more_link.should have_tag("a", :with => {:href => news_item_path(news_item), :class => "see_more_link"})
    end

    it "should return noting if it does not have see more link" do
      subject.stub(:has_see_more_link?).and_return(false)
      subject.see_more_link.should be_nil
    end
  end

  describe "#has_see_more_link?" do
    let(:news_item_with_many_comments) { FactoryGirl.create(:news_item) }
    let(:click_for_news_item) { FactoryGirl.create(:click, :user => user, :clickable => news_item_with_many_comments) }
    let(:timeline_event) { FactoryGirl.create(:timeline_event_click_created, :actor => user, :subject => click_for_news_item) }
    let(:presenter) { TimelineEvents::ClickCreatedPresenter.new(timeline_event, view) }

    it "should return true if number of comments is more than 5" do
      (Comment::DROPDOWN_LIMIT + 1).times{FactoryGirl.create(:comment, :commentable => news_item_with_many_comments)}
      news_item_with_many_comments.reload
      presenter.has_see_more_link?.should be_true
    end

    it "should return false if number of comments is less or equal 5" do
      Comment::DROPDOWN_LIMIT.times{FactoryGirl.create(:comment, :commentable => news_item_with_many_comments)}
      news_item_with_many_comments.reload
      presenter.has_see_more_link?.should be_false
    end
  end

  

end
