require 'spec_helper'

describe NewsItemPresenter do
  let(:user) { FactoryGirl.create(:user) }
  let(:news_item) { FactoryGirl.create(:news_item) }
  subject { NewsItemPresenter.new(news_item, view) }

  before(:each) { view.stub(:current_user).and_return(user) }

  describe "#like_button_id" do
    it "should return appropriate id" do
      subject.like_button_id.should eql("news-like-#{news_item.id}")
    end
  end

  describe "#like_link_url" do
    it "should return like link url" do
      subject.like_link_url.should eql(news_item_likes_path(news_item))
    end

    it "should return like link url with downvote true" do
      news_item.liked_by user
      subject.like_link_url.should eql(news_item_likes_path(news_item, :downvote => true))
    end
  end

  describe "#likeable" do
    it "should return news item" do
      subject.likeable.should eql(news_item)
    end
  end

  describe "#like_link_is_displayable?" do
    it "should return true" do
      subject.like_link_is_displayable?.should be_true
    end
  end

  describe "#after_create_like_partial" do
    it "should return appropriate partial" do
      subject.after_create_like_partial.should eql("news_item_create")
    end
  end
end
