require 'spec_helper'
require 'carrierwave/test/matchers'

describe CompanyUploader do
  include CarrierWave::Test::Matchers

  let(:company) { FactoryGirl.build(:company) }

  before do
    CompanyUploader.enable_processing = true
    @uploader = CompanyUploader.new(company, :logo)
    @uploader.store!(File.open("#{Rails.root}/spec/support/company.jpeg"))
  end

  after do
    CompanyUploader.enable_processing = false
    @uploader.remove!
  end

  it "should return the default url" do
    @uploader.default_url.should eql("/assets/icons/company.png")
  end

end
