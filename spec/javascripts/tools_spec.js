describe("show_company_summary", function() {
  it("should show traffic data", function() {
    loadFixtures("company_sammary_inputs.html")

    show_company_summary('traffic_data');

    expect($("#audience_data")).toBeHidden();
    expect($("#traffic_data")).toBeVisible();
  })
})
