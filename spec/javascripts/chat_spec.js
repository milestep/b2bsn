describe("Chat", function() {

  it("closes chat window", function() {
    loadFixtures("friend_chat.html")
    Chat.count = 1
    Chat.array = [68]
    Chat.window_width = 285
    close = $('#friend_chat_68 .close-chat').get(0)
    element = $('#friend_chat_68')
    minimize = $('#friend_chat_68 .minimize-chat').get(0)
    Chat.close(close)
    expect(element).toBeHidden();
    expect($(minimize)).toHaveClass('minimize-chat')
  });

  it("minimizes chat window", function() {
    loadFixtures("friend_chat.html")
    minimize = $('#friend_chat_68 .minimize-chat').get(0)
    Chat.minimize(minimize)
    expect($(minimize)).toHaveClass('maximize-chat')
  });
});

