describe("son_dom_ready", function() {
  it("should call method masonry for containers at tab connections", function() {
    loadFixtures("user_profile.html");
    on_dom_ready();
    expect($("#profile_connections .tab_cards_container .tab_card:visible")).toHaveClass('masonry-brick');
  });
})
