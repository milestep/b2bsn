describe("FineFilter", function() {

  it("toggle ul specified in data-toggle attribute", function() {
    loadFixtures("fine_filter.html")
    element = $('.trigger:first').get(0)
    expect($('ul' + $(element).data('toggle'))).toBeHidden();
    FineFilter.toggle(element)
    expect($('ul' + $(element).data('toggle'))).toBeVisible();
    expect($(element)).toHaveClass('icon-minus');
  });

  it("it should add filter to the filters contatiner", function() {
    loadFixtures("fine_filter.html")
    element = $(':checkbox:first').get(0)
    expect($('ul#filters')).toBeEmpty();
    $(element).attr('checked', true)
    FineFilter.filter(element)
    expect($('ul#filters')).toContain('li');
    expect($('ul#filters li')).toHaveClass($(element).parents('div').attr('class'));
    expect($('ul#filters li')).toHaveId('filter_' + element.id);
    expect($('ul#filters li')).toContain('label');
  });

  it("it should remove filter from the filters contatiner", function() {
    loadFixtures("fine_filter.html")
    element = $(':checkbox:first').get(0)
    $(element).attr('checked', true)
    FineFilter.filter(element)
    expect($('ul#filters')).toContain('li');
    $(element).attr('checked', false)
    FineFilter.filter(element)
    expect($('ul#filters')).toBeEmpty();
  });

  it("it should set the value of dropdwon to hiden field", function() {
    loadFixtures("fine_filter.html")
    element = $('.dropdown-menu a:first').get(0)
    FineFilter.submit(element)
    expect($('input#search_type')).toHaveValue($(element).data('type'));
  });

  it("it search the hide not matching elements", function() {
    loadFixtures("fine_filter.html")
    element = $('#search_name').val('_').get(0)
    elements = $('.search-results table tr')
    FineFilter.search(element.value, elements)
    expect(elements).toBeHidden()
  });

  it("it search the show matching elements", function() {
    loadFixtures("fine_filter.html")
    element = $('#search_name').val('a').get(0)
    elements = $('.search-results table tr')
    FineFilter.search(element.value, elements)
    expect(elements).toBeVisible()
  });

});
