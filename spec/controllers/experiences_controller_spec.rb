require 'spec_helper'

describe ExperiencesController do

  let(:user_profile) { FactoryGirl.create(:user_profile) }
  let(:user) { FactoryGirl.create(:user, :user_profile => user_profile) }
  let(:experience) { FactoryGirl.create(:experience, :user_profile => user_profile) }
  let(:valid_params) { FactoryGirl.attributes_for(:experience) }

  before(:each) { controller.stub(:current_user).and_return(user) }

  describe '#destroy' do
    before { xhr :delete, :destroy, :id => experience }

    it { should assign_to(:experience) }
    it { should render_template(:destroy) }
    it 'should destroy the experience' do
      assigns[:experience].destroyed?.should be_true
    end
  end

  describe '#edit' do
    before { xhr :get, :edit, :id => experience }

    it { should assign_to(:user_profile) }
    it { should assign_to(:experience) }
    it { should assign_to(:companies) }
    it { should respond_with(:success) }
    it { should render_template(:edit) }
  end

  describe '#update' do
    context 'with valid params' do
      before { xhr :put, :update, :id => experience, :experience => valid_params }

      it { should assign_to(:user_profile) }
      it { should assign_to(:experience) }
      it { should assign_to(:companies) }
      it { should respond_with(:success) }
      it { should render_template(:update_success) }
    end
  end

  describe '#new' do
    before { xhr :get, :new }

    it { should assign_to(:experience) }
    it { should assign_to(:companies) }
    it { should respond_with(:success) }
    it { should render_template(:new) }
  end

  describe '#create' do
    context 'with valid params' do
      before { xhr :post, :create, :experience => valid_params }

      it { should assign_to(:user_profile) }
      it { should assign_to(:experience) }
      it { should respond_with(:success) }
      it { should render_template(:create_success) }
    end
  end

end
