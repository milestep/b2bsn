require 'spec_helper'

describe CommentsController do

  let(:user) { FactoryGirl.create(:user) }
  let(:another_user) { FactoryGirl.create(:user) }
  let(:comment) { FactoryGirl.create(:comment, :user => user) }
  let(:comment_i18n) { I18n.t('comments.create.object') }
  let(:news_feed) { FactoryGirl.create(:news_feed) }
  let(:news_item) { FactoryGirl.create(:news_item, :news_feed => news_feed) }
  let(:post_item) { FactoryGirl.create(:post, :user => user) }
  let(:valid_params) {
    {
      :controller => 'comments',
      :comment => FactoryGirl.attributes_for(:comment),
      :commentable_id => news_item.id,
      :commentable_type => news_item.class.name
    }
  }
  let(:invalid_params) {
    {
      :controller => 'comments',
      :comment => FactoryGirl.attributes_for(:comment, :comment => ""),
      :commentable_id => news_item.id,
      :commentable_type => news_item.class.name
    }
  }
  let(:update_comment_params) { FactoryGirl.attributes_for(:comment, :comment => "some comment") }

  before(:each) { controller.stub(:current_user).and_return(user) }

  describe 'post create' do
    it 'should create a comment for news' do
      post :create, valid_params
      flash[:notice].should == I18n.t(:success, :obj => comment_i18n)
      assigns(:comment).should be_valid
      response.should redirect_to(news_item_path(news_item.id))
    end

    it 'should not create a comment for news' do
      post :create, invalid_params
      flash[:notice].should == I18n.t(:failure, :obj => comment_i18n)
      assigns(:comment).should_not be_valid
      response.should redirect_to(news_item_path(news_item.id))
    end

    it 'should create a comment for post' do
      post :create, valid_params.merge!(:commentable_id => post_item.id, :commentable_type => post_item.class.name)
      flash[:notice].should == I18n.t(:success, :obj => comment_i18n)
      assigns(:comment).should be_valid
      response.should redirect_to(post_path(post_item.id))
    end

    it 'should not create a comment for post' do
      post :create, invalid_params.merge!(:commentable_id => post_item.id, :commentable_type => post_item.class.name)
      flash[:notice].should == I18n.t(:failure, :obj => comment_i18n)
      assigns(:comment).should_not be_valid
      response.should redirect_to(post_path(post_item.id))
    end

    it 'should create a comment via ajax' do
      xhr :post, :create, valid_params
      assigns(:comment).should be_valid
      assigns(:subject).should eq(assigns(:comment).commentable)
    end

  end

  describe '#destroy' do
    context 'via http' do
      before { delete :destroy, :id => comment }

      it { should assign_to(:comment) }
      it { should redirect_to(comments_path)  }
      it 'should destroy the comment' do
        assigns[:comment].should be_destroyed
      end
    end

    context 'via ajax' do
      it 'should destroy the comment' do
        xhr :delete, :destroy, :id => comment
        assigns(:comment).should be_valid
        assigns(:subject).should eq(assigns(:comment).commentable)
        assigns[:comment].should be_destroyed
      end

      it 'should not destroy the comment due to permissions' do
        controller.stub(:current_user).and_return(another_user)
        xhr :delete, :destroy, :id => comment
        assigns(:comment).should be_valid
        assigns(:subject).should eq(assigns(:comment).commentable)
        assigns(:comment).should_not be_destroyed
      end
    end
  end

  describe '#update' do
    context 'via ajax' do
      it 'should update the comment' do
        xhr :put, :update, :id => comment, :comment => update_comment_params
        assigns(:comment).should be_valid
        assigns(:comment).comment.should eq(update_comment_params[:comment])
      end

      it 'should not update the comment due to permissions' do
        controller.stub(:current_user).and_return(another_user)
        xhr :put, :update, :id => comment, :comment => update_comment_params
        assigns(:comment).should be_valid
        assigns(:comment).comment.should eq(comment.comment)
      end
    end
  end

end

