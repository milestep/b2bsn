require 'spec_helper'

describe CategoriesController do

  let(:user) { FactoryGirl.create(:user) }
  let!(:first_category) { FactoryGirl.create(:category, :name => "test_category") }
  let!(:second_category) { FactoryGirl.create(:category, :name => "category") }

  before(:each) { controller.stub(:current_user).and_return(user) }

  describe '#autocomplete' do
    context "simple request" do
      before { get :autocomplete }

      it { should assign_to(:categories) }
      it { should respond_with(:success) }
      it { respond_with_content_type(:json) }
    end

    it "should return json" do
      get :autocomplete, :term => first_category.name[0..2]
      JSON(response.body).should eql([{
        "id" => first_category.id,
        "value" => first_category.name
      }])
    end

    it "should return empty json" do
      get :autocomplete, :term => "unexisting_category"
      JSON.parse(response.body).should be_empty
    end
  end

end
