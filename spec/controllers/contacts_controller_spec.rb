require 'spec_helper'

describe ContactsController do

  let(:user) { FactoryGirl.create(:user) }
  let(:company) { FactoryGirl.create(:company, :name => "Svitla Inc.") }
  let(:contact) { FactoryGirl.create(:contact, :company => company) }
  let(:valid_params) { FactoryGirl.attributes_for(:contact, :company => company) }

  before(:each) { controller.stub(:current_user).and_return(user) }

  describe '#show' do
    before { get :show, :id => contact }

    it { should assign_to(:contact) }
    it { should assign_to(:company_name) }
    it { should respond_with(:success) }
    it { should render_template(:show) }
  end

  describe '#new' do
    before { get :new }

    it { should assign_to(:contact) }
    it { should respond_with(:success) }
    it { should render_template(:new) }
  end

  describe '#edit' do
    before { get :edit, :id => contact }

    it { should assign_to(:contact) }
    it { should respond_with(:success) }
    it { should render_template(:edit) }
  end

  describe '#create' do
    context 'with valid params' do
      before { post :create, :contact => valid_params }

      it { should assign_to(:contact) }
      it { should redirect_to(assigns[:contact])  }
      it { should set_the_flash }
    end

  end

  describe '#update' do
    context 'with valid params' do
      before { put :update, :id => contact, :contact => valid_params }

      it { should assign_to(:contact) }
      it { should assign_to(:company_name) }
      it { should redirect_to(tools_path)  }
      it { should set_the_flash }
    end

  end

  describe '#destroy' do
    before { delete :destroy, :id => contact }

    it { should assign_to(:contact) }
    it { should redirect_to(contacts_path)  }
    it 'should destroy the contact' do
      assigns[:contact].destroyed?.should be_true
    end
  end

end
