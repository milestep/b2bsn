require 'spec_helper'

describe SessionsController do

  let!(:role) { FactoryGirl.create(:role) }
  let!(:buyer_role) { FactoryGirl.create(:buyer_role) }
  let!(:publisher_role) { FactoryGirl.create(:publisher_role) }
  let(:user) { FactoryGirl.create(:user) }
  let!(:user_profile) { FactoryGirl.create(:user_profile, :user => user) }
  let(:valid_params) { FactoryGirl.attributes_for(:address) }
  let(:invitation) { FactoryGirl.create(:invitation) }
  let(:valid_omniauth) { FactoryGirl.create(:omniauth, :provider => user.provider, :uid => user.uid) }
  let(:invalid_omniauth) { FactoryGirl.create(:omniauth) }

  before(:each) { controller.stub(:current_user).and_return(user) }

  describe '#new' do
    context 'with valid invitation code' do
      before do
        controller.stub(:signed_in?).and_return(false)
        get :new, :invite_code => invitation.token
      end

      it { should respond_with(:success) }
      it { should render_template(:login) }
      it { should set_session(:invite_id) }
      it { should set_the_flash }
    end

    context 'with invalid invitation code' do
      before do
        controller.stub(:signed_in?).and_return(false)
        get :new, :invite_code => 'invalid'
      end

      it { should respond_with(:success) }
      it { should render_template(:login) }
      it { should_not set_session(:invite_id) }
      it { should set_the_flash }
    end

    context 'already logged in user' do
      before do
        controller.stub(:current_user).and_return(user)
        get :new
      end

      it { should redirect_to(root_path)}
    end
  end

  describe '#create' do
    context 'with valid params' do
      before { request.env["omniauth.auth"] = valid_omniauth; session[:invite_id] = invitation.token; get :create, :provider => :linkedin }

      it { should assign_to(:user) }
      it { should assign_to(:auth) }
      it { should set_session(:user_id) }
      it { should redirect_to(root_path)  }
      it { should set_the_flash }
    end

    context 'with invalid params' do
      before { request.env["omniauth.auth"] = invalid_omniauth; session[:invite_id] = invitation.token; get :create, :provider => :linkedin }

      it { should assign_to(:user) }
      it { should assign_to(:auth) }
      it { should_not set_session(:user_id) }
      it { should redirect_to(edit_signup_user_path(assigns[:user].uid))  }
    end

    context 'with auth problem' do
      before { request.env["omniauth.auth"] = valid_omniauth; get :create, :provider => :linkedin, :oauth_problem => true }

      it { should assign_to(:auth) }
      it { should redirect_to(login_path)  }
      it { should set_the_flash }
    end
  end

  describe '#destroy' do
    before { controller.stub(:current_user).and_return(user) }
    before { delete :destroy }

    it { should_not set_session(:user_id) }
    it { should_not set_session(:invite_id) }
    it { should redirect_to(root_path)  }
  end

end
