require 'spec_helper'

describe Connections::MyConnectionsController do

  let(:user) { FactoryGirl.create(:user) }
  let(:company) { FactoryGirl.create(:company) }
  let(:friend) { FactoryGirl.create(:user, :first_name => "Friend", :last_name => "Friend'sLastName") }
  let!(:connection) { FactoryGirl.create(:connection, :user => user, :friend => friend) }

  before(:each) {
    controller.stub(:current_user).and_return(user)
    session[:rtoken] = "rtoken"
    session[:rsecret] = "rsecret"
  }

  context "index" do
    it "should return current user friends" do
      get :index
      assigns(:friends).should include(friend)
    end
  end

  context "full_user_profile" do
    it "should return full user profile" do
      xhr :get, :full_user_profile, :friend_id => friend
      assigns(:friend).should_not be_nil
    end

    it "should not return suggested users" do
      User.stub(:similar_profiles_for).and_return([])
      User.should_receive(:similar_profiles_for).once.with(friend).and_return([])
      xhr :get, :full_user_profile, :friend_id => friend
      assigns(:suggested_users).should be_empty
    end

    it "should not return mutual connections" do
      User.stub(:similar_profiles_for).and_return([])
      User.any_instance.stub(:mutual_connections_for).and_return([])
      user.should_receive(:mutual_connections_for).once.with(friend).and_return([])
      xhr :get, :full_user_profile, :friend_id => friend
      assigns(:mutual_connections).should be_empty
    end
  end

  it "should return all connections" do
    xhr :get, :all_connections
    assigns(:friends).should include(friend)
  end
end
