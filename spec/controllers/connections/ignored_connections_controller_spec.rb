require 'spec_helper'

describe Connections::IgnoredConnectionsController do

  let(:user) { FactoryGirl.create(:user) }
  let(:friend) { FactoryGirl.create(:user, :first_name => "Friend", :last_name => "Friend'sLastName") }
  let(:pending_connection) { FactoryGirl.create(:pending_connection, :user => user, :friend => friend) }

  before(:each) { controller.stub(:current_user).and_return(friend) }

  it "should add user to ignore list" do
    xhr :post, :create, :connection_id => pending_connection, :commit => "Ignore"
    expect(assigns(:connection)).to eql(pending_connection)
    expect(assigns(:ignored_user)).to eql(user)
    expect(friend.ignored_users).to include(user)
  end
  
end
