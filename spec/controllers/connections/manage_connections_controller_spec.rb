require 'spec_helper'

describe Connections::ManageConnectionsController do

  let(:user) { FactoryGirl.create(:user) }
  let(:company) { FactoryGirl.create(:company) }
  let(:friend) { FactoryGirl.create(:user, :first_name => "Friend", :last_name => "Friend'sLastName") }
  let!(:connection) { FactoryGirl.create(:connection, :user => user, :friend => friend) }
  let(:another_friend) { FactoryGirl.create(:user, :first_name => "NiceName", :last_name => "NiceLastName") }
  let(:another_connection) { FactoryGirl.create(:connection, :user => user, :friend => another_friend) }

  before(:each) {
    controller.stub(:current_user).and_return(user)
    session[:rtoken] = "rtoken"
    session[:rsecret] = "rsecret"
  }

  describe "manage connections" do
    let(:double_client) { double('Linkedin::Client') }
    let!(:another_user) { FactoryGirl.create(:user, :first_name => "Test", :last_name => "Testovich") }
    let!(:user_profile) { FactoryGirl.create(:user_profile, :user => user, :company => company) }
    let!(:co_worker_profile) { FactoryGirl.create(:user_profile, :user => another_user, :company => company) }

    before do
      controller.stub(:linkedin_client).and_return(double_client)
    end

    context "index" do

      it "should return co workers connections" do
        double_client.should_receive(:beanstock_connections_for).once.and_return([])
        double_client.should_receive(:linkedin_connections).once
        get :index
        assigns(:co_workers_connections).should include(another_user)
      end

      it "should not return co workers connections, because user without company" do
        user.user_profile.update_attribute(:company, nil)
        double_client.should_receive(:beanstock_connections_for).once.and_return([])
        double_client.should_receive(:linkedin_connections).once
        get :index
        assigns(:co_workers_connections).should be_empty
      end

      it "should return beanstock connections" do
        double_client.should_receive(:beanstock_connections_for).once.and_return([another_user])
        double_client.should_receive(:linkedin_connections).once
        get :index
        assigns(:beanstock_connections).should include(another_user)
      end

      it "should return linkedin connections" do
        double_client.should_receive(:beanstock_connections_for).once.and_return([])
        double_client.should_receive(:linkedin_connections).once.and_return([])
        get :index
        assigns(:beanstock_connections).should be_empty
      end
    end

    context "create" do
      it "should sent invitation to choosen user" do
        xhr :get, :create, :id => another_user
        assigns(:friend).should eq(another_user)
        assigns(:connection).should_not be_nil
        response.should redirect_to(manage_connections_path)
      end
    end

    it "should sent invitation to linkedin friend" do
      connections = "za8mNizN6f"
      Resque.should_receive(:enqueue).once.with(LinkedInMess, user.id, 'some_token', 'some_secret', [connections])
      double_client.should_receive(:token).once.and_return('some_token')
      double_client.should_receive(:secret).once.and_return('some_secret')
      xhr :get, :invite_users, :connections => connections
      response.should redirect_to(manage_connections_path)
    end

    it "should return co workers" do
      controller.should_receive(:user_co_workers).once.and_return([another_user])
      xhr :get, :co_workers, :page => 1
      assigns(:co_workers_connections).should include(another_user)
    end

    it "should not return co workers, user without company" do
      controller.should_receive(:user_co_workers).once.and_return([])
      xhr :get, :co_workers, :page => 1
      assigns(:co_workers_connections).should be_empty
    end

    it "should return beanstock connections" do
      double_client.should_receive(:beanstock_connections_for).once.and_return([another_user])
      xhr :get, :beanstock_connections, :page => 1
      assigns(:beanstock_connections).should include(another_user)
    end

    it "should return linkedin connections" do
      double_client.should_receive(:linkedin_connections).once.and_return([])
      xhr :get, :linkedin_connections, :page => 1
      assigns(:linked_in_connections).should be_empty
    end
  end
end
