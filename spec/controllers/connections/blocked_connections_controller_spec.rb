require 'spec_helper'

describe Connections::BlockedConnectionsController do

  let(:user) { FactoryGirl.create(:user) }
  let(:company) { FactoryGirl.create(:company) }
  let(:friend) { FactoryGirl.create(:user, :first_name => "Friend", :last_name => "Friend'sLastName") }
  let!(:connection) { FactoryGirl.create(:connection, :user => user, :friend => friend) }

  before(:each) {
    controller.stub(:current_user).and_return(user)
    session[:rtoken] = "rtoken"
    session[:rsecret] = "rsecret"
  }

  it "should return blocked users" do
    user.blocked_users << friend
    xhr :get, :index
    expect(assigns(:blocked_users)).to include(friend)
  end

  it "should block choosen user" do
    User.any_instance.should_receive(:add_to_blacklist).once
    get :block_user, :friend_id => friend
    expect(response).to be_success
  end

  it "should find and block user choosen user" do
    get :block_user, :friend_id => friend
    expect(user.blocked_users).to include(friend)
    expect(response).to be_success
  end

  it "should block user from autocomplete" do
    xhr :get, :block_autocomplete_user, :friend_id => friend
    expect(user.blocked_users).to include(friend)
    expect(response).to be_success
  end

  it "should unblock user" do
    user.blocked_users << friend
    xhr :get, :unblock_user, :user_id => friend
    expect(user.blocked_users).to be_empty
  end
end
