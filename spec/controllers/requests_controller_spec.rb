require 'spec_helper'

describe RequestsController do
  let(:user) { FactoryGirl.create(:user) }
  let(:another_user) { FactoryGirl.create(:user) }
  let(:outgoing_connection) { FactoryGirl.create(:pending_connection, :user => user, :friend => another_user) }
  let(:incoming_connection) { FactoryGirl.create(:pending_connection, :user => another_user, :friend => user) }
  let(:requests) { [incoming_connection, outgoing_connection].flatten }

  before(:each) { controller.stub(:current_user).and_return(user) }

  describe "#index" do
    it "should get requests" do
      get 'index'
      response.should be_success
    end

    describe "#incoming_reqests" do
      before(:each) do
        requests
        get 'index'
      end

      it "should show users who sent invitations to the user" do
        expect(assigns(:requests)).to eq(requests)
      end
    end
  end

end
