require 'spec_helper'

describe ViewingsController do
  let(:user) { FactoryGirl.create(:user) }
  let!(:profile) { FactoryGirl.create(:user_profile, :user => user) }

  before(:each) { controller.stub(:current_user).and_return(user) }

  describe '#index' do
    before { xhr :get, :index, :user_profile_id => user.user_profile }

    it { should assign_to(:viewers) }
    it { should respond_with(:success) }
    it { should render_template(:index) }
  end

end
