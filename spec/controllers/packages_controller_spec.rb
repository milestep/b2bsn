require 'spec_helper'

describe PackagesController do
  let(:user) { FactoryGirl.create(:user) }
  let(:package) { FactoryGirl.create(:package, :packagable => user) }
  let(:valid_params) { FactoryGirl.attributes_for(:package) }
  let(:invalid_params) { FactoryGirl.attributes_for(:package, :name => "") }
  let(:another_user) { FactoryGirl.create(:user) }

  before(:each) { controller.stub(:current_user).and_return(user) }

  it 'should get index' do
    get :index, :user_id => user.id
    response.should be_success
    flash.should be_blank
    assigns[:packages].should_not be_nil
    response.should render_template :index
  end

  it 'should get new' do
    xhr :get, :new, :user_id => user.id
    response.should be_success
    flash.should be_blank
    assigns[:package].should_not be_nil
    response.should render_template :new
  end

  describe 'post create' do
    it 'should create a package' do
      xhr :post, :create, :package => valid_params, :user_id => user.id
      assigns[:package].should_not be_nil
      assigns[:package].errors.should be_blank
      response.should render_template :create
    end

    it 'should not create a package' do
      xhr :post, :create, :package => invalid_params, :user_id => user.id
      assigns[:package].should_not be_nil
      assigns[:package].errors.should_not be_blank
      response.should render_template :create
    end
  end

  it 'should get edit' do
    xhr :get, :edit, :id => package.id, :user_id => user.id
    response.should be_success
    assigns[:package].should_not be_nil
    response.should render_template :new
  end

  describe 'put update' do
    it 'should update the package' do
      xhr :put, :update, :id => package.id, :package => valid_params, :user_id => user.id
      assigns[:package].errors.should be_blank
      response.should render_template :update
    end

    it 'should not update the package' do
      xhr :put, :update, :id => package.id, :package => invalid_params, :user_id => user.id
      assigns[:package].errors.should_not be_blank
      response.should render_template :update
    end
  end

  it 'should destroy the package' do
    xhr :delete, :destroy, :id => package.id, :user_id => user.id
    assigns[:package].should_not be_nil
    response.should render_template :destroy
  end

  it 'should increase views counter for package ' do
    controller.stub(:current_user).and_return(another_user)
    expect {
      xhr :get, :show, :user_id => user.id, :id => package.id
    }.to change{package.reload.views_count}.by(1)
  end

end
