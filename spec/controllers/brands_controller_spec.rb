require 'spec_helper'

describe BrandsController do

  let(:user) { FactoryGirl.create(:user) }
  let(:brand) { FactoryGirl.create(:brand) }
  let(:company) { FactoryGirl.create(:agency, :brands => [brand]) }

  before(:each) { controller.stub(:current_user).and_return(user) }

  describe '#index' do

    context "all brands" do
      before { get :index }

      it { should assign_to(:brands) }
      it { should respond_with(:success) }
      it { should render_template(:index) }
      it { should_not set_the_flash }
    end

    context "brands for company" do
      it "should get all brands for company" do
        get :index, :company => company.id
        assigns(:company).should eq(company)
        assigns(:brands).should eq(company.brands.order_by_name("ASC"))
      end
    end

    context "all brands" do
      it "should get all brands" do
        get :index
        assigns(:brands).should eq(Brand.order_by_name("ASC"))
      end
    end

  end

  describe '#new' do
    before { xhr :get, :new }

    it { should assign_to(:brand) }
    it { should respond_with(:success) }
    it { should render_template(:new) }
    it { assigns(:brand).should be_a_new(Brand) }
  end

  it "should create brand" do
    xhr :post, :create, :brand => { :name => "Created Brand" }
    assigns(:brand).should be_valid
    response.should render_template("create")
  end

  it "should not create brand" do
    xhr :post, :create, :brand => {}
    assigns(:brand).should have(1).error_on(:name)
    response.should render_template("create")
  end

  it "should create brand, html format" do
    post :create, :brand => { :name => "Created Brand" }
    assigns(:brand).should be_valid
    response.should redirect_to(tools_path)
    flash[:notice].should eql(I18n.t('brands.create.success'))
  end

  describe '#autocomplete' do
    before { get :autocomplete, :ids => [company.id] }

    it { should assign_to(:brands) }
    it { should respond_with(:success) }
    it { respond_with_content_type(:json) }
    it { should_not set_the_flash }
  end
end
