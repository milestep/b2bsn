require 'spec_helper'

describe Signup::ExperiencesController do

  let(:user_profile) { FactoryGirl.create(:user_profile) }
  let(:user) { FactoryGirl.create(:user, :user_profile => user_profile) }
  let(:experience) { FactoryGirl.create(:experience, :user_profile => user_profile) }
  let(:valid_params) { FactoryGirl.attributes_for(:experience) }

  before(:each) {
    controller.stub(:current_user).and_return(user)
    session[:rtoken] = "rtoken"
    session[:rsecret] = "rsecret"
  }

  describe '#index' do
    before { get :index, :id => user.uid }

    it { should assign_to(:experiences) }
    it { should respond_with(:success) }
    it { should render_template(:index) }
  end

  describe '#edit' do
    before { xhr :get, :edit, :id => experience }

    it { should assign_to(:experience) }
    it { should respond_with(:success) }
    it { should render_template(:edit) }
  end

  describe '#update' do
    context 'with valid params' do
      before { xhr :put, :update, :id => experience, :experience => valid_params }

      it { should assign_to(:experience) }
      it { should respond_with(:success) }
      it { should render_template(:update) }
    end
  end

  describe '#destroy' do
    before { xhr :delete, :destroy, :id => experience }

    it { should assign_to(:experience) }
    it { should render_template(:destroy) }
  end

end
