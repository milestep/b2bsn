require 'spec_helper'

describe Signup::SkillsController do

  let(:user_profile) { FactoryGirl.create(:user_profile) }
  let(:user) { FactoryGirl.create(:user, :user_profile => user_profile) }
  let(:skill) { FactoryGirl.create(:skill, :user_profiles => [user_profile]) }
  let(:valid_params) { FactoryGirl.attributes_for(:skill) }

  before(:each) {
    controller.stub(:current_user).and_return(user)
    session[:rtoken] = "rtoken"
    session[:rsecret] = "rsecret"
  }

  describe '#index' do
    before { get :index, :id => user.uid }

    it { should assign_to(:skills) }
    it { should respond_with(:success) }
    it { should render_template(:index) }
  end

  describe '#destroy' do
    before { xhr :delete, :destroy, :id => skill }

    it { should assign_to(:skill) }
    it { should respond_with(:success) }
    it { should render_template(:destroy) }
    it 'should be destroyed' do
      assigns[:skill].should be_destroyed
    end
  end

  describe '#create' do
    context 'with new skill' do
      before { xhr :post, :create, :skill => valid_params[:name] }

      it { should assign_to(:skill) }
      it { should respond_with(:success) }
      it { should respond_with_content_type(:json) }

    end

    context 'with existing skill' do
      before { skill; xhr :post, :create, :skill => valid_params[:name] }

      it { should assign_to(:skill) }
      it { should respond_with(:success) }
      it { should respond_with_content_type(:json) }
    end
  end

end
