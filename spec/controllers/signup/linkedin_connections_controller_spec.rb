require 'spec_helper'

describe Signup::LinkedinConnectionsController do
  let(:user) { FactoryGirl.create(:user) }
  let(:user_profile) { FactoryGirl.create(:user_profile) }

  before(:each) {
    controller.stub(:current_user).and_return(user)
    session[:rtoken] = "rtoken"
    session[:rsecret] = "rsecret"
  }

  context "create" do
    let(:double_client) { double('Linkedin::Client') }

    before do
      controller.stub(:linkedin_client).and_return(double_client)
    end

    it "should send invites to linkedin account" do
      connections = ["za8mNizN6f"]
      Resque.should_receive(:enqueue).once.with(LinkedInMess, user.id, 'some_token', 'some_secret', connections)
      double_client.should_receive(:token).once.and_return('some_token')
      double_client.should_receive(:secret).once.and_return('some_secret')
      get :create, :id => user.id, :connections => connections
      response.should redirect_to(root_path)
    end
  end
end
