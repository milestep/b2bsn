require 'spec_helper'

describe Signup::UsersController do

  let(:user) { FactoryGirl.create(:user) }
  let(:valid_params) { FactoryGirl.attributes_for(:user_params) }
  let(:invalid_params) { FactoryGirl.attributes_for(:user_params, :email => '$') }
  let(:double_client) { double('Linkedin::Client') }

  before(:each) {
    controller.stub(:current_user).and_return(user)
    session[:rtoken] = "rtoken"
    session[:rsecret] = "rsecret"
  }

  describe 'before_filter' do
    it "permit_signup_access should redirect for login with linkedin" do
      session[:rtoken] = nil
      session[:rsecret] = nil
      expect(get :edit, :id => user.uid).to redirect_to(login_path)
    end
  end

  describe '#update' do
    context 'with valid params' do
      before do
        controller.stub(:linkedin_client).and_return(double_client)
        double_client.should_receive(:import_experiences!).once.with(no_args())
        double_client.should_receive(:import_skills!).once.with(no_args())
        put :update, :id => user.uid, :user => valid_params
      end

      it { should assign_to(:user) }
      it { should redirect_to(signup_experiences_path(:id => user.uid))  }
    end

    context 'with invalid params' do
      before { put :update, :id => user.uid, :user => invalid_params }

      it { should assign_to(:user) }
      it { should render_template(:edit) }
    end
  end

  describe '#edit' do
    context 'with valid params' do
      before do
        get :edit, :id => user.uid
      end

      it { should respond_with(:success) }
      it { should assign_to(:user) }
      it { should render_template(:edit) }
    end

    context 'with invalid params' do
      it "should render not found page" do
        invalid_uid = 'invaliduid'
        expect {
          get :edit, :id => invalid_uid
        }.to raise_error(ActionController::RoutingError)
      end
    end
  end

end
