require 'spec_helper'

describe Signup::BeanstockConnectionsController do

  let(:user_profile) { FactoryGirl.create(:user_profile) }
  let(:user) { FactoryGirl.create(:user, :user_profile => user_profile) }

  before(:each) {
    controller.stub(:current_user).and_return(user)
    session[:rtoken] = "rtoken"
    session[:rsecret] = "rsecret"
  }

  context "create" do
    let(:double_client) { double('Linkedin::Client') }

    before do
      controller.stub(:linkedin_client).and_return(double_client)
    end

    it "should send invites to linkedin account" do
      connections = ["za8mNizN6f"]
      user.should_receive(:send_contact_requests).once.with(connections)
      get :create, :id => user.id, :connections => connections
      response.should redirect_to(signup_linkedin_connections_path(:id => user.uid))
    end
  end

end
