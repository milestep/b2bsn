require 'spec_helper'

describe ActivityFeedsController do
  let(:user) { FactoryGirl.create(:user) }

  before(:each) { controller.stub(:current_user).and_return(user) }

  describe "GET 'index'" do
    before { xhr :get, :index }

    it { should assign_to(:activity_items) }
    it { should respond_with(:success) }
    it { should_not set_the_flash }
  end
end
