require 'spec_helper'

describe InvitationsController do
  let(:user) { FactoryGirl.create(:user) }

  before(:each) { controller.stub(:current_user).and_return(user) }

  describe '#update' do
    before { get :update }

    it { should redirect_to(login_path) }
  end

end
