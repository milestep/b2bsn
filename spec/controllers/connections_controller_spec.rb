require 'spec_helper'

describe ConnectionsController do

  let(:user) { FactoryGirl.create(:user) }
  let(:company) { FactoryGirl.create(:company) }
  let(:circle) { FactoryGirl.create(:circle, :name => "UserCircle", :user => user) }
  let(:friend) { FactoryGirl.create(:user, :first_name => "Friend", :last_name => "Friend'sLastName") }
  let!(:connection) { FactoryGirl.create(:connection, :user => user, :friend => friend) }
  let(:another_friend) { FactoryGirl.create(:user, :first_name => "NiceName", :last_name => "NiceLastName") }
  let(:another_connection) { FactoryGirl.create(:connection, :user => user, :friend => another_friend) }
  let(:invalid_params) { FactoryGirl.attributes_for(:connection, :message => '!'*251) }
  let(:role_tester) { FactoryGirl.create(:role, :name => "tester") }

  before(:each) { controller.stub(:current_user).and_return(user) }

  context 'global filters' do

    before do
      controller.stub(:current_user).and_return(user)
      controller.stub(:get_notifications).and_return(true)
      controller.stub(:authenticate_user).and_return(true)
      user.stub_chain(:new_users, :newest, :count).and_return(25)
      get :index, :g => 'UserCircle'
    end

    it { should assign_to(:new_users_count).with(25) }

  end

  context "index action" do

    it "should fetch all new users in the system" do
      users, users_count = [], 50
      users_count.times do |item|
        users << FactoryGirl.create(:user, :first_name => "Bobby##{item}", :last_name => 'Willson')
      end
      get :index
      expect(assigns(:users)).to have(users_count).items
    end

    it "should give the list of the circles with specified name" do
      circle
      another_circle = FactoryGirl.create(:circle, :name => "AnotherCircle", :user => user)
      not_this_user_circle = FactoryGirl.create(:circle, :name => "PublicCircle")
      get :index, :g => "UserCircle"
      assigns(:circles).should include(circle)
      assigns(:circles).should_not include(another_circle, not_this_user_circle)
    end

    context "without specified circle name" do

      it "should give the list of the circles, users and friends without any params" do
        new_user1 = FactoryGirl.create(:user, :first_name => "NewUser", :last_name => "NewUserLastName")
        new_user2 = FactoryGirl.create(:user, :first_name => "AnotherNewUser", :last_name => "HisLastName")
        circle
        get :index
        assigns(:user_type).should eql("newest")
        assigns(:users).should include(new_user1, new_user2)
        assigns(:my_friends).should include(friend)
        assigns(:circles).should include(circle)
      end

      it "should give the list of the friends if there is friend's name" do
        another_connection
        get :index, :search => "Friend"
        assigns(:my_friends).should include(friend)
        assigns(:my_friends).should_not include(another_friend)
      end

      it "should give the list of the friends if there is friend's type" do
        another_connection.friend.update_attribute(:role, role_tester)
        get :index, :t => role_tester.name
        assigns(:my_friends).should include(another_friend)
        assigns(:my_friends).should_not include(friend)
      end

      it "should find friends by name if both params are specified" do
        another_connection.friend.update_attribute(:role, role_tester)
        get :index, :search => "Friend", :t => role_tester.name
        assigns(:my_friends).should include(friend)
        assigns(:my_friends).should_not include(another_friend)
      end

    end

  end

  it "should find users" do
    get :search, { :name => user.first_name, :email => user.email }
    assigns(:results).should_not include(user)
    response.should render_template("add_contacts")
  end

  it "should show a connection" do
    get :show, :id => connection.id
    assigns(:connection).should eql(connection)
  end

  it "should edit a connection" do
    get :edit, :id => connection.id
    assigns(:connection).should eql(connection)
  end

  describe "#new" do
    let(:reverse_connection) { FactoryGirl.create(:connection, :user => another_friend, :friend => user) }

    it "should render nothing with incorrect params" do
      get :new
      response.body.should be_blank
    end

    it "should render new connection" do
      get :new, {:friend_id => another_friend.id}
      assigns(:connection).should_not be_nil
    end

    it "should not render new template if there is reverse connection between two users" do
      reverse_connection
      xhr :get, :new, {:friend_id => another_friend.id}
      response.should render_template("show_notice")
      flash[:notice].should eql("#{another_friend.fullname} has already sent to you request for friendship.")
    end
  end

  describe '#create' do

    context 'with valid params' do

      it "should create new connection" do
        post :create, :connection => {:friend_id => another_friend.id}
        assigns(:connection).should be_valid
        flash[:notice].should eql("Connection was successfully requested.")
        response.should redirect_to(connections_path(:c => "outgoing"))
      end

      it "should create new connection with js response" do
        xhr :post, :create, :connection => {:friend_id => another_friend.id}
        flash[:notice].should eql("Connection was successfully requested.")
      end

      it "should create new connection with json response" do
        connection_to_create = FactoryGirl.build(:connection, :user => user, :friend => friend)
        post :create, :connection => {:friend_id => another_friend.id, :message => "This connection was created for test."}, :format => :json
        JSON.parse(response.body)["message"].should eql("This connection was created for test.")
      end

      it "should return existing connection if friend id is already present" do
        post :create, :connection => {:friend_id => friend.id, :message => "This connection was created for test."}
        connection = assigns(:connection)
        user.connections.should include(connection)
        connection.should be_pending
      end

      context 'mailer' do
        it "should be send message to mailer" do
          connection = {"friend_id" => another_friend.id.to_s, "message" => "This connection was created for test."}
          mock = double()
          mock.should_receive(:deliver)
          ConnectNotificationMailer.should_receive(:connect_request).with(connection).and_return(mock)
          post :create, :connection => connection
        end

        it "should not be send message to mailer if friend id is already present" do
          connection = {"friend_id" => friend.id.to_s, "message" => "This connection was created for test."}
          mock = double()
          mock.should_not_receive(:deliver)
          ConnectNotificationMailer.should_not_receive(:connect_request).with(connection).and_return(mock)
          post :create, :connection => connection
        end
      end
    end

    context 'with invalid params' do
      before { post :create, :connection => invalid_params }

      it { should assign_to(:connection) }
      it { should render_template(:new) }
      it 'should not be valid ' do
        assigns(:connection).should_not be_valid
      end
    end
  end

  context "update" do

    it "should update a connection" do
      put :update, :id => connection.id, :commit => "Delete"
      assigns(:connection).status.should eql("deleted")
      assigns(:deleted_friend).should eql(connection.friend)
      flash[:notice].should eql("Connection was successfully Deleteed.")
      response.should redirect_to(connections_path(:c => "incoming"))
    end

  end

  it "should add contacts" do
    user.stub(:suggested_friends).and_return([FactoryGirl.create(:user), FactoryGirl.create(:user)])
    get :add_contacts
    assigns(:suggestions).should have(2).items
  end

end
