require 'spec_helper'

describe NewsController do

  let(:user) { FactoryGirl.create(:user) }
  let(:news_item) { FactoryGirl.create(:news_item, :date_published => Time.now + 2.hour) }
  let(:bunch_of_news_items) { FactoryGirl.create_list(:news_item, Feeds::BaseFeed::FEED_NEWS_PER_PAGE * 2, :date_published => Time.now + 1.hour) }
  let(:user_timeline_event) { FactoryGirl.create(:timeline_event, :actor => user) }

  before(:each) { controller.stub(:current_user).and_return(user) }

  describe '#index' do
    before { xhr :get, :index, :feed_id => news_item.news_feed.id }

    it { should assign_to(:news) }
    it { should respond_with(:success) }
    it { should render_template(:index) }
    it { should_not set_the_flash }
  end

  describe '#show' do
    before { get :show, :id => news_item }

    it { should assign_to(:news) }
    it { should respond_with(:success) }
    it { should render_template(:show) }
    it { should_not set_the_flash }
  end

  describe '#slide elements' do
    before { xhr :get, :slide_elements, :current_page => 1 }
    it { should assign_to(:news) }
    it { should respond_with(:success) }
    it { should render_template(:slide_elements) }
  end

  context '#news' do
    before(:each) { Rails.cache.delete('news_feed') }

    it "should return news" do
      controller.stub(:build_feed).and_return(news_item)
      xhr :get, :feeds
      expect(response).to be_success
    end

    it "should slide news" do
      news_item
      xhr :get, :slide_news
      expect(assigns(:feed)).not_to be_empty
      expect(response).to be_success
    end

    it "should show chosen news item" do
      get :show_news, :id => news_item
      expect(assigns(:feed)).not_to be_empty
      expect(assigns(:page_number)).to eql(1)
      expect(assigns(:item).obj).to eql(news_item)
      expect(response).to be_success
    end

    it "should show chosen news item and return correct page number and feeds count" do
      news_item and bunch_of_news_items
      item = Feeds::EventsFeed.new(news_item)
      get :show_news, :id => news_item
      expect(assigns(:feed)).not_to be_empty
      expect(assigns(:feed)[item.feed_date].count).to eql(Feeds::BaseFeed::FEED_NEWS_PER_PAGE)
      expect(assigns(:page_number)).to eql(1)
      expect(assigns(:item).obj).to eql(news_item)
      expect(response).to be_success
    end

    it "should return only news feeds" do
      news_item and user_timeline_event
      item = Feeds::EventsFeed.new(news_item)
      xhr :get, :feeds, :feed_type => "news"
      expect(assigns(:feed)[item.feed_date]).to have(1).item
    end

    it "should return all feeds" do
      user_timeline_event
      item = Feeds::EventsFeed.new(news_item)
      xhr :get, :feeds, :feed_type => "all"
      expect(assigns(:feed)[item.feed_date]).to have(2).items
    end

    it "should return news feeds and actvities" do
      user_timeline_event
      item = Feeds::EventsFeed.new(news_item)
      xhr :get, :feeds
      expect(assigns(:feed)[item.feed_date]).to have(2).items
    end
  end
end