require 'spec_helper'

describe SharesController do

  let(:user) { FactoryGirl.create(:user) }
  let(:post_item) { FactoryGirl.create(:post) }
  let(:news_item) { FactoryGirl.create(:news_item) }
  let(:event) { FactoryGirl.create(:timeline_event) }
  let(:post_valid_params) {{ :activity => post_item.id, :event => event.id }}
  let(:news_item_valid_params) {{ :activity => news_item.id, :event => event.id, :type => 'NewsItem' }}

  before(:each) { controller.stub(:current_user).and_return(user) }

  describe '#create' do
    context 'with post params' do
      before { post :create, post_valid_params }

      it { should assign_to(:user) }
      it { should assign_to(:timeline_event) }
      it { should assign_to(:share) }
      it { should assign_to(:linkback) }
      it { should assign_to(:next) }
      it { should redirect_to(root_path)  }
      it { should set_the_flash }

      it 'should set post url to the linkback' do
        assigns[:linkback].should eql(post_url(post_item))
      end
    end

    context 'with post params' do
      before { post :create, news_item_valid_params }

      it { should assign_to(:user) }
      it { should assign_to(:timeline_event) }
      it { should assign_to(:share) }
      it { should assign_to(:linkback) }
      it { should assign_to(:next) }
      it { should redirect_to(root_path)  }
      it { should set_the_flash }

      it 'should set news item url to the linkback' do
        assigns[:linkback].should eql(news_item.url)
      end
    end

    it 'should increment post shares count' do
      expect { post :create, post_valid_params; post_item.reload }.to change(post_item, :shares_count).by(1)
    end

  end

end
