require 'spec_helper'

describe AddressesController do

  let(:user) { FactoryGirl.create(:user) }
  let!(:user_profile) { FactoryGirl.create(:user_profile, :user => user) }
  let(:address) { FactoryGirl.create(:address) }
  let(:valid_params) { FactoryGirl.attributes_for(:address) }

  before(:each) { controller.stub(:current_user).and_return(user) }

  describe '#show' do
    before { get :show, :id => address }

    it { should assign_to(:address) }
    it { should respond_with(:success) }
    it { should render_template(:show) }
    it { should_not set_the_flash }
  end

  describe '#new' do
    before { get :new, :id => address }

    it { should assign_to(:address) }
    it { should respond_with(:success) }
    it { should render_template(:new) }
    it { should_not set_the_flash }
  end

  describe '#edit' do
    before { get :edit, :id => address }

    it { should assign_to(:address) }
    it { should respond_with(:success) }
    it { should render_template(:edit) }
    it { should_not set_the_flash }
  end

  describe '#create' do
    context 'with valid params' do
      before { post :create, :post => valid_params }

      it { should assign_to(:address) }
      it { should redirect_to(address_path(assigns[:address]))  }
      it { should set_the_flash }
    end

  end

  describe '#update' do
    context 'with valid params' do
      before { put :update, :id => address, :address => valid_params }

      it { should assign_to(:address) }
      it { should redirect_to(address_path(address))  }
      it { should set_the_flash }
    end

  end

  describe '#destroy' do
    before { delete :destroy, :id => address }

    it { should assign_to(:address) }
    it { should redirect_to(addresses_path)  }
    it { should_not set_the_flash }
  end

end
