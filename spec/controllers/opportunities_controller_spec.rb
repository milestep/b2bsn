require 'spec_helper'

describe OpportunitiesController do

  let(:user) { FactoryGirl.create(:user) }
  let(:news_item) { FactoryGirl.create(:news_item) }

  before(:each) { controller.stub(:current_user).and_return(user) }

  describe '#index' do
    before { xhr :get, :index, :new_opportunity => true }

    it { should assign_to(:connections) }
    it { should assign_to(:opportunities_count) }
    it { should respond_with(:success) }
    it { should render_template(:index) }
  end

  describe '#show' do
    before { get :show, :id => news_item }

    it { should respond_with(:success) }
    it { should render_template(:show) }
  end

  describe '#new' do
    before { xhr :get, :new }

    it { should assign_to(:next_page) }
    it { should respond_with(:success) }
    it { should render_template(:new) }
  end

end
