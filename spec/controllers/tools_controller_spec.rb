require "spec_helper"

describe ToolsController do
  let!(:role) { FactoryGirl.create(:role) }
  let(:user) { FactoryGirl.create(:user) }
  let(:friend) { FactoryGirl.create(:user, :first_name => "Friend", :last_name => "Friend'sLastName") }
  let!(:connection) { FactoryGirl.create(:connection, :user => user, :friend => friend) }
  let(:company) { FactoryGirl.create(:company) }
  let(:company_location) { FactoryGirl.create(:company_location) }
  let(:agency_provider) { FactoryGirl.create(:location_for_agency) }

  let(:fine_filter_publishers) do
    publishers = []
    3.times { publishers << FactoryGirl.create(:publisher) }
    publishers
  end

  let(:formatted_fine_filter_publishers) do
    fine_filter_publishers.map do |p|
      {
        "id"               => p.id,
        "logo"             => p.logo,
        "like"             => p.search_like,
        "full_site_name"   => p.search_site_name,
        "short_site_name"  => p.short_search_site_name,
        "location"         => p.search_location,
        "category"         => p.search_category
      }
    end
  end

  before(:each) { controller.stub(:current_user).and_return(user) }

  describe 'before_filter' do
    it "authenticate_user should redirect to login page" do
      controller.stub(:current_user).and_return(nil)
      expect(get :index).to redirect_to(login_path)
    end

    it "verify_registration_completeness should redirect for input email" do
      user.update_attributes(:email => "")
      expect(get :index).to redirect_to(edit_signup_user_path(:id => user.uid))
    end
  end

  describe '#index' do
    before {
      agency_provider
      get :index
    }

    it { should assign_to(:discover_items) }
    it { should assign_to(:my_friends) }
    it { should respond_with(:success) }
    it { should render_template(:index) }
    it { should_not set_the_flash }

    it "should assign discover_items with particular data" do
      assigns[:discover_items].should eq([agency_provider])
    end

    it "should assign data_for_autocomplete with particular data" do
      assigns[:data_for_autocomplete].should eq([agency_provider].to_json(:only => :id, :methods => %w(fullname discover_location discover_type)))
    end

  end

  describe '#new_entry' do
    before { xhr :get, :new_entry }

    it { should assign_to(:agency) }
    it { should assign_to(:brand) }
    it { should assign_to(:publisher) }
    it { should assign_to(:ad_type_groups) }
    it { should respond_with(:success) }
    it { should render_template(:new_entry) }
    it { should_not set_the_flash }
  end

  describe '#show' do
    before { get :show, :id => company }

    it { should assign_to(:company) }
    it { should respond_with(:success) }
    it { should render_template(:show) }
    it { should_not set_the_flash }
  end

  describe "#fine_filter" do

    it "should assign collection of publishers" do
      fine_filter_publishers
      xhr :post, :fine_fetch, :search => { :type => 'publisher' }
      assigns(:collection).should have(3).items
    end

  end

end
