require 'spec_helper'

describe NetworksController do

  let(:user) { FactoryGirl.create(:user) }
  let!(:first_network) { FactoryGirl.create(:network, :name => "test_network") }
  let!(:second_network) { FactoryGirl.create(:network, :name => "network") }

  before(:each) { controller.stub(:current_user).and_return(user) }

  describe '#autocomplete' do
    context "simple request" do
      before { get :autocomplete }

      it { should assign_to(:networks) }
      it { should respond_with(:success) }
      it { respond_with_content_type(:json) }
    end

    it "should return json" do
      get :autocomplete, :term => first_network.name[0..2]
      JSON(response.body).should eql([{
        "id" => first_network.id,
        "value" => first_network.name
      }])
    end

    it "should return empty json" do
      get :autocomplete, :term => "unexisting_network"
      JSON.parse(response.body).should be_empty
    end
  end

end
