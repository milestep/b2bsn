require 'spec_helper'

describe AudiencesController do

  let(:user) { FactoryGirl.create(:user) }
  let!(:first_audience) { FactoryGirl.create(:audience, :name => "test_audience") }
  let!(:second_audience) { FactoryGirl.create(:audience, :name => "audience") }

  before(:each) { controller.stub(:current_user).and_return(user) }

  describe '#autocomplete' do
    context "simple request" do
      before { get :autocomplete }

      it { should assign_to(:audiences) }
      it { should respond_with(:success) }
      it { respond_with_content_type(:json) }
    end

    it "should return json" do
      get :autocomplete, :term => first_audience.name[0..2]
      JSON(response.body).should eql([{
        "id" => first_audience.id,
        "value" => first_audience.name
      }])
    end

    it "should return empty json" do
      get :autocomplete, :term => "unexisting_audience"
      JSON.parse(response.body).should be_empty
    end
  end

end
