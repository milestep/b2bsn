require 'spec_helper'

describe ConversationsController do
  let(:user) { FactoryGirl.create(:user) }
  let(:another_user) { FactoryGirl.create(:user, :first_name => 'Billy', :last_name => 'Bob') }
  let(:user_profile) { FactoryGirl.create(:user_profile, :user => user) }
  let(:connection) { FactoryGirl.create(:connection, :user => user, :friend => another_user) }
  let(:conversation) { FactoryGirl.create(:conversation) }
  let(:notification) { FactoryGirl.create(:notification, :sender => user) }
  let(:message) { FactoryGirl.create(:message) }
  let(:double_mailbox) { double('Mailbox') }
  let(:double_receipt) { double('Receipt') }
  let(:receipt) { FactoryGirl.create(:receipt, :receiver => user) }
  let(:another_receipt) { FactoryGirl.create(:receipt, :receiver => another_user) }
  let(:double_active_rec_relation) { double('ActiveRecord::Relation') }
  let(:archived_conversation) { FactoryGirl.create(:archived_conversation, :user => user, :conversation => conversation) }
  let(:data_for_message_creation) do
    {
      "recipients_ids" => user.id,
      "subject" => conversation.subject,
      "body" => conversation.messages.first.body
    }
  end
  let(:message_data) { { "body" => notification.body } }

  before(:each) do
    controller.stub(:current_user).and_return(user)
  end

  describe '#index valid params' do
    before { get :index }

    it { should respond_with(:success) }
    it { should assign_to(:conversations) }
  end

  describe '#index' do
    before {
      double_active_rec_relation.stub(:page).and_return(double_active_rec_relation)
      double_active_rec_relation.stub(:per).and_return(double_active_rec_relation)
    }

    it 'should receive mailbox method' do
      controller.stub(:fetch_not_trashed).and_return([conversation])
      controller.should_receive(:regularize_conversations).and_return([conversation])
      get :index
    end

    it 'user should have no conversations' do
      get :index
      expect(assigns(:conversations)).to be_blank
    end

    it 'should respond with success' do
      get :index
      response.status.should eq(200)
    end
  end

  describe '#show' do
    it 'should receive mark_as_read' do
      User.any_instance.stub(:conversations).and_return(Conversation)
      User.any_instance.should_receive(:mark_as_read).once.with(conversation).and_return(1)
      get :show, :id => conversation.id
    end
  end

  describe '#create' do
    before(:each) do
      Conversation.any_instance.stub(:find).and_return(conversation)
    end

    it 'should receive send_message' do
      message.receipts << receipt
      message.receipts << another_receipt
      conversation.messages << message
      User.any_instance.should_receive(:create_message).and_return(message)
      post :create, :message => data_for_message_creation
    end
  end

  describe '#add_chat_message' do
    before(:each) {
      xhr :get, :add_chat_message, :friend_id => another_user.id, :message => 'test'
    }

    it { should assign_to(:conversation) }
  end

  describe '#window' do
    before(:each) {
      controller.stub(:current_user).and_return(user)
      get :window, :friend_id => another_user.id
    }

    it { should assign_to(:friend) }
    it { should render_template(:window) }
  end

  describe '#reply' do
    before(:each) do
      User.any_instance.stub(:conversations).and_return(Conversation)
    end

    it 'should receive reply_to_conversation' do
      User.any_instance.should_receive(:reply_to_conversation).and_return(double_receipt)
      double_receipt.stub(:message).and_return([message])
      xhr :post, :create_reply, :id => conversation, :message => message_data
    end

    it 'should redirect to conversation' do
      message.receipts << receipt
      message.receipts << another_receipt
      conversation.messages << message
      User.any_instance.stub(:send_message).and_return(double_receipt)
      xhr :post, :create_reply, :id => conversation, :message => message_data
      response.should render_template('conversations/create_reply')
    end
  end

  describe '#trash' do
    before(:each) do
      User.any_instance.stub(:conversations).and_return(Conversation)
    end

    it 'should receive move_to_trash' do
      Conversation.any_instance.should_receive(:move_to_trash).and_return(1)
      post :trash, :id => conversation
    end
  end

  describe '#autocomplete' do
    before(:each) do
      connection
    end

    it 'should respond with success' do
      get :autocomplete, :term => 'John Smith'
      expect(response.status).to eq(200)
    end
  end

  describe '#show' do
    before { xhr :get, :new, :recipient_id => another_user.id }

    it { should assign_to(:recipient) }
    it { should respond_with(:success) }
    it { should render_template(:new) }
  end

  describe '#archive' do
    context 'valid params' do
      before do
        User.any_instance.should_receive(:mark_as_archived).and_return(1)
        controller.stub(:find_conversation)
        xhr :get, :archive, :id => conversation.id
      end

      it { should respond_with(:success) }
      it { should render_template(:archive) }
    end

    context 'action functionality' do
      before(:each) do
        controller.stub(:find_conversation).and_return(conversation)
        double_receipt.stub(:conversation).and_return(conversation)
      end

      it 'should receive mark_as_archived' do
        User.any_instance.should_receive(:mark_as_archived).and_return(archived_conversation)
        xhr :get, :archive, :id => conversation.id
      end

      it 'should respond with success' do
        User.any_instance.stub(:mark_as_archived).and_return(archived_conversation)
        xhr :get, :archive, :id => conversation.id
        expect(response.status).to eq(200)
      end
    end
  end

  describe '#unarchive' do
    context 'valid params' do
      before do
        User.any_instance.should_receive(:unarchive).and_return(1)
        controller.stub(:find_conversation)
        controller.stub(:regularize_conversations).and_return([conversation])
        xhr :get, :unarchive, :id => archived_conversation.id
      end

      it { should assign_to(:conversations) }
      it { should respond_with(:success) }
      it { should render_template(:unarchive) }
    end

    context 'action functionality' do
      before(:each) do
        controller.stub(:find_conversation).and_return(conversation)
      end
      
      it 'should receive unarchive' do
        User.any_instance.should_receive(:unarchive).and_return(1)
        xhr :get, :unarchive, :id => conversation.id
      end

      it 'should receive archived_for_user' do
        User.any_instance.stub(:unarchive).and_return(1)
        controller.should_receive(:regularize_conversations).and_return([conversation])
        xhr :get, :unarchive, :id => conversation.id
      end

      it 'conversations should be empty' do
        User.any_instance.stub(:unarchive).and_return(1)
        xhr :get, :unarchive, :id => conversation.id
        expect(assigns(:conversations)).to be_blank
      end

      it 'conversations should_not be empty' do
        User.any_instance.stub(:unarchive).and_return(1)
        controller.stub(:regularize_conversations).and_return([conversation])
        xhr :get, :unarchive, :id => conversation.id
        expect(assigns(:conversations)).not_to be_blank
      end
    end
  end

  describe '#archived' do
    context 'valid params' do
      before do
        controller.stub(:find_conversation)
        controller.stub(:regularize_conversations).and_return([conversation])
        xhr :get, :archived
      end

      it { should assign_to(:conversations) }
      it { should respond_with(:success) }
      it { should render_template(:archived) }
    end

    context 'action functionality' do
      it 'conversations should be empty' do
        User.any_instance.stub(:unarchive).and_return(1)
        xhr :get, :archived
        expect(assigns(:conversations)).to be_blank
      end

      it 'conversations should_not be empty' do
        User.any_instance.stub(:unarchive).and_return(1)
        controller.stub(:regularize_conversations).and_return([conversation])
        xhr :get, :archived
        expect(assigns(:conversations)).not_to be_blank
      end      
    end
  end

end
