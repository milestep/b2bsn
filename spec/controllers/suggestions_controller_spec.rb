require 'spec_helper'

describe SuggestionsController do

  let(:user) { FactoryGirl.create(:user) }

  before(:each) { controller.stub(:current_user).and_return(user) }

  describe '#index' do
    before { get :index }

    it { should assign_to(:suggested_users) }
    it { should assign_to(:roles) }
    it { should respond_with(:success) }
    it { should render_template(:index) }

  end

end
