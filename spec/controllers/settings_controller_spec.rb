require 'spec_helper'

describe SettingsController do

  let(:user) { FactoryGirl.create(:user) }
  let!(:profile) { FactoryGirl.create(:user_profile, :user => user) }
  let(:authorization) { FactoryGirl.create(:authorization, :user => user) }
  let(:admin) { FactoryGirl.create(:admin) }
  let(:social_network) { FactoryGirl.create(:social_network) }
  let(:setting) { user }
  let(:valid_params) { FactoryGirl.attributes_for(:user_params) }
  let(:invalid_params) { FactoryGirl.attributes_for(:user_params, :email => '#') }

  before(:each) { controller.stub(:current_user).and_return(admin) }

  describe '#index' do
    before { get :index }

    it { should assign_to(:settings) }
    it { should respond_with(:success) }
    it { should render_template(:index) }
    it { should_not set_the_flash }
  end

  describe '#show' do
    before { get :show, :id => setting }

    it { should assign_to(:setting) }
    it { should respond_with(:success) }
    it { should render_template(:show) }
  end

  describe '#edit' do
    before { authorization; get :edit, :id => setting }

    it { should assign_to(:setting) }
    it { should respond_with(:success) }
    it { should render_template(:edit) }
  end

  describe '#update' do
    context 'with valid params' do
      before { put :update, :id => setting, :setting => valid_params }

      it { should assign_to(:setting) }
      it { should redirect_to(edit_setting_path(assigns[:setting]))  }
      it { should set_the_flash }
    end

    context 'with invalid params' do
      before { put :update, :id => setting, :setting => invalid_params }

      it { should assign_to(:setting) }
      it { should render_template(:edit) }
      it 'should not be valid ' do
        assigns(:setting).should_not be_valid
      end
    end
  end

end
