require 'spec_helper'

describe ClicksController do

  let(:user) { FactoryGirl.create(:user) }
  let(:news_item) { FactoryGirl.create(:news_item) }
  let(:valid_params) { FactoryGirl.attributes_for(:click, :clickable_id => news_item.id, :clickable_type => 'NewsItem') }
  let(:invalid_params) { FactoryGirl.attributes_for(:click, :user_id => nil) }

  before(:each) { controller.stub(:current_user).and_return(user) }

  describe '#create' do
    context 'with valid params' do
      before { post :create, :click => valid_params }

      it { should assign_to(:click) }
    end

    context 'with invalid params' do
      before { post :create, :click => invalid_params }

      it { should assign_to(:click) }
    end
  end

end
