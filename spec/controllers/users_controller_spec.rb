require 'spec_helper'

describe UsersController do

  let(:user) { FactoryGirl.create(:user) }
  let(:user_post) { FactoryGirl.create(:post, :user => user) }
  let(:timeline_event) { FactoryGirl.create(:timeline_event, :actor => user, :subject => user_post) }
  let(:bunch_of_timeline_events) { FactoryGirl.create_list(:timeline_event, 20, :actor => user) }
  let(:another_user_post) { FactoryGirl.create(:post, :user => user) }
  let(:another_timeline_event) { FactoryGirl.create(:timeline_event, :actor => user, :subject => another_user_post) }
  let(:another_user_timeline_event) { FactoryGirl.create(:timeline_event, :actor => another_user) }
  let(:user_profile) { FactoryGirl.create(:user_profile, :user => user) }
  let(:another_user) { FactoryGirl.create(:user) }
  let(:connection) { FactoryGirl.create(:connection, :user => user, :friend => another_user) }
  let(:another_user_profile) { FactoryGirl.create(:another_user_profile, :user => another_user) }

  before(:each) { controller.stub(:current_user).and_return(user) }

  it "should display a user" do
    get :show, :user => FactoryGirl.create(:user)
    assigns(:user).should eql(user)
  end

  describe "activity feed" do

    it "should return activity items for choosen user" do
      another_user_timeline_event and timeline_event
      get :update_activity_feed, :user_id => another_user.id
      assigns(:activity_feed).should include(another_user_timeline_event)
    end

    it "should return empty activity feed" do
      get :update_activity_feed, :user => user
      assigns(:activity_feed).should be_empty
    end

    it "should return non empty activity feed" do
      timeline_event
      get :update_activity_feed, :user_id => user.id
      assigns(:activity_feed).should have(1).item
    end

    it "should return records created earlier than specified date" do
      timeline_event
      another_timeline_event.created_at = 2.day.ago
      another_timeline_event.save
      another_timeline_event
      get :update_activity_feed, :user_id => user.id, :t => 1.day.ago
      assigns(:activity_feed).should have(1).item
    end

    it "should update activity feed and select timeline events by type" do
      timeline_event
      another_timeline_event.event_type = "question_asked"
      another_timeline_event.save
      another_timeline_event
      get :update_activity_feed, :user_id => user.id, :event_type => "question_asked"
      assigns(:activity_feed).should have(1).item
    end

  end

  it "should calculate new users and their count" do
    (1..15).each do |number|
      FactoryGirl.create(:user, :first_name => "FirstName#{number}", :last_name => "LastName#{number}")
    end
    (1..4).each do |number|
      FactoryGirl.create(:user, :first_name => "FirstName#{number}", :last_name => "LastName#{number}", :created_at => 9.days.ago)
    end
    get :new_users_modal
    assigns(:users).should have(10).items
    assigns(:more_users).should eql(6)
  end

  describe "type" do

    it "should show users with featured type" do
      user_profile.featured = false
      user_profile.save
      another_user_profile.featured = true
      another_user_profile.save
      get :show_users, :t => "featured"
      assigns(:users).should include(another_user)
      assigns(:users).should_not include(user)
    end

    it "should show newest users" do
      get :show_users, :t => "newest"
      assigns(:users).should include(another_user)
      assigns(:users).should_not include(user)
    end

    it "should show suggested users" do
      user.stub(:suggested_users).and_return([another_user])
      get :show_users, :t => "suggested"
      assigns(:users).should include(another_user)
      assigns(:users).should_not include(user)
    end

    it "should show users who sent invitations to the user" do
      connection.status = "pending"
      connection.user = another_user
      connection.friend = user
      connection.save
      get :show_users, :t => "incoming"
      assigns(:connections).should include(connection)
    end

    it "should show users who received invitations from the user" do
      connection.status = "pending"
      connection.save
      get :show_users, :t => "outgoing"
      assigns(:connections).should include(connection)
    end

    it "should show activity stream items" do
      bunch_of_timeline_events
      get :show
      assigns(:activity_feed).should have(TimelineEvent::EVENTS_PER_PAGE).items
    end

  end

end
