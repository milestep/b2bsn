require 'spec_helper'

describe NoticesController do
  let(:user) { FactoryGirl.create(:user) }
  let(:question) { FactoryGirl.create(:question, :user => user) }
  let(:timeline_event) { FactoryGirl.create(:timeline_event_question_asked, :actor => user, :subject => question) }
  let!(:user_notice) { FactoryGirl.create(:users_notice, :user => user, :timeline_event => timeline_event) }

  before(:each) { controller.stub(:current_user).and_return(user) }

  describe "#index" do
    it "should get notices" do
      get 'index'
      response.should be_success
      assigns(:notices).should_not be_nil
    end
  end


  describe '#destroy' do
    context "with several notice events" do
      let(:another_timeline_event) { FactoryGirl.create(:timeline_event_question_asked, :actor => user, :subject => question) }
      let!(:another_user_notice) { FactoryGirl.create(:users_notice, :user => user, :timeline_event => another_timeline_event) }

      before { xhr :delete, :destroy, :id => another_timeline_event.id }

      it { should assign_to(:notice) }
      it { should respond_with(:success) }
      it { should render_template(:destroy) }

      it "should destroy notice_event for user" do
        user.notice_events.should_not include(another_timeline_event)
        user.notice_events.should_not be_empty
        assigns(:all_notices_size).should be > 0
        assigns(:notices).should_not be_empty
        assigns(:notice_events).should_not be_empty
      end
    end

    context "with last notice event" do
      before { xhr :delete, :destroy, :id => timeline_event.id }

      it { should assign_to(:notice) }
      it { should respond_with(:success) }
      it { should render_template(:destroy) }

      it "should destroy last notice_event for user" do
        user.notice_events.should_not include(timeline_event)
        user.notice_events.should be_empty
        assigns(:all_notices_size).should eq(0)
        assigns(:notices).should be_nil
      end
    end
  end

  describe '#destroy_all' do
    let(:another_timeline_event) { FactoryGirl.create(:timeline_event_question_asked, :actor => user, :subject => question) }
    let!(:user_notice) { FactoryGirl.create(:users_notice, :user => user, :timeline_event => another_timeline_event) }

    before { xhr :get, :destroy_all }

    it { should respond_with(:success) }
    it { should render_template(:destroy_all) }

    it "should destroy all notice_events for user" do
      user.notice_events.should_not include(timeline_event, another_timeline_event)
      user.notice_events.should be_empty
    end
  end

end
