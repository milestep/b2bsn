require 'spec_helper'

describe PostsController do
  let(:user) { FactoryGirl.create(:user) }
  let!(:user_profile) { FactoryGirl.create(:user_profile, :user => user) }
  let(:friend) { FactoryGirl.create(:user, :first_name => "Name", :last_name => "Surname") }
  let!(:connection) { FactoryGirl.create(:connection, :user => user, :friend => friend) }
  let(:feed_post) { FactoryGirl.create(:post, :user => user) }
  let(:question) { FactoryGirl.create(:question, :user => user) }
  let(:valid_params) { FactoryGirl.attributes_for(:post, :title => friend.fullname) }
  let(:invalid_params) { FactoryGirl.attributes_for(:post, :title => "") }

  before(:each) { controller.stub(:current_user).and_return(user) }

  describe '#show' do
    context 'for a post' do
      before { get :show, :id => feed_post }

      it { should assign_to(:post) }
      it { should assign_to(:other_posts) }
      it { should respond_with(:success) }
      it { should render_template(:show) }
      it { should_not set_the_flash }
    end

    context 'for a question' do
      before { get :show, :id => question }

      it { should assign_to(:post) }
      it { should assign_to(:other_questions) }
      it { should respond_with(:success) }
      it { should render_template(:show) }
      it { should_not set_the_flash }
    end
  end

  describe '#create' do
    context 'with valid params' do
      before { xhr :get, :create, :post => valid_params, 'auto-who-publishing' => '' }

      it { should assign_to(:post) }
      it { should assign_to(:authorizations) }
      it { should assign_to(:activity_item) }
      it { should respond_with(:success) }
      it { should render_template(:create) }
      it { should set_the_flash }

    end

    context 'with valid params and mentioned_users_ids' do
      before { xhr :get, :create, :post => valid_params, :mentioned_users_ids => "#{friend.id}", 'auto-who-publishing' => '' }

      it { should assign_to(:post) }
      it { should assign_to(:authorizations) }
      it { should assign_to(:activity_item) }
      it { should respond_with(:success) }
      it { should render_template(:create) }
      it { should set_the_flash }
    end

    context 'with invalid params' do
      before { xhr :get, :create, :post => invalid_params, 'auto-who-publishing' => '' }

      it { should assign_to(:post) }
      it { should assign_to(:authorizations) }
      it { should respond_with(:success) }
      it { should render_template(:create) }
      it { should_not set_the_flash }
    end
  end

  describe '#new' do
    before { xhr :get, :new }
    it { should assign_to(:post) }
    it { should respond_with(:success) }
    it { should render_template(:new) }

    it "should assign appropriate user profile" do
      expect(assigns[:user_profile]).to eql(user_profile)
    end

    it "variable autocomplete should has hash keys friend_summaries and circles" do
      expect(assigns[:autocomplete]).to include("friend_summaries","circles")
    end

    it "autocomplete hash key friend_summaries should include users id" do
      expect(assigns[:autocomplete][:friend_summaries]).to eql(User.friend_summaries(user))
    end

    it "autocomplete hash key circles should include post_circles" do
      expect(assigns[:autocomplete][:circles]).to eq(user.circles.select([:id, :user_id, :name]))
    end
  end

end

