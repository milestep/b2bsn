require 'spec_helper'

describe JiraIssuesController do
  let(:user) { FactoryGirl.create(:user) }
  let(:message) { "message" }

  before(:each) { controller.stub(:current_user).and_return(user) }

  ['feature_requests', 'support_requests'].each do |type|
    context type do
      describe '#new' do
        before { xhr :get, :new, :use_route => "new_#{type}", :type => type.classify }

        it { should respond_with(:success) }
        it { should render_template(:new) }
      end

      describe '#create' do
        context 'with valid params' do
          before { xhr :post, :create, :use_route => type, :type => type.classify, :description => 'message' }

          it { should respond_with(:success) }
          it { should render_template(:create) }

        end
      end
    end
  end
end