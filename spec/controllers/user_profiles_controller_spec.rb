require 'spec_helper'

describe UserProfilesController do

  let(:user) { FactoryGirl.create(:user) }
  let!(:user_profile) { FactoryGirl.create(:user_profile, :user => user) }
  let(:valid_params) { FactoryGirl.attributes_for(:user_profile) }

  before(:each) { controller.stub(:current_user).and_return(user) }

  describe '#show' do
    let(:another_user) { FactoryGirl.create(:user) }
    let!(:first_user_activity) { FactoryGirl.create(:timeline_event_question_asked, :actor => user) }
    let!(:second_user_activity) { FactoryGirl.create(:timeline_event_question_asked, :actor => user) }
    let!(:another_user_activity) { FactoryGirl.create(:timeline_event_question_asked, :actor => another_user) }

    before { get :show, :id => user_profile }

    it { should assign_to(:user_profile) }
    it { should assign_to(:activity_items) }
    it { should assign_to(:user_friends) }
    it { should respond_with(:success) }
    it { should render_template(:show) }

    it "should load to activity items only specific user's activity" do
      assigns[:activity_items].should include(second_user_activity, first_user_activity)
    end

    it "should not load to activity items another user's activity" do
      assigns[:activity_items].should_not include(another_user_activity)
    end
  end

  describe '#popup_profile' do
    before { get :popup_profile, :id => user_profile }

    it { should assign_to(:user_profile) }
    it { should respond_with(:success) }
    it { should render_template(:popup_profile) }
    it { should_not render_with_layout }
  end

  describe '#edit_base_info' do
    before { xhr :get, :edit_base_info, :id => user_profile }

    it { should assign_to(:user_profile) }
    it { should respond_with(:success) }
    it { should render_template(:edit_base_info) }
    it { should_not render_with_layout }
  end

  describe '#update_base_info' do
    context 'with valid params' do
      before { xhr :put, :update_base_info, :id => user_profile, :user_profile => valid_params }

      it { should assign_to(:user_profile) }
      it { should render_template(:success)  }
      context "with changed info" do
        let(:valid_params) { FactoryGirl.attributes_for(:user_profile).merge(:description => 'special test description') }

        it "should update info" do
          expect(assigns[:user_profile].reload.description).to eql('special test description')
        end
      end
    end
  end

  describe '#update_photo' do
    let(:avatar) { fixture_file_upload('/files/test_image.png', 'image/png') }
    before { xhr :post, :update_photo, :id => user_profile, :user_profile => { :avatar => avatar } }

    it { should assign_to(:user_profile) }
    it { respond_with_content_type(:json) }
    it "should update avatar" do
      expect(user_profile.reload.avatar_url).to include("test_image.png")
    end

    it "should return json with avatar url" do
      expect(JSON(response.body)).to eql([{"file_url" => user_profile.reload.avatar_url}])
    end
  end
end
