require 'spec_helper'

describe TimelineEventsController do

  let!(:role) { FactoryGirl.create(:role) }
  let(:user) { FactoryGirl.create(:user, :role => role) }
  let(:timeline_event) { FactoryGirl.create(:timeline_event) }
  let(:user_timeline_event) { FactoryGirl.create(:timeline_event, :actor => user) }

  before(:each) { controller.stub(:current_user).and_return(user) }

  describe '#hide' do

    before { xhr :get, :hide, :id => timeline_event.id }

    it { should assign_to(:timeline_event) }
    it { should respond_with(:success) }
    it { should render_template(:hide) }

    it "should hide event from user" do
      user.timeline_events.should_not include(timeline_event)
    end
  end

  describe '#bulk_hide' do
    let(:another_user) { FactoryGirl.create(:user) }
    let!(:status_updated) { FactoryGirl.create(:timeline_event_status_updated, :actor => another_user) }
    let!(:question_asked) { FactoryGirl.create(:timeline_event_question_asked, :actor => another_user) }
    let!(:own_status_updated) { FactoryGirl.create(:timeline_event_status_updated, :actor => user) }

    before { xhr :get, :bulk_hide, :user_id => another_user.id }

    it { should assign_to(:actor) }
    it { should respond_with(:success) }
    it { should render_template(:bulk_hide) }

    it "should hide events from selected user" do
      user.timeline_events.should_not include(status_updated, question_asked)
    end
  end

  describe '#undo_bulk_hide' do
    let(:another_user) { FactoryGirl.create(:user) }
    let!(:status_updated) { FactoryGirl.create(:timeline_event_status_updated, :actor => another_user) }
    let!(:question_asked) { FactoryGirl.create(:timeline_event_question_asked, :actor => another_user) }
    let(:own_status_updated) { FactoryGirl.create(:timeline_event_status_updated, :actor => user) }

    before { xhr :get, :undo_bulk_hide, :user_id => another_user.id }

    it { should assign_to(:activity_items) }
    it { should assign_to(:actor) }
    it { should respond_with(:success) }
    it { should render_template(:undo_bulk_hide) }

    it "should unhide all events from selected user" do
      user.timeline_events.should include(status_updated, question_asked)
    end
  end

  describe '#show' do
    before { xhr :get, :show, :id => user_timeline_event }

    it { should respond_with(:success) }
    it { should assign_to(:item) }
    it { should render_template(:show) }
  end

end
