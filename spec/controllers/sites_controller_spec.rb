require 'spec_helper'

describe SitesController do

  let(:user) { FactoryGirl.create(:user) }
  let!(:first_site) { FactoryGirl.create(:site, :name => "test_site") }
  let!(:second_site) { FactoryGirl.create(:site, :name => "site") }

  before(:each) { controller.stub(:current_user).and_return(user) }

  describe '#autocomplete' do
    context "simple request" do
      before { get :autocomplete }

      it { should assign_to(:sites) }
      it { should respond_with(:success) }
      it { respond_with_content_type(:json) }
    end

    it "should return json" do
      get :autocomplete, :term => first_site.name[0..2]
      JSON(response.body).should eql([{
        "id" => first_site.id,
        "value" => first_site.name
      }])
    end

    it "should return empty json" do
      get :autocomplete, :term => "unexisting_site"
      JSON.parse(response.body).should be_empty
    end
  end

end
