require 'spec_helper'

describe AnswersController do

  let(:user) { FactoryGirl.create(:user) }
  let(:answer_post) { FactoryGirl.create(:post) }
  let(:answer) { FactoryGirl.create(:answer, :answer => "I'm an answer.", :post => answer_post, :user => user) }

  before(:each) { controller.stub(:current_user).and_return(user) }

  context "create" do

    it "should not create an answer with incomplete params" do
      post :create, :answer => {}, :post_id => answer_post.id
      assigns(:answer).should_not be_valid
      assigns(:answer).errors[:answer].should_not be_empty
      response.should redirect_to(assigns(:answer).post)
      flash[:notice].should eql(I18n.t('answers.create.answer_can_not_be_blank'))
    end

    it "should create an answer" do
      post :create, :answer => { :answer => "Good answer" }, :post_id => answer_post.id
      assigns(:answer).should be_valid
      assigns(:answer).errors.should be_empty
      response.should redirect_to(assigns(:answer).post)
      flash[:notice].should eql(I18n.t('answers.create.answer_was_successfully_created'))
    end

  end

  it "should destroy an answer" do
    delete :destroy, :id => answer.id
    Answer.exists?(answer).should be_false
    response.should redirect_to(answer_post)
  end

end
