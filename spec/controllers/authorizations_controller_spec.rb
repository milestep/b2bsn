require 'spec_helper'

describe AuthorizationsController do

  let(:user) { FactoryGirl.create(:user) }
  let(:social_network) { FactoryGirl.create(:social_network) }
  let(:authorization) { FactoryGirl.create(:authorization, :user => user, :social_network => social_network) }
  let(:valid_params) { FactoryGirl.attributes_for(:authorization) }
  let(:invalid_params) { FactoryGirl.attributes_for(:authorization, :provider => nil) }
  let(:valid_omniauth) { FactoryGirl.create(:omniauth, :uid => user.uid) }

  before(:each) { controller.stub(:current_user).and_return(user) }

  describe '#index' do
    before { get :index, :user_id => user }

    it { should assign_to(:authorizations) }
    it { should respond_with(:success) }
    it { should render_template(:index) }
  end

  describe '#show' do
    before { get :show, :user_id => user, :id => authorization }

    it { should assign_to(:authorization) }
    it { should respond_with(:success) }
    it { should render_template(:show) }
  end

  describe '#new' do
    before { get :new, :user_id => user, :id => authorization, :social_network_id => social_network }

    it { should assign_to(:authorization) }
    it { should assign_to(:social_network) }
    it { should respond_with(:success) }
    it { should render_template(:new) }
  end

  describe '#edit' do
    before { get :edit, :user_id => user, :id => authorization }

    it { should assign_to(:authorization) }
    it { should respond_with(:success) }
    it { should render_template(:edit) }
  end

  describe '#create' do
    context 'with valid params' do
      before { request.env["omniauth.auth"] = valid_omniauth; post :create, :user_id => user, :post => valid_params }

      it { should redirect_to(edit_setting_path(user))  }
      it { should set_the_flash }
    end

  end

  describe '#update' do
    context 'with valid params' do
      before { put :update, :user_id => user, :id => authorization, :authorization => valid_params }

      it { should assign_to(:authorization) }
      it { should redirect_to(user_authorization_path(user, authorization))  }
      it { should set_the_flash }
    end

    context 'with invalid params' do
      before { put :update, :user_id => user, :id => authorization, :authorization => invalid_params }

      it { should assign_to(:authorization) }
      it { should render_template(:edit) }
      it 'should not be valid ' do
        assigns(:authorization).should_not be_valid
      end
    end

  end

  describe '#destroy' do
    before { delete :destroy, :user_id => user, :id => authorization }

    it { should assign_to(:authorization) }
    it { should redirect_to(edit_setting_path(user))  }
    it { should set_the_flash }
    it 'should destroy the authorization' do
      assigns[:authorization].should be_destroyed
    end
  end

end
