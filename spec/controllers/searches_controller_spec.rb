require 'spec_helper'

describe SearchesController do
  let(:user) { FactoryGirl.create(:user) }
  let(:found_user) { FactoryGirl.create(:user) }
  let(:valid_formatted_results_with_people) do
    { "people" => [['John Smith','John Smith'],['John Smith','John Smith']] }
  end
  let(:valid_formatted_results_with_people_for_autocomplete) do
    [ {"label" => 'John Smith', "value" => 'John Smith'}, {"label" => 'John Smith', "value" => 'John Smith'} ]
  end
  let(:users) do
    users = []
    5.times { users << FactoryGirl.create(:user, :first_name => 'John', :last_name => 'Smith') }
    users
  end
  let(:search_engine) { double('Sunspot::Search') }

  before(:each) { controller.stub(:current_user).and_return(user) }

  describe '#index' do
    context 'empty filter and search parameters' do
      before { get :index, :filter => '', :search => '' }

      it { should respond_with(:success) }
      it { should render_template(:index) }
    end

    context 'right filter and search parameters' do
      before { get :index, :filter => 'people', :search => found_user.first_name }

      it { should assign_to(:search_results) }
      it { should respond_with(:success) }
      it { should render_template(:index) }
    end

    context 'wrong filter parameters' do
      before { get :index, :filter => 'qwerty', :search => '' }

      it { should respond_with(:success) }
      it { should render_template(:index) }
    end
  end

  describe "#autocomplete" do

    it "should not receive a call of before filter #check_search_params on blank params" do
      controller.should_not_receive(:check_search_params)
      get :autocomplete
    end

    it "should receive a call of before filter #ensure_params_provided" do
      controller.should_receive(:ensure_params_provided)
      get :autocomplete, :filter => 'all', :term => 'John Smith'
    end

    it "shouldn't receive a call of before filter #ensure_params_provided on index action" do
      controller.should_not_receive(:ensure_params_provided)
      get :index
    end

    it "should respond with fail" do
      get :autocomplete
      response.status.should eq(400)
    end

    it "should respond with success" do
      get :autocomplete, :filter => 'all', :term => 'John Smith'
      response.status.should eq(200)
    end

    it "should respond with valid json data" do
      search_engine.stub(:results).and_return(users)
      User.stub(:solr_search).and_return(search_engine)
      get :autocomplete, :filter => 'all', :term => 'John Smith', :rows => '2'
      results = JSON.parse(response.body)
      results.should eq(valid_formatted_results_with_people_for_autocomplete)
    end

    it "should be case insensative" do
      get :autocomplete, :filter => 'People', :term => 'John Smith'
      response.status.should eq(200)
    end

  end

end
