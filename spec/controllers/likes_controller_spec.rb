require 'spec_helper'

describe LikesController do

  let(:user) { FactoryGirl.create(:user) }
  let(:user_post) { FactoryGirl.create(:post, :user => user) }
  let(:user_comment) { FactoryGirl.create(:comment, :user => user) }
  let(:user_answer) { FactoryGirl.create(:answer, :user => user) }
  let(:user_news_item) { FactoryGirl.create(:news_item) }

  before(:each) { controller.stub(:current_user).and_return(user) }

  context "callbacks" do

    context "get likes" do

      it "should get likes for post" do
        get :index, :post_id => user_post.id
        assigns(:likeable).should eq(user_post)
      end

      it "should get likes for comment" do
        get :index, :comment_id => user_comment.id
        assigns(:likeable).should eq(user_comment)
      end

      it "should get likes for answer" do
        get :index, :answer_id => user_answer.id
        assigns(:likeable).should eq(user_answer)
      end

    end

  end

  context "create" do

    it "should increase like value for comment" do
      post :create, :comment_id => user_comment.id
      assigns(:likeable).votes.should have(1).item
      assigns(:likeable).likes.should have(1).item
    end

    it "should decrease like value for comment" do
      post :create, :comment_id => user_comment.id, :downvote => true
      assigns(:likeable).votes.should have(1).item
      assigns(:likeable).dislikes.should have(1).item
    end

  end

  context "index" do

    it "should show likes for comment" do
      get :index, :comment_id => user_comment.id
      assigns(:likeable).should eq(user_comment)
    end

  end

end
