require "spec_helper"

describe PublishersController do
  let(:user) { FactoryGirl.create(:user) }
  let(:publisher) { FactoryGirl.create(:publisher) }
  let(:valid_params) { FactoryGirl.attributes_for(:publisher) }
  let(:invalid_params) { FactoryGirl.attributes_for(:publisher, :name => "") }

  before(:each) { controller.stub(:current_user).and_return(user) }

  describe '#show' do
    before { xhr :get, :show, :id => publisher }

    it { should assign_to(:publisher) }
    it { should assign_to(:contacts) }
    it { should respond_with(:success) }
    it { should render_template(:show) }
  end

  describe '#create' do
    context 'with valid params' do
      before { xhr :post, :create, :publisher => valid_params }

      it { should assign_to(:publisher) }
      it { should render_template(:create_success)  }
    end

    context 'with invalid params' do
      before { xhr :post, :create, :publisher => invalid_params }

      it { should assign_to(:publisher) }
      it { should render_template(:error) }
    end
  end

  describe '#edit' do
    before { xhr :get, :edit, :id => publisher }

    it { should assign_to(:publisher) }
    it { should assign_to(:ad_type_groups) }
    it { should respond_with(:success) }
    it { should render_template(:edit) }

    it "should build new company audience if it does not exist" do
      assigns[:publisher].company_audiences.should_not be_empty
    end

    it "should build new company category if it does not exist" do
      assigns[:publisher].company_categories.should_not be_empty
    end

    it "should build new company site if it does not exist" do
      assigns[:publisher].company_sites.should_not be_empty
    end

    it "should build new company category if it does not exist" do
      assigns[:publisher].company_networks.should_not be_empty
    end
  end

  describe '#update' do
    context 'with valid params' do
      before { xhr :put, :update, :id => publisher.id, :publisher => valid_params }

      it { should assign_to(:publisher) }
      it { should respond_with(:success) }
      it { should render_template(:update_success)  }
    end

    context 'with invalid params' do
      before { xhr :put, :update, :id => publisher.id, :publisher => invalid_params }

      it { should assign_to(:publisher) }
      it { should assign_to(:ad_type_groups) }
      it { should respond_with(:success) }
      it { should render_template(:error) }

      it "should build new company audience if it does not exist" do
        assigns[:publisher].company_audiences.should_not be_empty
      end

      it "should build new company category if it does not exist" do
        assigns[:publisher].company_categories.should_not be_empty
      end

      it "should build new company site if it does not exist" do
        assigns[:publisher].company_sites.should_not be_empty
      end

      it "should build new company category if it does not exist" do
        assigns[:publisher].company_networks.should_not be_empty
      end
    end
  end

end
