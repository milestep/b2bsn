require 'spec_helper'

describe AgenciesController do

  let(:user) { FactoryGirl.create(:user) }
  let(:agency) { FactoryGirl.create(:agency) }
  let(:company) { FactoryGirl.create(:brand, :agencies => [agency]) }
  let(:valid_params) { FactoryGirl.attributes_for(:agency) }
  let(:invalid_params) { FactoryGirl.attributes_for(:agency, :agency_brands_attributes => [{ :brand_id => nil, :brand_name => 'Brand' }]) }

  before(:each) { controller.stub(:current_user).and_return(user) }

  describe '#index' do

    context "all agencies" do
      before { get :index }

      it { should assign_to(:agencies) }
      it { should respond_with(:success) }
      it { should render_template(:index) }
      it { should_not set_the_flash }
    end

    context "agencies for company" do
      it "should get all agencies for company" do
        get :index, :company => company.id
        assigns(:company).should eq(company)
        assigns(:agencies).should eq(company.agencies.order_by_name("ASC"))
      end
    end

    context "all agencies" do
      it "should get all agencies" do
        get :index
        assigns(:agencies).should eq(Agency.order_by_name("ASC"))
      end
    end
  end

  describe '#show' do
    before { get :show, :id => agency }

    it { should assign_to(:agency) }
    it { should respond_with(:success) }
    it { should render_template(:show) }
    it { should_not set_the_flash }
  end

  describe '#edit' do
    before { get :edit, :id => agency }

    it { should assign_to(:agency) }
    it { should respond_with(:success) }
    it { should render_template(:edit) }
    it { should_not set_the_flash }
  end

  describe '#new' do
    before { xhr :get, :new }

    it { should assign_to(:agency) }
    it { should respond_with(:success) }
    it { should render_template(:new) }
    it { assigns(:agency).should be_a_new(Agency) }
  end

  describe '#create' do
    context 'with valid params' do
      before { post :create, :agency => valid_params }

      it { should assign_to(:agency) }
      it { should redirect_to(tools_path)  }
      it { should set_the_flash }
    end

    context 'with invalid params' do
      before { post :create, :agency => invalid_params }

      it { should assign_to(:agency) }
      it { should render_template(:new) }
      it { should_not set_the_flash }
    end
  end

  describe '#update' do
    context 'with valid params' do
      before { put :update, :id => agency, :agency => valid_params }

      it { should assign_to(:agency) }
      it { should redirect_to(agency_path(agency))  }
      it { should set_the_flash }
    end

    context 'with invalid params' do
      before { put :update, :id => agency, :agency => invalid_params }

      it { should assign_to(:agency) }
      it { should render_template(:edit) }
      it { should_not set_the_flash }
    end
  end

  describe '#destroy' do
    before { delete :destroy, :id => agency }

    it { should assign_to(:agency) }
    it { should redirect_to(agencies_path)  }
    it { should_not set_the_flash }
  end

  describe '#autocomplete' do
    before { get :autocomplete }

    it { should assign_to(:agencies) }
    it { should respond_with(:success) }
    it { respond_with_content_type(:json) }
    it { should_not set_the_flash }
  end

end
