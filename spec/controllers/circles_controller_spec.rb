require 'spec_helper'

describe CirclesController do

  let(:user) { FactoryGirl.create(:user) }
  let!(:circle) { FactoryGirl.create(:circle, :name => "Circle", :user => user) }
  let!(:another_circle) { FactoryGirl.create(:circle, :name => "AnotherCircle", :user => user) }
  let(:new_user) { FactoryGirl.create(:user, :first_name => "First", :last_name => "Last") }
  let!(:connection) { FactoryGirl.create(:connection, :user => user, :friend => new_user) }

  before(:each) { controller.stub(:current_user).and_return(user) }

  it "should prepare new circle" do
    get :new
    assigns(:circle).should_not be_nil
  end

  context "create" do

    it "should create new circle" do
      post :create, :circle => { :name => "NewCircle" }
      assigns(:circle).should be_valid
      response.should redirect_to(connections_path)
      flash[:notice].should eql(I18n.t('your_new_group_was_successfully_created'))
    end

    it "should create new circle with json format" do
      post :create, :circle => { :name => "NewCircle" }, :format => :json
      JSON.parse(response.body)["name"].should eql("NewCircle")
    end

  end

  it "should update the circle" do
    put :update, :id => circle.id, :circle => { :name => "UpdatedName" }
    assigns(:circle).should be_valid
    assigns(:circle).name.should eql("UpdatedName")
    response.should redirect_to(circle_path)
    flash[:notice].should eql(I18n.t('circle_was_successfully_updated'))
  end

  it "should destroy the circle" do
    delete :destroy, :id => another_circle.id
    assigns(:circles).should_not include(another_circle)
    assigns(:circles).should include(circle)
    response.should redirect_to(connections_path)
    flash[:notice].should eql(I18n.t("group_was_successfully_deleted"))
  end

  context "add user to a circle" do

    it "should add a user to the circle" do
      get :add_user, :id => circle.id, :user_id => new_user.id, :expanded => "expanded"
      assigns(:circle).users.should include(new_user)
      assigns(:expanded).should eql("expanded")
    end

    it "should add a user while he is in the circle" do
      circle.users << new_user
      get :add_user, :id => circle.id, :user_id => new_user.id, :expanded => "added"
      assigns(:circle).users.should include(new_user)
      assigns(:expanded).should eql("added")
    end

  end

  it "should delete a user from a circle" do
    circle.users << new_user
    get :delete_user, :id => circle.id, :user_id => new_user.id, :expanded => "deleted"
    assigns(:circle).users.should_not include(new_user)
    assigns(:expanded).should eql("deleted")
  end

  it "should confirm deleting" do
    get :confirm_delete, :id => circle.id
    assigns(:circle).should eql(circle)
  end

  it "should show expanded circles" do
    get :show_expanded, :id => circle.id
    assigns(:circle).should eql(circle)
    assigns(:circles).should_not include(circle)
  end

  it "should collapse expanded circles" do
    get :collapse_expanded
    assigns(:circles).should include(circle, another_circle)
  end

end
