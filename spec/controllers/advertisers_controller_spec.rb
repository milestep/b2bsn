require 'spec_helper'

describe AdvertisersController do

  let(:user) { FactoryGirl.create(:user) }

  before(:each) { controller.stub(:current_user).and_return(user) }

  describe '#index' do
    before { get :index }

    it { should assign_to(:advertisers) }
    it { should respond_with(:success) }
    it { should render_template(:index) }
    it { should_not set_the_flash }

  end

  it "should create advertiser" do
    xhr :post, :create, :advertiser => { :name => "Created Advertiser" }
    assigns(:advertiser).should be_valid
    response.should render_template("create")
  end

  it "should not create advertiser" do
    xhr :post, :create, :advertiser => {}
    assigns(:advertiser).should have(1).error_on(:name)
    response.should render_template("create")
  end

  it "should create advertiser, html format" do
    post :create, :advertiser => { :name => "Created Advertiser" }
    assigns(:advertiser).should be_valid
    response.should redirect_to(tools_path)
    flash[:notice].should eql(I18n.t('advertisers.create.success'))
  end

  describe '#autocomplete' do
    before { get :autocomplete }

    it { should assign_to(:advertisers) }
    it { should respond_with(:success) }
    it { respond_with_content_type(:json) }
    it { should_not set_the_flash }
  end

end
