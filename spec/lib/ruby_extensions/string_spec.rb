require "spec_helper"

describe String do
  context "#underline" do
    it "should replace spaces to underlines" do
      "test string".underline.should == "test_string"
    end
  end

  context "#replace_new_line_with_br" do
    it "should replace spaces to underlines" do
      expect("test\r\nstring\n\n".replace_new_line_with_br).to eql("test<br/>string<br/><br/>")
    end
  end

  context "#remove_html_tags" do
    it "should replace spaces to underlines" do
      expect("test<br/> <a>string</a><br/>".remove_html_tags).to eql("test string")
    end
  end
end
