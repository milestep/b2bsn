require "spec_helper"

describe Float do
  context "#to_minutes" do
    it "should convert seconds to minutes" do
      seconds = 600.00
      seconds.to_minutes.should eq(10)
    end
  end
end
