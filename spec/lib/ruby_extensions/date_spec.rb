require "spec_helper"

describe Date do
  context "#localized_month" do
    it "should return english month name" do
      Date.new(2012,9,24).localized_month.should eql("September")
    end

    it "should return another english month name" do
      Date.new(2012,5,24).localized_month.should eql("May")
    end
  end
end
