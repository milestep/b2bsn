require "spec_helper"

describe Array do
  context "#rejoin" do
    it "should reject blank items and join with coma" do
      [nil, "Hello", '', "World"].rejoin.should == "Hello, World"
    end
    it "should join items with particular divider" do
      [nil, "Hello", '', "There"].rejoin('-').should == "Hello-There"
    end
  end
end
