require 'spec_helper'

describe StandartBuilder do
  let(:user) { FactoryGirl.create(:user) } 
  let(:user_with_invalid_name) { FactoryGirl.build(:user, :email => "email") } 

  describe 'error_messages' do
    context 'valid object attribute' do
      it 'should has no error messages' do
        expect(StandartBuilder.new(:user, user, view, {}, proc{}).error_messages).to be_nil
      end
    end

    context 'invalid objet attribute' do
      before(:each) do
        user_with_invalid_name.save
      end

      it 'should has error messages' do
        expect(StandartBuilder.new(:user, user_with_invalid_name, view, {}, proc{}).error_messages(:attribute_name => false)).not_to be_nil
      end

      it 'should show error message with attribute name' do
        expect(StandartBuilder.new(:user, user_with_invalid_name, view, {}, proc{}).error_messages).to include('Email')
      end

      it 'should show error message with attribute name' do
        expect(StandartBuilder.new(:user, user_with_invalid_name, view, {}, proc{}).error_messages(:attribute_name => false)).not_to include('Email')
      end
    end
  end
end
