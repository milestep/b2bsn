require 'spec_helper'

describe FineFilter::Fetcher do

  subject { FineFilter::Fetcher }
  let(:users) do
    users = []
    3.times { users << FactoryGirl.create(:user) }
    users
  end

  { 'publisher' => { :type => 'publisher' }, 'brand' =>  { :type => 'brand' }, 'agency' => { :type => 'agency' }, 'user' => { :type => 'person'}}.each_pair do |key, value|

    it "should create valid #{key.to_s.classify} formatter" do
      subject.new(value).valid?.should be_true
    end

    it "should create #{key.to_s.classify} source object" do
      subject.new(value).source_class.should eq(key.classify.constantize)
    end

  end

  it "should create invalid formatter" do
    subject.new(:type =>'foobar').valid?.should be_false
  end

  it "should retrieve collection" do
    expect(subject.new(:type => 'person').fetch).to eq(User.all)
  end

end
