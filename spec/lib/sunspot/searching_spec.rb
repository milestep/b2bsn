require 'spec_helper'

describe Sunspot::Searching do
  let(:user) { FactoryGirl.create(:user) }
  let(:users) do
    users = []
    5.times { users << FactoryGirl.create(:user, :first_name => 'Billy', :last_name => 'Bob') }
    users
  end

  let(:double_results) { double('Sunspot::Search') }
  let(:random_name) { (0...8).map{65.+(rand(25)).chr}.join }
  let(:expected_no_result_found) {
    {
      :people => [],
      :search_text => random_name
    }
  }
  let(:question) { FactoryGirl.create(:question) }
  let(:no_result_found) { Sunspot::Searching.new(:people, random_name) }
  let(:search_by_people) { Sunspot::Searching.new(:people, user.first_name) }
  let(:search_by_posts) { Sunspot::Searching.new(:posts, question.title) }
  let(:search_by_all) { Sunspot::Searching.new(:all, user.first_name) }
  let(:search_by_people_with_many_results) { Sunspot::Searching.new(:people, users.first.first_name) }
  let(:found_user) {{ :people => [user], :search_text => user.first_name }}
  let(:found_question) {{ :posts => [question], :search_text => question.title }}

  context 'produce search' do

    context "with limit option" do

      before do
        double_results.stub(:results).and_return(users)
      end

      %w(1 3 4).each do |limit|

        it "should return #{limit} users" do
          User.stub(:solr_search).and_return(double_results)
          search_by_people_with_many_results.produce_search(:limit => limit)[:people].should have(limit).items
        end

      end

      it "should returm full list of search items" do
        User.stub(:solr_search).and_return(double_results)
        search_by_people_with_many_results.produce_search(:limit => 0)[:people].should have(users.size).items
      end

      it "should returm full list of search items if :limit option does't specified" do
        User.stub(:solr_search).and_return(double_results)
        search_by_people_with_many_results.produce_search()[:people].should have(users.size).items
      end

      it "should return all items even if limit is greater than actual list size" do
        User.stub(:solr_search).and_return(double_results)
        search_by_people_with_many_results.produce_search(:limit => 100)[:people].should have(users.size).items
      end

    end

    context 'return user' do
      before(:each) do
        double_results.stub(:results).and_return([user])
      end

      it 'should return found user' do
        User.stub(:solr_search).and_return(double_results)
        search_by_people.produce_search.should eql(found_user)
      end

      it 'should onec call solr_search' do
        User.should_receive(:solr_search).once.with(no_args()).and_return(double_results)
        search_by_people.produce_search
      end

      it 'should call solr_search for 5 times' do
        User.should_receive(:solr_search).at_most(Constants::Search::MODELS.count).times.with(no_args()).and_return(double_results)
        search_by_all.produce_search
      end
    end

    it 'should respond with no results for query' do
      no_result_found.produce_search.should eql(expected_no_result_found)
    end
  end

  context 'should return post' do
    before(:each) do
      double_results.stub(:results).and_return([question])
    end

    it 'should return found question' do
      Post.stub(:solr_search).and_return(double_results)
      search_by_posts.produce_search.should eql(found_question)
    end
  end

  context 'search_by' do
    it 'should once call search_by' do
      search_by_people.should_receive(:search_by).once.with(:people).and_return([User])
      search_by_people.produce_search
    end
  end

  context 'found?' do
    it 'should return true' do
      double_results.stub(:results).and_return([user])
      User.stub(:solr_search).and_return(double_results)
      search_by_people.produce_search.should be_true
    end

    it 'should return nil' do
      no_result_found.found?.should be_false
    end
  end
end
