require 'spec_helper'

describe LinkedInFetcher::CompanyAPI do
  let(:token) { "some token" }
  let(:secret) { "some secret" }
  let(:company) { FactoryGirl.create(:company) }

  subject { LinkedInFetcher::CompanyAPI.new(token, secret, company.linkedin_id) }

  context "#import_fields!" do
    let(:linkedin_company) {
      "{\"description\":\"description\",\"websiteUrl\":\"http://www.svitla.com/\",\"logoUrl\":\"http://image.png\"}"
    }

    it "should import company_fields" do
      OAuth::AccessToken.any_instance.stub_chain(:get, :body).and_return(linkedin_company)

      subject.import_fields!
      company.reload

      expect(company.description).to eql('description')
      expect(company.website).to eql('http://www.svitla.com/')
      expect(company.linkedin_logo).to eql('http://image.png')
    end
  end

end
