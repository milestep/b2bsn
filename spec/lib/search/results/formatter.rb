require 'spec_helper'

describe Search::Results::Formatter do

  let(:users) do
    users = []
    5.times { users << FactoryGirl.create(:user, :first_name => 'John', :last_name => 'Smith') }
    users
  end
  let(:companies) do
    companies = []
    5.times { companies << FactoryGirl.create(:company, :name => 'John Smith') }
    companies
  end
  let(:formatted_people_results) { users.map { |user| [user.search_title, user.full_search_title] } }
  let(:formatted_companies_results) { companies.map { |company| [comany.search_title, company.full_search_title] } }
  let(:valid_formatted_results_with_people) do
    { :people => formatted_people_results }.to_json
  end

  let(:formatted_people_results_for_autocomplete) { users.map { |user| {:label => user.search_title, :value => user.full_search_title} }}
  let(:valid_search_results_to_autocomplete_format) do
    {
      :people => users,
      :search_text => 'John Smith'
    }
  end
  let(:valid_formatted_results_with_people_for_autocomplete) do
    { :people => formatted_people_results_for_autocomplete }.to_autocompete_json
  end

  let(:valid_formatted_results_with_people_and_companies) do
    { 
      :people => formatted_people_results,
      :companies => formatted_companies_results
    }.to_json
  end
  let(:valid_search_results_to_format) do
    {
      :people => users,
      :search_text => 'John Smith'
    }
  end
  let(:valid_search_results_to_format_with_people_and_company) do
    {
      :people => users,
      :companies => companies,
      :search_text => 'John Smith'
    }
  end
  let(:formatter_class) { Search::Results::Formatter }

  it "should raise SearchError exception" do
    expect{ formatter_class.new }.to raise_error(SearchError)
  end

  it "should not raise SearchError exception" do
    expect(formatter_class.new(valid_search_results_to_format)).not_to raise_error(SearchError)
  end

  it "should return valid formatted results for autocomplete" do
    formatter_class.new(valid_search_results_to_format).to_json.should eq(valid_formatted_results_with_people)
  end

  it "should return valid formatted results for autocomplete in fix_navigation_bar" do
    formatter_class.new(valid_search_results_to_autocomplete_format).to_autocomplete_json.should eq(valid_formatted_results_with_people_for_autocomplete)
  end

  it "should return results if an object has particular abilities" do
    companies.each { |company| company.stub(:respond_to?, :search_title).and_return(false) }
    formatter_class.new(valid_search_results_to_format_with_people_and_company).to_json.should eq(valid_formatted_results_with_people)
  end

  it "should return results if an object has particular abilities" do
    companies.each { |company| company.stub(:respond_to?, :full_search_title).and_return(false) }
    formatter_class.new(valid_search_results_to_format_with_people_and_company).to_json.should eq(valid_formatted_results_with_people)
  end

  it "should return non blank results array" do
    valid_search_results_to_format_with_people_and_company[:companies] = []
    formatter_class.new(valid_search_results_to_format_with_people_and_company).to_json.should eq(valid_formatted_results_with_people)
  end


  it "should return valid formatted results for autocomplete" do
    formatter_class.new(valid_search_results_to_format).to_json.should_not include("search_text")
  end

  it "should return valid formatted results for autocomplete in fix_navigation_bar" do
    formatter_class.new(valid_search_results_to_autocomplete_format).to_autocomplete_json.should_not include("search_text")
  end
end
