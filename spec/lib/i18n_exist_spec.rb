require "spec_helper"
require "i18n_exist"

describe I18n do

  context "#I18n_exist" do

    it "should have translation in en.yml" do
			key1 = "models.circle"
      I18n.exist?(key1).should be_true
    end

		it "should not have translation missing text" do
			key2 = "models.superman"
			I18n.exist?(key2).should be_false
		end

  end
end
