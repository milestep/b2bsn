require 'spec_helper'

describe ApiClient::Linkedin do
  let(:user_profile) { FactoryGirl.create(:user_profile) }
  let(:token) { "some token" }
  let(:secret) { "some secret" }
  let(:double_client) { double('Linkedin::Client') }

  subject { ApiClient::Linkedin.new(user_profile, token, secret) }

  before(:each) do
    subject.stub(:client).and_return(double_client)
  end

  context "#import_experiences!" do
    let(:experiences_data) {
      {
        'positions' => {
        'all' => [
                    {
                      'company'    => { 'name' => 'Google' },
                      'title'      => 'ROR developer',
                      'summary'    => 'greate ruby on rails skills',
                      'start_date' => { 'year' => '2008', 'month' => '10' },
                      'end_date'   => { 'year' => '2009', 'month' => '10' },
                      'id'         => 'fDOzSsDcZ7'
                    },
                    {
                      'company'    => { 'name' => 'Yahoo' },
                      'title'      => 'Java',
                      'summary'    => 'java skills',
                      'start_date' => { 'year' => '2010', 'month' => '10' },
                      'end_date'   => { 'year' => '2011', 'month' => '10' },
                      'id'         => 'aFOStDrr7'
                    }
                 ]
        }
      }
    }

    let(:empty_experiences_data) { { 'positions' => {'all' => nil} } }

    it "should import experiences" do
      double_client.stub(:profile) { experiences_data }

      subject.import_experiences!
      user_profile.reload

      user_profile.experiences.should have(2).items
      user_profile.experiences.find_by_eid('fDOzSsDcZ7').should_not be_nil
      user_profile.experiences.find_by_eid('aFOStDrr7').should_not be_nil
    end

    it "should not fail if there is no linkedin data" do
      double_client.stub(:profile) { {} }

      expect { subject.import_experiences! }.to_not raise_error
      user_profile.reload
      user_profile.experiences.should be_empty
    end

    it "should not fail if the linkedin data is empty" do
      double_client.stub(:profile) { empty_experiences_data }

      expect { subject.import_experiences! }.to_not raise_error
      user_profile.reload
      user_profile.experiences.should be_empty
    end

  end

  context "#import_skills!" do
    let(:skills_data) {
      {
        'skills' => {
        'all' => [
                    { 'skill' => { 'name' => 'ROR' } },
                    { 'skill' => { 'name' => 'SQL' } },
                    { 'skill' => { 'name' => 'VIM' } }
                 ]
        }
      }
    }

    before do
      double_client.stub(:profile) { skills_data }
    end

    it "should import skills" do
      subject.import_skills!
      user_profile.reload

      user_profile.skills.should have(3).items
    end

    it "should not import existing skills" do
      user_profile.skills << FactoryGirl.create(:skill, :name => 'SQL')
      subject.import_skills!
      user_profile.reload

      user_profile.skills.should have(3).items
    end
  end

  context "#beanstock_connections" do
    let(:uid) { 'XBgfDcTG' }

    it "should find beanstock users" do
      user = FactoryGirl.create(:user, :uid => uid)

      mock_connection = double()
      mock_connection.stub(:id).and_return(uid)

      subject.stub(:connections) { [mock_connection] }

      subject.beanstock_connections.should have(1).item
      subject.beanstock_connections.first.should eq(user)
    end
  end

  context "#beanstock_connections_for" do
    let!(:user) { FactoryGirl.create(:user) }
    let!(:friend) { FactoryGirl.create(:user)}
    let!(:connection) { FactoryGirl.create(:connection, :user => user, :friend => friend, :status => "accepted") }
    it "should return empty list of users, because beanstock user already is friend of current user" do

      mock_connection = double()
      mock_connection.stub(:id).and_return([friend.uid])
      subject.stub(:connections) { [mock_connection] }

      subject.beanstock_connections_for(user).should be_empty
    end
  end

end
