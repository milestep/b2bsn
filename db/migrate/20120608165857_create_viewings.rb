class CreateViewings < ActiveRecord::Migration
  def change
    create_table :viewings do |t|
      t.references :viewable, :polymorphic => true
      t.references :user
      t.timestamps
    end

    add_index :viewings, :viewable_type
    add_index :viewings, [:viewable_id, :viewable_type, :user_id]
    add_index :viewings, :user_id
  end
end
