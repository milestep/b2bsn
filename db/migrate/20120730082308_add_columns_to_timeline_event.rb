class AddColumnsToTimelineEvent < ActiveRecord::Migration
  def change
    add_column :timeline_events, :type, :string
    add_column :timeline_events, :fields_changes, :text
  end
end