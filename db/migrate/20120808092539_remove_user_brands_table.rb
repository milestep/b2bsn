class RemoveUserBrandsTable < ActiveRecord::Migration
  def up
    begin drop_table :user_brands rescue true end
  end

  def down
    #this table is unnecessary
  end
end
