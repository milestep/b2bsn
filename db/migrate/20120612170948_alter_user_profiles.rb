class AlterUserProfiles < ActiveRecord::Migration
  def change
    remove_column :user_profiles, :visibility #integer
    add_column  :user_profiles, :visibility, :string, default: 'public' #string
  end
end
