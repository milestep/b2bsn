class CreateCompanyAndUserTables < ActiveRecord::Migration
  def change
    
    create_table :addresses do |t|
      t.string :street
      t.string :street2
      t.string :city
      t.string :state_abbrev, :limit => 5
      t.string :postal_code, :limit => 12
      t.string :country_code, :limit => 2
      t.timestamps
    end

    create_table :phones do |t|
      t.integer :address_id
      t.string :phone_type
      t.string :number
      t.timestamps
    end
    add_foreign_key(:phones, :addresses)

    create_table :companies do |t|
      t.string :name
      t.string :type
      t.string :logo
      t.text :description
      t.timestamps
    end

    create_table :company_locations do |t|
      t.string :name
      t.integer :company_id
      t.integer :address_id
      t.timestamps
    end
    add_foreign_key(:company_locations, :addresses)
    add_foreign_key(:company_locations, :companies)
    
    create_table :user_profiles do |t|
      t.integer :user_id
      t.integer :company_id
      t.integer :address_id
      t.string :title
      t.string :my_status
      t.string :avatar
      t.text :description
      t.timestamps
    end
    add_foreign_key(:user_profiles, :users)
    add_foreign_key(:user_profiles, :companies)
    add_foreign_key(:user_profiles, :addresses)
    
    create_table :contacts do |t|
      t.string :first_name
      t.string :last_name
      t.string :email
      t.string :title
      t.integer :company_id
      t.integer :address_id
      t.integer :user_id
      t.timestamps
    end
    add_foreign_key(:contacts, :users)
    add_foreign_key(:contacts, :companies)
    add_foreign_key(:contacts, :addresses)
    
    create_table :organizations do |t|
      t.string :name
      t.string :org_type
      t.string :logo
      t.text :description
      t.integer :address_id
      t.timestamps
    end
    add_foreign_key(:organizations, :addresses)
    
    create_table :memberships do |t|
      t.integer :user_id
      t.integer :organization_id
      t.string :status
      t.timestamps
    end
    add_foreign_key(:memberships, :users)
    add_foreign_key(:memberships, :organizations)
    
    create_table :brands do |t|
      t.string :name
      t.string :logo
      t.string :sector
      t.text :description
      t.timestamps
    end
    
    create_table :connections do |t|
      t.integer :user_id
      t.integer :friend_id
      t.string :status
      t.datetime :responded_at
      t.timestamps
    end
    add_foreign_key(:connections, :users)
    add_foreign_key(:connections, :users, :column => 'friend_id')
    
    create_table :circles do |t|
      t.integer :user_id
      t.string :name
      t.timestamps
    end
    add_foreign_key(:circles, :users)
    
    create_table :company_brands do |t|
      t.integer :company_id
      t.integer :brand_id
      t.string :relationship
      t.timestamps
    end
    add_foreign_key(:company_brands, :companies)
    add_foreign_key(:company_brands, :brands)
    
    create_table :user_brands do |t|
      t.integer :user_id
      t.integer :brand_id
      t.string :relationship
      t.timestamps
    end
    add_foreign_key(:user_brands, :users)
    add_foreign_key(:user_brands, :brands)
    
    create_table :circles_connections, :id => false do |t|
      t.integer :circle_id
      t.integer :connection_id
    end
    add_foreign_key(:circles_connections, :circles)
    add_foreign_key(:circles_connections, :connections)
    
  end
end
