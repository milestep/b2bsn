class CreateNotices < ActiveRecord::Migration
  def change
    create_table :users_notices do |t|
      t.references :user
      t.references :timeline_event
      t.boolean :unread, :default => true
      t.timestamps
    end

    add_index :users_notices, [:timeline_event_id, :user_id]
    add_index :users_notices, [:user_id, :timeline_event_id]
  end

end
