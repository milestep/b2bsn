class ChangeExperienceDates < ActiveRecord::Migration
  def up
    add_column :experiences, :start_date, :date
    add_column :experiences, :end_date, :date
    remove_column :experiences, :time_period
  end

  def down
    add_column :experiences, :time_period, :string
    remove_column :experiences, :start_date
    remove_column :experiences, :end_date
  end
end