class CreateAuthorizations < ActiveRecord::Migration
  def change
    create_table :authorizations do |t|
      t.integer :user_id
      t.string :provider
      t.boolean :posting_default
      t.string :username
      t.string :uid
      t.string :oauth_token
      t.string :oauth_token_secret

      t.timestamps
    end
    add_index :authorizations,  [:user_id, :provider]
    add_index :authorizations,  [:user_id, :posting_default]
  end
end
