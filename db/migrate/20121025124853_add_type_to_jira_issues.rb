class AddTypeToJiraIssues < ActiveRecord::Migration
  def change
    add_column :jira_issues, :type, :string, :default => 'FeatureRequest'
  end
end
