class CreateAgencyAdvertisers < ActiveRecord::Migration
  def change
    create_table :agency_advertisers do |t|
      t.integer :agency_id
      t.integer :advertiser_id
    end
  end
end
