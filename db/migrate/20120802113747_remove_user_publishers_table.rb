class RemoveUserPublishersTable < ActiveRecord::Migration
  def up
    begin drop_table :user_publishers rescue true end
  end

  def down
    #this table is unnecessary
  end
end
