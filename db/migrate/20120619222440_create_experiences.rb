class CreateExperiences < ActiveRecord::Migration
  def change
    create_table :experiences do |t|
      t.integer :user_profile_id
      t.string :company
      t.string :position
      t.string :time_period
      t.timestamps
    end
  end
end
