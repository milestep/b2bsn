class AddPackagesCountToPublisher < ActiveRecord::Migration
  def up
    add_column :companies, :packages_count, :integer, :default => 0

    Publisher.reset_column_information
    Publisher.find_each do |u|
      Publisher.reset_counters u.id, :packages
    end
  end

  def down
    remove_column :companies, :packages_count
  end
end
