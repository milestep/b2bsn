class RemoveColumnTitleFromCommentTable < ActiveRecord::Migration
  def self.up
    remove_column :comments, :title
  end

  def self.down
    add_column :coments, :title, :string
  end
end
