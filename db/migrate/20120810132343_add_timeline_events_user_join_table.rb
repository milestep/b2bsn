class AddTimelineEventsUserJoinTable < ActiveRecord::Migration
  def change
    create_table :timeline_events_user_shares, :id => false do |t|
      t.integer :timeline_event_id
      t.integer :user_id
    end
  end
end
