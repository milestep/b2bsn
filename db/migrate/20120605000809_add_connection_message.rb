class AddConnectionMessage < ActiveRecord::Migration
  def up
    add_column :connections, :message, :text
  end

  def down
    remove_column :connections, :message
  end
end