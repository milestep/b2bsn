class AddVoteCacheToNewsItems < ActiveRecord::Migration
  def change
    change_table :news_items do |t|
      t.integer :cached_votes_total, :default => 0
      t.integer :cached_votes_up, :default => 0
      t.integer :cached_votes_down, :default => 0
      t.integer :clicks_count, :default => 0
    end
  end
end
