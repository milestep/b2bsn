class CreateNewsFeeds < ActiveRecord::Migration
  def change
    create_table :news_feeds do |t|
      t.string :title
      t.string :description
      t.string :url
      t.datetime :last_updated
      t.string :copyright
      t.string :image
      t.boolean :active
      t.timestamps
    end
    
    create_table :news_items do |t|
      t.string :title
      t.integer :news_feed_id
      t.string :permalink
      t.string :description
      t.string :url
      t.datetime :date_published
      t.string :copyright
      t.string :author
      t.text :content
      t.timestamps
    end
    add_foreign_key(:news_items, :news_feeds)
    
  end
end
