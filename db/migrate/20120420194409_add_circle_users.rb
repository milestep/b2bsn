class AddCircleUsers < ActiveRecord::Migration
  def change
    drop_table :circles_connections
    create_table :circles_users, :id => false do |t|
      t.integer :circle_id
      t.integer :user_id
    end
    add_foreign_key(:circles_users, :circles)
    add_foreign_key(:circles_users, :users)
  end
end
