class AddDescriptionToExperiences < ActiveRecord::Migration
  def change
    add_column :experiences, :description, :text
    add_column :experiences, :location, :string
    add_column :experiences, :eid, :string
  end
end
