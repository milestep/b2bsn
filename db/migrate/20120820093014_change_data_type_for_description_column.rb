class ChangeDataTypeForDescriptionColumn < ActiveRecord::Migration
  change_table :news_items do |t|
    t.change :description, :text
  end
end
