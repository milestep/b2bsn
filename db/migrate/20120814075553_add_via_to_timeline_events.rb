class AddViaToTimelineEvents < ActiveRecord::Migration
  def change
    add_column :timeline_events, :via, :string
  end
end
