class AddCircleDescription < ActiveRecord::Migration
  def up
    add_column :circles, :description, :string
  end

  def down
    remove_column :circles, :description
  end
end
