class AddDeletedAtToUsers < ActiveRecord::Migration
  def change
    remove_foreign_key(:circles_users, :circles)
    remove_foreign_key(:circles_users, :users)

    add_column :users, :deleted_at, :datetime
  end
end
