class ChangeRelationshipBetweenCompanyAndExperience < ActiveRecord::Migration
  def change
    add_column :experiences, :company_id, :integer
    add_column :companies, :linkedin_id, :integer
    add_column :companies, :linkedin_logo, :string
    rename_column :experiences, :company, :company_name
  end
end
