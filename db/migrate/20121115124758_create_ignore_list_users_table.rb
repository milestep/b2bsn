class CreateIgnoreListUsersTable < ActiveRecord::Migration
  def change
    create_table(:ignore_list_users) do |t|
      t.integer :user_id
      t.integer :ignored_user_id
      t.date :stop_ignoring_at
      t.timestamps
    end
  end
end
