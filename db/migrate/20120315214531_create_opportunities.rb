class CreateOpportunities < ActiveRecord::Migration
  def change
    create_table :opportunities do |t|
      t.integer :user_id
      t.string :name
      t.date :start_date
      t.date :end_date
      t.date :close_date
      t.decimal :cpm
      t.integer :impressions
      t.string :creative_types
      t.string :account_name
      t.string :probability
      t.decimal :budget
      t.string :stage
      t.text :notes
      t.text :demo_description
      t.text :demo_messages
      t.timestamps
    end
    add_foreign_key(:opportunities, :users)
  end
end
