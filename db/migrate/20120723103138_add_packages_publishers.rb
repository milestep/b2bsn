class AddPackagesPublishers < ActiveRecord::Migration
  def change
    create_table :package_publishers, :id => false do |t|
      t.integer :package_id
      t.integer :publisher_id
    end
  end
end
