# This migration comes from mailboxer_engine (originally 20120813110712)
class RenameReceiptsRead < ActiveRecord::Migration
  def change
    rename_column :receipts, :read, :is_read
  end
end
