class CreateSocialNetworks < ActiveRecord::Migration
  def change
    create_table :social_networks, {:id => false, primary_key: :provider}do |t|
      t.string :provider, null: false     #twitter, linkedin
      t.string :name                      #Twitter, LinkedIn
      t.string :consumer_key, default: 'unspecified'
      t.string :consumer_secret, default: 'unspecified'
      t.timestamps
    end
    #execute "ALTER TABLE social_networks ADD PRIMARY KEY (provider);"

  end
end
