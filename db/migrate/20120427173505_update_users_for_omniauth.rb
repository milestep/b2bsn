class UpdateUsersForOmniauth < ActiveRecord::Migration
  def up
    change_table :users do |t|
      t.string :provider, :uid
      t.index [:provider, :uid]
    end
    remove_index :users, :email
  end
  
  def down
    remove_index :users, [:provider, :uid]
    remove_column :users, :provider, :uid
    add_index :users, :email, :unique => true
  end
end