class CreateUserProfileSkills < ActiveRecord::Migration
  def change
    create_table :skills_user_profiles, :id => false do |t|
      t.integer :user_profile_id
      t.integer :skill_id
      t.timestamps
    end
  end
end
