class AddPackagableTypeAndPackagableIdToPackages < ActiveRecord::Migration
  def change
    change_table :packages do |t|
      t.belongs_to :packagable, :polymorphic => true
      t.remove :user_id
    end
    add_index :packages, [:packagable_id, :packagable_type]
  end
end
