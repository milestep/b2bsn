class CreatePackages < ActiveRecord::Migration
  def change
    create_table :packages do |t|
      t.string :name
      t.text :description
      t.date :flight_start_date
      t.date :flight_end_date
      t.boolean :dma_targeting_available
      t.boolean :demo_targeting_available
      t.string :brand_restriction
      t.float :total_cost
      t.integer :total_impressions
      t.float :cpm
      t.references :user

      t.timestamps
    end
    add_index :packages, :user_id
  end
end
