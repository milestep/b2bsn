class AddFeaturedColumnToUserProfiles < ActiveRecord::Migration
  def change
    change_table :user_profiles do |t|
      t.boolean :featured, :default => false
    end
  end
end