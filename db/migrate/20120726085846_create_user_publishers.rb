class CreateUserPublishers < ActiveRecord::Migration
  def change
    create_table :user_publishers do |t|
      t.integer :user_id
      t.integer :publisher_id
      t.string :relationship
      t.timestamps
    end
  end
end
