class AddMoreFieldsToCompanies < ActiveRecord::Migration
  def change
    add_column :companies, :phone, :string
    add_column :companies, :email, :string
    add_column :companies, :website, :string
    add_column :companies, :number_of_employees, :integer
    add_column :companies, :annual_revenue, :string
  end

end
