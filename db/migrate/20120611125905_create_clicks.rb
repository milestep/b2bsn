class CreateClicks < ActiveRecord::Migration
  def change
    create_table :clicks do |t|
      t.string :url
      t.references :clickable, :polymorphic => true
      t.references :user
      t.timestamps
    end

    add_index :clicks, :clickable_type
    add_index :clicks, [:clickable_id, :clickable_type, :user_id]
    add_index :clicks, :user_id
    
    add_column :posts, :clicks_count, :integer, :default => 0
  end
end
