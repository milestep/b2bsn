class AddRoleIdToPostsToInvitationsToBadges < ActiveRecord::Migration
  def change
    add_column :posts, :role_id, :integer
    add_column :invitations, :role_id, :integer
    add_column :badges, :role_id, :integer

    Rake::Task['add_roles:create'].invoke
    Rake::Task['add_role_id_data:create'].invoke
    remove_column :invitations, :invite_role
    remove_column :badges, :role
    remove_column :users, :role
  end
end
