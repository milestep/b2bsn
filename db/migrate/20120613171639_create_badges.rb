class CreateBadges < ActiveRecord::Migration
  def change
    create_table :badges do |t|
      t.string  :name
      t.text    :description
      t.string  :role
      t.boolean :active
      t.string  :icon
      t.string  :icon_disabled
      t.timestamps
    end
    
    create_table :user_badges do |t|
      t.integer :badge_id
      t.integer :user_id
      t.integer :percent_complete
      t.datetime :completed_at 
    end
  end
end
