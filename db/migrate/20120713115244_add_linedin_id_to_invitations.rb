class AddLinedinIdToInvitations < ActiveRecord::Migration
  def change
    add_column :invitations, :linkedin_id, :string
  end
end
