class DeleteJabberCredentials < ActiveRecord::Migration
  def up
    drop_table :jabber_credentials
  end

  def down
    create_table :jabber_credentials
  end
end
