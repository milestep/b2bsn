class AddVisibilityToUserProfiles < ActiveRecord::Migration
  def change
    # defaults to full profile visibility.
    add_column :user_profiles, :visibility, :integer, {null:  false, default:  0x7}
  end
end
