class CreateTimelineEventUserDeletions < ActiveRecord::Migration
  def up
    create_table :timeline_event_user_deletions do |t|
      t.integer :timeline_event_id
      t.integer :user_id
    end
  end
end
