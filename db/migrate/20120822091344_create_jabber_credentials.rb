class CreateJabberCredentials < ActiveRecord::Migration
  def change
    create_table :jabber_credentials do |t|
      t.string :jabber_id
      t.string :jabber_password
      t.references :user

      t.timestamps
    end
    add_index :jabber_credentials, :user_id
  end
end
