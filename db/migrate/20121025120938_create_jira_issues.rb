class CreateJiraIssues < ActiveRecord::Migration
  def change
    create_table :jira_issues do |t|
      t.string :title
      t.text :description
      t.timestamp :sent_at

      t.timestamps
    end
  end
end
