class AddUserIdToJiraIssues < ActiveRecord::Migration
  def change
    add_column :jira_issues, :user_id, :integer
  end
end
