class CreatePostsUsers < ActiveRecord::Migration
  def change
    create_table :posts_users do |t|
      t.references :user
      t.references :post 
      t.timestamps
    end

    add_index :posts_users, [:post_id, :user_id]
    add_index :posts_users, [:user_id, :post_id]
  end
end
