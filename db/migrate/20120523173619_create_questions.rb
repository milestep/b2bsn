class CreateQuestions < ActiveRecord::Migration
  def change
    create_table :questions do |t|
      t.integer :user_id
      t.string :question
      t.text :description
      t.timestamps
    end
    add_foreign_key(:questions, :users)
  end
end
