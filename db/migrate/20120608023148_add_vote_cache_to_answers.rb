class AddVoteCacheToAnswers < ActiveRecord::Migration
  def change
    change_table :answers do |t|
      t.integer :cached_votes_total, :default => 0
      t.integer :cached_votes_up, :default => 0
      t.integer :cached_votes_down, :default => 0
    end
  end
end
