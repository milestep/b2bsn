class ConvertQuestionsToPosts < ActiveRecord::Migration
  def up
    Answer.delete_all
    TimelineEvent.delete_all
    remove_foreign_key(:answers, :questions)
    remove_column :answers, :question_id
    add_column :answers, :post_id, :integer
    add_foreign_key(:answers, :posts)
    drop_table :questions
    remove_column :user_profiles, :my_status
  end

  def down
  end
end
