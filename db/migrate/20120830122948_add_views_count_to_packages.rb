class AddViewsCountToPackages < ActiveRecord::Migration
  def change
    add_column :packages, :views_count, :integer, :default => 0
  end
end
