class AddOwnerToCompanies < ActiveRecord::Migration
  def change
    add_column :companies, :owner_id, :integer
    add_column :companies, :owner_type, :string
  end
end
