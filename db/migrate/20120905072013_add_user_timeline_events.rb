class AddUserTimelineEvents < ActiveRecord::Migration
  def up
    drop_table :timeline_event_user_deletions
    create_table :user_timeline_events do |t|
      t.integer :user_id
      t.integer :timeline_event_id
    end
    add_index :user_timeline_events, [:user_id, :timeline_event_id], :unique => true
    User.all.each do |user|
      user.timeline_events << TimelineEvent.for_user(user)
    end
  end

  def down
    create_table :timeline_event_user_deletions do |t|
      t.integer :timeline_event_id
      t.integer :user_id
    end
    drop_table :user_timeline_events
  end
end
