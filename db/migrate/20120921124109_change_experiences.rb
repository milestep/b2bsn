class ChangeExperiences < ActiveRecord::Migration
  def change
    add_column :experiences, :company_logo, :string
    add_column :experiences, :company_title, :string
  end
end
