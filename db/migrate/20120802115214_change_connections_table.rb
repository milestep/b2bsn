require "migration_helpers"
class ChangeConnectionsTable < ActiveRecord::Migration
  include MigrationHelpers

  def up
    drop_fk :connections, :friend_id
    rename_column :connections, :friend_id, :connectable_id
    add_column :connections, :connectable_type, :string
    Connection.reset_column_information
    Connection.update_all(connectable_type: 'User')
  end

  def down
    remove_column :connections, :connectable_type
    rename_column :connections, :connectable_id, :friend_id
    fk :connections, :friend_id, :users
  end
end
