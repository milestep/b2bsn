class AddSecondarySubjectFieldsIfTheyDontExists < ActiveRecord::Migration
  def up
    add_column(:timeline_events, :secondary_subject_id, :integer) unless column_exists?(:timeline_events, :secondary_subject_id)
    add_column(:timeline_events, :secondary_subject_type, :string) unless column_exists?(:timeline_events, :secondary_subject_type)
  end

  def down
    remove_columns(:timeline_events, :secondary_subject_id, :secondary_subject_type)
  end

end
