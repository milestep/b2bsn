class UpdatesToUserProfile < ActiveRecord::Migration
  def up
    add_column :user_profiles, :timezone, :string
    add_column :user_profiles, :twitter_username, :string
  end

  def down
    remove_column :user_profiles, :twitter_username
    remove_column :user_profiles, :timezone
  end
end