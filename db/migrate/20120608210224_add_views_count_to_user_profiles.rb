class AddViewsCountToUserProfiles < ActiveRecord::Migration
  def change
    add_column :user_profiles, :views_count, :integer, :default => 0
  end
end