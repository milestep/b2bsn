class CreateBlackListUsers < ActiveRecord::Migration
  def change
    create_table :black_list_users do |t|
      t.integer :user_id
      t.integer :blocked_user_id
    end
  end
end
