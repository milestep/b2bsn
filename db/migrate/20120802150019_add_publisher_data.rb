class AddPublisherData < ActiveRecord::Migration
  def change
    add_column :companies, :monthly_uniques, :string
    add_column :companies, :monthly_impressions, :string
    add_column :companies, :monthly_page_views, :string
    create_table :ad_types do |t|
      t.string :name
      t.integer :ad_type_group_id
      t.timestamps
    end
    create_table :ad_type_groups do |t|
      t.string :name
      t.timestamps
    end
    create_table :company_ad_types do |t|
      t.integer :company_id
      t.integer :ad_type_id
    end
    create_table :audiences do |t|
      t.string :name
      t.timestamps
    end
    create_table :company_audiences do |t|
      t.integer :company_id
      t.integer :audience_id
    end
    create_table :categories do |t|
      t.string :name
      t.timestamps
    end
    create_table :company_categories do |t|
      t.integer :company_id
      t.integer :category_id
    end
    create_table :networks do |t|
      t.string :name
      t.timestamps
    end
    create_table :company_networks do |t|
      t.integer :company_id
      t.integer :network_id
    end
    create_table :sites do |t|
      t.string :name
      t.timestamps
    end
    create_table :company_sites do |t|
      t.integer :company_id
      t.integer :site_id
    end
  end
end
