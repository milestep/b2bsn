class CreateBrandAdvertisers < ActiveRecord::Migration
  def up
    create_table :brand_advertisers do |t|
      t.integer :brand_id
      t.integer :advertiser_id
    end
  end

  def down
    drop_table :brand_advertisers
  end
end
