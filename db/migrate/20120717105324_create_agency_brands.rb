class CreateAgencyBrands < ActiveRecord::Migration
  def change
    create_table :agency_brands do |t|
      t.integer :agency_id
      t.integer :brand_id
    end
  end
end
