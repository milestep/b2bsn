class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.integer :user_id
      t.string :post_type
      t.string :title
      t.text :description
      t.string :url
      t.integer :comments_count, default: 0
      t.integer :shares_count, default: 0
      t.integer :views_count, default: 0
      t.integer :cached_votes_total, :default => 0
      t.integer :cached_votes_up, :default => 0
      t.integer :cached_votes_down, :default => 0
      t.string :who_with
      t.string :who_asking
      t.timestamps
    end
  end
end
