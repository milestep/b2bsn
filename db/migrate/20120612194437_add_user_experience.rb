class AddUserExperience < ActiveRecord::Migration
  def change
    change_table :user_profiles do |t|
      t.text :experience
    end
  end
end
