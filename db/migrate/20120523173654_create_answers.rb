class CreateAnswers < ActiveRecord::Migration
  def change
    create_table :answers do |t|
      t.integer :user_id
      t.integer :question_id
      t.text :answer
      t.timestamps
    end
    add_foreign_key(:answers, :users)
    add_foreign_key(:answers, :questions)
  end
end
