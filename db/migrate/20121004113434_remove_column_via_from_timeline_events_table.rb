class RemoveColumnViaFromTimelineEventsTable < ActiveRecord::Migration
   def self.up
    remove_column :timeline_events, :via
  end

  def self.down
    add_column :timeline_events, :via, :string
  end
end
