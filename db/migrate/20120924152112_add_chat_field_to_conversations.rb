class AddChatFieldToConversations < ActiveRecord::Migration
  def change
    add_column :conversations, :chat, :boolean, :default => false
  end
end
