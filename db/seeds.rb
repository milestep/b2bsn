# Populate social_networks table, idempotent
['LinkedIn', 'Twitter', 'Facebook','foursquare', 'Pinterest'].each do |social|
    s = SocialNetwork.create(
      name: social,
      provider: social.downcase)
end
SocialNetwork.create(name: 'Google+', provider: 'google')

# Create roles
AddRoles.create

# Initial Badges
role = Role.seller

Badge.create(name: 'Early Adopter Achievement', description: '', active: true, role_id: role, remote_icon_url: "http://share.bellacrest.com/bsm/badges/badge_early_adopter.png", remote_icon_disabled_url: "http://share.bellacrest.com/bsm/badges/badge_early_adopterOFF.png")
Badge.create(name: 'Auto Expertise', description: '', active: true, role_id: role, remote_icon_url: "http://share.bellacrest.com/bsm/badges/badge_expertise_auto.png", remote_icon_disabled_url: "http://share.bellacrest.com/bsm/badges/badge_expertise_autoOFF.png")
Badge.create(name: 'Sports Expertise', description: '', active: true, role_id: role, remote_icon_url: "http://share.bellacrest.com/bsm/badges/badge_expertise_sports.png", remote_icon_disabled_url: "http://share.bellacrest.com/bsm/badges/badge_expertise_sportsOFF.png")
Badge.create(name: 'Travel Expertise', description: '', active: true, role_id: role, remote_icon_url: "http://share.bellacrest.com/bsm/badges/badge_expertise_travel.png", remote_icon_disabled_url: "http://share.bellacrest.com/bsm/badges/badge_expertise_travelOFF.png")
Badge.create(name: 'Featured Writer', description: '', active: true, role_id: role, remote_icon_url: "http://share.bellacrest.com/bsm/badges/badge_featuredwriter.png", remote_icon_disabled_url: "http://share.bellacrest.com/bsm/badges/badge_featuredwriterOFF.png")
Badge.create(name: 'Key Organizer', description: '', active: true, role_id: role, remote_icon_url: "http://share.bellacrest.com/bsm/badges/badge_key_organizer.png", remote_icon_disabled_url: "http://share.bellacrest.com/bsm/badges/badge_key_organizerOFF.png")
Badge.create(name: 'Key Speaker', description: '', active: true, role_id: role, remote_icon_url: "http://share.bellacrest.com/bsm/badges/badge_key_speaker.png", remote_icon_disabled_url: "http://share.bellacrest.com/bsm/badges/badge_key_speakerOFF.png")
Badge.create(name: 'Margin King', description: '', active: true, role_id: role, remote_icon_url: "http://share.bellacrest.com/bsm/badges/badge_margin_king.png", remote_icon_disabled_url: "http://share.bellacrest.com/bsm/badges/badge_margin_kingOFF.png")
Badge.create(name: 'Party Animal', description: '', active: true, role_id: role, remote_icon_url: "http://share.bellacrest.com/bsm/badges/badge_partyanimal.png", remote_icon_disabled_url: "http://share.bellacrest.com/bsm/badges/badge_partyanimalOFF.png")
Badge.create(name: 'Power Seller', description: '', active: true, role_id: role, remote_icon_url: "http://share.bellacrest.com/bsm/badges/badge_power_seller.png", remote_icon_disabled_url: "http://share.bellacrest.com/bsm/badges/badge_power_sellerOFF.png")
Badge.create(name: 'Publisher Companion', description: '', active: true, role_id: role, remote_icon_url: "http://share.bellacrest.com/bsm/badges/badge_publisher_companion.png", remote_icon_disabled_url: "http://share.bellacrest.com/bsm/badges/badge_publisher_companionOFF.png")
Badge.create(name: 'Bronze Seller', description: '', active: true, role_id: role, remote_icon_url: "http://share.bellacrest.com/bsm/badges/badge_seller_bronze.png", remote_icon_disabled_url: "http://share.bellacrest.com/bsm/badges/badge_seller_bronzeOFF.png")
Badge.create(name: 'Gold Seller', description: '', active: true, role_id: role, remote_icon_url: "http://share.bellacrest.com/bsm/badges/badge_seller_gold.png", remote_icon_disabled_url: "http://share.bellacrest.com/bsm/badges/badge_seller_goldOFF.png")
Badge.create(name: 'Silver Seller', description: '', active: true, role_id: role, remote_icon_url: "http://share.bellacrest.com/bsm/badges/badge_seller_silver.png", remote_icon_disabled_url: "http://share.bellacrest.com/bsm/badges/badge_seller_silverOFF.png")
Badge.create(name: 'Social Butterfly', description: '', active: true, role_id: role, remote_icon_url: "http://share.bellacrest.com/bsm/badges/badge_socialbutterfly.png", remote_icon_disabled_url: "http://share.bellacrest.com/bsm/badges/badge_socialbutterflyOFF.png")
Badge.create(name: 'Validated Seller', description: '', active: true, role_id: role, remote_icon_url: "http://share.bellacrest.com/bsm/badges/badge_validatedseller.png", remote_icon_disabled_url: "http://share.bellacrest.com/bsm/badges/badge_validatedsellerOFF.png")


# First batch of invitations
Invitation.generate_tokens(role, 20)

# News
NewsFeed.create(:url => 'http://feeds.mashable.com/Mashable',
                :title => 'Mashable',
                :remote_image_url => 'http://a1.mzstatic.com/us/r1000/075/Purple/fb/54/bc/mzl.aptkwezb.175x175-75.jpg',
                :active => true)

NewsFeed.create(:url => 'http://feeds.feedburner.com/TechCrunch/',
                :title => 'TechCrunch',
                :remote_image_url => 'http://tctechcrunch2011.wordpress.com/wp-content/themes/vip/tctechcrunch2/images/logos/green.png',
                :active => true)

NewsFeed.create(:url => 'http://allthingsd.com/feed/',
                :title => 'AllThingsD',
                :remote_image_url => 'http://allthingsd.com/wp-content/themes/atd-2.0/img/logo-footer.png',
                :active => true)

NewsFeed.create(:url => 'http://feeds.feedburner.com/businessinsider?format=xml',
                :title => 'Business Insider',
                :remote_image_url => 'http://a5.mzstatic.com/us/r1000/091/Purple/2e/8e/1e/mzl.hilomsfr.175x175-75.jpg',
                :active => true)

NewsFeed.create(:url => 'http://feeds.paidcontent.org/pcorg/',
                :title => 'paidContent',
                :remote_image_url => 'http://profile.ak.fbcdn.net/hprofile-ak-snc4/50514_99557207225_1608547_q.jpg',
                :active => true)
                
                
NewsFeed.process_all_feeds

AdTypeData.create
