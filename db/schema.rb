# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20121115124758) do

  create_table "active_admin_comments", :force => true do |t|
    t.string   "resource_id",   :null => false
    t.string   "resource_type", :null => false
    t.integer  "author_id"
    t.string   "author_type"
    t.text     "body"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
    t.string   "namespace"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], :name => "index_active_admin_comments_on_author_type_and_author_id"
  add_index "active_admin_comments", ["namespace"], :name => "index_active_admin_comments_on_namespace"
  add_index "active_admin_comments", ["resource_type", "resource_id"], :name => "index_admin_notes_on_resource_type_and_resource_id"

  create_table "ad_type_groups", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "ad_types", :force => true do |t|
    t.string   "name"
    t.integer  "ad_type_group_id"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  create_table "addresses", :force => true do |t|
    t.string   "street"
    t.string   "street2"
    t.string   "city"
    t.string   "state_abbrev", :limit => 5
    t.string   "postal_code",  :limit => 12
    t.string   "country_code", :limit => 2
    t.datetime "created_at",                 :null => false
    t.datetime "updated_at",                 :null => false
  end

  create_table "admin_users", :force => true do |t|
    t.string   "email",                  :default => "", :null => false
    t.string   "encrypted_password",     :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
  end

  add_index "admin_users", ["email"], :name => "index_admin_users_on_email", :unique => true
  add_index "admin_users", ["reset_password_token"], :name => "index_admin_users_on_reset_password_token", :unique => true

  create_table "agency_advertisers", :force => true do |t|
    t.integer "agency_id"
    t.integer "advertiser_id"
  end

  create_table "agency_brands", :force => true do |t|
    t.integer "agency_id"
    t.integer "brand_id"
  end

  create_table "answers", :force => true do |t|
    t.integer  "user_id"
    t.text     "answer"
    t.datetime "created_at",                        :null => false
    t.datetime "updated_at",                        :null => false
    t.integer  "post_id"
    t.integer  "cached_votes_total", :default => 0
    t.integer  "cached_votes_up",    :default => 0
    t.integer  "cached_votes_down",  :default => 0
  end

  add_index "answers", ["post_id"], :name => "answers_post_id_fk"
  add_index "answers", ["user_id"], :name => "answers_user_id_fk"

  create_table "archived_conversations", :force => true do |t|
    t.integer "user_id"
    t.integer "conversation_id"
  end

  create_table "audiences", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "authorizations", :force => true do |t|
    t.integer  "user_id"
    t.string   "provider"
    t.boolean  "posting_default"
    t.string   "username"
    t.string   "uid"
    t.string   "oauth_token"
    t.string   "oauth_token_secret"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
    t.string   "profile_url"
  end

  add_index "authorizations", ["user_id", "posting_default"], :name => "index_authorizations_on_user_id_and_posting_default"
  add_index "authorizations", ["user_id", "provider"], :name => "index_authorizations_on_user_id_and_provider"

  create_table "badges", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.boolean  "active"
    t.string   "icon"
    t.string   "icon_disabled"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
    t.integer  "role_id"
  end

  create_table "black_list_users", :force => true do |t|
    t.integer "user_id"
    t.integer "blocked_user_id"
  end

  create_table "brand_advertisers", :force => true do |t|
    t.integer "brand_id"
    t.integer "advertiser_id"
  end

  create_table "brands", :force => true do |t|
    t.string   "name"
    t.string   "logo"
    t.string   "sector"
    t.text     "description"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "categories", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "circles", :force => true do |t|
    t.integer  "user_id"
    t.string   "name"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
    t.string   "description"
  end

  add_index "circles", ["user_id"], :name => "circles_user_id_fk"

  create_table "circles_users", :id => false, :force => true do |t|
    t.integer "circle_id"
    t.integer "user_id"
  end

  add_index "circles_users", ["circle_id"], :name => "circles_users_circle_id_fk"
  add_index "circles_users", ["user_id"], :name => "circles_users_user_id_fk"

  create_table "clicks", :force => true do |t|
    t.string   "url"
    t.integer  "clickable_id"
    t.string   "clickable_type"
    t.integer  "user_id"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  add_index "clicks", ["clickable_id", "clickable_type", "user_id"], :name => "index_clicks_on_clickable_id_and_clickable_type_and_user_id"
  add_index "clicks", ["clickable_type"], :name => "index_clicks_on_clickable_type"
  add_index "clicks", ["user_id"], :name => "index_clicks_on_user_id"

  create_table "comments", :force => true do |t|
    t.text     "comment"
    t.integer  "commentable_id"
    t.string   "commentable_type"
    t.integer  "user_id"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  add_index "comments", ["commentable_id"], :name => "index_comments_on_commentable_id"
  add_index "comments", ["commentable_type"], :name => "index_comments_on_commentable_type"
  add_index "comments", ["user_id"], :name => "index_comments_on_user_id"

  create_table "companies", :force => true do |t|
    t.string   "name"
    t.string   "type"
    t.string   "logo"
    t.text     "description"
    t.datetime "created_at",                         :null => false
    t.datetime "updated_at",                         :null => false
    t.string   "phone"
    t.string   "email"
    t.string   "website"
    t.integer  "number_of_employees"
    t.string   "annual_revenue"
    t.datetime "deleted_at"
    t.integer  "owner_id"
    t.string   "owner_type"
    t.integer  "packages_count",      :default => 0
    t.string   "monthly_uniques"
    t.string   "monthly_impressions"
    t.string   "monthly_page_views"
    t.integer  "linkedin_id"
    t.string   "linkedin_logo"
  end

  create_table "company_ad_types", :force => true do |t|
    t.integer "company_id"
    t.integer "ad_type_id"
  end

  create_table "company_audiences", :force => true do |t|
    t.integer "company_id"
    t.integer "audience_id"
  end

  create_table "company_brands", :force => true do |t|
    t.integer  "company_id"
    t.integer  "brand_id"
    t.string   "relationship"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  add_index "company_brands", ["brand_id"], :name => "company_brands_brand_id_fk"
  add_index "company_brands", ["company_id"], :name => "company_brands_company_id_fk"

  create_table "company_categories", :force => true do |t|
    t.integer "company_id"
    t.integer "category_id"
  end

  create_table "company_locations", :force => true do |t|
    t.string   "name"
    t.integer  "company_id"
    t.integer  "address_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "company_locations", ["address_id"], :name => "company_locations_address_id_fk"
  add_index "company_locations", ["company_id"], :name => "company_locations_company_id_fk"

  create_table "company_networks", :force => true do |t|
    t.integer "company_id"
    t.integer "network_id"
  end

  create_table "company_sites", :force => true do |t|
    t.integer "company_id"
    t.integer "site_id"
  end

  create_table "connections", :force => true do |t|
    t.integer  "user_id"
    t.integer  "friend_id"
    t.string   "status"
    t.datetime "responded_at"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
    t.text     "message"
  end

  add_index "connections", ["friend_id"], :name => "connections_friend_id_fk"
  add_index "connections", ["user_id"], :name => "connections_user_id_fk"

  create_table "contacts", :force => true do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "email"
    t.string   "title"
    t.integer  "company_id"
    t.integer  "address_id"
    t.integer  "user_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "contacts", ["address_id"], :name => "contacts_address_id_fk"
  add_index "contacts", ["company_id"], :name => "contacts_company_id_fk"
  add_index "contacts", ["user_id"], :name => "contacts_user_id_fk"

  create_table "conversations", :force => true do |t|
    t.string   "subject",    :default => ""
    t.datetime "created_at",                    :null => false
    t.datetime "updated_at",                    :null => false
    t.boolean  "chat",       :default => false
  end

  create_table "experiences", :force => true do |t|
    t.integer  "user_profile_id"
    t.string   "company_name"
    t.string   "position"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
    t.date     "start_date"
    t.date     "end_date"
    t.text     "description"
    t.string   "location"
    t.string   "eid"
    t.string   "company_logo"
    t.string   "company_title"
    t.boolean  "current"
    t.integer  "company_id"
  end

  create_table "ignore_list_users", :force => true do |t|
    t.integer  "user_id"
    t.integer  "ignored_user_id"
    t.date     "stop_ignoring_at"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  create_table "invitations", :force => true do |t|
    t.integer  "sender_id"
    t.string   "recipient_email"
    t.string   "token"
    t.datetime "used_at"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
    t.string   "linkedin_id"
    t.integer  "role_id"
  end

  create_table "jira_issues", :force => true do |t|
    t.string   "title"
    t.text     "description"
    t.datetime "sent_at"
    t.datetime "created_at",                                :null => false
    t.datetime "updated_at",                                :null => false
    t.string   "type",        :default => "FeatureRequest"
    t.integer  "user_id"
  end

  create_table "memberships", :force => true do |t|
    t.integer  "user_id"
    t.integer  "organization_id"
    t.string   "status"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  add_index "memberships", ["organization_id"], :name => "memberships_organization_id_fk"
  add_index "memberships", ["user_id"], :name => "memberships_user_id_fk"

  create_table "networks", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "news_feeds", :force => true do |t|
    t.string   "title"
    t.string   "description"
    t.string   "url"
    t.datetime "last_updated"
    t.string   "copyright"
    t.string   "image"
    t.boolean  "active"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "news_items", :force => true do |t|
    t.string   "title"
    t.integer  "news_feed_id"
    t.string   "permalink"
    t.text     "description"
    t.string   "url"
    t.datetime "date_published"
    t.string   "copyright"
    t.string   "author"
    t.text     "content"
    t.datetime "created_at",                        :null => false
    t.datetime "updated_at",                        :null => false
    t.integer  "cached_votes_total", :default => 0
    t.integer  "cached_votes_up",    :default => 0
    t.integer  "cached_votes_down",  :default => 0
    t.integer  "clicks_count",       :default => 0
    t.string   "image"
  end

  add_index "news_items", ["news_feed_id"], :name => "news_items_news_feed_id_fk"

  create_table "notifications", :force => true do |t|
    t.string   "type"
    t.text     "body"
    t.string   "subject",              :default => ""
    t.integer  "sender_id"
    t.string   "sender_type"
    t.integer  "conversation_id"
    t.boolean  "draft",                :default => false
    t.datetime "updated_at",                              :null => false
    t.datetime "created_at",                              :null => false
    t.integer  "notified_object_id"
    t.string   "notified_object_type"
    t.string   "notification_code"
    t.string   "attachment"
  end

  add_index "notifications", ["conversation_id"], :name => "index_notifications_on_conversation_id"

  create_table "opportunities", :force => true do |t|
    t.integer  "user_id"
    t.string   "name"
    t.date     "start_date"
    t.date     "end_date"
    t.date     "close_date"
    t.decimal  "cpm",              :precision => 10, :scale => 0
    t.integer  "impressions"
    t.string   "creative_types"
    t.string   "account_name"
    t.string   "probability"
    t.decimal  "budget",           :precision => 10, :scale => 0
    t.string   "stage"
    t.text     "notes"
    t.text     "demo_description"
    t.text     "demo_messages"
    t.datetime "created_at",                                      :null => false
    t.datetime "updated_at",                                      :null => false
  end

  add_index "opportunities", ["user_id"], :name => "opportunities_user_id_fk"

  create_table "organizations", :force => true do |t|
    t.string   "name"
    t.string   "org_type"
    t.string   "logo"
    t.text     "description"
    t.integer  "address_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  add_index "organizations", ["address_id"], :name => "organizations_address_id_fk"

  create_table "package_publishers", :id => false, :force => true do |t|
    t.integer "package_id"
    t.integer "publisher_id"
  end

  create_table "packages", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.date     "flight_start_date"
    t.date     "flight_end_date"
    t.boolean  "dma_targeting_available"
    t.boolean  "demo_targeting_available"
    t.string   "brand_restriction"
    t.float    "total_cost"
    t.integer  "total_impressions"
    t.float    "cpm"
    t.datetime "created_at",                              :null => false
    t.datetime "updated_at",                              :null => false
    t.string   "logo"
    t.integer  "packagable_id"
    t.string   "packagable_type"
    t.integer  "views_count",              :default => 0
  end

  add_index "packages", ["packagable_id", "packagable_type"], :name => "index_packages_on_packagable_id_and_packagable_type"

  create_table "phones", :force => true do |t|
    t.integer  "address_id"
    t.string   "phone_type"
    t.string   "number"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "phones", ["address_id"], :name => "phones_address_id_fk"

  create_table "post_circles", :force => true do |t|
    t.integer  "post_id"
    t.integer  "circle_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "posts", :force => true do |t|
    t.integer  "user_id"
    t.string   "post_type"
    t.text     "title"
    t.text     "description"
    t.string   "url"
    t.integer  "comments_count",     :default => 0
    t.integer  "shares_count",       :default => 0
    t.integer  "views_count",        :default => 0
    t.integer  "cached_votes_total", :default => 0
    t.integer  "cached_votes_up",    :default => 0
    t.integer  "cached_votes_down",  :default => 0
    t.string   "who_with"
    t.string   "who_asking"
    t.datetime "created_at",                               :null => false
    t.datetime "updated_at",                               :null => false
    t.integer  "clicks_count",       :default => 0
    t.string   "visibility",         :default => "public"
    t.integer  "role_id"
  end

  create_table "posts_users", :force => true do |t|
    t.integer  "user_id"
    t.integer  "post_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "posts_users", ["post_id", "user_id"], :name => "index_posts_users_on_post_id_and_user_id"
  add_index "posts_users", ["user_id", "post_id"], :name => "index_posts_users_on_user_id_and_post_id"

  create_table "receipts", :force => true do |t|
    t.integer  "receiver_id"
    t.string   "receiver_type"
    t.integer  "notification_id",                                  :null => false
    t.boolean  "is_read",                       :default => false
    t.boolean  "trashed",                       :default => false
    t.boolean  "deleted",                       :default => false
    t.string   "mailbox_type",    :limit => 25
    t.datetime "created_at",                                       :null => false
    t.datetime "updated_at",                                       :null => false
  end

  add_index "receipts", ["notification_id"], :name => "index_receipts_on_notification_id"

  create_table "roles", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "sites", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "skills", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "skills_user_profiles", :id => false, :force => true do |t|
    t.integer  "user_profile_id"
    t.integer  "skill_id"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  create_table "social_networks", :id => false, :force => true do |t|
    t.string   "provider",                                   :null => false
    t.string   "name"
    t.string   "consumer_key",    :default => "unspecified"
    t.string   "consumer_secret", :default => "unspecified"
    t.datetime "created_at",                                 :null => false
    t.datetime "updated_at",                                 :null => false
  end

  create_table "timeline_events", :force => true do |t|
    t.string   "event_type"
    t.string   "subject_type"
    t.string   "actor_type"
    t.integer  "subject_id"
    t.integer  "actor_id"
    t.string   "from"
    t.string   "to"
    t.datetime "created_at",             :null => false
    t.datetime "updated_at",             :null => false
    t.string   "type"
    t.text     "fields_changes"
    t.integer  "secondary_subject_id"
    t.string   "secondary_subject_type"
  end

  create_table "timeline_events_user_shares", :id => false, :force => true do |t|
    t.integer "timeline_event_id"
    t.integer "user_id"
  end

  create_table "user_badges", :force => true do |t|
    t.integer  "badge_id"
    t.integer  "user_id"
    t.integer  "percent_complete"
    t.datetime "completed_at"
  end

  create_table "user_profiles", :force => true do |t|
    t.integer  "user_id"
    t.integer  "company_id"
    t.integer  "address_id"
    t.string   "title"
    t.string   "avatar"
    t.text     "description"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
    t.string   "timezone"
    t.string   "twitter_username"
    t.integer  "views_count",      :default => 0
    t.boolean  "featured",         :default => false
    t.string   "visibility",       :default => "public"
    t.text     "experience"
    t.datetime "deleted_at"
  end

  add_index "user_profiles", ["address_id"], :name => "user_profiles_address_id_fk"
  add_index "user_profiles", ["company_id"], :name => "user_profiles_company_id_fk"
  add_index "user_profiles", ["user_id"], :name => "user_profiles_user_id_fk"

  create_table "user_statuses", :force => true do |t|
    t.integer  "user_id"
    t.string   "status"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "user_timeline_events", :force => true do |t|
    t.integer "user_id"
    t.integer "timeline_event_id"
  end

  add_index "user_timeline_events", ["user_id", "timeline_event_id"], :name => "index_user_timeline_events_on_user_id_and_timeline_event_id", :unique => true

  create_table "users", :force => true do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "email",                  :default => "", :null => false
    t.string   "encrypted_password",     :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
    t.string   "provider"
    t.string   "uid"
    t.integer  "packages_count",         :default => 0
    t.datetime "deleted_at"
    t.integer  "role_id"
  end

  add_index "users", ["provider", "uid"], :name => "index_users_on_provider_and_uid"
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true

  create_table "users_notices", :force => true do |t|
    t.integer  "user_id"
    t.integer  "timeline_event_id"
    t.boolean  "unread",            :default => true
    t.datetime "created_at",                          :null => false
    t.datetime "updated_at",                          :null => false
  end

  add_index "users_notices", ["timeline_event_id", "user_id"], :name => "index_users_notices_on_timeline_event_id_and_user_id"
  add_index "users_notices", ["user_id", "timeline_event_id"], :name => "index_users_notices_on_user_id_and_timeline_event_id"

  create_table "users_notifications", :force => true do |t|
    t.integer  "user_id"
    t.integer  "timeline_event_id"
    t.boolean  "unread",            :default => true
    t.datetime "created_at",                          :null => false
    t.datetime "updated_at",                          :null => false
  end

  add_index "users_notifications", ["timeline_event_id", "user_id"], :name => "index_users_notifications_on_timeline_event_id_and_user_id"
  add_index "users_notifications", ["user_id", "timeline_event_id"], :name => "index_users_notifications_on_user_id_and_timeline_event_id"

  create_table "viewings", :force => true do |t|
    t.integer  "viewable_id"
    t.string   "viewable_type"
    t.integer  "user_id"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  add_index "viewings", ["user_id"], :name => "index_viewings_on_user_id"
  add_index "viewings", ["viewable_id", "viewable_type", "user_id"], :name => "index_viewings_on_viewable_id_and_viewable_type_and_user_id"
  add_index "viewings", ["viewable_type"], :name => "index_viewings_on_viewable_type"

  create_table "votes", :force => true do |t|
    t.integer  "votable_id"
    t.string   "votable_type"
    t.integer  "voter_id"
    t.string   "voter_type"
    t.boolean  "vote_flag"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  add_index "votes", ["votable_id", "votable_type"], :name => "index_votes_on_votable_id_and_votable_type"
  add_index "votes", ["voter_id", "voter_type"], :name => "index_votes_on_voter_id_and_voter_type"

  add_foreign_key "answers", "posts", :name => "answers_post_id_fk"
  add_foreign_key "answers", "users", :name => "answers_user_id_fk"

  add_foreign_key "circles", "users", :name => "circles_user_id_fk"

  add_foreign_key "company_brands", "brands", :name => "company_brands_brand_id_fk"
  add_foreign_key "company_brands", "companies", :name => "company_brands_company_id_fk"

  add_foreign_key "company_locations", "addresses", :name => "company_locations_address_id_fk"
  add_foreign_key "company_locations", "companies", :name => "company_locations_company_id_fk"

  add_foreign_key "connections", "users", :name => "connections_friend_id_fk", :column => "friend_id"
  add_foreign_key "connections", "users", :name => "connections_user_id_fk"

  add_foreign_key "contacts", "addresses", :name => "contacts_address_id_fk"
  add_foreign_key "contacts", "companies", :name => "contacts_company_id_fk"
  add_foreign_key "contacts", "users", :name => "contacts_user_id_fk"

  add_foreign_key "memberships", "organizations", :name => "memberships_organization_id_fk"
  add_foreign_key "memberships", "users", :name => "memberships_user_id_fk"

  add_foreign_key "news_items", "news_feeds", :name => "news_items_news_feed_id_fk"

  add_foreign_key "notifications", "conversations", :name => "notifications_on_conversation_id"

  add_foreign_key "opportunities", "users", :name => "opportunities_user_id_fk"

  add_foreign_key "organizations", "addresses", :name => "organizations_address_id_fk"

  add_foreign_key "phones", "addresses", :name => "phones_address_id_fk"

  add_foreign_key "receipts", "notifications", :name => "receipts_on_notification_id"

  add_foreign_key "user_profiles", "addresses", :name => "user_profiles_address_id_fk"
  add_foreign_key "user_profiles", "companies", :name => "user_profiles_company_id_fk"
  add_foreign_key "user_profiles", "users", :name => "user_profiles_user_id_fk"

end
